-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 11, 2021 at 03:26 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `u-care`
--

-- --------------------------------------------------------

--
-- Table structure for table `summary_lead`
--

CREATE TABLE `summary_lead` (
  `id_lead` int(11) NOT NULL,
  `id_estimasi_lead` int(11) NOT NULL,
  `body_repair_pause` datetime DEFAULT NULL,
  `body_repair_start` datetime DEFAULT NULL,
  `body_repair_note` text,
  `body_repair_status` varchar(100) DEFAULT NULL,
  `body_repair_lead` varchar(400) DEFAULT NULL,
  `preparation_pause` datetime DEFAULT NULL,
  `preparation_start` datetime DEFAULT NULL,
  `preparation_note` text,
  `preparation_status` varchar(100) DEFAULT NULL,
  `preparation_lead` varchar(400) DEFAULT NULL,
  `masking_pause` datetime DEFAULT NULL,
  `masking_start` datetime DEFAULT NULL,
  `masking_note` text,
  `masking_status` varchar(100) DEFAULT NULL,
  `masking_lead` varchar(400) DEFAULT NULL,
  `painting_pause` datetime DEFAULT NULL,
  `painting_start` datetime DEFAULT NULL,
  `painting_note` text,
  `painting_status` varchar(100) DEFAULT NULL,
  `painting_lead` varchar(400) DEFAULT NULL,
  `polishing_pause` datetime DEFAULT NULL,
  `polishing_start` datetime DEFAULT NULL,
  `polishing_note` text,
  `polishing_status` varchar(100) DEFAULT NULL,
  `polishing_lead` varchar(400) DEFAULT NULL,
  `re_assembling_pause` datetime DEFAULT NULL,
  `re_assembling_start` datetime DEFAULT NULL,
  `re_assembling_note` text,
  `re_assembling_status` varchar(100) DEFAULT NULL,
  `re_assembling_lead` varchar(400) DEFAULT NULL,
  `washing_pause` datetime DEFAULT NULL,
  `washing_start` datetime DEFAULT NULL,
  `washing_note` text,
  `washing_status` varchar(100) DEFAULT NULL,
  `washing_lead` varchar(400) DEFAULT NULL,
  `final_inspection_pause` datetime DEFAULT NULL,
  `final_inspection_start` datetime DEFAULT NULL,
  `final_inspection_note` text,
  `final_inspection_status` varchar(100) DEFAULT NULL,
  `final_inspection_lead` varchar(400) DEFAULT NULL,
  `total_lead` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `summary_lead`
--

INSERT INTO `summary_lead` (`id_lead`, `id_estimasi_lead`, `body_repair_pause`, `body_repair_start`, `body_repair_note`, `body_repair_status`, `body_repair_lead`, `preparation_pause`, `preparation_start`, `preparation_note`, `preparation_status`, `preparation_lead`, `masking_pause`, `masking_start`, `masking_note`, `masking_status`, `masking_lead`, `painting_pause`, `painting_start`, `painting_note`, `painting_status`, `painting_lead`, `polishing_pause`, `polishing_start`, `polishing_note`, `polishing_status`, `polishing_lead`, `re_assembling_pause`, `re_assembling_start`, `re_assembling_note`, `re_assembling_status`, `re_assembling_lead`, `washing_pause`, `washing_start`, `washing_note`, `washing_status`, `washing_lead`, `final_inspection_pause`, `final_inspection_start`, `final_inspection_note`, `final_inspection_status`, `final_inspection_lead`, `total_lead`) VALUES
(1, 620, '2021-02-10 13:44:35', '2021-02-10 13:55:56', NULL, 'start', '94616', '2021-02-10 14:05:51', '2021-02-10 14:05:56', '', 'start', '11', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-12-11 10:47:34', NULL, 'start', NULL, '19'),
(2, 879, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, 973, NULL, NULL, NULL, 'pause', NULL, NULL, NULL, NULL, 'pause', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, 1003, NULL, NULL, NULL, 'pause', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, 976, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'start', NULL, '2020-12-11 21:14:44', '2020-12-11 21:22:49', '', 'start', '6974', '2020-12-11 21:37:51', '2020-12-11 21:37:57', NULL, 'start', '3', NULL, '2021-02-11 21:51:32', NULL, 'start', '87386', '2021-02-11 21:52:02', NULL, '', 'pause', '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '174733'),
(6, 157, '2020-12-11 11:01:25', NULL, '', 'pause', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-12-11 21:06:41', '2020-12-11 21:07:22', '', 'start', '606', NULL),
(7, 822, '2020-12-11 11:12:01', NULL, 'bababa', 'pause', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-12-11 21:19:16', 'REDO', 'start', '9915', NULL, NULL, NULL, NULL, '1', NULL, NULL, NULL, NULL, '1', NULL, '2020-12-11 21:56:25', NULL, 'start', '3', NULL, NULL, NULL, NULL, '2', NULL, NULL, NULL, NULL, NULL, NULL),
(8, 231, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-12-11 11:41:52', '2020-12-11 11:48:54', '', 'start', NULL, '2021-02-10 16:51:04', '2020-12-11 13:09:44', '', 'pause', '88061', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '88061'),
(9, 897, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10, 87, NULL, NULL, NULL, NULL, NULL, '2021-02-10 15:48:29', '2021-02-10 14:22:14', '', 'pause', '4150', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '86'),
(11, 69, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-12-11 11:55:02', '2020-12-11 11:57:11', 'Yayayaya', 'start', NULL, '2020-12-11 13:04:09', '2020-12-11 13:03:35', '', 'pause', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(12, 151, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(13, 162, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(14, 532, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(15, 707, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(16, 817, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(17, 830, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(18, 729, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(19, 929, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(20, 803, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(21, 919, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(22, 943, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(23, 979, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `summary_lead`
--
ALTER TABLE `summary_lead`
  ADD PRIMARY KEY (`id_lead`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `summary_lead`
--
ALTER TABLE `summary_lead`
  MODIFY `id_lead` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
