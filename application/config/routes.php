<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'homepage';
$route['admin'] = 'admin';
$route['login'] = 'login';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

// service advisor routes
$route['sa/delivery/(:any)']  = 'Service_advisor/delivery/$1';
$route['ptm/delivery/(:any)'] = 'Service_advisor/delivery/$1';
$route['teknisi/delivery/(:any)'] = 'Service_advisor/delivery/$1';
$route['foreman/delivery/(:any)'] = 'Service_advisor/delivery/$1';
$route['pimpinan/delivery/(:any)'] = 'Service_advisor/delivery/$1';

// ptm routes
$route['sa/monitoringProduksi']  = 'ptm/monitoringProduksi';
$route['ptm/monitoringProduksi'] = 'ptm/monitoringProduksi';
$route['foreman/monitoringProduksi'] = 'ptm/monitoringProduksi';

// ptm routes
$route['service_advisor/monitoringProduksi']  = 'ptm/monitoringProduksi';
$route['ptm/monitoringProduksi'] = 'ptm/monitoringProduksi';
$route['foreman/monitoringProduksi'] = 'ptm/monitoringProduksi';