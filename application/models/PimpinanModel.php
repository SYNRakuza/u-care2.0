<?php defined('BASEPATH') OR exit('No direct script access allowed');


class PimpinanModel extends CI_Model {

    function get_all_item_parts(){
        $this->load->library('datatables');
        $this->datatables
             ->select('id_item, nama_item, nomor_parts, nomor_parts_lama, harga_item')
             ->add_column('view', '<a href="javascript:void(0);" class="edit_record btn bg-orange waves-effect btn-xs" data-nomor="$1" data-nama="$2" data-harga="$3" data-id = "$4" data-nomorlama="$5"><i class="material-icons">mode_edit</i></a> <a href="javascript:void(0);" class="hapus_record btn btn-danger waves-effect btn-xs" data-id="$4"><i class="material-icons">delete_forever</i></a>','nomor_parts,nama_item,harga_item, id_item, nomor_parts_lama')
             ->from('item')
             ->where('tipe_item', '2')
             ->where('id_jenis != "0"');
        return $this->datatables->generate();
    }

    function get_all_item_jasa(){
        $this->load->library('datatables');
        $this->datatables->select('item.id_item, item.nama_item, item.nomor_parts, item.harga_item, jenis_kendaran.id_jenis, jenis_kendaran.nama_jenis');
        $this->datatables->add_column('view', '<a href="javascript:void(0);" class="edit_record btn btn-info btn-xs" data-nomor="$1" data-nama="$2" data-harga="$3" data-id = "$4">Update</a> <a href="javascript:void(0);" class="hapus_record btn btn-danger btn-xs" data-id="$4">Hapus</a>','nomor_parts,nama_item,harga_item, id_item');
        $this->datatables->from('item');
        $this->datatables->join('jenis_kendaran', 'item.id_jenis=jenis_kendaran.id_jenis');

        $this->datatables->where('tipe_item', '1');
        $this->datatables->where('id_jenis != "0"');

        return $this->datatables->generate();
    }

    function get_all_estimasi_in(){
        $this->load->library('datatables');
        $this->datatables
             ->select('id_estimasi, nomor_wo, no_polisi, tgl_estimasi, tgl_masuk, tgl_janji_penyerahan, jenis_customer, nama_asuransi, status_inout')
             ->from('estimasi')
             ->join('customer', 'id_customer')
             ->select('nama_lengkap')
             ->join('user', 'id_user')
             ->select('nama_lengkap_user')
             ->add_column('view', '<a style="margin-right: 5px;" href="../service_advisor/printEstimasi/$1" class="btn bg-orange btn-xs waves-effect" data-id="$1"><i class="material-icons">print</i></a>', 'id_estimasi')
             ->where('status_inout = "1"');
        return $this->datatables->generate();
    }

    function get_all_estimasi_inSA(){
        $id_user    = $this->session->userdata('id_user');
        $this->load->library('datatables');
        $this->datatables
             ->select('id_estimasi, nomor_wo, no_polisi, tgl_estimasi, tgl_masuk, tgl_janji_penyerahan, jenis_customer, nama_asuransi, status_inout')
             ->from('estimasi')
             ->join('customer', 'id_customer')
             ->select('nama_lengkap')
             ->join('user', 'id_user')
             ->select('nama_lengkap_user')
             ->add_column('view', '<a style="margin-right: 5px;" href="../service_advisor/printEstimasi/$1" class="btn bg-orange btn-xs waves-effect" data-id="$1"><i class="material-icons">print</i></a>', 'id_estimasi')
             ->where('status_inout = "1"')
             ->where('estimasi.id_user = "'.$id_user.'"');
        return $this->datatables->generate();
    }

    function get_all_estimasi_all(){
        $this->load->library('datatables');
        $this->datatables
             ->select('id_estimasi, nomor_wo, no_polisi, tgl_estimasi, tgl_masuk, tgl_janji_penyerahan, jenis_customer, nama_asuransi, status_inout')
             ->from('estimasi')
             ->join('customer', 'id_customer' , 'left')
             ->select('nama_lengkap')
             ->join('user', 'id_user')
             ->select('nama_lengkap_user')
             ->add_column('view', '<a style="margin-right: 5px;" href="../service_advisor/printEstimasiAll/$1" class="btn bg-orange btn-xs waves-effect" data-id="$1"><i class="material-icons">print</i></a>', 'id_estimasi');
        return $this->datatables->generate();
    }

    function get_all_estimasi_out(){
        $this->load->library('datatables');
        $this->datatables
             ->select('id_estimasi, nomor_wo, no_polisi, tgl_estimasi, tgl_janji_penyerahan, jenis_customer, nama_asuransi')
             ->from('estimasi')
             ->join('customer', 'id_customer', 'left')
             ->select('nama_lengkap')
             ->join('user', 'id_user')
             ->select('nama_lengkap_user')
             ->add_column('view', '<a class="form_in btn btn-success btn-xs waves-effect doIn" href="javascript:void(0);" data-row="$1" data-status="0"><i class="material-icons">call_received</i></a> <a class="print_record btn bg-orange btn-xs waves-effect" href="../service_advisor/printEstimasi/$1" target="_blank"><i class="material-icons">print</i></a> <a data-toggle="modal" data-target="#modal_hapus$1" class="hapus_record btn btn-danger btn-xs waves-effect btn-xs"><i class="material-icons"> delete_forever </i></a>', 'id_estimasi')
             ->where('status_inout = "0"');
        return $this->datatables->generate();
    }

    function get_all_estimasi_outSA(){
        $id_user    = $this->session->userdata('id_user');
        $this->load->library('datatables');
        $this->datatables
             ->select('id_estimasi, nomor_wo, no_polisi, tgl_estimasi, tgl_janji_penyerahan, jenis_customer, nama_asuransi')
             ->from('estimasi')
             ->join('customer', 'id_customer', 'left')
             ->select('nama_lengkap')
             ->join('user', 'id_user')
             ->select('nama_lengkap_user')
             ->add_column('view', '<a class="form_in btn btn-success btn-xs waves-effect doIn" href="javascript:void(0);" data-row="$1" data-status="0"><i class="material-icons">call_received</i></a> <a class="print_record btn bg-orange btn-xs waves-effect" href="../service_advisor/printEstimasi/$1" target="_blank"><i class="material-icons">print</i></a> <a data-toggle="modal" data-target="#modal_hapus$1" class="hapus_record btn btn-danger btn-xs waves-effect btn-xs"><i class="material-icons"> delete_forever </i></a>', 'id_estimasi')
             ->where('status_inout = "0"')
             ->where('estimasi.id_user = "'.$id_user.'"');
        return $this->datatables->generate();
    }

    function get_all_customer(){
        $this->load->library('datatables');
        $this->datatables
             ->select('id_estimasi, no_polisi, tgl_estimasi, tgl_janji_penyerahan')
             ->from('estimasi')
             ->join('customer', 'id_customer', 'left')
             ->select('id_customer, nama_lengkap, no_hp, alamat')
             ->join('jenis_kendaran', 'id_jenis', 'left')
             ->select('nama_jenis');
        return $this->datatables->generate();

    }

    function get_all_delivery_readySA(){
        $id_user = $this->session->userdata('id_user');
        $level = $this->session->userdata('level');
        if($level == 'service advisor'){
            $joinQuery = 'estimasi.tim_teknisi=user.id_user';
            $atributQuery = 'estimasi.id_user = '.$id_user.'';
        }else{
            $joinQuery = 'id_user';
            $atributQuery = 'estimasi.tim_teknisi = "'.$id_user.'"';
        }
        $this->load->library('datatables');
        $this->datatables
             ->select('id_estimasi, nomor_wo, no_polisi, tim_teknisi, kategori_jasa, jenis_customer, nama_asuransi, tgl_estimasi, tgl_masuk, tgl_janji_penyerahan')
             ->from('estimasi')
             ->join('customer', 'id_customer', 'left')
             ->select('nama_lengkap, no_hp')
             ->join('user', $joinQuery)
             ->select('nama_lengkap_user')
             ->join('summary', 'estimasi.id_estimasi=summary.id_estimasi_summary')
             ->select('final_inspection_out')
             ->add_column('view', '<a class="form_in btn btn-success btn-xs waves-effect doIn" href="javascript:void(0);" data-row="$1" data-status="0"><i class="material-icons">call_received</i></a> <a class="print_record btn bg-orange btn-xs waves-effect" href="../../service_advisor/printEstimasi/$1" target="_blank"><i class="material-icons">print</i></a>', 'id_estimasi')
             ->where('status_produksi = "9"')
             ->where($atributQuery)
             ->where('tgl_penyerahan IS NULL');
        return $this->datatables->generate();
    }

    function get_all_delivery_doneSA(){
        $id_user = $this->session->userdata('id_user');
        $level = $this->session->userdata('level');
        if($level == 'service advisor'){
            $joinQuery = 'estimasi.tim_teknisi=user.id_user';
            $atributQuery = 'estimasi.id_user = '.$id_user.'';
        }else{
            $joinQuery = 'id_user';
            $atributQuery = 'estimasi.tim_teknisi = "'.$id_user.'"';
        }
        $this->load->library('datatables');
        $this->datatables
             ->select('id_estimasi, nomor_wo, no_polisi, tim_teknisi, kategori_jasa, jenis_customer, nama_asuransi, tgl_estimasi, tgl_masuk, tgl_janji_penyerahan, tgl_penyerahan')
             ->from('estimasi')
             ->join('customer', 'id_customer', 'left')
             ->select('nama_lengkap, no_hp')
             ->join('user', $joinQuery)
             ->select('nama_lengkap_user')
             ->join('summary', 'estimasi.id_estimasi=summary.id_estimasi_summary')
             ->select('final_inspection_out')
             ->add_column('view', '<a class="print_record btn bg-orange btn-xs waves-effect" href="../../service_advisor/printEstimasi/$1" target="_blank"><i class="material-icons">print</i></a>', 'id_estimasi')
             ->where($atributQuery)
             ->where('tgl_penyerahan IS NOT NULL');
        return $this->datatables->generate();
    }

    function get_all_delivery_readyPimpinan(){
        $id_user = $this->session->userdata('id_user');
        $level = $this->session->userdata('level');
        $this->load->library('datatables');
        $this->datatables
             ->select('id_estimasi, nomor_wo, no_polisi, tim_teknisi, kategori_jasa, jenis_customer, nama_asuransi, tgl_estimasi, tgl_masuk, tgl_janji_penyerahan')
             ->from('estimasi')
             ->join('customer', 'id_customer', 'left')
             ->select('nama_lengkap, no_hp')
             ->join('user as sa', 'estimasi.id_user=sa.id_user', 'left')
             ->join('user as teknisi', 'estimasi.tim_teknisi=teknisi.id_user', 'left')
             ->select('sa.nama_lengkap_user as nama_sa')
             ->select('teknisi.nama_lengkap_user as nama_teknisi')
             ->join('summary', 'estimasi.id_estimasi=summary.id_estimasi_summary')
             ->select('final_inspection_out')
             ->add_column('view', '<a class="form_in btn btn-success btn-xs waves-effect doIn" href="javascript:void(0);" data-row="$1" data-status="0"><i class="material-icons">call_received</i></a> <a class="print_record btn bg-orange btn-xs waves-effect" href="../../service_advisor/printEstimasi/$1" target="_blank"><i class="material-icons">print</i></a>', 'id_estimasi')
             ->where('status_produksi = "9"')
             ->where('tgl_penyerahan IS NULL');
        return $this->datatables->generate();
    }

    function get_all_delivery_donePimpinan(){
        $id_user = $this->session->userdata('id_user');
        $level = $this->session->userdata('level');
        $this->load->library('datatables');
        $this->datatables
             ->select('id_estimasi, nomor_wo, no_polisi, tim_teknisi, kategori_jasa, jenis_customer, nama_asuransi, tgl_estimasi, tgl_masuk, tgl_janji_penyerahan, tgl_penyerahan')
             ->from('estimasi')
             ->join('customer', 'id_customer', 'left')
             ->select('nama_lengkap, no_hp')
             ->join('user as sa', 'estimasi.id_user=sa.id_user', 'left')
             ->join('user as teknisi', 'estimasi.tim_teknisi=teknisi.id_user', 'left')
             ->select('sa.nama_lengkap_user as nama_sa')
             ->select('teknisi.nama_lengkap_user as nama_teknisi')
             ->join('summary', 'estimasi.id_estimasi=summary.id_estimasi_summary')
             ->select('final_inspection_out')
             ->add_column('view', '<a class="form_in btn btn-success btn-xs waves-effect doIn" href="javascript:void(0);" data-row="$1" data-status="0"><i class="material-icons">call_received</i></a> <a class="print_record btn bg-orange btn-xs waves-effect" href="../../service_advisor/printEstimasi/$1" target="_blank"><i class="material-icons">print</i></a>', 'id_estimasi')
             ->where('tgl_penyerahan IS NOT NULL');
        return $this->datatables->generate();
    }

    function get_all_summary_lead(){
        $this->load->library('datatables');
        $this->datatables
             ->select('id_lead, body_repair_lead, preparation_lead, masking_lead, painting_lead, polishing_lead, re_assembling_lead, washing_lead, final_inspection_lead, total_lead')
             ->from('summary_lead')
             ->join('estimasi', 'summary_lead.id_estimasi_lead=estimasi.id_estimasi', 'left')
             ->select('nomor_wo, no_polisi')
             ->join('summary', 'estimasi.id_estimasi=summary.id_estimasi_summary')
             ->select('body_repair_out, preparation_out, masking_out, painting_out, polishing_out, re_assembling_out, wasling_out, final_inspection_out')
             ->join('user as teknisi', 'estimasi.tim_teknisi=teknisi.id_user', 'left')
             ->select('teknisi.nama_lengkap_user as nama_teknisi')
             ->add_column('view', '<a style="margin-right: 5px;" type="button" class="lead_record btn bg-orange btn-xs waves-effect" href="../pimpinan/historyLead/$1"><i class="material-icons">history</i></a>', 'id_lead');
        return $this->datatables->generate();
    }

    function hitungDetail($id){
        $query = $this->db->query("SELECT * FROM detail_estimasi WHERE id_estimasi = '".$id."'");
        $total = $query->num_rows();
        return $total;
    }

}

