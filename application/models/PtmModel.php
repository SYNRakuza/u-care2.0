<?php defined('BASEPATH') OR exit('No direct script access allowed');


class PtmModel extends CI_Model {

    public function get_ready(){
        //card1
        $where1= array('2','0');
        $this->db->select( '*' );
        $this->db->from( 'estimasi' );
        $this->db->join( 'customer', 'customer.id_customer = estimasi.id_customer', 'left' );
        $this->db->join( 'user', 'user.id_user = estimasi.id_user', 'left' );
        $this->db->where_in( 'jenis_estimasi',$where1);
        $this->db->where( 'status_inout', '1');
        $this->db->where( 'tim_teknisi', NULL);
        // $this->db->where( 'status_ptm', NULL);

        $querry = $this->db->get();
        return $querry->result();
    }

    public function get_done(){
        $this->db->select('tim_teknisi, COUNT(tim_teknisi) as total');
        $this->db->group_by('tim_teknisi'); 
        $this->db->order_by('tim_teknisi', 'asc');
        $this->db->where('status_ptm', "2");

        $querry = $this->db->get('estimasi');
        return $querry->result();
    }

//===================================================================================\\
    // public function get_estimasi_parts(){
    //     $id_where = array('0', '1');
    //     $this->db->from ( 'detail_estimasi' );
    //     $this->db->join ( 'customer', 'customer.id_customer = detail_estimasi.id_customer' , 'left' );
    //     $this->db->join('estimasi', 'estimasi.id_estimasi = detail_estimasi.id_estimasi');
    //     $this->db->join ( 'jenis_kendaran', 'jenis_kendaran.id_jenis = estimasi.id_jenis');
    //     $this->db->where_in('estimasi.done_order', $id_where);
    //     $this->db->order_by('estimasi.done_order', 'ASC');
    //     $this->db->group_by('detail_estimasi.id_estimasi');


    //    // untuk fetch by estimasi sudah di order $this->db->where('tipe_order', 1);
    //     $query = $this->db->get();
    //     return $query->result();
    // }
    // public function get_detail_parts($where){
    //     $this->db->select('*');
    //     $this->db->from('detail_estimasi');
    //     $this->db->join('item', 'item.id_item = detail_estimasi.id_item', 'left');
    //     $this->db->join('estimasi', 'estimasi.id_estimasi = detail_estimasi.id_estimasi', 'left');
    //     $this->db->join('customer', 'customer.id_customer = detail_estimasi.id_customer', 'left');
    //     $this->db->join('jenis_kendaran', 'jenis_kendaran.id_jenis = estimasi.id_jenis');
    //     $this->db->where($where);

    //     $query = $this->db->get();
    //     return $query->result();
    // }

    // public function get_count(){
    //     $this->db->select('tim_teknisi, COUNT(tim_teknisi) as total');
    //     $this->db->group_by('tim_teknisi'); 
    //     $this->db->order_by('tim_teknisi', 'asc');
    //     $this->db->where('status_ptm', "1");
         
    //     // $this->db->where('jenis_estimasi', $where);
    //     // $this->db->where('status_estimasi', $status_estimasi);
    //     // $this->db->where('tim_teknisi', 'IS NOT NULL');

    //     $querry = $this->db->get('estimasi');
    //     return $querry->result();
    // }

    // public function get_done(){
    //     $this->db->select('tim_teknisi, COUNT(tim_teknisi) as total');
    //     $this->db->group_by('tim_teknisi'); 
    //     $this->db->order_by('tim_teknisi', 'asc');
    //     $this->db->where('status_ptm', "2");

    //     $querry = $this->db->get('estimasi');
    //     return $querry->result();
    // }

    // public function get_proses1($teknisi){
    //     //card2
    //     $where1= array('2','0');
    //     $this->db->select( '*' );
    //     $this->db->from('detail_estimasi'); //bisa langsung pake estimasi
    //     $this->db->join('estimasi', 'estimasi.id_estimasi = detail_estimasi.id_estimasi', 'left');
    //     $this->db->join('customer', 'customer.id_customer = estimasi.id_customer', 'left' );
    //     // $this->db->join( 'detail_estimasi', 'detail_estimasi.id_estimasi = estimasi.id_estimasi', 'left');
    //     $this->db->where_in('estimasi.jenis_estimasi',$where1);// tidak perlu pake "estimasi."
    //     $this->db->where('estimasi.status_inout', '1');
    //     $this->db->where('estimasi.tim_teknisi', $teknisi);
    //     $this->db->where('estimasi.status_ptm', '1');

    //     $querry = $this->db->get();
    //     return $querry->result();
    // }

}
