<?php defined('BASEPATH') OR exit('No direct script access allowed');
class HomeModel extends CI_Model {

    public function login($username, $password){

        $set_table = $this->db->get('user');
        $query = $this->db->get_where('user', array('username' => $username, 'password' => $password));
        //$query = $this->db->query ("SELECT * FROM user WHERE username = '$username' AND password = '$password'");

        if($query->num_rows()>0){
            foreach ($query->result() as $x){
                $sess = array(
                    "nama_user" => $x->nama_lengkap_user,
                    "no_telp" => $x->no_tlpUser,
                    "id_user" => $x->id_user,
                    "foto" => $x->foto,
                    "username" => $x->username,
                    "level" => $x->level,
                    "sub_level" => $x->sub_level,
                    "status" => "login",
                );
                $level = $x->level;

            }
            $this->session->set_userdata($sess);

            //login level by controller
            if($level == "partsman"){
                redirect('partsman');
            }else if($level == "admin"){
                redirect("admin");
            }else if($level == "service advisor"){
                redirect("service_advisor");
            }else if($level == "teknisi"){
                redirect("teknisi");
            }else if($level == "ptm"){
                redirect("ptm");
            }elseif($level == "qc"){
                redirect("qc");
            }elseif($level == 'pimpinan'){
                redirect('pimpinan/service_advisor');
            }elseif($level == 'customer'){
                    redirect('customer');
            }

           // redirect('partsman');
        }else{
           $this->session->set_flashdata('info', 'Username dan Password Anda Salah !');
            redirect('login');
        }
    }


}
