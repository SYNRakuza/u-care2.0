<?php defined('BASEPATH') OR exit('No direct script access allowed');


class ExcelModel extends CI_Model {

	function get_nomor_parts($parts_lama)
	{
    	$this->db->select('*');
    	$this->db->from('item');
    	$this->db->where('item.nomor_parts',$parts_lama);
    	$query = $this->db->get();
    	return $query->row();
	}

	function get_tambah_parts()
	{
    	$sql = "SELECT nomor_parts FROM item";
		$query = $this->db->query($sql);
		$array = $query->result_array();
		$arr = array_column($array, 'nomor_parts');
		return $arr;
	}

}
