<?php defined('BASEPATH') OR exit('No direct script access allowed');


class PartsmanModel extends CI_Model {

    function get_all_item(){
        $this->load->library('datatables');
        $this->datatables->select('id_item, nama_item, nomor_parts, nomor_parts_lama, harga_item');
        $this->datatables->add_column('action', anchor('partsman/daftar_parts/$1','Update',array('class'=>'btn btn-primary btn-sm')), 'id_item');
                $this->datatables->from('item');
        $this->datatables->where('tipe_item', '2');
        $this->datatables->where('id_jenis != "0"');

        return $this->datatables->generate();
    }


    public function data_customer($where){
        $this->db->select( '*' );
        $this->db->from( 'estimasi' );
        $this->db->join( 'customer', 'customer.id_customer = estimasi.id_customer' , 'left' );
        $this->db->join('color', 'color.id_color = estimasi.id_color', 'left');
        $this->db->join( 'jenis_kendaran', 'jenis_kendaran.id_jenis = estimasi.id_jenis');
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result();
    }

    public function data_item($where){
        $this->db->select( '*' );
        $this->db->from( 'item' );
        $this->db->where('id_item', $where);
        $query = $this->db->get();
        return $query->result();
    }

    public function data_detail_estimasi($where){
        $this->db->select ( '*' );
        $this->db->from ( 'detail_estimasi' );
        $this->db->join ( 'estimasi', 'estimasi.id_estimasi = detail_estimasi.id_estimasi');
        $this->db->join ( 'user', 'user.id_user = estimasi.id_user' , 'left' );
        $this->db->join ( 'customer', 'customer.id_customer = estimasi.id_customer' , 'left' );
        $this->db->join ( 'jenis_kendaran', 'jenis_kendaran.id_jenis = estimasi.id_jenis');
        $this->db->join ( 'item', 'item.id_item = detail_estimasi.id_item');
        $this->db->order_by('estimasi.tgl_masuk', 'ASC');
        $this->db->where('estimasi.done_order !=', 2);
        $this->db->where('detail_estimasi.status_order', NULL);
        $this->db->where('estimasi.status_inout', 1);
        $this->db->where('detail_estimasi.id_item', $where);
        $filter = array('1', '2');
        $this->db->where_in('estimasi.jenis_estimasi', $filter);

       // untuk fetch by estimasi sudah di order $this->db->where('tipe_order', 1);
        $query = $this->db->get ();
        return $query->result();
    }

    public function data_detail_order($where){
        $this->db->select ( '*' );
        $this->db->from ( 'detail_estimasi' );
        $this->db->join ( 'estimasi', 'estimasi.id_estimasi = detail_estimasi.id_estimasi');
        $this->db->join ( 'user', 'user.id_user = estimasi.id_user' , 'left' );
        $this->db->join ( 'customer', 'customer.id_customer = estimasi.id_customer' , 'left' );
        $this->db->join ( 'jenis_kendaran', 'jenis_kendaran.id_jenis = estimasi.id_jenis');
        $this->db->join ( 'item', 'item.id_item = detail_estimasi.id_item');
        $this->db->order_by('estimasi.tgl_masuk', 'ASC');
        $this->db->where('estimasi.done_order !=', 2);
        $this->db->where('detail_estimasi.status_order', 0);
        $this->db->where('estimasi.status_inout', 1);
        $this->db->where('detail_estimasi.id_item', $where);
        $filter = array('1', '2');
        $this->db->where_in('estimasi.jenis_estimasi', $filter);

       // untuk fetch by estimasi sudah di order $this->db->where('tipe_order', 1);
        $query = $this->db->get ();
        return $query->result();
    }

    public function get_estimasi(){
        $this->db->select ( '*' );
        $this->db->from ( 'estimasi' );
        $this->db->join ( 'user', 'user.id_user = estimasi.id_user' , 'left' );
        $this->db->join ( 'customer', 'customer.id_customer = estimasi.id_customer' , 'left' );
        $this->db->join ( 'jenis_kendaran', 'jenis_kendaran.id_jenis = estimasi.id_jenis');
        $this->db->order_by('tgl_masuk', 'ASC');
        $this->db->where('done_order', NULL);
        $this->db->where('status_inout', 1);
        $filter = array('1', '2');
        $this->db->where_in('estimasi.jenis_estimasi', $filter);

       // untuk fetch by estimasi sudah di order $this->db->where('tipe_order', 1);
        $query = $this->db->get ();
        return $query->result();
    }

    public function get_order_estimasi(){
        $this->db->select ( 'SUM(detail_estimasi.qty) as totalQuantity, detail_estimasi.*, estimasi.*, item.*' );
        $this->db->from ( 'detail_estimasi' );
        $this->db->join ( 'estimasi', 'estimasi.id_estimasi = detail_estimasi.id_estimasi');
        $this->db->join ( 'item', 'item.id_item = detail_estimasi.id_item');
        $this->db->order_by('estimasi.tgl_masuk', 'ASC');
        $this->db->where('estimasi.done_order !=', 2);
        $this->db->where('detail_estimasi.status_order', NULL);
        $this->db->where('estimasi.status_inout', 1);
        $this->db->where('item.nomor_parts IS NOT NULL');
        $filter = array('1', '2');
        $this->db->where_in('estimasi.jenis_estimasi', $filter);
        $this->db->group_by('detail_estimasi.id_item');

       // untuk fetch by estimasi sudah di order $this->db->where('tipe_order', 1);
        $query = $this->db->get ();
        return $query->result();
    }


    public function get_ordering(){

        $this->db->select ( 'SUM(detail_estimasi.qty) as totalQuantity, GROUP_CONCAT(detail_estimasi.id_detail SEPARATOR "-") as id_detail_estimasi, detail_estimasi.*, estimasi.*, item.*' );
        $this->db->from ( 'detail_estimasi' );
        $this->db->join ( 'estimasi', 'estimasi.id_estimasi = detail_estimasi.id_estimasi');
        $this->db->join ( 'item', 'item.id_item = detail_estimasi.id_item');
        $this->db->order_by('estimasi.tgl_masuk', 'ASC');
        $this->db->where('estimasi.done_order !=', 2);
        $this->db->where('detail_estimasi.status_order', 0);
        $this->db->where('estimasi.status_inout', 1);
        $this->db->where('item.nomor_parts IS NOT NULL');
        $filter = array('1', '2');
        $this->db->where_in('estimasi.jenis_estimasi', $filter);
        $this->db->group_by('detail_estimasi.id_item');

       // untuk fetch by estimasi sudah di order $this->db->where('tipe_order', 1);
        $query = $this->db->get ();
        return $query->result();
    }

     public function get_item(){
        $where = array('2', '4');
        $this->db->select( '*');
        $this->db->from('item');
        $this->db->join ( 'jenis_kendaran', 'jenis_kendaran.id_jenis = item.id_jenis' , 'left' );
        $this->db->order_by('item.id_jenis', 'ASC');
        $this->db->where_in('item.tipe_item', $where);
        $this->db->where('item.id_jenis != ', '0');

        //untuk bisa tidak full memori
       $this->db->limit(1000);

       // untuk fetch by estimasi sudah di order $this->db->where('tipe_order', 1);
        $query = $this->db->get();
        return $query->result();
     }

     public function detail_item($where){
        $this->db->select('*');
        $this->db->from('item');
        $this->db->join ( 'jenis_kendaran', 'jenis_kendaran.id_jenis = item.id_jenis' , 'left' );
        $this->db->where($where);

        $query = $this->db->get ();
        return $query->result();
     }

    public function get_detail_customer($where){
        $this->db->select('*');
        $this->db->from('detail_estimasi');
        $this->db->join('item', 'item.id_item = detail_estimasi.id_item', 'left');
        $this->db->join('estimasi', 'estimasi.id_estimasi = detail_estimasi.id_estimasi', 'left');
        $this->db->join('customer', 'customer.id_customer = detail_estimasi.id_customer', 'left');
        $this->db->join('jenis_kendaran', 'jenis_kendaran.id_jenis = estimasi.id_jenis');
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result();
    }

    public function get_detail($where){
        $this->db->select('*');
        $this->db->from('detail_estimasi');
        $this->db->join('item', 'item.id_item = detail_estimasi.id_item', 'left');
        $this->db->join('estimasi', 'estimasi.id_estimasi = detail_estimasi.id_estimasi', 'left');
        $filter = array('2', '4');
        $this->db->where_in('item.tipe_item', $filter);
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result();
    }

    public function get_detail_reorder($where){
        $this->db->select('*');
        $this->db->from('detail_estimasi');
        $this->db->join('item', 'item.id_item = detail_estimasi.id_item', 'left');
        $this->db->join('estimasi', 'estimasi.id_estimasi = detail_estimasi.id_estimasi', 'left');
        $filter = array('2', '4');
        $this->db->where_in('item.tipe_item', $filter);
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result();
    }


    public function proses_order($where){
        $arr_id = implode(',', $where);
        $id_where = explode(',', $arr_id);

        $this->db->select('*');
        $this->db->from('detail_estimasi');
        $this->db->join('item', 'item.id_item = detail_estimasi.id_item');
        $this->db->join('customer', 'customer.id_customer = detail_estimasi.id_customer');
        $this->db->join('estimasi', 'estimasi.id_estimasi = detail_estimasi.id_estimasi');

        $this->db->where_in('detail_estimasi.id_estimasi', $id_where);
        //$filter = array('2', '4');
        //$this->db->where_in('item.tipe_item', $filter);
        //$this->db->order_by('detail_estimasi.id_estimasi', 'asc');
       // $this->db->where('eta', NULL);

        $query = $this->db->get();
        return $query->result();
    }

    public function proses_get_estimasi($where){
        $arr_id = implode(',', $where);
        $id_where = explode(',', $arr_id);

        $this->db->select('*');
        $this->db->from('estimasi');

        $this->db->where_in('id_estimasi', $id_where);

        $query = $this->db->get();
        return $query->result();
    }

    public function get_barang($where){
        $arr_id = implode(',', $where);
        $id_where = explode(',', $arr_id);

        $this->db->select('*');
        $this->db->from('detail_estimasi');
        $this->db->join('estimasi', 'estimasi.id_estimasi = detail_estimasi.id_estimasi');
        $this->db->join('item', 'item.id_item = detail_estimasi.id_item');
        $this->db->join('customer', 'estimasi.id_customer = customer.id_customer');

        $this->db->where_in('detail_estimasi.id_estimasi', $id_where);
        $this->db->where('detail_estimasi.eta', NULL);
        $filter = array('2', '4');
        $this->db->where_in('item.tipe_item', $filter);

        $query = $this->db->get();
        return $query->result();
    }

    public function get_barang_order($where){
        $arr_id = implode(',', $where);
        $id_where = explode(',', $arr_id);

        $this->db->select('*');
        $this->db->from('detail_estimasi');
        $this->db->join('estimasi', 'estimasi.id_estimasi = detail_estimasi.id_estimasi');
        $this->db->join('item', 'item.id_item = detail_estimasi.id_item');
        $this->db->join('customer', 'estimasi.id_customer = customer.id_customer');

        $this->db->where_in('detail_estimasi.id_item', $id_where);
        $this->db->where('estimasi.done_order !=', 2);
        $this->db->where('detail_estimasi.status_order', NULL);
        $this->db->where('detail_estimasi.eta', NULL);
        $this->db->where('estimasi.status_inout', 1);
        $filter = array('2', '4');
        $this->db->where_in('item.tipe_item', $filter);

        $query = $this->db->get();
        return $query->result();
    }

    public function estimasi_group($where){
        $arr_id = implode(',', $where);
        $id_where = explode(',', $arr_id);

        $this->db->select('*');
        $this->db->from('estimasi');
        $this->db->join('customer', 'customer.id_customer = estimasi.id_customer');

        $this->db->where_in('id_estimasi', $id_where);
        $this->db->group_by('estimasi.id_estimasi');

        $query = $this->db->get();
        return $query->result();
    }

    public function excel_get_detail($where){
        $arr_id = implode(',', $where);
        $id_where = explode(',', $arr_id);

        $this->db->select('');
        $this->db->from('detail_estimasi');
        $this->db->join('estimasi', 'estimasi.id_estimasi = detail_estimasi.id_estimasi', 'left');
        $this->db->join('item', 'item.id_item = detail_estimasi.id_item');
        $this->db->join('customer', 'customer.id_customer = estimasi.id_customer', 'left');
        $this->db->join('jenis_kendaran', 'jenis_kendaran.id_jenis = estimasi.id_jenis');

        $this->db->where_in('detail_estimasi.id_detail', $id_where);
        $this->db->order_by('detail_estimasi.id_detail', 'asc');
        $filter = array ('2', '4');
        $this->db->where_in('item.tipe_item', $filter);

        $query = $this->db->get();
        return $query->result();
    }

    public function reexcel_get_detail($where){
        $arr_id = implode(',', $where);
        $id_where = explode(',', $arr_id);

        $this->db->select('*');
        $this->db->from('detail_estimasi');
        $this->db->join('estimasi', 'estimasi.id_estimasi = detail_estimasi.id_estimasi', 'left');
        $this->db->join('item', 'item.id_item = detail_estimasi.id_item');
        $this->db->join('customer', 'customer.id_customer = estimasi.id_customer', 'left');
        $this->db->join('jenis_kendaran', 'jenis_kendaran.id_jenis = estimasi.id_jenis');

        $this->db->where_in('detail_estimasi.id_detail', $id_where);
        $this->db->order_by('detail_estimasi.id_detail', 'asc');
        $filter = array ('2', '4');
        $this->db->where_in('item.tipe_item', $filter);
        $this->db->where('detail_estimasi.status_barang', 1);

        $query = $this->db->get();
        return $query->result();
    }


    public function get_estimasi_order(){
        $this->db->select('*');
        $this->db->from ( 'detail_estimasi' );
        $this->db->join('estimasi', 'estimasi.id_estimasi = detail_estimasi.id_estimasi');
        $this->db->join('jenis_kendaran', 'jenis_kendaran.id_jenis = estimasi.id_jenis');
        $this->db->join('customer', 'estimasi.id_customer = customer.id_customer');

        $this->db->order_by('estimasi.tgl_masuk', 'ASC');
        $this->db->group_by('detail_estimasi.id_estimasi');
        $this->db->where('done_order', 1);
        $this->db->where('estimasi.status_inout', 1);

       // untuk fetch by estimasi sudah di order $this->db->where('tipe_order', 1);
        $query = $this->db->get();
        return $query->result();
    }

    public function get_estimasi_parts(){
        $id_where = array('1', '2');
        $this->db->from ( 'detail_estimasi' );
        $this->db->join('estimasi', 'estimasi.id_estimasi = detail_estimasi.id_estimasi');
        $this->db->join('customer', 'customer.id_customer = estimasi.id_customer');
        $this->db->join('user', 'user.id_user = estimasi.id_user');
        $this->db->join ( 'jenis_kendaran', 'jenis_kendaran.id_jenis = estimasi.id_jenis');
        $this->db->where_in('estimasi.done_order', $id_where);
        $this->db->group_by('detail_estimasi.id_estimasi');
        $this->db->order_by('estimasi.done_order', 'ASC');


       // untuk fetch by estimasi sudah di order $this->db->where('tipe_order', 1);
        $query = $this->db->get();
        return $query->result();
    }

      public function get_detail_order($where){
        $this->db->select('*');
        $this->db->from('detail_estimasi');
        $this->db->join('item', 'item.id_item = detail_estimasi.id_item', 'left');
        $this->db->join('estimasi', 'estimasi.id_estimasi = detail_estimasi.id_estimasi', 'left');
        $this->db->join('customer', 'customer.id_customer = detail_estimasi.id_customer', 'left');
        $this->db->join('jenis_kendaran', 'jenis_kendaran.id_jenis = estimasi.id_jenis');
        $this->db->where($where);
        $filter = array ('2', '4');
        $this->db->where_in('item.tipe_item', $filter);
        $this->db->where($where);

        $query = $this->db->get();
        return $query->result();
    }



    public function get_history_order(){
        $this->db->from ( 'detail_estimasi' );
        $this->db->join('estimasi', 'estimasi.id_estimasi = detail_estimasi.id_estimasi');
        $this->db->join('customer', 'customer.id_customer = estimasi.id_customer');
        $this->db->join('user', 'user.id_user = estimasi.id_user');
        $this->db->join ( 'jenis_kendaran', 'jenis_kendaran.id_jenis = estimasi.id_jenis');
        $this->db->order_by('estimasi.tgl_masuk', 'ASC');
        $this->db->group_by('detail_estimasi.id_estimasi');
        $this->db->where('done_order', 2);

       // untuk fetch by estimasi sudah di order $this->db->where('tipe_order', 1);
        $query = $this->db->get();
        return $query->result();
    }

     public function get_estimasi_reorder(){
        $this->db->select ( '*' );
        $this->db->from ( 'estimasi' );
        $this->db->join ( 'user', 'user.id_user = estimasi.id_user' , 'left' );
        $this->db->join ( 'customer', 'customer.id_customer = estimasi.id_customer' , 'left' );
        $this->db->join ( 'jenis_kendaran', 'jenis_kendaran.id_jenis = estimasi.id_jenis');
        $this->db->order_by('tgl_masuk', 'ASC');
        $this->db->where('is_order', 1);
        $this->db->where('status_inout', 1);
        $filter = array('1', '2');
        $HumanError = "done_order is  NOT NULL";
        $this->db->where($HumanError);
        $this->db->order_by('estimasi.id_estimasi', 'ASC');
        $this->db->where_in('estimasi.jenis_estimasi', $filter);

       // untuk fetch by estimasi sudah di order $this->db->where('tipe_order', 1);
        $query = $this->db->get ();
        return $query->result();
    }

    public function query_detail_list_order($where){
        $this->db->select('*');
        $this->db->from('detail_estimasi');
        $this->db->join('item', 'item.id_item = detail_estimasi.id_item', 'left');
        $this->db->join('estimasi', 'estimasi.id_estimasi = detail_estimasi.id_estimasi', 'left');
        $filter = array('2', '4');
        $this->db->where_in('item.tipe_item', $filter);
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result();
    }


}
