<?php defined('BASEPATH') OR exit('No direct script access allowed');


class TeknisiModel extends CI_Model {

	function teknisi_script(){
		$this->db->select('*');
		$this->db->from('estimasi');
		$this->db->join('customer','customer.id_customer=estimasi.id_customer');
		$this->db->join('summary','summary.id_estimasi_summary=estimasi.id_estimasi');
		$query = $this->db->get();
		return $query->result();
		
	}

	function summary_script(){
		$this->db->select('*');
		$this->db->from('estimasi');
		$this->db->join('summary','summary.id_estimasi_summary=estimasi.id_estimasi');
		$query=$this->db->get();
		return$query->result();
	}

	

}
