<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Partsman extends CI_Controller {

    public function __construct() {
        parent::__construct();
        ini_set('max_execution_time', 0); 
        ini_set('memory_limit','2048M');
        date_default_timezone_set('Asia/Makassar');
        if($this->session->userdata('status') != "login"){
            redirect('login');
        }
        if($this->session->userdata('level') != "partsman" && $this->session->userdata('level') != "pimpinan"){
            $level = $this->session->userdata('level');
            $controller = preg_replace("/[^a-z]/", "_", $level);
            redirect($controller);
        }
    }

    public function index() {
        $data = array(  'title'     => 'Dashboard Partsman | U-care',
                        'isi'       => 'admin/dashboard/beranda',
                        'dataScript'=> 'admin/dataScript/beranda-script-parts');
        $this->load->view('admin/_layout/wrapper', $data);
    }

    public function getDataChart(){
        $year = $this->input->post('year');
        //$id_user = $this->session->userdata('id_user');
        //$id_user = 19;
        $done = 0;
        $all = 0;
        $yearNow = date('Y');
        $dataCountLight = array();
        $dataCountMedium = array();
        $dataCountHeavy = array();
        $dataPersenD = array();
        $dataPersenA = array();
        $dataPersen = array();

        if($year !== NULL ){
            $yearNow = $year;
            for($i = 0 ; $i < 13; $i++){
                $j = $i+1;
                $queryD = 'SELECT COUNT(*) as totDone FROM estimasi
                    WHERE status_inout = "1" AND done_order="2" AND YEAR(tgl_estimasi) = "'.$yearNow.'" AND MONTH(tgl_estimasi) = "'.$j.'" AND (jenis_estimasi = "1" OR jenis_estimasi = "2")
                ';
                $queryA = 'SELECT COUNT(*) as totAll FROM estimasi
                    WHERE status_inout = "1" AND (done_order="2" OR done_order="1" OR done_order="0") AND YEAR(tgl_estimasi) = "'.$yearNow.'" AND MONTH(tgl_estimasi) = "'.$j.'" AND (jenis_estimasi = "1" OR jenis_estimasi = "2")
                ';

                $tempD = $this->Crud->q($queryD);
                $tempA = $this->Crud->q($queryA);

                foreach ($tempD as $value) {
                    $dataPersenD[$i] = (int)$value->totDone;
                    $done = $dataPersenD[$i];
                }
                foreach ($tempA as $value) {
                    $dataPersenA[$i] = (int)$value->totAll;
                    $all = $dataPersenA[$i];
                }

                if($all == 0){
                    $dataPersen[] = 0;
                }else{
                    $dataPersen[] = ($done/$all)*100;
                }

            }
            $data = array (
                           'dataPersen'     => $dataPersen,
                        );

        }else{
            for($i = 0 ; $i < 13; $i++){
                $j = $i+1;
                $queryD = 'SELECT COUNT(*) as totDone FROM estimasi
                    WHERE status_inout = "1" AND done_order="2" AND YEAR(tgl_estimasi) = "'.$yearNow.'" AND MONTH(tgl_estimasi) = "'.$j.'" AND (jenis_estimasi = "1" OR jenis_estimasi = "2")
                ';
                $queryA = 'SELECT COUNT(*) as totAll FROM estimasi
                    WHERE status_inout = "1" AND (done_order="2" OR done_order="1" OR done_order="0") AND YEAR(tgl_estimasi) = "'.$yearNow.'" AND MONTH(tgl_estimasi) = "'.$j.'" AND (jenis_estimasi = "1" OR jenis_estimasi = "2")
                ';

                $tempD = $this->Crud->q($queryD);
                $tempA = $this->Crud->q($queryA);

                foreach ($tempD as $value) {
                    $dataPersenD[$i] = (int)$value->totDone;
                    $done = $dataPersenD[$i];
                }
                foreach ($tempA as $value) {
                    $dataPersenA[$i] = (int)$value->totAll;
                    $all = $dataPersenA[$i];
                }

                if($all == 0){
                    $dataPersen[] = 0;
                }else{
                    $dataPersen[] = ($done/$all)*100;
                }

            }
            $data = array (
                           'dataPersen'     => $dataPersen,
                        );
        }

        echo json_encode($data);
    }

    public function listPartsDashboard(){
        $dateFrom = $this->input->post('dateFrom');
        $dateTo = $this->input->post('dateTo');
        $inOut = $this->input->post('inOut');
        $output = '';
        if($inOut == 'in'){
            $queryEstimasi = ' SELECT * FROM estimasi
                            LEFT JOIN customer ON customer.id_customer = estimasi.id_customer
                            LEFT JOIN jenis_kendaran ON jenis_kendaran.id_jenis = estimasi.id_jenis
                            WHERE (estimasi.tgl_estimasi >= "'.$dateFrom.'" AND estimasi.tgl_estimasi <= "'.$dateTo.'") AND estimasi.status_inout = "1" AND (estimasi.jenis_estimasi = "1" OR estimasi.jenis_estimasi = "2") AND estimasi.done_order = "2"
                           ';
        }elseif($inOut == 'out'){
            $queryEstimasi = ' SELECT * FROM estimasi
                            LEFT JOIN customer ON customer.id_customer = estimasi.id_customer
                            LEFT JOIN jenis_kendaran ON jenis_kendaran.id_jenis = estimasi.id_jenis
                            WHERE (estimasi.tgl_estimasi >= "'.$dateFrom.'" AND estimasi.tgl_estimasi <= "'.$dateTo.'") AND estimasi.status_inout = "1" AND (estimasi.jenis_estimasi = "1" OR estimasi.jenis_estimasi = "2") AND (estimasi.done_order = "1" OR estimasi.done_order = "0")
                           ';
        }else{
            $queryEstimasi = ' SELECT * FROM estimasi
                            LEFT JOIN customer ON customer.id_customer = estimasi.id_customer
                            LEFT JOIN jenis_kendaran ON jenis_kendaran.id_jenis = estimasi.id_jenis
                            WHERE (estimasi.tgl_estimasi >= "'.$dateFrom.'" AND estimasi.tgl_estimasi <= "'.$dateTo.'") AND estimasi.status_inout = "1" AND (estimasi.jenis_estimasi = "1" OR estimasi.jenis_estimasi = "2") AND (estimasi.done_order = "1" OR estimasi.done_order = "0" OR estimasi.done_order="2")
                           ';
        }
        $dataEstimasi = $this->Crud->q($queryEstimasi);
        $no = 1;
        foreach ($dataEstimasi as $datas) {
            $tgl = date('d-M-Y', strtotime($datas->tgl_estimasi));
            if($datas->tgl_janji_penyerahan == NULL || $datas->tgl_janji_penyerahan == "0000-00-00"){
                $tgl_janji = "-";
            }else{
                $tgl_janji = date('d-M-Y', strtotime($datas->tgl_janji_penyerahan));
            }
            $output .= '
                        <tr>
                            <td style="text-align: center;vertical-align:middle;">'.$no.'</td>
                            <td style="text-align: center;vertical-align:middle;">'.$datas->nomor_wo.'</td>
                            <td style="text-align: center;vertical-align:middle;">'.$datas->no_polisi.'</td>
                            <td style="text-align: center;vertical-align:middle;">'.$datas->nama_lengkap.'</td>
                            <td style="text-align: center;vertical-align:middle;">'.$tgl.'</td>
                            <td style="text-align: center;vertical-align:middle;">'.$tgl_janji.'</td>
                            <td style="text-align: center;vertical-align:middle;">'.($datas->jenis_customer == "0" ? 'Asuransi - '.$datas->nama_asuransi.'' : 'Tunai').'</td>
                        </tr>
            ';
            $no++;
        }
        $data = array('listEstimasi' => $output);

        echo json_encode($data);
    }

    public function profile(){
        $data = array(
                        'title'     => 'Ubah Akun | U-Care',
                        'isi'       => 'admin/form/formPartsmanProfileUpdate',
                        'dataScript'=> 'admin/dataScript/beranda-script',
        );

        $this->load->view('admin/_layout/wrapper', $data);
    }

    public function userUpdateAction(){
        $input = $this->input->post(NULL, TRUE); //get all post with xss filter

        //deteck with password or not
        if($input['new_pass']==NULL){
            $items = array(//save all post in array
                'nama_lengkap_user' => $input['nama_lengkap'],
                'username' => $input['username'],
            );
        }else{
            $items = array(//save all post in array
                'nama_lengkap_user' => $input['nama_lengkap'],
                'username' => $input['username'],
                'password' => md5($input['new_pass']),
            );
        }

        $where = array(//get id for models Crud params w
            "id_user" => $this->session->userdata('id_user'),
        );




        //update session
        $sess = array(
            "nama_user" => $input['nama_lengkap'],
            "username" => $input['username'],
        );
        $this->session->set_userdata($sess);

        $this->Crud->u('user', $items, $where ); //save in database
        $this->session->set_flashdata('update_sukses', 'Update Berhasil !'); //for notif it is succes

        redirect('partsman/profile');
    }

    public function list_estimasi($id_item = NULL){

            if($id_item == NULL){
                $data = array(  'title'     => 'Daftar Estimasi Order | U-Care',
                'isi'       => 'admin/dashboard/partsman/partsman_list_item',
                'datas'      => $this->PartsmanModel->get_order_estimasi(),
                'dataScript'=> 'admin/dataScript/tabel-script' );
                $this->load->view('admin/_layout/wrapper', $data);
            }else{
                //masuk ke detail nya
                $content    = 'admin/dashboard/partsman/detail_estimasi';

                $data = array(  'title'     => 'Detail Item Order | U-Care',
                            'isi'       => $content,
                            'data'      => $this->PartsmanModel->data_item($id_item),
                            'data1'     => $this->PartsmanModel->data_detail_estimasi($id_item),
                            'dataScript'=> 'admin/dataScript/tabel-script' );
                $this->load->view('admin/_layout/wrapper', $data);
            }
    }

    public function proses_order(){
        $input = $this->input->post(NULL, TRUE); //get all post with xss filter
        $get_cek = $this->input->post('chk',TRUE);
        $get_id = $this->input->post('id_estimasi',TRUE);
        $var  = implode(',', $get_cek);//add , to var 1 or 2 etc
       // print_r($var);
        //print_r($get_cek);


        $data = array(  'title'     => 'Akan Diorder | U-Care',
                        'isi'       => 'admin/dashboard/partsman/proses_order',
                        'data'      => $this->PartsmanModel->get_barang_order($get_cek),
                        'data1'     => $this->PartsmanModel->proses_get_estimasi($get_id),
                        'data2'     => $this->PartsmanModel->estimasi_group($get_id),
                        'dataScript'=> 'admin/dataScript/tabel-script' );
        $this->load->view('admin/_layout/wrapper', $data);

    }

    public function proses_order_action(){
        $input = $this->input->post(NULL, TRUE);//get all post from form
        $get_cek = $this->input->post('chk', TRUE); //get lokasi_order (array)
        $get_tgl = $this->input->post('tgl', TRUE); //get eta (array)
        $get_id = $this->input->post('id', TRUE); //get id detail (array)
        $get_wo = $this->input->post('nomor_wo', TRUE);
        $get_est = $this->input->post('est', TRUE); //get id_estimasi yang kalasi


        $updateArray = array(); //make variable to array for save the data
        $var_done = array();
        $tgl_nows = date('Y-m-d');

        $tgl_now = date('Y-m-d');
        if($get_cek == 1){
            $tgl_now = date('Y-m-d', strtotime('+16 days', strtotime($tgl_now)));
        }else{
            $tgl_now = date('Y-m-d', strtotime('+1 days', strtotime($tgl_now)));
        }

        for($x = 0; $x < sizeof($get_id); $x++){ //make loop

        $updateArray[] = array(//add data into update array
            'id_detail'=>$get_id[$x], //insert data into database from array data
            'eta' => $tgl_now, //same
            'lokasi_order' => $get_cek, //same
            'tgl_order' => $tgl_nows,
            'status_order' => 0,
            );
        }

        for($y = 0; $y < sizeof($get_est); $y++){ //make loop

        $var_done[] = array(//add data into update array
            'id_estimasi'=>$get_est[$y], //insert data into database from array data
            // 'done_order' => 0, //same
            'nomor_wo' => $get_wo[$y],
            );
        }

        $this->db->update_batch('detail_estimasi', $updateArray, 'id_detail'); //update all data
        $this->db->update_batch('estimasi', $var_done, 'id_estimasi');

        $this->session->set_flashdata('order_sukses', 'Order Part Berhasil !');

        redirect('partsman/list_estimasi');
    }

    public function ordering($id_item = NULL){

            if($id_item == NULL){
                $data = array(  'title'     => 'Lakukan Order | U-Care',
                'isi'       => 'admin/dashboard/partsman/partsman_ordering',
                'data'      => $this->PartsmanModel->get_ordering(),
                'dataScript'=> 'admin/dataScript/tabel-script' );
                $this->load->view('admin/_layout/wrapper', $data);
            }else{
                //masuk ke detail nya
                $content    = 'admin/dashboard/partsman/detail_ordering';

                $data = array(  'title'     => 'Detail Item Order | U-Care',
                            'isi'       => $content,
                            'data'      => $this->PartsmanModel->data_item($id_item),
                            'data1'     => $this->PartsmanModel->data_detail_order($id_item),
                            'dataScript'=> 'admin/dataScript/tabel-script' );
                $this->load->view('admin/_layout/wrapper', $data);
            }
    }

    public function excel(){

        $input = $this->input->post(NULL, TRUE);//get all post from form
        $get_cek = $this->input->post('chk', TRUE); //get lokasi_order (array)

        $get_detail = array();

        foreach($get_cek as $x){
            $arr_id_detail = explode('-', $x);
            foreach($arr_id_detail as $y){
                $get_detail[] = $y;
            }
        }

        $data = array(
            'data' => $this->PartsmanModel->excel_get_detail($get_detail),
            //'data' => $this->Crud->ga('detail_estimasi'),
        );
        $this->load->view('admin/dashboard/partsman/excel', $data);
    }

    public function proses_ordering(){
        $input = $this->input->post(NULL, TRUE);//get all post from form
        $get_cek = $this->input->post('chk', TRUE); //get lokasi_order (array)
        $get_id_estimasi = $this->input->post('id_estimasi', TRUE);
        $get_no_order = $this->input->post('no_order', TRUE);
        $var_done = array();
        $var_detail = array();

        for($y = 0; $y < sizeof($get_id_estimasi); $y++){ //make loop
        $var_done[] = array(//add data into update array
            'id_estimasi'=>$get_id_estimasi[$y], //insert data into database from array data
            'done_order' => 1, //same
            );
        }

        foreach($get_cek as $x){
            $arr_id_detail = explode('-', $x);
            foreach($arr_id_detail as $z){
                $var_detail[] = array(
                'id_detail' => $z,
                'status_order' => 1,
                'no_order' => $get_no_order,
                );
            }
        }

        $this->db->update_batch('estimasi', $var_done, 'id_estimasi');
        $this->db->update_batch('detail_estimasi', $var_detail, 'id_detail');
        $this->session->set_flashdata('order_sukses', 'Order Part Berhasil !');
        redirect('partsman/ordering');

    }



    public function list_order($id_estimasi = NULL){
        if($id_estimasi == NULL){
                $queryEstimasi  = ' SELECT *, u.nama_lengkap_user as nama_lengkap_user, uu.nama_lengkap_user as namaTeknisi FROM estimasi
                            LEFT JOIN customer ON customer.id_customer = estimasi.id_customer
                            LEFT JOIN jenis_kendaran ON jenis_kendaran.id_jenis = estimasi.id_jenis
                            LEFT JOIN user u ON u.id_user = estimasi.id_user
                            LEFT JOIN user uu ON uu.id_user = estimasi.tim_teknisi
                            WHERE estimasi.status_inout = "1" AND (estimasi.jenis_estimasi = "1" OR estimasi.jenis_estimasi = "2") AND (estimasi.done_order = "1")
                           ';
        $queryCountParts = 'SELECT id_estimasi, COUNT(*) as countParts FROM detail_estimasi
                            LEFT JOIN item on item.id_item = detail_estimasi.id_item
                            WHERE item.tipe_item = "2" OR item.tipe_item = "2"
                            GROUP BY id_estimasi ORDER BY id_estimasi ASC';

        $queryCountParts2 = 'SELECT id_estimasi, COUNT(ata) as countParts2 FROM detail_estimasi
                             LEFT JOIN item on item.id_item = detail_estimasi.id_item
                             WHERE item.tipe_item = "2" OR item.tipe_item = "2"
                             GROUP BY id_estimasi ORDER BY id_estimasi ASC' ;

        $listEstimasi = array();
        $dataEstimasi = $this->Crud->q($queryEstimasi);
        $countTotalParts = $this->Crud->q($queryCountParts);
        $countParts = $this->Crud->q($queryCountParts2);
        foreach ($dataEstimasi as $satuanEstimasi) {
            $estimasi = new stdClass();
            $estimasi->id = $satuanEstimasi->id_estimasi;
            $estimasi->nomor_wo = $satuanEstimasi->nomor_wo;
            $estimasi->tgl_masuk = $satuanEstimasi->tgl_masuk;
            $estimasi->tgl_janji_penyerahan = $satuanEstimasi->tgl_janji_penyerahan;
            $estimasi->nama_lengkap_user = $satuanEstimasi->nama_lengkap_user;
            $estimasi->namaTeknisi = $satuanEstimasi->namaTeknisi;
            $estimasi->no_polisi = $satuanEstimasi->no_polisi;
            $estimasi->nama_lengkap = $satuanEstimasi->nama_lengkap;
            $estimasi->tgl_estimasi = $satuanEstimasi->tgl_estimasi;
            foreach ($countTotalParts as $totalParts) {
                if ($satuanEstimasi->id_estimasi == $totalParts->id_estimasi){
                    $estimasi->totalParts = $totalParts->countParts;
                }
            }
             foreach ($countParts as $totalParts) {
                if ($satuanEstimasi->id_estimasi == $totalParts->id_estimasi){
                    $estimasi->totalReadyParts = $totalParts->countParts2;
                }
            }
            array_push($listEstimasi, $estimasi);
        }

        $data       = array ('title'        => 'Daftar Order Parts | U-Care',
                             'isi'          => 'admin/dashboard/partsman/partsman_list_order',
                             'dataScript'   => 'admin/dataScript/tabel-script',
                             'listEstimasi' => $listEstimasi
                         );
       $this->load->view('admin/_layout/wrapper', $data);
            }else{
                //masuk ke detail nya

                $where      = array( 'estimasi.id_estimasi' => $id_estimasi, );
                $content    = 'admin/dashboard/partsman/detail_order';

                $queryCountParts = 'SELECT id_estimasi, COUNT(*) as countParts FROM detail_estimasi
                            LEFT JOIN item on item.id_item = detail_estimasi.id_item
                            WHERE item.tipe_item = "2" OR item.tipe_item = "2"
                            GROUP BY id_estimasi ORDER BY id_estimasi ASC';

                $queryCountParts2 = 'SELECT id_estimasi, COUNT(ata) as countParts2 FROM detail_estimasi
                             LEFT JOIN item on item.id_item = detail_estimasi.id_item
                             WHERE item.tipe_item = "2" OR item.tipe_item = "2"
                             GROUP BY id_estimasi ORDER BY id_estimasi ASC' ;

                $listDetail = array();
                $dataDetail = $this->PartsmanModel->query_detail_list_order($where);
                $countTotalParts = $this->Crud->q($queryCountParts);
                $countParts = $this->Crud->q($queryCountParts2);

            foreach ($dataDetail as $satuanDetail) {
            $detail = new stdClass();
            $detail->id_detail = $satuanDetail->id_detail;
            $detail->id_estimasi = $satuanDetail->id_estimasi;
            $detail->ata = $satuanDetail->ata;
            $detail->eta = $satuanDetail->eta;
            $detail->tgl_pengambilan = $satuanDetail->tgl_pengambilan;
            $detail->pengambil = $satuanDetail->pengambil;
            $detail->status_barang = $satuanDetail->status_barang;
            $detail->nomor_parts  = $satuanDetail->nomor_parts;
            $detail->nama_item = $satuanDetail->nama_item;
            $detail->qty = $satuanDetail->qty;
            $detail->tipe_order = $satuanDetail->lokasi_order;
            $detail->no_order = $satuanDetail->no_order;

            foreach ($countTotalParts as $totalParts) {
                if ($satuanDetail->id_estimasi == $totalParts->id_estimasi){
                    $detail->totalParts = $totalParts->countParts;
                }
            }
             foreach ($countParts as $totalParts) {
                if ($satuanDetail->id_estimasi == $totalParts->id_estimasi){
                    $detail->totalReadyParts = $totalParts->countParts2;
                }
            }
            array_push($listDetail, $detail);
        }

                $data = array(  'title'     => 'Daftar Detail Order  | U-Care',
                            'isi'       => $content,
                            'data'      => $this->PartsmanModel->data_customer($where),
                            'data1'     => $listDetail,
                            'dataScript'=> 'admin/dataScript/tabel-script' );
                $this->load->view('admin/_layout/wrapper', $data);
            }
    }

    public function updatePart($id){
        $where      = array( 'detail_estimasi.id_detail' => $id, );
        $data = array(  'title'     => 'Daftar Detail Order | U-Care',
                'isi'       => 'admin/form/formPartsmanUpdateParts',
                'data'      => $this->Crud->gw('detail_estimasi', $where),
                'dataScript'=> 'admin/dataScript/tabel-script' );
        $this->load->view('admin/_layout/wrapper', $data);
    }

    public function updatePartAction($id_detail, $id_estimasi){
        $where = array('detail_estimasi.id_detail' => $id_detail);
        $input =  $this->input->post(NULL, TRUE);

        //logic untuk filter supaya bug 1970/- tidak masuk ke database dan bug kosong field

        if($input['tgl_pengambilan'] == '0000-00-00' || $input['tgl_pengambilan'] == NULL){
            $input['tgl_pengambilan'] = NULL;
        }

        if($input['status_barang'] == NULL || $input['status_barang'] == ""){
            $input['status_barang'] = NULL;
        }

        if($input['ata'] == '0000-00-00' || $input['ata'] == NULL){
            $input['ata'] =  NULL;
        }else{//auto ready barang kalo ada ata nya
            $input['status_barang'] = 0;
        }

        if($input['pengambil'] == ""){
            $input['pengambil'] == NULL;
        }

        $items = array(
            "eta" => $input['eta'],
            "ata" => $input['ata'],
            "tgl_pengambilan" => $input['tgl_pengambilan'],
            "status_barang" => $input['status_barang'],
            "pengambil" => $input['pengambil'],
        );

        $this->Crud->u('detail_estimasi', $items, $where ); //save in database
        $this->session->set_flashdata('update_sukses', 'Data part telah diperbaharui !');
        redirect('partsman/list_order/'.$id_estimasi);
    }

    public function updateBulgData($id_est){
        $input = $this->input->post(NULL, TRUE);

        $get_id = $input['id_detail'];
        $tgl_ata =  $input['ata'];
        $nama = $input['pengambil'];
        $tgl_pengambilan = $input['tgl_pengambilan'];

        $updateArray = array();


        for($x = 0; $x < sizeof($get_id) ; $x++){
            if($tgl_ata[$x] == NULL || $tgl_ata[$x] == '0000-00-00'){
                $status_barang = NULL;
                $tgl_ata[$x] = NULL;
            }else{
                $status_barang = 0;
            }

            $updateArray[] = array(
                'id_detail' => $get_id[$x],
                'pengambil' => $nama[$x],
                'ata' => $tgl_ata[$x],
                'tgl_pengambilan' => $tgl_pengambilan[$x],
                'status_barang' => $status_barang,
            );

        }

        $this->db->update_batch('detail_estimasi', $updateArray, 'id_detail');
        $this->session->set_flashdata('update', 'Data Sukses Diupdate !');
        redirect('partsman/list_order/'.$id_est);

    }

    public function reOrder($id_detail, $id_estimasi){
        $where = array('detail_estimasi.id_detail' => $id_detail);
        $wheres = array('estimasi.id_estimasi' => $id_estimasi);
        $items = array(
            "eta" => NULL,
            "ata" => NULL,
            "lokasi_order" => NULL,
            "status_barang" => NULL,
            "tgl_pengambilan" => NULL,
            "pengambil" => NULL,
            "no_order" => NULL,
            "status_order" => NULL,
        );

        // $item = array(
        //     "is_order" => 1,
        // );

        $this->Crud->u('detail_estimasi', $items, $where );
        // $this->Crud->u('estimasi', $item, $wheres);
         $this->session->set_flashdata('reorder_sukses', 'Parts Telah Diorder Kembali !');
        redirect('partsman/list_estimasi');
    }

    public function saveToHistory($id_est){
        $where = array('estimasi.id_estimasi' => $id_est);
        $item = array(
            'done_order' => 2,
            'is_order' => NULL,
        );
        $this->Crud->u('estimasi', $item, $where);
        $this->session->set_flashdata('history_sukses', 'Data Sukses di Update. Temukan di Riwayat Order !');
        redirect('partsman/list_order');
    }

    public function goToListOrder($id_est){
        $where = array('estimasi.id_estimasi' => $id_est);
        $item = array(
            'is_order' => NULL,
        );
        $this->Crud->u('estimasi', $item, $where);
        $this->session->set_flashdata('goToListOrder', 'Data Sukses di Update !');
        redirect('partsman/reordering_parts');
    }

    public function history_order($id_estimasi = NULL){
       // 'data'      => $this->PartsmanModel->data_customer($where),
         //                   'data1'     => $this->PartsmanModel->get_detail($where),
        if($id_estimasi == NULL){


                $queryEstimasi = '
                    SELECT *, u.nama_lengkap_user as nama_lengkap_user, uu.nama_lengkap_user as namaTeknisi FROM estimasi
                    LEFT JOIN customer ON customer.id_customer = estimasi.id_customer
                    LEFT JOIN jenis_kendaran ON jenis_kendaran.id_jenis = estimasi.id_jenis
                    LEFT JOIN user u ON u.id_user = estimasi.id_user
                    LEFT JOIN user uu ON uu.id_user = estimasi.tim_teknisi
                    WHERE estimasi.status_inout = "1" AND (estimasi.jenis_estimasi = "1" OR estimasi.jenis_estimasi = "2") AND (estimasi.done_order = "2")
                    ORDER BY tgl_estimasi DESC
                ';

                $data = array(  'title'     => 'Riwayat Order | U-Care',
                'isi'       => 'admin/dashboard/partsman/partsman_list_history',
                'data'      => $this->Crud->q($queryEstimasi),
                'dataScript'=> 'admin/dataScript/tabel-script' );
                $this->load->view('admin/_layout/wrapper', $data);
            }else{
                //masuk ke detail nya
                $where      = array( 'estimasi.id_estimasi' => $id_estimasi, );
                $content    = 'admin/dashboard/partsman/detail_history';

                $data = array(  'title'     => 'Detail Riwayat Order  | U-Care',
                            'isi'       => $content,
                            'data'      => $this->PartsmanModel->data_customer($where),
                            'data1'     => $this->PartsmanModel->get_detail($where),
                            'dataScript'=> 'admin/dataScript/tabel-script' );
                $this->load->view('admin/_layout/wrapper', $data);
            }
    }

     public function monitor_parts($id_estimasi = NULL){
        if($id_estimasi == NULL){
        $queryEstimasi  = 'SELECT *, u.nama_lengkap_user as nama_lengkap_user, uu.nama_lengkap_user as namaTeknisi FROM estimasi
                            LEFT JOIN customer ON customer.id_customer = estimasi.id_customer
                            LEFT JOIN jenis_kendaran ON jenis_kendaran.id_jenis = estimasi.id_jenis
                            LEFT JOIN user u ON u.id_user = estimasi.id_user
                            LEFT JOIN user uu ON uu.id_user = estimasi.tim_teknisi
                            WHERE estimasi.status_inout = "1" AND (estimasi.jenis_estimasi = "1" OR estimasi.jenis_estimasi = "2") AND (estimasi.done_order = "1")
                            ORDER BY tgl_estimasi DESC';

        $queryCountParts = 'SELECT id_estimasi, COUNT(*) as countParts FROM detail_estimasi
                            LEFT JOIN item on item.id_item = detail_estimasi.id_item
                            WHERE item.tipe_item = "2" OR item.tipe_item = "2"
                            GROUP BY id_estimasi ORDER BY id_estimasi ASC';

        $queryCountParts2 = 'SELECT id_estimasi, COUNT(ata) as countParts2 FROM detail_estimasi
                             LEFT JOIN item on item.id_item = detail_estimasi.id_item
                             WHERE item.tipe_item = "2" OR item.tipe_item = "2"
                             GROUP BY id_estimasi ORDER BY id_estimasi ASC' ;

        $listEstimasi = array();
        $dataEstimasi = $this->Crud->q($queryEstimasi);
        $countTotalParts = $this->Crud->q($queryCountParts);
        $countParts = $this->Crud->q($queryCountParts2);
        foreach ($dataEstimasi as $satuanEstimasi) {
            $estimasi = new stdClass();
            $estimasi->id = $satuanEstimasi->id_estimasi;
            $estimasi->nomor_wo = $satuanEstimasi->nomor_wo;
            $estimasi->tgl_estimasi = $satuanEstimasi->tgl_estimasi;
            $estimasi->tgl_janji_penyerahan = $satuanEstimasi->tgl_janji_penyerahan;
            $estimasi->nama_lengkap_user = $satuanEstimasi->nama_lengkap_user;
            $estimasi->namaTeknisi = $satuanEstimasi->namaTeknisi;
            $estimasi->no_polisi = $satuanEstimasi->no_polisi;
            $estimasi->done_order = $satuanEstimasi->done_order;
            $estimasi->nama_lengkap = $satuanEstimasi->nama_lengkap;
            $estimasi->tgl_estimasi = $satuanEstimasi->tgl_estimasi;
            foreach ($countTotalParts as $totalParts) {
                if ($satuanEstimasi->id_estimasi == $totalParts->id_estimasi){
                    $estimasi->totalParts = $totalParts->countParts;
                }
            }
             foreach ($countParts as $totalParts) {
                if ($satuanEstimasi->id_estimasi == $totalParts->id_estimasi){
                    $estimasi->totalReadyParts = $totalParts->countParts2;
                }
            }
            array_push($listEstimasi, $estimasi);
        }

        $data       = array ('title'        => 'Daftar Order Parts | U-Care',
                             'isi'          => 'admin/dashboard/partsman/partsman_monitor_parts',
                             'dataScript'   => 'admin/dataScript/tabel-script',
                             'listEstimasi' => $listEstimasi
                         );
       $this->load->view('admin/_layout/wrapper', $data);
        /*
                $data = array(  'title'     => 'Monitoring Parts | U-Care',
                'isi'       => 'admin/dashboard/partsman/partsman_monitor_parts',
                'dataNotif' => $this->notif(),
                'dataCountNotRead' => $this->countNotifNotRead(),
                'data'      => $this->PartsmanModel->get_estimasi_parts(),
                'dataScript'=> 'admin/dataScript/tabel-script' );
                $this->load->view('admin/_layout/wrapper', $data);*/
            }else{
                //masuk ke detail nya
                $where      = array( 'estimasi.id_estimasi' => $id_estimasi, );
                $content    = 'admin/dashboard/partsman/detail_monitor_parts';

                $data = array(  'title'     => 'Detail Monitor Parts  | U-Care',
                            'isi'       => $content,
                            'data'      => $this->PartsmanModel->data_customer($where),
                            'data1'     => $this->PartsmanModel->get_detail($where),
                            'dataScript'=> 'admin/dataScript/tabel-script' );
                $this->load->view('admin/_layout/wrapper', $data);
            }
    }

    public function daftar_parts($id_item = NULL){
        if($id_item == NULL){
                $data = array(  'title'     => 'Daftar Parts | U-Care',
                'isi'       => 'admin/dashboard/partsman/daftar_parts',
                'dataScript'=> 'admin/dataScript/tabel-script');
                $this->load->view('admin/_layout/wrapper', $data);
            }else{
                //masuk ke detail nya
                $where      = array( 'item.id_item' => $id_item, );
                $content    = 'admin/dashboard/partsman/detail_parts';

                $data = array(  'title'     => 'Detail Parts  | U-Care',
                            'isi'       => $content,
                            'data'      => $this->PartsmanModel->detail_item($where),
                            'dataScript'=> 'admin/dataScript/tabel-script' );
                $this->load->view('admin/_layout/wrapper', $data);
            }
    }

    public function parts_action($id_item){
        $where = array('item.id_item' => $id_item);
        $input =  $this->input->post(NULL, TRUE);

        $items = array(
            'nomor_parts' => $input['nomor_parts'],
            'nama_item' => $input['nama_item'],
            'harga_item' => $input['harga_item'],
        );

         $this->Crud->u('item', $items, $where );
         $this->session->set_flashdata('update_sukses', 'Data part telah diperbaharui !');
         redirect('partsman/daftar_parts');
    }

    public function reordering_parts($id_estimasi = NULL){

            if($id_estimasi == NULL){
                $data = array(  'title'     => 'Daftar Order Ulang Parts | U-Care',
                'isi'       => 'admin/dashboard/partsman/partsman_list_reorder',
                'data'      => $this->PartsmanModel->get_estimasi_reorder(),
                'dataScript'=> 'admin/dataScript/tabel-script' );
                $this->load->view('admin/_layout/wrapper', $data);
            }else{
                //masuk ke detail nya
                $where      = array( 'estimasi.id_estimasi' => $id_estimasi, );
                $content    = 'admin/dashboard/partsman/detail_reordering';

                $queryCountParts = 'SELECT id_estimasi, COUNT(*) as countParts FROM detail_estimasi
                            LEFT JOIN item on item.id_item = detail_estimasi.id_item
                            WHERE item.tipe_item = "2" OR item.tipe_item = "2"
                            GROUP BY id_estimasi ORDER BY id_estimasi ASC';

                $queryCountParts2 = 'SELECT id_estimasi, COUNT(eta) as countParts2 FROM detail_estimasi
                             LEFT JOIN item on item.id_item = detail_estimasi.id_item
                             WHERE item.tipe_item = "2" OR item.tipe_item = "2"
                             GROUP BY id_estimasi ORDER BY id_estimasi ASC' ;

                $listDetail = array();
                $dataDetail = $this->PartsmanModel->get_detail_reorder($where);
                $countTotalParts = $this->Crud->q($queryCountParts);
                $countParts = $this->Crud->q($queryCountParts2);

            foreach ($dataDetail as $satuanDetail) {
            $detail = new stdClass();
            $detail->id_estimasi = $satuanDetail->id_estimasi;
            $detail->nomor_parts  = $satuanDetail->nomor_parts;
            $detail->nama_item = $satuanDetail->nama_item;
            $detail->qty = $satuanDetail->qty;
            $detail->eta = $satuanDetail->eta;

            foreach ($countTotalParts as $totalParts) {
                if ($satuanDetail->id_estimasi == $totalParts->id_estimasi){
                    $detail->totalParts = $totalParts->countParts;
                }
            }
             foreach ($countParts as $totalParts) {
                if ($satuanDetail->id_estimasi == $totalParts->id_estimasi){
                    $detail->totalDoneParts = $totalParts->countParts2;
                }
            }
            array_push($listDetail, $detail);
        }



                $data = array(  'title'     => 'Detail Item Order | U-Care',
                            'isi'       => $content,
                            'data'      => $this->PartsmanModel->data_customer($where),
                            'data1'     => $listDetail,
                            'dataScript'=> 'admin/dataScript/tabel-script' );
                $this->load->view('admin/_layout/wrapper', $data);
            }
    }

    public function proses_reorder(){
        $input = $this->input->post(NULL, TRUE); //get all post with xss filter
        $get_cek = $this->input->post('chk',TRUE); //get variable array from checkbox, estimasi
        $var  = implode(',', $get_cek);//add , to var 1 or 2 etc
       // print_r($var);
        //print_r($get_cek);


        $data = array(  'title'     => 'Akan Diorder | U-Care',
                        'isi'       => 'admin/dashboard/partsman/proses_reorder',
                        'data'      => $this->PartsmanModel->get_barang($get_cek),
                        'data1'     => $this->PartsmanModel->proses_get_estimasi($get_cek),
                        'data2'     => $this->PartsmanModel->estimasi_group($get_cek),
                        'dataScript'=> 'admin/dataScript/tabel-script' );
        $this->load->view('admin/_layout/wrapper', $data);

    }

    public function proses_reorder_action(){
        $input = $this->input->post(NULL, TRUE);//get all post from form
        $get_cek = $this->input->post('chk', TRUE); //get lokasi_order (array)
        $get_tgl = $this->input->post('tgl', TRUE); //get eta (array)
        $get_id = $this->input->post('id', TRUE); //get id detail (array)
        $get_wo = $this->input->post('nomor_wo', TRUE);
        $get_komen = $this->input->post('komen', TRUE);
        $get_est = $this->input->post('est', TRUE); //get id_estimasi yang kalasi


        $updateArray = array(); //make variable to array for save the data
        $var_done = array();
        $tgl_nows = date('Y-m-d');

        for($x = 0; $x < sizeof($get_id); $x++){ //make loop
        $tgl_now = date('Y-m-d');
        //conditional for auto get date by location code
        if($get_cek[$x] == 1){
            $tgl_now = date('Y-m-d', strtotime('+16 days', strtotime($tgl_now)));
        }else{
            $tgl_now = date('Y-m-d', strtotime('+1 days', strtotime($tgl_now)));
        }

        $updateArray[] = array(//add data into update array
            'id_detail'=>$get_id[$x], //insert data into database from array data
            'eta' => $tgl_now, //same
            'lokasi_order' => $get_cek[$x], //same
            'tgl_order' => $tgl_nows,
            'komentar' => $get_komen[$x],
            'status_barang' => NULL,
            );
        }

        for($y = 0; $y < sizeof($get_est); $y++){ //make loop

        $var_done[] = array(//add data into update array
            'id_estimasi'=>$get_est[$y], //insert data into database from array data
            'nomor_wo' => $get_wo[$y],
            'done_order' =>1,
            'is_order' => NULL,
            );
        }

        $this->db->update_batch('detail_estimasi', $updateArray, 'id_detail'); //update all data
        $this->db->update_batch('estimasi', $var_done, 'id_estimasi');

        $this->session->set_flashdata('order_sukses', 'Order Part Berhasil !');

        redirect('partsman/reordering_parts');
    }

    public function reexcel(){
        $input = $this->input->post(NULL, TRUE);//get all post from form
        $get_id = $this->input->post('id', TRUE); //get id detail (array)
        $get_komen = $this->input->post('komen', TRUE);
        $get_cek = $this->input->post('chk', TRUE); //get lokasi_order (array)
        $get_est = $this->input->post('est', TRUE); //get id_estimasi yang kalasi

        $tgl_nows = date('Y-m-d');
        $updateArray = array(); //make variable to array for save the data

        for($x = 0; $x < sizeof($get_id); $x++){ //make loop
        $tgl_now = date('Y-m-d');
        //conditional for auto get date by location code
        if($get_cek[$x] == 1){
            $tgl_now = date('Y-m-d', strtotime('+16 days', strtotime($tgl_now)));
        }else{
            $tgl_now = date('Y-m-d', strtotime('+1 days', strtotime($tgl_now)));
        }

        $updateArray[] = array(//add data into update array
            'id_detail'=>$get_id[$x], //insert data into database from array data
            'eta' => $tgl_now, //same
            'lokasi_order' => $get_cek[$x], //same
            'tgl_order' => $tgl_nows,
            'komentar' => $get_komen[$x],
            'status_barang' => 1,
            );
        }

        $this->db->update_batch('detail_estimasi', $updateArray, 'id_detail'); //update all data

        $data = array(
            'data' => $this->PartsmanModel->reexcel_get_detail($get_est),
            //'data' => $this->Crud->ga('detail_estimasi'),
        );
        $this->load->view('admin/dashboard/partsman/reexcel', $data);
    }

    public function importNewParts(){
        if(isset($_POST['upload'])){//untuk cek apakah sdh terklik actionnya
            if($_FILES['fileImport']['name']){//kondisi file required
                $fileName = explode('.', $_FILES['fileImport']['name']);//memecah ke dalam array untuk cari nama file

                if(end($fileName) == "csv"){//cek ekstensi
                    $handle = fopen($_FILES['fileImport']['tmp_name'], 'r');
                    while($data = fgetcsv($handle)){//fetch data file
                        //mengambil data dari csv ke variabel
                        $nama_item = array();
                        $nomor_parts =  $this->db->escape_str($data[0]);
                        $harga_item = $this->db->escape_str($data[1]);
                        $nama_item1 = $this->db->escape_str($data[2]);
                        array_push($nama_item, $nama_item1);
                        if(isset($data[3])){
                          $nama_item2 = $this->db->escape_str($data[3]);
                          array_push($nama_item, $nama_item2);
                        };
                        if(isset($data[4])){
                          $nama_item3 = $this->db->escape_str($data[4]);
                          array_push($nama_item, $nama_item3);
                        };
                        if(isset($data[5])){
                          $nama_item4 = $this->db->escape_str($data[5]);
                          array_push($nama_item, $nama_item4);
                        };
                        //echo $nomor_parts.' '.$harga_item."<br/>";

                        $nama_parts = implode( ", ", $nama_item );

                        $parts = $this->ExcelModel->get_tambah_parts();

                        if($no_parts == "No. Parts" || $harga_item == "Harga"){

                        }else{
                            if(in_array($nomor_parts, $parts)){
                          
                            }
                            else{
                              $data = [
                              'nama_item'   => $nama_parts,
                              'nomor_parts' => $nomor_parts,
                              'harga_item'  => $harga_item,
                              'tipe_item'   => 2,
                              'id_jenis'    => 80,
                            ];

                            $this->db->insert('item', $data);
                            }
                        }
                    }
                    fclose($handle);//close file after cache
                    $this->session->set_flashdata('not_select', 'Data Sukses Ditambah !');
                    redirect("partsman/daftar_parts");
                }else{
                    $this->session->set_flashdata('not_select', 'Gagal !<br><br> Jenis File Bukan .csv.');
                    redirect('partsman/daftar_parts');
                }

            }else{//kondisi tidak ada file
                $this->session->set_flashdata('not_select', 'Gagal !<br><br> Pilih File Dengan Cermat.');
                redirect('partsman/daftar_parts');
            }
        }
  }

    public function importUpdate(){

        if(isset($_POST['upload'])){//untuk cek apakah sdh terklik actionnya
            if($_FILES['fileImport']['name']){//kondisi file required
                $fileName = explode('.', $_FILES['fileImport']['name']);//memecah ke dalam array untuk cari nama file

                if(end($fileName) == "csv"){//cek ekstensi
                    $handle = fopen($_FILES['fileImport']['tmp_name'], 'r');
                    while($data = fgetcsv($handle)){//fetch data file
                        //mengambil data dari csv ke variabel
                        $nomor_parts =  $this->db->escape_str($data[0]);
                        $harga_item = $this->db->escape_str($data[1]);
                        //echo $nomor_parts.' '.$harga_item."<br/>";

                        $query = "
                            UPDATE item
                            SET nomor_parts = '$nomor_parts',
                            harga_item = '$harga_item'
                            WHERE nomor_parts = '$nomor_parts'
                        ";
                        $this->db->query($query);
                    }
                    fclose($handle);//close file after cache
                    $this->session->set_flashdata('not_select', 'Data Sukses Diupdate !');
                    redirect("partsman/daftar_parts");
                }else{
                    $this->session->set_flashdata('not_select', 'Gagal !<br><br> Jenis File Bukan .csv.');
                    redirect('partsman/daftar_parts');
                }

            }else{//kondisi tidak ada file
                $this->session->set_flashdata('not_select', 'Gagal !<br><br> Pilih File Dengan Cermat.');
                redirect('partsman/daftar_parts');
            }
        }

    }

    public function importUpdateSubs(){

        if(isset($_POST['upload'])){//untuk cek apakah sdh terklik actionnya
            if($_FILES['fileImport']['name']){//kondisi file required
                $fileName = explode('.', $_FILES['fileImport']['name']);//memecah ke dalam array untuk cari nama file

                if(end($fileName) == "csv"){//cek ekstensi
                    $handle = fopen($_FILES['fileImport']['tmp_name'], 'r');
                    while($data = fgetcsv($handle)){//fetch data file
                        //mengambil data dari csv ke variabel
                        $nomor_parts_lama =  $this->db->escape_str($data[0]);
                         $nomor_parts_baru =  $this->db->escape_str($data[1]);
                        $harga_item = $this->db->escape_str($data[2]);
                        //echo $nomor_parts.' '.$harga_item."<br/>";

                        $parts = $this->ExcelModel->get_nomor_parts($nomor_parts_lama);

                        if($parts->nomor_parts !== null){
                        $data = [
                          'nama_item'   => $parts->nama_item,
                          'nomor_parts' => $nomor_parts_baru,
                          'nomor_parts_lama'  => $nomor_parts_lama,
                          'harga_item'  => $harga_item,
                          'tipe_item'   => $parts->tipe_item,
                          'id_jenis'    => $parts->id_jenis,
                          'posisi_jasa' => $parts->posisi_jasa,
                        ];

                        $this->db->insert('item', $data);
                        };

                        // $query = "
                        //     UPDATE item
                        //     SET nomor_parts = '$nomor_parts_baru',
                        //     harga_item = '$harga_item'
                        //     WHERE nomor_parts = '$nomor_parts_lama'
                        // ";
                        // $this->db->query($query);
                    }
                    fclose($handle);//close file after cache
                    $this->session->set_flashdata('not_select', 'Data Sukses Diupdate !');
                    redirect("partsman/daftar_parts");
                }else{
                    $this->session->set_flashdata('not_select', 'Gagal !<br><br> Jenis File Bukan .csv.');
                    redirect('partsman/daftar_parts');
                }

            }else{//kondisi tidak ada file
                $this->session->set_flashdata('not_select', 'Gagal !<br><br> Pilih File Dengan Cermat.');
                redirect('partsman/daftar_parts');
            }
        }

    }

    public function tambahParts(){
        $input = $this->input->post(NULL, TRUE);
        $data = array(
            'nama_item' => $input['nama_item'],
            'nomor_parts' => $input['nomor_parts'],
            'nomor_parts_lama' => $input['nomor_parts_lama'],
            'harga_item' => $input['harga_item'],
            'tipe_item' => 2,
            'id_jenis' => 80,
        );
        $this->Crud->i('item', $data);
        $this->session->set_flashdata('update_sukses', 'Parts Berhasil Ditambahkan !');
        redirect('partsman/daftar_parts');
    }

     public function update(){ //function update data
    $kode= $this->input->post('id_item');
    $data=array(
      'nama_item'     => $this->input->post('nama_item'),
      'harga_item'    => $this->input->post('harga_item'),
      'nomor_parts' => $this->input->post('nomor_parts'),
      'nomor_parts_lama' => $this->input->post('nomor_parts_lama'),
    );
    $this->db->where('id_item',$kode);
    $this->db->update('barang', $data);
    redirect('crud');
  }

  public function delete(){ //function hapus data
    $kode=$this->input->post('kode_barang');
    $this->db->where('barang_kode',$kode);
    $this->db->delete('barang');
    redirect('crud');
  }

     function get_item_json() { //get product data and encode to be JSON object
      header('Content-Type: application/json');
      echo $this->PartsmanModel->get_all_item();
  }

  public function history_reorder($id_estimasi){
        $items = array(
            "done_order" => 1,
        );
        $where = array(//get id for models Crud params w
            "id_estimasi" => $id_estimasi,
        );
         $this->Crud->u('estimasi', $items, $where ); //save in database

         $this->session->set_flashdata('order_sukses', 'Data Telah Sukses Dipindahkan');
         redirect('partsman/history_order');
  }

}
