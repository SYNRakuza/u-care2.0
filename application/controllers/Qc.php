<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Qc extends CI_Controller {

    public function __construct() {
        parent::__construct();
        date_default_timezone_set('Asia/Makassar');
        if($this->session->userdata('status') != "login"){
            redirect('login');
        }
        if($this->session->userdata('level') != "qc" && $this->session->userdata('level') != "pimpinan" && $this->session->userdata('level') != "teknisi"){
            $level = $this->session->userdata('level');
            $controller = preg_replace("/[^a-z]/", "_", $level);
            redirect($controller);
        }
    }

    public function index() {
        if($this->session->userdata('sub_level') == 'body repair'){
            redirect('qc/atBodyRepair');
        }elseif($this->session->userdata('sub_level') == 'preparation'){
            redirect('qc/atPreparation');
        }elseif($this->session->userdata('sub_level') == 'masking'){
            redirect('qc/atMasking');
        }elseif($this->session->userdata('sub_level') == 'painting'){
            redirect('qc/atPainting');
        }elseif($this->session->userdata('sub_level') == 'polishing'){
            redirect('qc/atPolishing');
        }elseif($this->session->userdata('sub_level') == 're-assembling'){
            redirect('qc/atReAssembling');
        }elseif($this->session->userdata('sub_level') == 'washing'){
            redirect('qc/atWashing');
        }elseif($this->session->userdata('sub_level') == 'final inspection'){
            redirect('qc/atFinalInspection');
        };
    }

    public function ready($id_estimasi = null){
        $id_user = $this->session->userdata('id_user');
        if($id_estimasi != null){
            $queryEstimasi  = ' SELECT * FROM estimasi
                                INNER JOIN customer ON customer.id_customer = estimasi.id_customer
                                INNER JOIN jenis_kendaran ON jenis_kendaran.id_jenis = estimasi.id_jenis
                                INNER JOIN user ON user.id_user = estimasi.id_user
                                WHERE estimasi.id_estimasi = "'.$id_estimasi.'"
                                ';
            
            $queryPartsJasa      = 'SELECT * FROM detail_estimasi
                                    LEFT JOIN item ON item.id_item = detail_estimasi.id_item
                                    WHERE detail_estimasi.id_estimasi = "'.$id_estimasi.'"
                                    ORDER BY item.tipe_item';

            
            $data = array(
                            'title'             => 'Teknisi | U-Care',
                            'isi'               => 'admin/dashboard/teknisi/ready_detail',
                            'dataEstimasi'      => $this->Crud->q($queryEstimasi),
                            'dataPartsJasa'     => $this->Crud->q($queryPartsJasa),
                            'dataScript'        => 'admin/dataScript/tabel-script',
                        );
        }else{
            $queryEstimasi  = ' SELECT * FROM estimasi
                                LEFT JOIN customer ON customer.id_customer = estimasi.id_customer
                                LEFT JOIN jenis_kendaran ON jenis_kendaran.id_jenis = estimasi.id_jenis
                                LEFT JOIN user ON user.id_user = estimasi.id_user
                                WHERE estimasi.tim_teknisi = "'.$id_user.'" AND estimasi.status_produksi = "0"
                                ';
            
            $listEstimasi = array();
            $dataEstimasi = $this->Crud->q($queryEstimasi);
            
            foreach($dataEstimasi as $datas){
                $estimasi = new stdClass();
                $estimasi->nomor_wo         = $datas->nomor_wo;
                $estimasi->id_estimasi      = $datas->id_estimasi;
                $estimasi->no_polisi        = $datas->no_polisi;
                $estimasi->nama_lengkap     = $datas->nama_lengkap;
                $estimasi->tgl_masuk        = $datas->tgl_masuk;
                $estimasi->tgl_janji_penyerahan       = $datas->tgl_janji_penyerahan;
                $estimasi->nama_sa          = $datas->nama_lengkap_user;
                $estimasi->kategori_jasa    = $datas->kategori_jasa;
                $estimasi->status_produksi  = $datas->status_produksi;
                
                array_push($listEstimasi, $estimasi);

            }

            $data = array(
                            'title'             => 'Teknisi | U-Care',
                            'listEstimasi'      => $listEstimasi,
                            'isi'               => 'admin/dashboard/teknisi/ready',
                            'dataScript'        => 'admin/dataScript/tabel-script',
                        );
        }
        // var_dump($listEstimasi);
        $this->load->view('admin/_layout/wrapper', $data);
    }

    public function go_process(){
        $id_estimasi    = $this->input->post('id_estimasi');
        $tahap          = $this->input->post('tahap');
        $date = date('Y-m-d H:i:s');
        $whereEstimasi  = array('id_estimasi'         => $id_estimasi);
        $itemsEstimasi  = array('status_produksi'     => $tahap);

        if($tahap == '1'):
            $tahap_to_update_in = 'body_repair_in';
        elseif($tahap == '2'):
            $tahap_to_update_in = 'preparation_in';
        elseif($tahap == '3'):
            $tahap_to_update_in = 'masking_in';
        elseif($tahap == '4'):
            $tahap_to_update_in = 'painting_in';
        elseif($tahap == '5'):
            $tahap_to_update_in = 'polishing_in';
        elseif($tahap == '6'):
            $tahap_to_update_in = 're_assembling_in';
        elseif($tahap == '7'):
            $tahap_to_update_in = 'wasling_in';
        elseif($tahap == '8'):
            $tahap_to_update_in = 'final_inspection_in';
        elseif($tahap == '9'):
            $tahap_to_update_in = 'done';
        endif;

        $itemsSummary   = array('id_estimasi_summary' => $id_estimasi,
                                $tahap_to_update_in   => $date
                                );
        $this->Crud->u('estimasi', $itemsEstimasi, $whereEstimasi); 
        $this->Crud->i('summary', $itemsSummary);
        echo "success";
    }

    public function on_process($id_estimasi = null){
        $id_user = $this->session->userdata('id_user');
        if($id_estimasi != null){
            $queryEstimasi  = ' SELECT * FROM estimasi
                                INNER JOIN customer ON customer.id_customer = estimasi.id_customer
                                INNER JOIN jenis_kendaran ON jenis_kendaran.id_jenis = estimasi.id_jenis
                                INNER JOIN user ON user.id_user = estimasi.id_user
                                WHERE estimasi.id_estimasi = "'.$id_estimasi.'"
                                ';
            
            $queryPartsJasa = 'SELECT * FROM detail_estimasi
                                LEFT JOIN item ON item.id_item = detail_estimasi.id_item
                                WHERE detail_estimasi.id_estimasi = "'.$id_estimasi.'"
                                ORDER BY item.tipe_item';

            $queryJasa     = 'SELECT * FROM summary
                              LEFT JOIN estimasi ON estimasi.id_estimasi = summary.id_estimasi_summary
                              WHERE  summary.id_estimasi_summary = "'.$id_estimasi.'"';
            
            $data = array(
                            'title'             => 'Teknisi | U-Care',
                            'isi'               => 'admin/dashboard/teknisi/on_process_detail',
                            'dataEstimasi'      => $this->Crud->q($queryEstimasi),
                            'dataPartsJasa'     => $this->Crud->q($queryPartsJasa),
                            'dataJasa'     => $this->Crud->q($queryJasa),
                            'dataScript'        => 'admin/dataScript/tabel-script',
                        );
        }else{
            $queryEstimasi  = ' SELECT * FROM estimasi
                                LEFT JOIN customer ON customer.id_customer = estimasi.id_customer
                                LEFT JOIN jenis_kendaran ON jenis_kendaran.id_jenis = estimasi.id_jenis
                                LEFT JOIN user ON user.id_user = estimasi.id_user
                                WHERE estimasi.tgl_masuk IS NOT NULL
                                AND estimasi.tim_teknisi = "'.$id_user.'" AND estimasi.status_produksi > "0" AND estimasi.status_produksi != "9"
                                ';

            $listEstimasi = array();
            $dataEstimasi = $this->Crud->q($queryEstimasi);
            
            foreach($dataEstimasi as $datas){
                $estimasi = new stdClass();
                $estimasi->nomor_wo         = $datas->nomor_wo;
                $estimasi->id_estimasi      = $datas->id_estimasi;
                $estimasi->no_polisi        = $datas->no_polisi;
                $estimasi->nama_lengkap     = $datas->nama_lengkap;
                $estimasi->tgl_masuk        = $datas->tgl_masuk;
                $estimasi->tgl_janji_penyerahan       = $datas->tgl_janji_penyerahan;
                $estimasi->nama_sa          = $datas->nama_lengkap_user;
                $estimasi->kategori_jasa    = $datas->kategori_jasa;
                $estimasi->status_produksi  = $datas->status_produksi;
                      
                array_push($listEstimasi, $estimasi);

            }

            $data = array(
                            'title'             => 'Teknisi | U-Care',
                            'listEstimasi'      => $listEstimasi,
                            'isi'               => 'admin/dashboard/teknisi/on_process',
                            'dataScript'        => 'admin/dataScript/tabel-script',
                        );
        }
        // var_dump($listEstimasi);
        $this->load->view('admin/_layout/wrapper', $data);
    }

    public function update_tahap_produksi(){
        $tahap_to_update_in = '';
        $tahap_to_update_out = '';
        $date = date('Y-m-d H:i:s');
        $id_estimasi = $this->input->post('id_estimasi');
        $tahap = $this->input->post('tahap');
        
        if($tahap == '1'):
            $tahap_to_update_in = 'body_repair_in';
            $tahap_to_update_out = 'body_repair_out';
        elseif($tahap == '2'):
            $tahap_to_update_in = 'preparation_in';
            $tahap_to_update_out = 'body_repair_out';
            // $tahap_to_update_out = 'preparation_out';
        elseif($tahap == '3'):
            $tahap_to_update_in = 'masking_in';
            $tahap_to_update_out = 'preparation_out';
            // $tahap_to_update_out = 'masking_out';
        elseif($tahap == '4'):
            $tahap_to_update_in = 'painting_in';
            $tahap_to_update_out = 'masking_out';
            // $tahap_to_update_out = 'painting_out';
        elseif($tahap == '5'):
            $tahap_to_update_in = 'polishing_in';
            $tahap_to_update_out = 'painting_out';
            // $tahap_to_update_out = 'polishing_out';
        elseif($tahap == '6'):
            $tahap_to_update_in = 're_assembling_in';
            $tahap_to_update_out = 'polishing_out';
            // $tahap_to_update_out = 're_assembling_out';
        elseif($tahap == '7'):
            $tahap_to_update_in = 'wasling_in';
            $tahap_to_update_out = 're_assembling_out';
            // $tahap_to_update_out = 'wasling_out';
        elseif($tahap == '8'):
            $tahap_to_update_in = 'final_inspection_in';
            $tahap_to_update_out = 'wasling_out';
            // $tahap_to_update_out = 'final_inspection_out';
        elseif($tahap == '9'):
            $tahap_to_update_in = 'done';
            $tahap_to_update_out = 'final_inspection_out';
        endif;

        $whereEstimasi  = array('id_estimasi' => $id_estimasi);
        $whereSummary   = array('id_estimasi_summary' => $id_estimasi);
        $itemsEstimasi  = array('status_produksi' => $tahap);
        $itemsSummary   = array($tahap_to_update_in => $date,
                                $tahap_to_update_out => $date
                                );

        $this->Crud->u('estimasi', $itemsEstimasi, $whereEstimasi);
        $this->Crud->u('summary', $itemsSummary, $whereSummary);

    }

    public function done($id_estimasi = null){
        // $id_user = $this->session->userdata('id_user');
        if($id_estimasi != null){
            $queryEstimasi  = ' SELECT * FROM estimasi
                                INNER JOIN customer ON customer.id_customer = estimasi.id_customer
                                INNER JOIN jenis_kendaran ON jenis_kendaran.id_jenis = estimasi.id_jenis
                                INNER JOIN user ON user.id_user = estimasi.id_user
                                WHERE estimasi.id_estimasi = "'.$id_estimasi.'"
                                ';
            
            $queryPartsJasa      = 'SELECT * FROM detail_estimasi
                                    LEFT JOIN item ON item.id_item = detail_estimasi.id_item
                                    WHERE detail_estimasi.id_estimasi = "'.$id_estimasi.'"
                                    ORDER BY item.tipe_item';

            $queryJasa     = 'SELECT * FROM summary
                          LEFT JOIN estimasi ON estimasi.id_estimasi = summary.id_estimasi_summary
                          WHERE  summary.id_estimasi_summary = "'.$id_estimasi.'"';
            
            $data = array(
                            'title'             => 'Teknisi | U-Care',
                            'isi'               => 'admin/dashboard/teknisi/on_process_detail',
                            'dataEstimasi'      => $this->Crud->q($queryEstimasi),
                            'dataPartsJasa'     => $this->Crud->q($queryPartsJasa),
                            'dataJasa'     => $this->Crud->q($queryJasa),
                            'dataScript'        => 'admin/dataScript/tabel-script',
                        );
        }else{
            $queryEstimasi  = ' SELECT * FROM estimasi
                                LEFT JOIN customer ON customer.id_customer = estimasi.id_customer
                                LEFT JOIN jenis_kendaran ON jenis_kendaran.id_jenis = estimasi.id_jenis
                                LEFT JOIN user ON user.id_user = estimasi.id_user
                                LEFT JOIN summary ON summary.id_estimasi_summary = estimasi.id_estimasi
                                WHERE estimasi.status_produksi = "9" AND estimasi.tgl_penyerahan IS NULL AND summary.final_inspection_out IS NOT NULL
                                ';
            
            $listEstimasi = array();
            $dataEstimasi = $this->Crud->q($queryEstimasi);
            
            foreach($dataEstimasi as $datas){
                $estimasi = new stdClass();
                $estimasi->nomor_wo         = $datas->nomor_wo;
                $estimasi->id_estimasi      = $datas->id_estimasi;
                $estimasi->no_polisi        = $datas->no_polisi;
                $estimasi->nama_lengkap     = $datas->nama_lengkap;
                $estimasi->tgl_masuk        = $datas->tgl_masuk;
                $estimasi->tgl_done        = $datas->done;
                $estimasi->nama_sa          = $datas->nama_lengkap_user;
                $estimasi->kategori_jasa    = $datas->kategori_jasa;
                
                      
                array_push($listEstimasi, $estimasi);

            }

            $data = array(
                            'title'             => 'Teknisi | U-Care',
                            'titleCard'        => 'Estimasi To Delivery',
                            'listEstimasi'      => $listEstimasi,
                            'isi'               => 'admin/dashboard/teknisi/done',
                            'dataScript'        => 'admin/dataScript/tabel-script',
                        );
        }
        // var_dump($listEstimasi);
        $this->load->view('admin/_layout/wrapper', $data);
    }

    public function doneComplete($id_estimasi = null){
        if($id_estimasi != null){
            $queryEstimasi  = ' SELECT * FROM estimasi
                                LEFT JOIN customer ON customer.id_customer = estimasi.id_customer
                                LEFT JOIN jenis_kendaran ON jenis_kendaran.id_jenis = estimasi.id_jenis
                                LEFT JOIN summary ON summary.id_estimasi_summary = estimasi.id_estimasi
                                LEFT JOIN user ON user.id_user = estimasi.id_user
                                WHERE estimasi.id_estimasi = "'.$id_estimasi.'"
                                ';
            
            $queryPartsJasa      = 'SELECT * FROM detail_estimasi
                                    LEFT JOIN item ON item.id_item = detail_estimasi.id_item
                                    WHERE detail_estimasi.id_estimasi = "'.$id_estimasi.'"
                                    ORDER BY item.tipe_item';

            $queryJasa     = 'SELECT * FROM summary
                          LEFT JOIN estimasi ON estimasi.id_estimasi = summary.id_estimasi_summary
                          WHERE  summary.id_estimasi_summary = "'.$id_estimasi.'"';
            
            $data = array(
                            'title'             => 'Teknisi | U-Care',
                            'isi'               => 'admin/dashboard/teknisi/on_process_detail',
                            'dataEstimasi'      => $this->Crud->q($queryEstimasi),
                            'dataPartsJasa'     => $this->Crud->q($queryPartsJasa),
                            'dataJasa'     => $this->Crud->q($queryJasa),
                            'dataScript'        => 'admin/dataScript/tabel-script',
                        );
        }else{
            $queryEstimasi  = ' SELECT * FROM estimasi
                                LEFT JOIN customer ON customer.id_customer = estimasi.id_customer
                                LEFT JOIN jenis_kendaran ON jenis_kendaran.id_jenis = estimasi.id_jenis
                                LEFT JOIN user ON user.id_user = estimasi.id_user
                                LEFT JOIN summary ON summary.id_estimasi_summary = estimasi.id_estimasi
                                WHERE estimasi.status_produksi = "9" AND estimasi.tgl_penyerahan IS NOT NULL summary.final_inspection_out IS NOT NULL
                                ';
            
            $listEstimasi = array();
            $dataEstimasi = $this->Crud->q($queryEstimasi);
            
            foreach($dataEstimasi as $datas){
                $estimasi = new stdClass();
                $estimasi->nomor_wo         = $datas->nomor_wo;
                $estimasi->id_estimasi      = $datas->id_estimasi;
                $estimasi->no_polisi        = $datas->no_polisi;
                $estimasi->nama_lengkap     = $datas->nama_lengkap;
                $estimasi->tgl_masuk        = $datas->tgl_masuk;
                $estimasi->tgl_done        = $datas->final_inspection_out;
                $estimasi->nama_sa          = $datas->nama_lengkap_user;
                $estimasi->kategori_jasa    = $datas->kategori_jasa;
                
                      
                array_push($listEstimasi, $estimasi);

            }

            $data = array(
                            'title'             => 'Teknisi | U-Care',
                            'titleCard'        => 'Estimasi Delivery Complete',
                            'listEstimasi'      => $listEstimasi,
                            'isi'               => 'admin/dashboard/teknisi/done',
                            'dataScript'        => 'admin/dataScript/tabel-script',
                        );
        }
        // var_dump($listEstimasi);
        $this->load->view('admin/_layout/wrapper', $data);
    }

    public function getDataChart(){
        $year = $this->input->post('year');
        $id_user = $this->session->userdata('id_user');
        $yearNow = date('Y');
        $dataCountLight = array();
        $dataCountMedium = array();
        $dataCountHeavy = array();
        
        if($year !== NULL ){
            $yearNow = $year;
            for($i = 1 ; $i < 13; $i++){
                $queryCountLight = 'SELECT COUNT(*) as totLight FROM estimasi
                                    WHERE kategori_jasa = "0" AND tim_teknisi = "'.$id_user.'"
                                    AND YEAR(tgl_masuk) = "'.$yearNow.'" AND MONTH(tgl_masuk) = "'.$i.'"';
                $queryCountMedium = 'SELECT COUNT(*) as totMedium FROM estimasi
                                     WHERE kategori_jasa = "1" AND tim_teknisi = "'.$id_user.'"
                                     AND YEAR(tgl_masuk) = "'.$yearNow.'" AND MONTH(tgl_masuk) = "'.$i.'"';
                $queryCountHeavy = 'SELECT COUNT(*) as totHeavy FROM estimasi
                                    WHERE kategori_jasa = "2" AND tim_teknisi = "'.$id_user.'" AND YEAR(tgl_masuk) = "'.$yearNow.'" 
                                    AND MONTH(tgl_masuk) = "'.$i.'" 
                                      ';
                
                $tempLight = $this->Crud->q($queryCountLight);
                $tempMedium = $this->Crud->q($queryCountMedium);
                $tempHeavy = $this->Crud->q($queryCountHeavy);
                
                foreach ($tempLight as $value) {
                    $dataCountLight[] = (int)$value->totLight; 
                }
                foreach ($tempMedium as $value) {
                    $dataCountMedium[] = (int)$value->totMedium; 
                }
                foreach ($tempHeavy as $value) {
                    $dataCountHeavy[] = (int)$value->totHeavy; 
                }
                
                $data = array ('dataCountLight'  => $dataCountLight,
                               'dataCountMedium' => $dataCountMedium,
                               'dataCountHeavy'  => $dataCountHeavy,
                       
                               );
            }
        }else{
            for($i = 1 ; $i < 13; $i++){
                $queryCountLight = 'SELECT COUNT(*) as totLight FROM estimasi
                                    WHERE kategori_jasa = "1" AND tim_teknisi = "'.$id_user.'"
                                    AND YEAR(tgl_masuk) = "'.$yearNow.'" AND MONTH(tgl_masuk) = "'.$i.'"';
                $queryCountMedium = 'SELECT COUNT(*) as totMedium FROM estimasi
                                     WHERE kategori_jasa = "0" AND tim_teknisi = "'.$id_user.'"
                                     AND YEAR(tgl_masuk) = "'.$yearNow.'" AND MONTH(tgl_masuk) = "'.$i.'"';
                $queryCountHeavy = 'SELECT COUNT(*) as totLight FROM estimasi
                                    WHERE kategori_jasa = "2" AND tim_teknisi = "'.$id_user.'" AND YEAR(tgl_masuk) = "'.$yearNow.'" 
                                    AND MONTH(tgl_masuk) = "'.$i.'" 
                                    ';
                
                $tempLight = $this->Crud->q($queryCountLight);
                $tempMedium = $this->Crud->q($queryCountMedium);
                $tempHeavy = $this->Crud->q($queryCountHeavy);
                
                foreach ($tempLight as $value) {
                    $dataCountLight[] = (int)$value->totLight; 
                }
                foreach ($tempMedium as $value) {
                    $dataCountMedium[] = (int)$value->totMedium; 
                }
                foreach ($tempHeavy as $value) {
                    $dataCountHeavy[] = (int)$value->totLight; 
                }
                
            }
            $data = array ('dataCountLight' => $dataCountLight,
                           'dataCountMedium' => $dataCountMedium,
                           'dataCountHeavy' => $dataCountHeavy
                        );
        }
        
        echo json_encode($data);
    }

    public function atBodyRepair(){
        // $id_user = $this->session->userdata('id_user');
        $queryEstimasi  = ' SELECT *, u.nama_lengkap_user as namaSa, uu.nama_lengkap_user as
                            namaTeknisi  FROM estimasi
                            LEFT JOIN customer ON customer.id_customer = estimasi.id_customer
                            LEFT JOIN jenis_kendaran ON jenis_kendaran.id_jenis = estimasi.id_jenis
                            LEFT JOIN user u ON u.id_user = estimasi.id_user
                            LEFT JOIN user uu ON uu.id_user = estimasi.tim_teknisi
                            LEFT JOIN summary ON summary.id_estimasi_summary = estimasi.id_estimasi
                            LEFT JOIN summary_lead ON summary_lead.id_estimasi_lead = estimasi.id_estimasi
                            WHERE estimasi.status_produksi = "1" AND summary.body_repair_qc IS NULL';
        $listEstimasi = array();
        $dataEstimasi = $this->Crud->q($queryEstimasi);
        
        foreach($dataEstimasi as $datas){
            $estimasi = new stdClass();
            $estimasi->nomor_wo         = $datas->nomor_wo;
            $estimasi->id_estimasi      = $datas->id_estimasi;
            $estimasi->no_polisi        = $datas->no_polisi;
            $estimasi->nama_lengkap     = $datas->nama_lengkap;
            $estimasi->tgl_masuk        = $datas->tgl_masuk;
            $estimasi->tgl_janji_penyerahan       = $datas->tgl_janji_penyerahan;
            $estimasi->nama_sa          = $datas->namaSa;
            $estimasi->kategori_jasa    = $datas->kategori_jasa;
            $estimasi->status_produksi  = $datas->status_produksi;
            $estimasi->body_repair_in   = $datas->body_repair_in;
            $estimasi->body_repair_status = $datas->body_repair_status; 
            $estimasi->body_repair_out    = $datas->body_repair_out;
            $estimasi->id_lead          = $datas->id_lead;
            $estimasi->body_repair_pause    = $datas->body_repair_pause;
            $estimasi->body_repair_start    = $datas->body_repair_start;
            $estimasi->body_repair_note     = $datas->body_repair_note;
            $estimasi->body_repair_lead     = $datas->body_repair_lead;
            $estimasi->total_lead           = $datas->total_lead;
            $estimasi->nama_teknisi     = $datas->namaTeknisi;
            $estimasi->is_confirm_teknisi = $datas->is_confirm_teknisi;
            $estimasi->ket              = $datas->body_repair_ket;
            array_push($listEstimasi, $estimasi);
        }

        $data = array(
                        'title'             => 'Body Repair | U-Care',
                        'listEstimasi'      => $listEstimasi,
                        'isi'               => 'admin/dashboard/teknisi/atBodyRepair',
                        'dataScript'        => 'admin/dataScript/tabel-script',
                        );
        // var_dump($listEstimasi);
        $this->load->view('admin/_layout/wrapper', $data);
    } 

    public function atPreparation(){
        // $id_user = $this->session->userdata('id_user');
        $queryEstimasi  = ' SELECT *, u.nama_lengkap_user as namaSa, uu.nama_lengkap_user as
                            namaTeknisi FROM estimasi
                            LEFT JOIN customer ON customer.id_customer = estimasi.id_customer
                            LEFT JOIN jenis_kendaran ON jenis_kendaran.id_jenis = estimasi.id_jenis
                            LEFT JOIN user u ON u.id_user = estimasi.id_user
                            LEFT JOIN user uu ON uu.id_user = estimasi.tim_teknisi
                            LEFT JOIN summary ON summary.id_estimasi_summary = estimasi.id_estimasi
                            LEFT JOIN summary_lead ON summary_lead.id_estimasi_lead = estimasi.id_estimasi
                            WHERE estimasi.status_produksi = "2"';
        $listEstimasi = array();
        $dataEstimasi = $this->Crud->q($queryEstimasi);
        
        foreach($dataEstimasi as $datas){
            $estimasi = new stdClass();
            $estimasi->nomor_wo         = $datas->nomor_wo;
            $estimasi->id_estimasi      = $datas->id_estimasi;
            $estimasi->no_polisi        = $datas->no_polisi;
            $estimasi->nama_lengkap     = $datas->nama_lengkap;
            $estimasi->tgl_masuk        = $datas->tgl_masuk;
            $estimasi->tgl_janji_penyerahan       = $datas->tgl_janji_penyerahan;
            $estimasi->nama_sa          = $datas->namaSa;
            $estimasi->kategori_jasa    = $datas->kategori_jasa;
            $estimasi->status_produksi  = $datas->status_produksi;
            $estimasi->preparation_in   = $datas->preparation_in;
            $estimasi->preparation_status   = $datas->preparation_status;
            $estimasi->preparation_out   = $datas->preparation_out;
            $estimasi->id_lead          = $datas->id_lead;
            $estimasi->preparation_pause    = $datas->preparation_pause;
            $estimasi->preparation_start    = $datas->preparation_start;
            $estimasi->preparation_note     = $datas->preparation_note;
            $estimasi->preparation_lead     = $datas->preparation_lead;
            $estimasi->total_lead           = $datas->total_lead;
            $estimasi->nama_teknisi     = $datas->namaTeknisi;
            $estimasi->is_confirm_teknisi = $datas->is_confirm_teknisi;
            $estimasi->ket              = $datas->preparation_ket;
            array_push($listEstimasi, $estimasi);
        }

        $data = array(
                        'title'             => 'Preparation | U-Care',
                        'listEstimasi'      => $listEstimasi,
                        'isi'               => 'admin/dashboard/teknisi/atPreparation',
                        'dataScript'        => 'admin/dataScript/tabel-script',
                        );
        // var_dump($listEstimasi);
        $this->load->view('admin/_layout/wrapper', $data);
    }
    public function atMasking(){
        // $id_user = $this->session->userdata('id_user');
        $queryEstimasi  = ' SELECT *, u.nama_lengkap_user as namaSa, uu.nama_lengkap_user as
                            namaTeknisi FROM estimasi
                            LEFT JOIN customer ON customer.id_customer = estimasi.id_customer
                            LEFT JOIN jenis_kendaran ON jenis_kendaran.id_jenis = estimasi.id_jenis
                            LEFT JOIN user u ON u.id_user = estimasi.id_user
                            LEFT JOIN user uu ON uu.id_user = estimasi.tim_teknisi
                            LEFT JOIN summary ON summary.id_estimasi_summary = estimasi.id_estimasi
                            LEFT JOIN summary_lead ON summary_lead.id_estimasi_lead = estimasi.id_estimasi
                            WHERE estimasi.status_produksi = "3"';
        $listEstimasi = array();
        $dataEstimasi = $this->Crud->q($queryEstimasi);
        
        foreach($dataEstimasi as $datas){
            $estimasi = new stdClass();
            $estimasi->nomor_wo         = $datas->nomor_wo;
            $estimasi->id_estimasi      = $datas->id_estimasi;
            $estimasi->no_polisi        = $datas->no_polisi;
            $estimasi->nama_lengkap     = $datas->nama_lengkap;
            $estimasi->tgl_masuk        = $datas->tgl_masuk;
            $estimasi->tgl_janji_penyerahan       = $datas->tgl_janji_penyerahan;
            $estimasi->nama_sa          = $datas->namaSa;
            $estimasi->kategori_jasa    = $datas->kategori_jasa;
            $estimasi->status_produksi  = $datas->status_produksi;
            $estimasi->masking_in   = $datas->masking_in;
            $estimasi->masking_status   = $datas->masking_status;
            $estimasi->masking_out   = $datas->masking_out;
            $estimasi->id_lead          = $datas->id_lead;
            $estimasi->masking_pause    = $datas->masking_pause;
            $estimasi->masking_start    = $datas->masking_start;
            $estimasi->masking_note     = $datas->masking_note;
            $estimasi->masking_lead     = $datas->masking_lead;
            $estimasi->total_lead           = $datas->total_lead;
            $estimasi->nama_teknisi     = $datas->namaTeknisi;
            $estimasi->is_confirm_teknisi = $datas->is_confirm_teknisi;
            $estimasi->ket              = $datas->masking_ket;
            array_push($listEstimasi, $estimasi);
        }

        $data = array(
                        'title'             => 'Masking | U-Care',
                        'listEstimasi'      => $listEstimasi,
                        'isi'               => 'admin/dashboard/teknisi/atMasking',
                        'dataScript'        => 'admin/dataScript/tabel-script',
                        );
        // var_dump($listEstimasi);
        $this->load->view('admin/_layout/wrapper', $data);
    }

    public function atPainting(){
        // $id_user = $this->session->userdata('id_user');
        $queryEstimasi  = ' SELECT *, u.nama_lengkap_user as namaSa, uu.nama_lengkap_user as
                            namaTeknisi FROM estimasi
                            LEFT JOIN customer ON customer.id_customer = estimasi.id_customer
                            LEFT JOIN jenis_kendaran ON jenis_kendaran.id_jenis = estimasi.id_jenis
                            LEFT JOIN user u ON u.id_user = estimasi.id_user
                            LEFT JOIN user uu ON uu.id_user = estimasi.tim_teknisi
                            LEFT JOIN summary ON summary.id_estimasi_summary = estimasi.id_estimasi
                            LEFT JOIN summary_lead ON summary_lead.id_estimasi_lead = estimasi.id_estimasi
                            WHERE estimasi.status_produksi = "4"';
        $listEstimasi = array();
        $dataEstimasi = $this->Crud->q($queryEstimasi);
        
        foreach($dataEstimasi as $datas){
            $estimasi = new stdClass();
            $estimasi->nomor_wo         = $datas->nomor_wo;
            $estimasi->id_estimasi      = $datas->id_estimasi;
            $estimasi->no_polisi        = $datas->no_polisi;
            $estimasi->nama_lengkap     = $datas->nama_lengkap;
            $estimasi->tgl_masuk        = $datas->tgl_masuk;
            $estimasi->tgl_janji_penyerahan       = $datas->tgl_janji_penyerahan;
            $estimasi->nama_sa          = $datas->namaSa;
            $estimasi->kategori_jasa    = $datas->kategori_jasa;
            $estimasi->status_produksi  = $datas->status_produksi;
            $estimasi->painting_in   = $datas->painting_in;
            $estimasi->painting_status   = $datas->painting_status;
            $estimasi->painting_out   = $datas->painting_out;
            $estimasi->id_lead          = $datas->id_lead;
            $estimasi->painting_pause    = $datas->painting_pause;
            $estimasi->painting_start    = $datas->painting_start;
            $estimasi->painting_note     = $datas->painting_note;
            $estimasi->painting_lead     = $datas->painting_lead;
            $estimasi->total_lead           = $datas->total_lead;
            $estimasi->nama_teknisi     = $datas->namaTeknisi;
            $estimasi->is_confirm_teknisi = $datas->is_confirm_teknisi;
            $estimasi->ket              = $datas->painting_ket;
            array_push($listEstimasi, $estimasi);
        }

        $data = array(
                        'title'             => 'Painting | U-Care',
                        'listEstimasi'      => $listEstimasi,
                        'isi'               => 'admin/dashboard/teknisi/atPainting',
                        'dataScript'        => 'admin/dataScript/tabel-script',
                        );
        // var_dump($listEstimasi);
        $this->load->view('admin/_layout/wrapper', $data);
    } 

    public function atPolishing(){
        // $id_user = $this->session->userdata('id_user');
        $queryEstimasi  = ' SELECT *, u.nama_lengkap_user as namaSa, uu.nama_lengkap_user as
                            namaTeknisi FROM estimasi
                            LEFT JOIN customer ON customer.id_customer = estimasi.id_customer
                            LEFT JOIN jenis_kendaran ON jenis_kendaran.id_jenis = estimasi.id_jenis
                            LEFT JOIN user u ON u.id_user = estimasi.id_user
                            LEFT JOIN user uu ON uu.id_user = estimasi.tim_teknisi
                            LEFT JOIN summary ON summary.id_estimasi_summary = estimasi.id_estimasi
                            LEFT JOIN summary_lead ON summary_lead.id_estimasi_lead = estimasi.id_estimasi
                            WHERE estimasi.status_produksi = "5"';
        $listEstimasi = array();
        $dataEstimasi = $this->Crud->q($queryEstimasi);
        
        foreach($dataEstimasi as $datas){
            $estimasi = new stdClass();
            $estimasi->nomor_wo         = $datas->nomor_wo;
            $estimasi->id_estimasi      = $datas->id_estimasi;
            $estimasi->no_polisi        = $datas->no_polisi;
            $estimasi->nama_lengkap     = $datas->nama_lengkap;
            $estimasi->tgl_masuk        = $datas->tgl_masuk;
            $estimasi->tgl_janji_penyerahan       = $datas->tgl_janji_penyerahan;
            $estimasi->nama_sa          = $datas->namaSa;
            $estimasi->kategori_jasa    = $datas->kategori_jasa;
            $estimasi->status_produksi  = $datas->status_produksi;
            $estimasi->polishing_in   = $datas->polishing_in;
            $estimasi->polishing_status   = $datas->polishing_status;
            $estimasi->polishing_out   = $datas->polishing_out;
            $estimasi->id_lead          = $datas->id_lead;
            $estimasi->polishing_pause    = $datas->polishing_pause;
            $estimasi->polishing_start    = $datas->polishing_start;
            $estimasi->polishing_note     = $datas->polishing_note;
            $estimasi->polishing_lead     = $datas->polishing_lead;
            $estimasi->total_lead           = $datas->total_lead;
            $estimasi->nama_teknisi     = $datas->namaTeknisi;
            $estimasi->is_confirm_teknisi = $datas->is_confirm_teknisi;
            $estimasi->ket              = $datas->polishing_ket;
            array_push($listEstimasi, $estimasi);
        }

        $data = array(
                        'title'             => 'Polishing | U-Care',
                        'listEstimasi'      => $listEstimasi,
                        'isi'               => 'admin/dashboard/teknisi/atPolishing',
                        'dataScript'        => 'admin/dataScript/tabel-script',
                        );
        // var_dump($listEstimasi);
        $this->load->view('admin/_layout/wrapper', $data);
    }

    public function atReAssembling(){
        // $id_user = $this->session->userdata('id_user');
        $queryEstimasi  = ' SELECT *, u.nama_lengkap_user as namaSa, uu.nama_lengkap_user as
                            namaTeknisi FROM estimasi
                            LEFT JOIN customer ON customer.id_customer = estimasi.id_customer
                            LEFT JOIN jenis_kendaran ON jenis_kendaran.id_jenis = estimasi.id_jenis
                            LEFT JOIN user u ON u.id_user = estimasi.id_user
                            LEFT JOIN user uu ON uu.id_user = estimasi.tim_teknisi
                            LEFT JOIN summary ON summary.id_estimasi_summary = estimasi.id_estimasi
                            LEFT JOIN summary_lead ON summary_lead.id_estimasi_lead = estimasi.id_estimasi
                            WHERE estimasi.status_produksi = "6"';
        $listEstimasi = array();
        $dataEstimasi = $this->Crud->q($queryEstimasi);
        
        foreach($dataEstimasi as $datas){
            $estimasi = new stdClass();
            $estimasi->nomor_wo         = $datas->nomor_wo;
            $estimasi->id_estimasi      = $datas->id_estimasi;
            $estimasi->no_polisi        = $datas->no_polisi;
            $estimasi->nama_lengkap     = $datas->nama_lengkap;
            $estimasi->tgl_masuk        = $datas->tgl_masuk;
            $estimasi->tgl_janji_penyerahan       = $datas->tgl_janji_penyerahan;
            $estimasi->nama_sa          = $datas->namaSa;
            $estimasi->kategori_jasa    = $datas->kategori_jasa;
            $estimasi->status_produksi  = $datas->status_produksi;
            $estimasi->re_assembling_in = $datas->re_assembling_in;
            $estimasi->re_assembling_status = $datas->re_assembling_status;
            $estimasi->re_assembling_out = $datas->re_assembling_out;
            $estimasi->id_lead          = $datas->id_lead;
            $estimasi->re_assembling_pause    = $datas->re_assembling_pause;
            $estimasi->re_assembling_start    = $datas->re_assembling_start;
            $estimasi->re_assembling_note     = $datas->re_assembling_note;
            $estimasi->re_assembling_lead     = $datas->re_assembling_lead;
            $estimasi->total_lead           = $datas->total_lead;
            $estimasi->nama_teknisi     = $datas->namaTeknisi;
            $estimasi->is_confirm_teknisi = $datas->is_confirm_teknisi;
            $estimasi->ket              = $datas->re_assembling_ket;
            array_push($listEstimasi, $estimasi);
        }

        $data = array(
                        'title'             => 'Re-Assembling | U-Care',
                        'listEstimasi'      => $listEstimasi,
                        'isi'               => 'admin/dashboard/teknisi/atReAssembling',
                        'dataScript'        => 'admin/dataScript/tabel-script',
                        );
        // var_dump($listEstimasi);
        $this->load->view('admin/_layout/wrapper', $data);
    }

    public function atWashing(){
        // $id_user = $this->session->userdata('id_user');
        $queryEstimasi  = ' SELECT *, u.nama_lengkap_user as namaSa, uu.nama_lengkap_user as
                            namaTeknisi FROM estimasi
                            LEFT JOIN customer ON customer.id_customer = estimasi.id_customer
                            LEFT JOIN jenis_kendaran ON jenis_kendaran.id_jenis = estimasi.id_jenis
                            LEFT JOIN user u ON u.id_user = estimasi.id_user
                            LEFT JOIN user uu ON uu.id_user = estimasi.tim_teknisi
                            LEFT JOIN summary ON summary.id_estimasi_summary = estimasi.id_estimasi
                            LEFT JOIN summary_lead ON summary_lead.id_estimasi_lead = estimasi.id_estimasi
                            WHERE estimasi.status_produksi = "7"';
        $listEstimasi = array();
        $dataEstimasi = $this->Crud->q($queryEstimasi);
        
        foreach($dataEstimasi as $datas){
            $estimasi = new stdClass();
            $estimasi->nomor_wo         = $datas->nomor_wo;
            $estimasi->id_estimasi      = $datas->id_estimasi;
            $estimasi->no_polisi        = $datas->no_polisi;
            $estimasi->nama_lengkap     = $datas->nama_lengkap;
            $estimasi->tgl_masuk        = $datas->tgl_masuk;
            $estimasi->tgl_janji_penyerahan       = $datas->tgl_janji_penyerahan;
            $estimasi->nama_sa          = $datas->namaSa;
            $estimasi->kategori_jasa    = $datas->kategori_jasa;
            $estimasi->status_produksi  = $datas->status_produksi;
            $estimasi->washing_in = $datas->wasling_in;
            $estimasi->washing_status = $datas->washing_status;
            $estimasi->washing_out = $datas->wasling_out;
            $estimasi->id_lead          = $datas->id_lead;
            $estimasi->washing_status = $datas->washing_status;
            $estimasi->washing_pause    = $datas->washing_pause;
            $estimasi->washing_start    = $datas->washing_start;
            $estimasi->washing_note     = $datas->washing_note;
            $estimasi->washing_lead     = $datas->washing_lead;
            $estimasi->total_lead           = $datas->total_lead;
            $estimasi->nama_teknisi     = $datas->namaTeknisi;
            $estimasi->is_confirm_teknisi = $datas->is_confirm_teknisi;
            $estimasi->ket              = $datas->wasling_ket;
            array_push($listEstimasi, $estimasi);
        }

        $data = array(
                        'title'             => 'Washing | U-Care',
                        'listEstimasi'      => $listEstimasi,
                        'isi'               => 'admin/dashboard/teknisi/atWashing',
                        'dataScript'        => 'admin/dataScript/tabel-script',
                        );
        // var_dump($listEstimasi);
        $this->load->view('admin/_layout/wrapper', $data);
    }

    public function atFinalInspection(){
        // $id_user = $this->session->userdata('id_user');
        $queryEstimasi  = ' SELECT *, u.nama_lengkap_user as namaSa, uu.nama_lengkap_user as
                            namaTeknisi FROM estimasi
                            LEFT JOIN customer ON customer.id_customer = estimasi.id_customer
                            LEFT JOIN jenis_kendaran ON jenis_kendaran.id_jenis = estimasi.id_jenis
                            LEFT JOIN user u ON u.id_user = estimasi.id_user
                            LEFT JOIN user uu ON uu.id_user = estimasi.tim_teknisi
                            LEFT JOIN summary ON summary.id_estimasi_summary = estimasi.id_estimasi
                            LEFT JOIN summary_lead ON summary_lead.id_estimasi_lead = estimasi.id_estimasi
                            WHERE estimasi.status_produksi = "8"';
        $listEstimasi = array();
        $dataEstimasi = $this->Crud->q($queryEstimasi);
        
        foreach($dataEstimasi as $datas){
            $estimasi = new stdClass();
            $estimasi->nomor_wo         = $datas->nomor_wo;
            $estimasi->id_estimasi      = $datas->id_estimasi;
            $estimasi->no_polisi        = $datas->no_polisi;
            $estimasi->nama_lengkap     = $datas->nama_lengkap;
            $estimasi->tgl_masuk        = $datas->tgl_masuk;
            $estimasi->tgl_janji_penyerahan       = $datas->tgl_janji_penyerahan;
            $estimasi->nama_sa          = $datas->namaSa;
            $estimasi->kategori_jasa    = $datas->kategori_jasa;
            $estimasi->status_produksi  = $datas->status_produksi;
            $estimasi->final_inspection_in = $datas->final_inspection_in;
            $estimasi->final_inspection_status = $datas->final_inspection_status;
            $estimasi->final_inspection_out = $datas->final_inspection_out;
            $estimasi->id_lead          = $datas->id_lead;
            $estimasi->final_inspection_pause    = $datas->final_inspection_pause;
            $estimasi->final_inspection_start    = $datas->final_inspection_start;
            $estimasi->final_inspection_note     = $datas->final_inspection_note;
            $estimasi->final_inspection_lead     = $datas->final_inspection_lead;
            $estimasi->total_lead           = $datas->total_lead;
            $estimasi->ket              = $datas->final_inspection_ket;
            $estimasi->nama_teknisi     = $datas->namaTeknisi;
            $estimasi->is_confirm_teknisi = $datas->is_confirm_teknisi;
            $queryCountNotReadyParts = 'SELECT COUNT(*) as totNotReadyParts FROM detail_estimasi 
                                        LEFT JOIN item ON item.id_item = detail_estimasi.id_item
                                        WHERE id_estimasi = "'.$datas->id_estimasi.'" 
                                        AND item.tipe_item = "2" 
                                        AND detail_estimasi.ata IS NULL';
            $dataCountNotReadyParts = $this->Crud->q($queryCountNotReadyParts);
            foreach($dataCountNotReadyParts as $dataCount){
                $estimasi->countNotReadyParts = $dataCount->totNotReadyParts;
            }
            array_push($listEstimasi, $estimasi);
        }

        $data = array(
                        'title'             => 'Final Inspection | U-Care',
                        'listEstimasi'      => $listEstimasi,
                        'isi'               => 'admin/dashboard/teknisi/atFinalInspection',
                        'dataScript'        => 'admin/dataScript/tabel-script',
                        );
        // var_dump($listEstimasi);
        $this->load->view('admin/_layout/wrapper', $data);
    }

    public function clockOn(){
        $id_estimasi = $this->input->post('id_estimasi');
        $whereEstimasi = array('id_estimasi' => $id_estimasi);
        $whereSummary = array('id_estimasi_summary' => $id_estimasi);
        $status = $this->input->post('status');
        if($status == "1"){
            $itemsSummary = array('body_repair_in' => date('Y-m-d H:i:s'));
            $itemsEstimasi = array('status_produksi' => "1");
        }elseif ($status == "2") {
            $itemsSummary = array('preparation_in' => date('Y-m-d H:i:s'));
            $itemsEstimasi = array('status_produksi' => "2");
        }elseif ($status == "3") {
            $itemsSummary = array('masking_in' => date('Y-m-d H:i:s'));
            $itemsEstimasi = array('status_produksi' => "3");
        }elseif ($status == "4") {
            $itemsSummary = array('painting_in' => date('Y-m-d H:i:s'));
            $itemsEstimasi = array('status_produksi' => "4");
        }elseif ($status == "5") {
            $itemsSummary = array('polishing_in' => date('Y-m-d H:i:s'));
            $itemsEstimasi = array('status_produksi' => "5");
        }elseif ($status == "6") {
            $itemsSummary = array('re_assembling_in' => date('Y-m-d H:i:s'));
            $itemsEstimasi = array('status_produksi' => "6");
        }elseif ($status == "7") {
            $itemsSummary = array('wasling_in' => date('Y-m-d H:i:s'));
            $itemsEstimasi = array('status_produksi' => "7");
        }else{
          $itemsSummary = array('final_inspection_in' => date('Y-m-d H:i:s')); 
          $itemsEstimasi = array('status_produksi' => "8"); 
        }

        $this->Crud->u('estimasi', $itemsEstimasi, $whereEstimasi);
        $this->Crud->u('summary', $itemsSummary, $whereSummary);

    }

    public function clockOff(){
        $id_estimasi = $this->input->post('id_estimasi');
        $whereEstimasi = array('id_estimasi' => $id_estimasi);
        $whereSummary = array('id_estimasi_summary' => $id_estimasi);
        $status = $this->input->post('status');
        if($status == "1"){
            $itemsSummary = array('body_repair_out' => date('Y-m-d H:i:s'));
            $itemsEstimasi = array('status_produksi' => "1");
        }elseif ($status == "2") {
            $itemsSummary = array('preparation_out' => date('Y-m-d H:i:s'));
            $itemsEstimasi = array('status_produksi' => "2");
        }elseif ($status == "3") {
            $itemsSummary = array('masking_out' => date('Y-m-d H:i:s'));
            $itemsEstimasi = array('status_produksi' => "3");
        }elseif ($status == "4") {
            $itemsSummary = array('painting_out' => date('Y-m-d H:i:s'));
            $itemsEstimasi = array('status_produksi' => "4");
        }elseif ($status == "5") {
            $itemsSummary = array('polishing_out' => date('Y-m-d H:i:s'));
            $itemsEstimasi = array('status_produksi' => "5");
        }elseif ($status == "6") {
            $itemsSummary = array('re_assembling_out' => date('Y-m-d H:i:s'));
            $itemsEstimasi = array('status_produksi' => "6");
        }elseif ($status == "7") {
            $itemsSummary = array('wasling_out' => date('Y-m-d H:i:s'));
            $itemsEstimasi = array('status_produksi' => "7");
        }else{
          $itemsSummary = array('final_inspection_out' => date('Y-m-d H:i:s')); 
          $itemsEstimasi = array('status_produksi' => "8"); 
        }

        $this->Crud->u('estimasi', $itemsEstimasi, $whereEstimasi);
        $this->Crud->u('summary', $itemsSummary, $whereSummary);

    }

    public function qualityControl(){
        $id_estimasi = $this->input->post('id_estimasi');
        $qc = $this->input->post('qc');
        $ket = $this->input->post('ket');
        $status = $this->input->post('status');
        $whereEstimasi = array('id_estimasi' => $id_estimasi);
        $whereSummary = array('id_estimasi_summary' => $id_estimasi);
        if($status == "1"){
            if($qc == "1"){
                $itemsEstimasi = array('status_produksi' => "2");
                $itemsSummary = array('body_repair_qc'   => $qc,
                                      'body_repair_ket'  => $ket);        
            }else{
                $itemsEstimasi = array('status_produksi' => "1");
                $itemsSummary = array('body_repair_qc'   => NULL,
                                      'body_repair_ket'  => $ket,
                                      'body_repair_out'  => NULL);    
            }
        }elseif ($status == "2") {
            if($qc == "1"){
                $itemsEstimasi = array('status_produksi' => "3");
                $itemsSummary = array('preparation_qc'   => $qc,
                                      'preparation_ket'  => $ket);        
            }else{
                $itemsEstimasi = array('status_produksi' => "2");
                $itemsSummary = array('preparation_qc'   => NULL,
                                      'preparation_ket'  => $ket,
                                      'preparation_out'  => NULL);    
            }
        }elseif ($status == "3") {
            if($qc == "1"){
                $itemsEstimasi = array('status_produksi' => "4");
                $itemsSummary = array('masking_qc'   => $qc,
                                      'masking_ket'  => $ket);        
            }else{
                $itemsEstimasi = array('status_produksi' => "3");
                $itemsSummary = array('masking_qc'   => NULL,
                                      'masking_ket'  => $ket,
                                      'masking_out'  => NULL);    
            }
        }elseif ($status == "4") {
            if($qc == "1"){
                $itemsEstimasi = array('status_produksi' => "5");
                $itemsSummary = array('painting_qc'   => $qc,
                                      'painting_ket'  => $ket);        
            }else{
                $itemsEstimasi = array('status_produksi' => "4");
                $itemsSummary = array('painting_qc'   => NULL,
                                      'painting_ket'  => $ket,
                                      'painting_out'  => NULL);    
            }
        }elseif ($status == "5") {
            if($qc == "1"){
                $itemsEstimasi = array('status_produksi' => "6");
                $itemsSummary = array('polishing_qc'   => $qc,
                                      'polishing_ket'  => $ket);        
            }else{
                $itemsEstimasi = array('status_produksi' => "5");
                $itemsSummary = array('polishing_qc'   => NULL,
                                      'polishing_ket'  => $ket,
                                      'polishing_out'  => NULL);    
            }
        }elseif ($status == "6") {
            if($qc == "1"){
                $itemsEstimasi = array('status_produksi' => "7");
                $itemsSummary = array('re_assembling_qc'   => $qc,
                                      're_assembling_ket'  => $ket);        
            }else{
                $itemsEstimasi = array('status_produksi' => "6");
                $itemsSummary = array('re_assembling_qc'   => NULL,
                                      're_assembling_ket'  => $ket,
                                      're_assembling_out'  => NULL);    
            }
        }elseif ($status == "7") {
            if($qc == "1"){
                $itemsEstimasi = array('status_produksi' => "8");
                $itemsSummary = array('wasling_qc'   => $qc,
                                      'wasling_ket'  => $ket);        
            }else{
                $itemsEstimasi = array('status_produksi' => "7");
                $itemsSummary = array('wasling_qc'   => NULL,
                                      'wasling_ket'  => $ket,
                                      'wasling_out'  => NULL);    
            }
        }else{
          if($qc == "1"){
                $itemsEstimasi = array('status_produksi' => "9");
                $itemsSummary = array('final_inspection_qc'   => $qc,
                                      'final_inspection_ket'  => $ket);        
            }else{
                $itemsEstimasi = array('status_produksi' => "8");
                $itemsSummary = array('final_inspection_qc'   => NULL,
                                      'final_inspection_ket'  => $ket,
                                      'final_inspection_out'  => NULL);    
            }
        }
        $this->Crud->u('estimasi', $itemsEstimasi, $whereEstimasi);
        $this->Crud->u('summary', $itemsSummary, $whereSummary);
    } 

    public function profile(){
        $data = array(
                        'title'     => 'Profile Service Advisor | U-Care',
                        'isi'       => 'admin/form/formTeknisiProfile',
                        'dataScript'=> 'admin/dataScript/form-script'
                      );
        $this->load->view('admin/_layout/wrapper', $data);
    }

    public function userUpdateAction(){
        $input = $this->input->post(NULL, TRUE); //get all post with xss filter

        //deteck with password or not
        if($input['new_pass']==NULL){
            $items = array(//save all post in array
                'nama_lengkap_user' => $input['nama_lengkap'],
                'username' => $input['username'],
            );
        }else{
            $items = array(//save all post in array
                'nama_lengkap_user' => $input['nama_lengkap'],
                'username' => $input['username'],
                'password' => md5($input['new_pass']),
            );
        }

        $where = array(//get id for models Crud params w
            "id_user" => $this->session->userdata('id_user'),
        );

        //update session
        $sess = array(
            "nama_user" => $input['nama_lengkap'],
            "username" => $input['username'],
        );
        $this->session->set_userdata($sess);

        $this->Crud->u('user', $items, $where ); //save in database
        $this->session->set_flashdata('update_sukses', 'Update Berhasil !'); //for notif it is succes

        redirect('teknisi/profile');
    }

    public function monitoringParts(){
        $queryEstimasi  = ' SELECT * FROM estimasi
                            LEFT JOIN customer ON customer.id_customer = estimasi.id_customer
                            LEFT JOIN jenis_kendaran ON jenis_kendaran.id_jenis = estimasi.id_jenis
                            LEFT JOIN user ON user.id_user = estimasi.id_user
                            WHERE estimasi.status_inout = "1" AND (estimasi.jenis_estimasi = "1" OR estimasi.jenis_estimasi = "2")
                           ';
        $queryCountParts = 'SELECT id_estimasi, COUNT(*) as countParts FROM detail_estimasi 
                            LEFT JOIN item on item.id_item = detail_estimasi.id_item
                            WHERE item.tipe_item = "2"
                            GROUP BY id_estimasi ORDER BY id_estimasi ASC';

        $queryCountParts2 = 'SELECT id_estimasi, COUNT(ata) as countParts2 FROM detail_estimasi 
                             LEFT JOIN item on item.id_item = detail_estimasi.id_item
                             WHERE item.tipe_item = "2"
                             GROUP BY id_estimasi ORDER BY id_estimasi ASC' ;
        $queryTeknisi     = 'SELECT * FROM estimasi 
                             LEFT JOIN user ON user.id_user = estimasi.tim_teknisi'; 

        $listEstimasi = array();
        $dataEstimasi = $this->Crud->q($queryEstimasi);
        $countTotalParts = $this->Crud->q($queryCountParts);
        $countParts = $this->Crud->q($queryCountParts2);
        $dataTeknisi = $this->Crud->q($queryTeknisi);
        foreach ($dataEstimasi as $satuanEstimasi) {
            $estimasi = new stdClass();
            $estimasi->no_wo = $satuanEstimasi->nomor_wo; 
            $estimasi->id = $satuanEstimasi->id_estimasi;
            $estimasi->no_polisi = $satuanEstimasi->no_polisi;
            $estimasi->nama_lengkap = $satuanEstimasi->nama_lengkap;
            $estimasi->nama_sa      = $satuanEstimasi->nama_lengkap_user;
            $estimasi->tgl_estimasi = $satuanEstimasi->tgl_estimasi;
            $estimasi->tgl_janji_penyerahan = $satuanEstimasi->tgl_janji_penyerahan;
            foreach ($countTotalParts as $totalParts) {
                if ($satuanEstimasi->id_estimasi == $totalParts->id_estimasi){
                    $estimasi->totalParts = $totalParts->countParts;
                }
            }
             foreach ($countParts as $totalParts) {
                if ($satuanEstimasi->id_estimasi == $totalParts->id_estimasi){
                    $estimasi->totalReadyParts = $totalParts->countParts2;
                }
            }
            foreach ($dataTeknisi as $teknisi){
                if($satuanEstimasi->tim_teknisi == $teknisi->tim_teknisi){
                    $estimasi->nama_teknisi  = $teknisi->nama_lengkap_user;
                }
            }
            array_push($listEstimasi, $estimasi);
        }
        
        $data       = array ('title'        => 'Monitoring Parts | U-Care',
                             'isi'          => 'admin/dashboard/teknisi/monitoringParts',
                             'dataScript'   => 'admin/dataScript/tabel-script',
                             'listEstimasi' => $listEstimasi
                         );
       $this->load->view('admin/_layout/wrapper', $data);
    }  

    public function detailParts($id_estimasi = null){
        $queryEstimasi  = ' SELECT * FROM estimasi
                            INNER JOIN customer ON customer.id_customer = estimasi.id_customer
                            INNER JOIN jenis_kendaran ON jenis_kendaran.id_jenis = estimasi.id_jenis
                            INNER JOIN color ON color.id_color = estimasi.id_color
                            INNER JOIN user ON user.id_user = estimasi.id_user
                            WHERE estimasi.id_estimasi = "'.$id_estimasi.'"
                           ';
        $queryParts     = 'SELECT * FROM detail_estimasi
                           LEFT JOIN item ON item.id_item = detail_estimasi.id_item
                           WHERE item.tipe_item= "2" AND detail_estimasi.id_estimasi = "'.$id_estimasi.'"';
        $queryTeknisi   = 'SELECT * FROM estimasi
                           INNER JOIN user ON user.id_user = estimasi.tim_teknisi
                           WHERE estimasi.id_estimasi = "'.$id_estimasi.'"';
        if ($id_estimasi !== null) {

            $where      = array( 'id_estimasi' => $id_estimasi );
            $content    = 'admin/dashboard/teknisi/detailParts';

            $data = array(  'title'     => 'Detail Parts | U-Care',
                            'isi'       => $content,
                            'dataParts'      => $this->Crud->q($queryParts),
                            'dataEstimasi' => $this->Crud->q($queryEstimasi),
                            'dataTeknisi'       => $this->Crud->q($queryTeknisi),
                            'dataScript'=> 'admin/dataScript/tabel-script' );
        }else{
           redirect(base_url());
        }

        $this->load->view('admin/_layout/wrapper', $data);
    }

}
