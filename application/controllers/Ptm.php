<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ptm extends CI_Controller {

    public function __construct() {
        parent::__construct();
        date_default_timezone_set('Asia/Makassar');
        if($this->session->userdata('status') != "login"){
            redirect('login');
        }
        $method = $this->router->fetch_method();
        if($method != 'monitoringProduksi' && $method != 'detailProduksi'){
          if($this->session->userdata('level') != "ptm" && $this->session->userdata('level') != "pimpinan"){
              $level = $this->session->userdata('level');
              $controller = preg_replace("/[^a-z]/", "_", $level);
              redirect($controller);
          }
        }
    }

    //01 halaman awal / dispatching
    public function index() {
        redirect(base_url('ptm/workInProcess'));
    }

    //01 halaman awal / dispatching
    public function workInProcess(){

        // $content    = 'admin/dashboard/ptm/estimasi_onProses';
        $content    = 'admin/dashboard/ptm/workInProcess';

        $getLevel   = array('level' => 'teknisi');               
        $getTeknisi = $this->Crud->gw('user', $getLevel);

        $countTask  = array();
        foreach ($getTeknisi as $datas) {
          $teknisi = new stdClass();
          $teknisi->id_teknisi          = $datas->id_user;
          $teknisi->nama_lengkap_user   = $datas->nama_lengkap_user;
          $queryCount  ='SELECT * ,
                          sum(e.kategori_jasa = "0" and e.tgl_masuk IS NOT NULL and e.status_produksi = "1") countLight,
                          sum(e.kategori_jasa = "1" and e.tgl_masuk IS NOT NULL and e.status_produksi = "1") countMedium,
                          sum(e.kategori_jasa = "2" and e.tgl_masuk IS NOT NULL and e.status_produksi = "1") countHeavy,

                          sum(e.kategori_jasa = "0" and e.tgl_masuk IS NOT NULL and e.status_produksi = "2") countLight2,
                          sum(e.kategori_jasa = "1" and e.tgl_masuk IS NOT NULL and e.status_produksi = "2") countMedium2,
                          sum(e.kategori_jasa = "2" and e.tgl_masuk IS NOT NULL and e.status_produksi = "2") countHeavy2,

                          sum(e.kategori_jasa = "0" and e.tgl_masuk IS NOT NULL and e.status_produksi = "3") countLight3,
                          sum(e.kategori_jasa = "1" and e.tgl_masuk IS NOT NULL and e.status_produksi = "3") countMedium3,
                          sum(e.kategori_jasa = "2" and e.tgl_masuk IS NOT NULL and e.status_produksi = "3") countHeavy3, 

                          sum(e.kategori_jasa = "0" and e.tgl_masuk IS NOT NULL and e.status_produksi = "4") countLight4,
                          sum(e.kategori_jasa = "1" and e.tgl_masuk IS NOT NULL and e.status_produksi = "4") countMedium4,
                          sum(e.kategori_jasa = "2" and e.tgl_masuk IS NOT NULL and e.status_produksi = "4") countHeavy4,      

                          sum(e.kategori_jasa = "0" and e.tgl_masuk IS NOT NULL and e.status_produksi = "5") countLight5,
                          sum(e.kategori_jasa = "1" and e.tgl_masuk IS NOT NULL and e.status_produksi = "5") countMedium5,
                          sum(e.kategori_jasa = "2" and e.tgl_masuk IS NOT NULL and e.status_produksi = "5") countHeavy5,

                          sum(e.kategori_jasa = "0" and e.tgl_masuk IS NOT NULL and e.status_produksi = "6") countLight6,
                          sum(e.kategori_jasa = "1" and e.tgl_masuk IS NOT NULL and e.status_produksi = "6") countMedium6,
                          sum(e.kategori_jasa = "2" and e.tgl_masuk IS NOT NULL and e.status_produksi = "6") countHeavy6,    

                          sum(e.kategori_jasa = "0" and e.tgl_masuk IS NOT NULL and e.status_produksi = "7") countLight7,
                          sum(e.kategori_jasa = "1" and e.tgl_masuk IS NOT NULL and e.status_produksi = "7") countMedium7,
                          sum(e.kategori_jasa = "2" and e.tgl_masuk IS NOT NULL and e.status_produksi = "7") countHeavy7,  

                          sum(e.kategori_jasa = "0" and e.tgl_masuk IS NOT NULL and e.status_produksi = "8") countLight8,
                          sum(e.kategori_jasa = "1" and e.tgl_masuk IS NOT NULL and e.status_produksi = "8") countMedium8,
                          sum(e.kategori_jasa = "2" and e.tgl_masuk IS NOT NULL and e.status_produksi = "8") countHeavy8,  

                          sum(e.kategori_jasa = "0" and e.tgl_masuk IS NOT NULL and e.status_produksi != "9") countLightT,
                          sum(e.kategori_jasa = "1" and e.tgl_masuk IS NOT NULL and e.status_produksi != "9") countMediumT,
                          sum(e.kategori_jasa = "2" and e.tgl_masuk IS NOT NULL and e.status_produksi != "9") countHeavyT  
                          -- count(e.kategori_jasa) total
                          FROM estimasi e 
                          LEFT JOIN user u ON u.id_user = e.tim_teknisi
                          WHERE e.tgl_masuk IS NOT NULL AND e.status_produksi > "0" AND e.status_produksi != "9" AND is_confirm_teknisi IS NOT NULL AND e.tim_teknisi = "'.$datas->id_user.'"
                          ';
              $getCount = $this->Crud->q($queryCount);
              foreach ($getCount as $count) {
                $teknisi->bodyPaintLight        = $count->countLight;
                $teknisi->bodyPaintMedium       = $count->countMedium;
                $teknisi->bodyPaintHeavy        = $count->countHeavy;
                $teknisi->preparationLight      = $count->countLight2;
                $teknisi->preparationMedium     = $count->countMedium2;
                $teknisi->preparationHeavy      = $count->countHeavy2;
                $teknisi->maskingLight          = $count->countLight3;
                $teknisi->maskingMedium         = $count->countMedium3;
                $teknisi->maskingHeavy          = $count->countHeavy3;
                $teknisi->paintingLight         = $count->countLight4;
                $teknisi->paintingMedium        = $count->countMedium4;
                $teknisi->paintingHeavy         = $count->countHeavy4;
                $teknisi->polishingLight        = $count->countLight5;
                $teknisi->polishingMedium       = $count->countMedium5;
                $teknisi->polishingHeavy        = $count->countHeavy5;
                $teknisi->reassemblingLight     = $count->countLight6;
                $teknisi->reassemblingMedium    = $count->countMedium6;
                $teknisi->reassemblingHeavy     = $count->countHeavy6;
                $teknisi->washingLight          = $count->countLight7;
                $teknisi->washingMedium         = $count->countMedium7;
                $teknisi->washingHeavy          = $count->countHeavy7;
                $teknisi->finalInspectionLight  = $count->countLight8;
                $teknisi->finalInspectionMedium = $count->countMedium8;
                $teknisi->finalInspectionHeavy  = $count->countHeavy8;
                $teknisi->totalLight            = $count->countLightT;
                $teknisi->totalMedium           = $count->countMediumT;
                $teknisi->totalHeavy            = $count->countHeavyT;
              }
            array_push($countTask, $teknisi);
        }
          // var_dump($getTeknisi);
          // die();
        $data         = array(  
                            'title'             => 'PTM | U-Care',
                            'isi'               => $content,
                            'hitung'            => $countTask,
                            'dataScript'        => 'admin/dataScript/tabel-script' );    
        $this->load->view('admin/_layout/wrapper', $data);
    }

    //02 Job Dispatch
    public function nextJob(){
        $getTeknisi = array('level' => 'teknisi');
        $queryEstimasi   = 'SELECT * FROM estimasi e
                            LEFT JOIN customer c ON c.id_customer = e.id_customer
                            LEFT JOIN user u ON u.id_user = e.id_user
                            WHERE e.jenis_estimasi IN ("2", "0") 
                            AND e.status_inout = "1" 
                            AND e.tim_teknisi IS NULL';

        $queryCountParts = 'SELECT id_estimasi, COUNT(*) as countParts FROM detail_estimasi 
                            LEFT JOIN item on item.id_item = detail_estimasi.id_item
                            WHERE item.tipe_item = "2"
                            GROUP BY id_estimasi ORDER BY id_estimasi ASC';

        $queryCountParts2 = 'SELECT id_estimasi, COUNT(ata) as countParts2 FROM detail_estimasi 
                             LEFT JOIN item on item.id_item = detail_estimasi.id_item
                             WHERE item.tipe_item = "2"
                             GROUP BY id_estimasi ORDER BY id_estimasi ASC' ;
        $listEstimasi = array();
        $dataEstimasi = $this->Crud->q($queryEstimasi);
        $countTotalParts = $this->Crud->q($queryCountParts);
        $countParts = $this->Crud->q($queryCountParts2);

        foreach ($dataEstimasi as $satuanEstimasi) {
            $estimasi = new stdClass();
            $estimasi->no_wo = $satuanEstimasi->nomor_wo;
            $estimasi->id_estimasi = $satuanEstimasi->id_estimasi;
            $estimasi->no_polisi = $satuanEstimasi->no_polisi;
            $estimasi->nama_customer = $satuanEstimasi->nama_lengkap;
            $estimasi->tgl_janji_penyerahan = $satuanEstimasi->tgl_janji_penyerahan;
            $estimasi->kategori_jasa = $satuanEstimasi->kategori_jasa;
            $estimasi->nama_sa = $satuanEstimasi->nama_lengkap_user;
            if($satuanEstimasi->jenis_estimasi == "1" || $satuanEstimasi->jenis_estimasi == "2"){
                $estimasi->isParts = true;
            }else{
                $estimasi->isParts = false;
            }
            foreach ($countTotalParts as $totalParts) {
                if ($satuanEstimasi->id_estimasi == $totalParts->id_estimasi){
                    $estimasi->totalParts = $totalParts->countParts;
                }
            }
            foreach ($countParts as $totalParts) {
                if ($satuanEstimasi->id_estimasi == $totalParts->id_estimasi){
                    $estimasi->totalReadyParts = $totalParts->countParts2;
                }
            }

            array_push($listEstimasi, $estimasi);
        }
        $content    = 'admin/dashboard/ptm/estimasi_ready';

        $data       = array(  
                        'title'             => 'PTM | U-Care',
                        'isi'               => $content,
                        'data'              => $listEstimasi,
                        'dataT'             => $this->Crud->gw('user', $getTeknisi),
                        'dataScript'        => 'admin/dataScript/tabel-script' );    
        $this->load->view('admin/_layout/wrapper', $data);
    }

    //02 Job Dispatch
    public function nextJobDetail($id_estimasi = null){

        $queryEstimasi  = ' SELECT * FROM estimasi
                            INNER JOIN customer ON customer.id_customer = estimasi.id_customer
                            INNER JOIN jenis_kendaran ON jenis_kendaran.id_jenis = estimasi.id_jenis
                            INNER JOIN color ON color.id_color = estimasi.id_color
                            INNER JOIN user ON user.id_user = estimasi.id_user
                            WHERE estimasi.id_estimasi = "'.$id_estimasi.'"
                           ';
        $queryParts     = 'SELECT * FROM detail_estimasi
                           LEFT JOIN item ON item.id_item = detail_estimasi.id_item
                           WHERE detail_estimasi.id_estimasi = "'.$id_estimasi.'"';

            $where      = array( 'id_estimasi'  => $id_estimasi );
            $content    = 'admin/dashboard/ptm/nextJobDetail';

            $data = array(  'title'             => 'Detail Parts | U-Care',
                            'isi'               => $content,
                            'dataParts'         => $this->Crud->q($queryParts),
                            'dataEstimasi'      => $this->Crud->q($queryEstimasi),
                            'dataScript'        => 'admin/dataScript/tabel-script' );

        $this->load->view('admin/_layout/wrapper', $data);      
    }

    //02 ajaxinput Modal
    public function ajaxInputModal(){

        $input          = $this->input->post(NULL, TRUE);

        $where          = array('id_estimasi'     => $input['id_estimasi']);
        $data           = array('tim_teknisi'     => $input['selectTim'],
                                'status_produksi' => $input['selectProses'],
                                'tgl_masuk'       => date('y-m-d')
                                );

        $data2          = array('ptm_processed' => "1");

        $this->Crud->u('estimasi', $data, $where);
        $this->Crud->u('detail_estimasi', $data2, $where);
        echo 'success';
    }

    //03 Monitoring Produksi
    public function monitoringProduksi(){
      $getTeknisi = array('level' => 'teknisi');
      $id_user = $this->session->userdata('id_user');
      $level = $this->session->userdata('level');

      $queryCountJasa =  'SELECT *, COUNT(*) as countJasa FROM detail_estimasi 
                          LEFT JOIN item on item.id_item = detail_estimasi.id_item
                          LEFT JOIN estimasi on estimasi.id_estimasi = detail_estimasi.id_estimasi
                          WHERE item.tipe_item = "1" AND estimasi.status_inout = "1"
                          GROUP BY detail_estimasi.id_estimasi ORDER BY detail_estimasi.id_estimasi ASC';

      if($level == 'service advisor' || $level == 'teknisi'){
          if($level == 'service advisor'){
              $atributQuery = 'AND estimasi.id_user = '.$id_user.'';
          }else{
              $atributQuery = 'AND estimasi.tim_teknisi = "'.$id_user.'"';
          }

          $queryEstimasi  = ' SELECT * FROM estimasi
                            LEFT JOIN customer ON customer.id_customer = estimasi.id_customer
                            LEFT JOIN jenis_kendaran ON jenis_kendaran.id_jenis = estimasi.id_jenis
                            LEFT JOIN user ON user.id_user = estimasi.id_user
                            WHERE estimasi.status_produksi != "9" AND estimasi.tgl_masuk IS NOT NULL '.$atributQuery.'
                            ';
          $queryTimTeknisi = ' SELECT * FROM estimasi 
                              LEFT JOIN user ON user.id_user = estimasi.tim_teknisi
                            ';                  
          $getJasa        =  $queryCountJasa;
                            
      }elseif ($level == 'ptm' || $level == 'qc'){
          $queryEstimasi  = ' SELECT * FROM estimasi
                              LEFT JOIN customer ON customer.id_customer = estimasi.id_customer
                              LEFT JOIN jenis_kendaran ON jenis_kendaran.id_jenis = estimasi.id_jenis
                              LEFT JOIN user ON user.id_user = estimasi.id_user
                              WHERE estimasi.tgl_masuk IS NOT NULL AND estimasi.status_produksi != "9"
                              ORDER BY estimasi.status_produksi';
         $queryTimTeknisi = ' SELECT * FROM estimasi 
                              LEFT JOIN user ON user.id_user = estimasi.tim_teknisi
                            ';
          $getJasa        =  $queryCountJasa;
          

      }

      $listEstimasi = array();
      $dataEstimasi = $this->Crud->q($queryEstimasi);
      $countJasa    = $this->Crud->q($getJasa);
      $dataTimTeknisi = $this->Crud->q($queryTimTeknisi);
      foreach($dataEstimasi as $datas){
          $estimasi = new stdClass();
          $estimasi->id_estimasi            = $datas->id_estimasi;
          $estimasi->nomor_wo               = $datas->nomor_wo;
          $estimasi->no_polisi              = $datas->no_polisi;
          $estimasi->nama_lengkap           = $datas->nama_lengkap;
          $estimasi->nama_lengkap_user      = $datas->nama_lengkap_user;
          $estimasi->tgl_masuk              = $datas->tgl_masuk;
          $estimasi->tgl_janji_penyerahan   = $datas->tgl_janji_penyerahan;
          $estimasi->kategori_jasa          = $datas->kategori_jasa;
          $estimasi->status_produksi        = $datas->status_produksi;
          $estimasi->is_confirm_teknisi     = $datas->is_confirm_teknisi;
          
          foreach($dataTimTeknisi as $teknisi){
              if($datas->tim_teknisi == $teknisi->tim_teknisi){
                  $estimasi->nama_teknisi = $teknisi->nama_lengkap_user;
                  $estimasi->id_teknisi   = $teknisi->id_user;
              }
          }
          array_push($listEstimasi, $estimasi);

      }

      $data            = array ('title'            => 'Monitoring Produksi | U-Care',
                                'isi'              => 'admin/dashboard/ptm/monitor_produksi',
                                'dataEstimasi'     => $listEstimasi,
                                'dataCount'        => $this->Crud->q($queryCountJasa),
                                'dataT'            => $this->Crud->gw('user', $getTeknisi),
                                'dataScript'       => 'admin/dataScript/tabel-script',
                                );
      $this->load->view('admin/_layout/wrapper', $data);
    }

    //03 Monitoring Produksi Detail
    public function detailProduksi($id_estimasi = null){
        $queryEstimasi  = ' SELECT * FROM estimasi
                            INNER JOIN customer ON customer.id_customer = estimasi.id_customer
                            INNER JOIN jenis_kendaran ON jenis_kendaran.id_jenis = estimasi.id_jenis
                            INNER JOIN color ON color.id_color = estimasi.id_color
                            INNER JOIN user ON user.id_user = estimasi.id_user
                            WHERE estimasi.id_estimasi = "'.$id_estimasi.'"';
        $queryJasa      = ' SELECT * FROM summary
                            LEFT JOIN estimasi ON estimasi.id_estimasi = summary.id_estimasi_summary
                            LEFT JOIN summary_lead ON summary_lead.id_estimasi_lead = summary.id_estimasi_summary
                            WHERE  summary.id_estimasi_summary = "'.$id_estimasi.'"';
        $queryTeknisi   = ' SELECT * FROM estimasi
                            INNER JOIN user ON user.id_user = estimasi.tim_teknisi
                            WHERE estimasi.id_estimasi = "'.$id_estimasi.'"';

        $where          = array( 'id_estimasi' => $id_estimasi );
        $content        = 'admin/dashboard/ptm/monitor_produksi_detail';

        $data           = array('title'             => 'Detail Parts | U-Care',
                                'isi'               => $content,
                                'dataJasa'          => $this->Crud->q($queryJasa),
                                'dataEstimasi'      => $this->Crud->q($queryEstimasi),
                                'dataTeknisi'       => $this->Crud->q($queryTeknisi),
                                'dataScript'        => 'admin/dataScript/tabel-script' );
        $this->load->view('admin/_layout/wrapper', $data);
    }

    //03 ajaxUpdate Modal
    public function ajaxUpdateModal(){

        $input          = $this->input->post(NULL, TRUE);

        $where          = array('id_estimasi'   => $input['id_estimasi']);
        $data           = array('tim_teknisi'   => $input['selectTim']);

        $this->Crud->u('estimasi', $data, $where);
        echo 'success';
    }

    //03 Monitoring Parts
    public function monitoringParts($id_estimasi = NULL){
      
        $queryEstimasi    = 'SELECT * FROM estimasi
                             INNER JOIN customer ON customer.id_customer = estimasi.id_customer
                             INNER JOIN jenis_kendaran ON jenis_kendaran.id_jenis = estimasi.id_jenis
                             INNER JOIN user ON user.id_user = estimasi.id_user 
                             WHERE estimasi.tgl_masuk IS NOT NULL AND estimasi.status_produksi < "9" AND estimasi.jenis_estimasi != "0"
                             ORDER BY estimasi.status_produksi';
        $queryCountParts  = 'SELECT id_estimasi, COUNT(*) as countParts FROM detail_estimasi 
                             INNER JOIN item on item.id_item = detail_estimasi.id_item
                             WHERE item.tipe_item = "2"
                             GROUP BY id_estimasi ORDER BY id_estimasi ASC';
        $queryCountParts2 = 'SELECT id_estimasi, COUNT(ata) as countParts2 FROM detail_estimasi 
                             INNER JOIN item on item.id_item = detail_estimasi.id_item
                             WHERE item.tipe_item = "2"
                             GROUP BY id_estimasi ORDER BY id_estimasi ASC' ; 
        $queryTimTeknisi  = 'SELECT * FROM estimasi 
                             LEFT JOIN user ON user.id_user = estimasi.tim_teknisi';

        $listEstimasi     = array();
        $dataEstimasi     = $this->Crud->q($queryEstimasi);
        $countTotalParts  = $this->Crud->q($queryCountParts);
        $countParts       = $this->Crud->q($queryCountParts2);
        $dataTimTeknisi   = $this->Crud->q($queryTimTeknisi);

        foreach ($dataEstimasi as $satuanEstimasi) {
            $estimasi     = new stdClass();
            $estimasi->id                   = $satuanEstimasi->id_estimasi;
            $estimasi->nomor_wo             = $satuanEstimasi->nomor_wo;
            $estimasi->no_polisi            = $satuanEstimasi->no_polisi;
            $estimasi->nama_lengkap         = $satuanEstimasi->nama_lengkap;
            $estimasi->nama_lengkap_user    = $satuanEstimasi->nama_lengkap_user;
            $estimasi->tgl_masuk            = $satuanEstimasi->tgl_masuk;
            $estimasi->tgl_janji_penyerahan = $satuanEstimasi->tgl_janji_penyerahan;
            

            foreach ($countTotalParts as $totalParts) {
                if ($satuanEstimasi->id_estimasi == $totalParts->id_estimasi){
                    $estimasi->totalParts = $totalParts->countParts;
                }
            }
             foreach ($countParts as $totalParts) {
                if ($satuanEstimasi->id_estimasi == $totalParts->id_estimasi){
                    $estimasi->totalReadyParts = $totalParts->countParts2;
                }
            }
            foreach($dataTimTeknisi as $teknisi){
                if($satuanEstimasi->tim_teknisi == $teknisi->tim_teknisi){
                    $estimasi->nama_teknisi = $teknisi->nama_lengkap_user;
                }
            }
            array_push($listEstimasi, $estimasi);
        }
        
        $data       = array ('title'            => 'Monitoring Parts | U-Care',
                             'isi'              => 'admin/dashboard/ptm/monitor_parts',
                             'dataEstimasi'     => $this->Crud->q($queryEstimasi),
                             'dataCount'        => $this->Crud->q($queryCountParts),
                             'dataCount2'       => $this->Crud->q($queryCountParts2),
                             'dataScript'       => 'admin/dataScript/tabel-script',
                             'listEstimasi'     => $listEstimasi
                         );
       $this->load->view('admin/_layout/wrapper', $data);
    }

    //03 Monitoring Parts Detail
    public function detailParts($id_estimasi = null){
        $queryEstimasi  = ' SELECT * FROM estimasi
                            INNER JOIN customer ON customer.id_customer = estimasi.id_customer
                            INNER JOIN jenis_kendaran ON jenis_kendaran.id_jenis = estimasi.id_jenis
                            INNER JOIN color ON color.id_color = estimasi.id_color
                            INNER JOIN user ON user.id_user = estimasi.id_user
                            WHERE estimasi.id_estimasi = "'.$id_estimasi.'"
                           ';
        $queryParts     = 'SELECT * FROM detail_estimasi
                           LEFT JOIN item ON item.id_item = detail_estimasi.id_item
                           WHERE detail_estimasi.id_estimasi = "'.$id_estimasi.'" AND item.tipe_item = "2"
                           ';
        $queryTeknisi   = 'SELECT * FROM estimasi
                           INNER JOIN user ON user.id_user = estimasi.tim_teknisi
                           WHERE estimasi.id_estimasi = "'.$id_estimasi.'"';

        $where      = array( 'id_estimasi' => $id_estimasi );
        $content    = 'admin/dashboard/ptm/monitor_parts_detail';

        $data       = array(  'title'             => 'Detail Parts | U-Care',
                              'isi'               => $content,
                              'dataParts'         => $this->Crud->q($queryParts),
                              'dataEstimasi'      => $this->Crud->q($queryEstimasi),
                              'dataTeknisi'       => $this->Crud->q($queryTeknisi),
                              'dataScript'        => 'admin/dataScript/tabel-script' );

    $this->load->view('admin/_layout/wrapper', $data);
    }

    //04 Unit Finish
    public function readyToDelivery(){

      $queryEstimasi  = ' SELECT * FROM estimasi
                          INNER JOIN customer ON customer.id_customer = estimasi.id_customer
                          INNER JOIN jenis_kendaran ON jenis_kendaran.id_jenis = estimasi.id_jenis
                          INNER JOIN user ON user.id_user = estimasi.id_user
                          INNER JOIN summary ON summary.id_estimasi_summary = estimasi.id_estimasi
                          WHERE estimasi.status_produksi = "9" AND estimasi.tgl_penyerahan IS NULL
                          ORDER BY estimasi.status_produksi';  

      $queryCountJasa =  'SELECT *, COUNT(*) as countJasa FROM detail_estimasi 
                          LEFT JOIN item on item.id_item = detail_estimasi.id_item
                          LEFT JOIN estimasi on estimasi.id_estimasi = detail_estimasi.id_estimasi
                          WHERE item.tipe_item = "1" AND estimasi.status_inout = "1"
                          GROUP BY detail_estimasi.id_estimasi ORDER BY detail_estimasi.id_estimasi ASC';

      $queryTimTeknisi = 'SELECT * FROM estimasi 
                          LEFT JOIN user ON user.id_user = estimasi.tim_teknisi
                         ';
      
      $listEstimasi = array();
      $dataEstimasi = $this->Crud->q($queryEstimasi);
      $countJasa    = $this->Crud->q($queryCountJasa);
      $dataTimTeknisi = $this->Crud->q($queryTimTeknisi);

      foreach($dataEstimasi as $datas){
          $estimasi = new stdClass();
          $estimasi->id_estimasi            = $datas->id_estimasi;
          $estimasi->nomor_wo               = $datas->nomor_wo;
          $estimasi->no_polisi              = $datas->no_polisi;
          $estimasi->nama_lengkap           = $datas->nama_lengkap;
          $estimasi->nama_lengkap_user      = $datas->nama_lengkap_user;
          $estimasi->tgl_masuk              = $datas->tgl_masuk;
          $estimasi->tgl_janji_penyerahan   = $datas->tgl_janji_penyerahan;
          $estimasi->final_inspection_out   = $datas->final_inspection_out;
          $estimasi->kategori_jasa          = $datas->kategori_jasa;
          
          foreach($dataTimTeknisi as $teknisi){
              if($datas->tim_teknisi == $teknisi->tim_teknisi){
                  $estimasi->nama_teknisi = $teknisi->nama_lengkap_user;
              }
          }
          array_push($listEstimasi, $estimasi);

      }                                     

      $data            = array ('title'            => 'Monitoring Produksi | U-Care',
                                'isi'              => 'admin/dashboard/ptm/readyToDelivery',
                                'dataEstimasi'     => $listEstimasi,
                                'dataScript'       => 'admin/dataScript/tabel-script',
                              );
      $this->load->view('admin/_layout/wrapper', $data);
    }

    //05 Unit Finish
    public function unitFinish(){

      $queryEstimasi  = ' SELECT * FROM estimasi
                           INNER JOIN customer ON customer.id_customer = estimasi.id_customer
                           INNER JOIN jenis_kendaran ON jenis_kendaran.id_jenis = estimasi.id_jenis
                           INNER JOIN user ON user.id_user = estimasi.id_user
                           INNER JOIN summary ON summary.id_estimasi_summary = estimasi.id_estimasi
                           WHERE estimasi.tgl_penyerahan IS NOT NULL
                           ORDER BY estimasi.tgl_penyerahan';

      $queryCountJasa =  'SELECT *, COUNT(*) as countJasa FROM detail_estimasi 
                          LEFT JOIN item on item.id_item = detail_estimasi.id_item
                          LEFT JOIN estimasi on estimasi.id_estimasi = detail_estimasi.id_estimasi
                          WHERE item.tipe_item = "1" AND estimasi.status_inout = "1"
                          GROUP BY detail_estimasi.id_estimasi ORDER BY detail_estimasi.id_estimasi ASC';

      $queryTimTeknisi= 'SELECT * FROM estimasi 
                         LEFT JOIN user ON user.id_user = estimasi.tim_teknisi';
      
      $listEstimasi = array();
      $dataEstimasi = $this->Crud->q($queryEstimasi);
      $countJasa    = $this->Crud->q($queryCountJasa);
      $dataTimTeknisi = $this->Crud->q($queryTimTeknisi);
      foreach($dataEstimasi as $datas){
          $estimasi = new stdClass();
          $estimasi->id_estimasi            = $datas->id_estimasi;
          $estimasi->nomor_wo               = $datas->nomor_wo;
          $estimasi->no_polisi              = $datas->no_polisi;
          $estimasi->nama_lengkap           = $datas->nama_lengkap;
          $estimasi->nama_lengkap_user      = $datas->nama_lengkap_user;
          $estimasi->tgl_masuk              = $datas->tgl_masuk;
          $estimasi->tgl_janji_penyerahan   = $datas->tgl_janji_penyerahan;
          $estimasi->final_inspection_out   = $datas->final_inspection_out;
          $estimasi->tgl_penyerahan         = $datas->tgl_penyerahan;
          $estimasi->kategori_jasa          = $datas->kategori_jasa;
          $estimasi->status_produksi        = $datas->status_produksi;
          
          foreach($dataTimTeknisi as $teknisi){
              if($datas->tim_teknisi == $teknisi->tim_teknisi){
                  $estimasi->nama_teknisi = $teknisi->nama_lengkap_user;
              }
          }
          array_push($listEstimasi, $estimasi);

      }

      $data           = array ('title'          => 'Monitoring Produksi | U-Care',
                              'isi'             => 'admin/dashboard/ptm/unitFinish',
                              'dataEstimasi'    => $listEstimasi,
                              'dataScript'      => 'admin/dataScript/tabel-script',
                              );
      $this->load->view('admin/_layout/wrapper', $data);
    }

    //06 Profile
    //For User Detail And Update
    public function profile(){
        $data = array(
                        'title'             => 'Partsman | U-Care',
                        'isi'               => 'admin/form/formPtmProfile',
                        'dataScript'        => 'admin/dataScript/beranda-script'
                      );
        $this->load->view('admin/_layout/wrapper', $data);
    }

    //06 Profile
    public function userUpdateAction(){
        $input = $this->input->post(NULL, TRUE); //get all post with xss filter

        //deteck with password or not
        if($input['new_pass']==NULL){
            $items = array(//save all post in array
                'nama_lengkap_user'   => $input['nama_lengkap'],
                'no_tlpUser'          => $input['no_tlpUser'],
                'username'            => $input['username'],
            );
        }else{
            $items = array(//save all post in array
                'nama_lengkap_user'   => $input['nama_lengkap'],
                'no_tlpUser'          => $input['no_tlpUser'],
                'username'            => $input['username'],
                'password'            => md5($input['new_pass']),
            );
        }

        $where = array(//get id for models Crud params w
            "id_user" => $this->session->userdata('id_user'),
        );

        //update session
        $sess = array(
            "nama_user"   => $input['nama_lengkap'],
            "username"    => $input['username'],
            "no_telp"     => $input['no_tlpUser'],
        );
        $this->session->set_userdata($sess);

        $this->Crud->u('user', $items, $where ); //save in database
        $this->session->set_flashdata('update_sukses', 'Update Berhasil !'); //for notif it is succes

        redirect('ptm/profile');
    }

}
