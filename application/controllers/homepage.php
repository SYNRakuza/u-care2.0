<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Homepage extends CI_Controller {
	/*
	| -------------------------------------------------------------------
	| HOMEPAGE CONTROLLER
	| -------------------------------------------------------------------
	| USAHAKAN BERIKAN KOMENTAR UNTUK SETIAP FUNGSI AGAR BISA DI MENGERTI
	| DENGAN CEPAT (^_^)9
	|
	| MAIN INFORMATION :
	| $data 		=> digunakan untuk membungkus informasi - informasi terkait page tertentu
	| dataScript 	=> digunakan untuk memasukkan file javascript yang hanya digunakan pada
	| 				halaman tertentu , (untuk optimasi) di view/nama_folder/dataScript
	*/
    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $data = array( 'title'     => 'U-Care' );
        $this->load->view('homepage/index', $data);
    }

    public function search(){
    	$nomor_wo = $this->input->post('no_wo');
    	$queryEstimasi  = ' SELECT * FROM estimasi
                            LEFT JOIN customer ON customer.id_customer = estimasi.id_customer
                            LEFT JOIN jenis_kendaran ON jenis_kendaran.id_jenis = estimasi.id_jenis
                            LEFT JOIN summary ON summary.id_estimasi_summary = estimasi.id_estimasi
                            LEFT JOIN user ON user.id_user = estimasi.id_user
                            WHERE estimasi.nomor_wo = "'.$nomor_wo.'"';
        $queryTimTeknisi = 'SELECT * FROM estimasi 
                            LEFT JOIN user ON user.id_user = estimasi.tim_teknisi
                           '; 
        $listEstimasi = $this->Crud->q($queryEstimasi);
        $dataTimTeknisi = $this->Crud->q($queryTimTeknisi);
		$dataEstimasi = array();                       
        foreach($listEstimasi as $datas){
        	$estimasi = new stdClass();
            $estimasi->id_estimasi  = $datas->id_estimasi;
            $estimasi->nomor_wo     = $datas->nomor_wo;
            $estimasi->no_polisi    = $datas->no_polisi;
            $estimasi->nama_lengkap = $datas->nama_lengkap;
            $estimasi->nama_sa      = $datas->nama_lengkap_user;
            $estimasi->no_telp      = $datas->no_tlpUser;
            $estimasi->tgl_masuk    = date('d M y', strtotime($datas->tgl_masuk));
            $estimasi->tgl_janji_penyerahan = date('d M y', strtotime($datas->tgl_janji_penyerahan));
            if($datas->jenis_estimasi == "2"){
                if($datas->status_produksi == "1"){
                    $estimasi->status_produksi = "Body Repair";
                }elseif($datas->status_produksi == "2"){
                    $estimasi->status_produksi = "Preparation";
                }elseif($datas->status_produksi == "3"){
                    $estimasi->status_produksi = "Masking";
                }elseif($datas->status_produksi == "4"){
                    $estimasi->status_produksi = "Painting";
                }elseif($datas->status_produksi == "5"){
                    $estimasi->status_produksi = "Polishing";
                }elseif($datas->status_produksi == "6"){
                    $estimasi->status_produksi = "Re Assembling";
                }elseif($datas->status_produksi == "7"){
                    $estimasi->status_produksi = "Washing";
                }elseif($datas->status_produksi == "8"){
                    $estimasi->status_produksi = "Final Inspection";   
                }elseif($datas->status_produksi == "9"){
                    $estimasi->status_produksi = "Done";   
                }else{
                    $estimasi->status_produksi = "None";
                }

                if($datas->done_order == "2"){
                    $estimasi->status_parts = "Complete";
                }else{
                    $estimasi->status_parts = "Waiting Parts";
                }
            }elseif($datas->jenis_estimasi == "1"){
                $estimasi->status_produksi = "None";
                if($datas->done_order == "2"){
                    $estimasi->status_parts = "Complete";
                }else{
                    $estimasi->status_parts = "Waiting Parts";
                }
            }else{
                if($datas->status_produksi == "1"){
                    $estimasi->status_produksi = "Body Repair";
                }elseif($datas->status_produksi == "2"){
                    $estimasi->status_produksi = "Preparation";
                }elseif($datas->status_produksi == "3"){
                    $estimasi->status_produksi = "Masking";
                }elseif($datas->status_produksi == "4"){
                    $estimasi->status_produksi = "Painting";
                }elseif($datas->status_produksi == "5"){
                    $estimasi->status_produksi = "Polishing";
                }elseif($datas->status_produksi == "6"){
                    $estimasi->status_produksi = "Re Assembling";
                }elseif($datas->status_produksi == "7"){
                    $estimasi->status_produksi = "Washing";
                }elseif($datas->status_produksi == "8"){
                    $estimasi->status_produksi = "Final Inspection";   
                }elseif($datas->status_produksi == "9"){
                    $estimasi->status_produksi = "Done";   
                }else{
                    $estimasi->status_produksi = "None";
                }

                $estimasi->status_parts = "None";

            }
            
            foreach($dataTimTeknisi as $teknisi){
                if($datas->tim_teknisi == $teknisi->tim_teknisi){
                    $estimasi->nama_teknisi = $teknisi->nama_lengkap_user;
                }
            }
            array_push($dataEstimasi, $estimasi);
        }
    	
    	$result = array('dataEstimasi' => $dataEstimasi);
    	echo json_encode($result);
    }

    public function detail($id_estimasi = null){
        $queryEstimasi  = ' SELECT * FROM estimasi
                            LEFT JOIN customer ON customer.id_customer = estimasi.id_customer
                            LEFT JOIN jenis_kendaran ON jenis_kendaran.id_jenis = estimasi.id_jenis
                            LEFT JOIN user ON user.id_user = estimasi.id_user
                            LEFT JOIN color ON color.id_color = estimasi.id_color
                            WHERE estimasi.id_estimasi = "'.$id_estimasi.'"';
        $queryJasa      = ' SELECT * FROM summary
                            LEFT JOIN estimasi ON estimasi.id_estimasi = summary.id_estimasi_summary
                            WHERE  summary.id_estimasi_summary = "'.$id_estimasi.'"';
        $queryTeknisi   = ' SELECT * FROM estimasi
                            LEFT JOIN user ON user.id_user = estimasi.tim_teknisi
                            WHERE estimasi.id_estimasi = "'.$id_estimasi.'"';

        $content        = 'homepage/detailTracking';

        $data           = array('title'           => 'Detail Tracking | U-Care',
                                'isi'             => $content,
                                'dataJasa'        => $this->Crud->q($queryJasa),
                                'dataEstimasi'    => $this->Crud->q($queryEstimasi),
                                'dataTeknisi'     => $this->Crud->q($queryTeknisi),
                                'dataScript'      => 'admin/dataScript/tabel-script' );
        $this->load->view('admin/_layout/wrapperv2', $data);
    }
}