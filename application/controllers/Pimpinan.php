<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pimpinan extends CI_Controller {

    public function __construct() {

        parent::__construct();
        date_default_timezone_set('Asia/Makassar');
        ini_set('max_execution_time', 0); 
        ini_set('memory_limit','2048M');
        if($this->session->userdata('status') != "login"){
            redirect('login');
        }
        if($this->session->userdata('level') != "pimpinan"){
            $level = $this->session->userdata('level');
            $controller = preg_replace("/[^a-z]/", "_", $level);
            redirect($controller);
        }
    }

    public function index() {
      $this->service_advisor();
    }

    public function ptm(){
        $getLevel   = array('level' => 'teknisi');               
        $getTeknisi = $this->Crud->gw('user', $getLevel);
        $countTask  = array();
        foreach ($getTeknisi as $datas) {
          $teknisi = new stdClass();
          $teknisi->id_teknisi          = $datas->id_user;
          $teknisi->nama_lengkap_user   = $datas->nama_lengkap_user;
          $queryCount  ='SELECT * ,
                          sum(e.kategori_jasa = "0" and e.tgl_masuk IS NOT NULL and e.status_produksi = "1") countLight,
                          sum(e.kategori_jasa = "1" and e.tgl_masuk IS NOT NULL and e.status_produksi = "1") countMedium,
                          sum(e.kategori_jasa = "2" and e.tgl_masuk IS NOT NULL and e.status_produksi = "1") countHeavy,

                          sum(e.kategori_jasa = "0" and e.tgl_masuk IS NOT NULL and e.status_produksi = "2") countLight2,
                          sum(e.kategori_jasa = "1" and e.tgl_masuk IS NOT NULL and e.status_produksi = "2") countMedium2,
                          sum(e.kategori_jasa = "2" and e.tgl_masuk IS NOT NULL and e.status_produksi = "2") countHeavy2,

                          sum(e.kategori_jasa = "0" and e.tgl_masuk IS NOT NULL and e.status_produksi = "3") countLight3,
                          sum(e.kategori_jasa = "1" and e.tgl_masuk IS NOT NULL and e.status_produksi = "3") countMedium3,
                          sum(e.kategori_jasa = "2" and e.tgl_masuk IS NOT NULL and e.status_produksi = "3") countHeavy3, 

                          sum(e.kategori_jasa = "0" and e.tgl_masuk IS NOT NULL and e.status_produksi = "4") countLight4,
                          sum(e.kategori_jasa = "1" and e.tgl_masuk IS NOT NULL and e.status_produksi = "4") countMedium4,
                          sum(e.kategori_jasa = "2" and e.tgl_masuk IS NOT NULL and e.status_produksi = "4") countHeavy4,      

                          sum(e.kategori_jasa = "0" and e.tgl_masuk IS NOT NULL and e.status_produksi = "5") countLight5,
                          sum(e.kategori_jasa = "1" and e.tgl_masuk IS NOT NULL and e.status_produksi = "5") countMedium5,
                          sum(e.kategori_jasa = "2" and e.tgl_masuk IS NOT NULL and e.status_produksi = "5") countHeavy5,

                          sum(e.kategori_jasa = "0" and e.tgl_masuk IS NOT NULL and e.status_produksi = "6") countLight6,
                          sum(e.kategori_jasa = "1" and e.tgl_masuk IS NOT NULL and e.status_produksi = "6") countMedium6,
                          sum(e.kategori_jasa = "2" and e.tgl_masuk IS NOT NULL and e.status_produksi = "6") countHeavy6,    

                          sum(e.kategori_jasa = "0" and e.tgl_masuk IS NOT NULL and e.status_produksi = "7") countLight7,
                          sum(e.kategori_jasa = "1" and e.tgl_masuk IS NOT NULL and e.status_produksi = "7") countMedium7,
                          sum(e.kategori_jasa = "2" and e.tgl_masuk IS NOT NULL and e.status_produksi = "7") countHeavy7,  

                          sum(e.kategori_jasa = "0" and e.tgl_masuk IS NOT NULL and e.status_produksi = "8") countLight8,
                          sum(e.kategori_jasa = "1" and e.tgl_masuk IS NOT NULL and e.status_produksi = "8") countMedium8,
                          sum(e.kategori_jasa = "2" and e.tgl_masuk IS NOT NULL and e.status_produksi = "8") countHeavy8,  

                          sum(e.kategori_jasa = "0" and e.tgl_masuk IS NOT NULL and e.status_produksi != "9") countLightT,
                          sum(e.kategori_jasa = "1" and e.tgl_masuk IS NOT NULL and e.status_produksi != "9") countMediumT,
                          sum(e.kategori_jasa = "2" and e.tgl_masuk IS NOT NULL and e.status_produksi != "9") countHeavyT  
                          -- count(e.kategori_jasa) total
                          FROM estimasi e 
                          LEFT JOIN user u ON u.id_user = e.tim_teknisi
                          WHERE e.tgl_masuk IS NOT NULL AND e.status_produksi > "0" AND e.status_produksi != "9" AND is_confirm_teknisi IS NOT NULL AND e.tim_teknisi = "'.$datas->id_user.'"
                          ';
              $getCount = $this->Crud->q($queryCount);
              foreach ($getCount as $count) {
                $teknisi->bodyPaintLight        = $count->countLight;
                $teknisi->bodyPaintMedium       = $count->countMedium;
                $teknisi->bodyPaintHeavy        = $count->countHeavy;
                $teknisi->preparationLight      = $count->countLight2;
                $teknisi->preparationMedium     = $count->countMedium2;
                $teknisi->preparationHeavy      = $count->countHeavy2;
                $teknisi->maskingLight          = $count->countLight3;
                $teknisi->maskingMedium         = $count->countMedium3;
                $teknisi->maskingHeavy          = $count->countHeavy3;
                $teknisi->paintingLight         = $count->countLight4;
                $teknisi->paintingMedium        = $count->countMedium4;
                $teknisi->paintingHeavy         = $count->countHeavy4;
                $teknisi->polishingLight        = $count->countLight5;
                $teknisi->polishingMedium       = $count->countMedium5;
                $teknisi->polishingHeavy        = $count->countHeavy5;
                $teknisi->reassemblingLight     = $count->countLight6;
                $teknisi->reassemblingMedium    = $count->countMedium6;
                $teknisi->reassemblingHeavy     = $count->countHeavy6;
                $teknisi->washingLight          = $count->countLight7;
                $teknisi->washingMedium         = $count->countMedium7;
                $teknisi->washingHeavy          = $count->countHeavy7;
                $teknisi->finalInspectionLight  = $count->countLight8;
                $teknisi->finalInspectionMedium = $count->countMedium8;
                $teknisi->finalInspectionHeavy  = $count->countHeavy8;
                $teknisi->totalLight            = $count->countLightT;
                $teknisi->totalMedium           = $count->countMediumT;
                $teknisi->totalHeavy            = $count->countHeavyT;
              }
            array_push($countTask, $teknisi);
        }



         $data = array(  'title'     => 'Daftar Estimasi Order | U-Care',
                        'isi'       => 'admin/dashboard/pimpinan/beranda',
                        'hitung'            => $countTask,
                        'dataScript'=> 'admin/dataScript/beranda-script-pimpinan' );
                $this->load->view('admin/_layout/wrapper', $data);
    }

    public function service_advisor(){
         $data = array(  'title'     => 'Daftar Estimasi Order | U-Care',
                        'isi'       => 'admin/dashboard/pimpinan/beranda',
                        'dataScript'=> 'admin/dataScript/beranda-script-pimpinan' );
                $this->load->view('admin/_layout/wrapper', $data);
    }

    public function partsman(){
         $data = array(  'title'     => 'Daftar Estimasi Order | U-Care',
                        'isi'       => 'admin/dashboard/pimpinan/beranda',
                        'dataScript'=> 'admin/dataScript/beranda-script-pimpinan' );
                $this->load->view('admin/_layout/wrapper', $data);
    }

    public function teknisi(){
        $where = array('level'  => 'teknisi');
         $data = array(  'title'     => 'Daftar Estimasi Order | U-Care',
                        'isi'       => 'admin/dashboard/pimpinan/beranda',
                        'dataTeknisi'   => $this->Crud->gw('user', $where),
                        'dataScript'=> 'admin/dataScript/beranda-script-pimpinan' );
                $this->load->view('admin/_layout/wrapper', $data);
    }

    public function foreman(){
         $data = array(  'title'     => 'Daftar Estimasi Order | U-Care',
                        'isi'       => 'admin/dashboard/pimpinan/beranda',
                        'dataScript'=> 'admin/dataScript/beranda-script-pimpinan' );
                $this->load->view('admin/_layout/wrapper', $data);
    }

    public function workInProcess(){
          //PTM DASHBOARD (Work In Process)
        


         $data = array(  'title'     => 'Daftar Estimasi Order | U-Care',
                        'isi'       => 'admin/dashboard/pimpinan/beranda',
                        'hitung'            => $countTask,
                        'dataScript'=> 'admin/dataScript/beranda-script-pimpinan' );
                $this->load->view('admin/_layout/wrapper', $data);
    }

    public function listPartsDashboard(){
        $dateFrom = $this->input->post('dateFromParts');
        $dateTo = $this->input->post('dateToParts');
        $inOut = $this->input->post('inOutParts');
        $output = '';
        if($inOut == 'in'){
            $queryEstimasi = ' SELECT * FROM estimasi
                            LEFT JOIN customer ON customer.id_customer = estimasi.id_customer
                            LEFT JOIN user ON user.id_user = estimasi.id_user
                            LEFT JOIN jenis_kendaran ON jenis_kendaran.id_jenis = estimasi.id_jenis
                            WHERE (estimasi.tgl_estimasi >= "'.$dateFrom.'" AND estimasi.tgl_estimasi <= "'.$dateTo.'") AND estimasi.status_inout = "1" AND (estimasi.jenis_estimasi = "1" OR estimasi.jenis_estimasi = "2") AND estimasi.done_order = "2"
                           ';
        }elseif($inOut == 'out'){
            $queryEstimasi = ' SELECT * FROM estimasi
                            LEFT JOIN customer ON customer.id_customer = estimasi.id_customer
                            LEFT JOIN user ON user.id_user = estimasi.id_user
                            LEFT JOIN jenis_kendaran ON jenis_kendaran.id_jenis = estimasi.id_jenis
                            WHERE (estimasi.tgl_estimasi >= "'.$dateFrom.'" AND estimasi.tgl_estimasi <= "'.$dateTo.'") AND estimasi.status_inout = "1" AND (estimasi.jenis_estimasi = "1" OR estimasi.jenis_estimasi = "2") AND (estimasi.done_order = "1" OR estimasi.done_order = "0")
                           ';
        }else{
            $queryEstimasi = ' SELECT * FROM estimasi
                            LEFT JOIN customer ON customer.id_customer = estimasi.id_customer
                            LEFT JOIN user ON user.id_user = estimasi.id_user
                            LEFT JOIN jenis_kendaran ON jenis_kendaran.id_jenis = estimasi.id_jenis
                            WHERE (estimasi.tgl_estimasi >= "'.$dateFrom.'" AND estimasi.tgl_estimasi <= "'.$dateTo.'") AND estimasi.status_inout = "1" AND (estimasi.jenis_estimasi = "1" OR estimasi.jenis_estimasi = "2") AND (estimasi.done_order = "1" OR estimasi.done_order = "0" OR estimasi.done_order="2")
                           ';
        }
        $dataEstimasi = $this->Crud->q($queryEstimasi);
        $no = 1;
        foreach ($dataEstimasi as $datas) {
            $tgl = date('d-M-Y', strtotime($datas->tgl_estimasi));
            if($datas->tgl_janji_penyerahan == NULL || $datas->tgl_janji_penyerahan == "0000-00-00"){
                $tgl_janji = "-";
            }else{
                $tgl_janji = date('d-M-Y', strtotime($datas->tgl_janji_penyerahan));
            }
            $output .= '
                        <tr>
                            <td style="text-align: center;vertical-align:middle;">'.$no.'</td>
                            <td style="text-align: center;vertical-align:middle;">'.$datas->nomor_wo.'</td>
                            <td style="text-align: center;vertical-align:middle;">'.$datas->no_polisi.'</td>
                            <td style="text-align: center;vertical-align:middle;">'.$datas->nama_lengkap.'</td>
                            <td style="text-align: center;vertical-align:middle;">'.$datas->nama_lengkap_user.'</td>
                            <td style="text-align: center;vertical-align:middle;">'.$tgl.'</td>
                            <td style="text-align: center;vertical-align:middle;">'.$tgl_janji.'</td>
                            <td style="text-align: center;vertical-align:middle;">'.($datas->jenis_customer == "0" ? 'Asuransi - '.$datas->nama_asuransi.'' : 'Tunai').'</td>
                        </tr>
            ';
            $no++;
        }
        $data = array('listEstimasi' => $output);

        echo json_encode($data);
    }

    public function listEstimasiDashboard(){

        $dateFrom = $this->input->post('dateFrom');
        $dateTo = $this->input->post('dateTo');
        $inOut = $this->input->post('inOut');
        $id_user = $this->session->userdata('id_user');
        $output = '';
        if($inOut == 'in'){
            $queryEstimasi = ' SELECT * FROM estimasi
                            LEFT JOIN customer ON customer.id_customer = estimasi.id_customer
                            LEFT JOIN jenis_kendaran ON jenis_kendaran.id_jenis = estimasi.id_jenis
                            LEFT JOIN user ON user.id_user = estimasi.id_user
                            WHERE estimasi.tgl_masuk >= "'.$dateFrom.'" AND estimasi.tgl_masuk <= "'.$dateTo.'" AND estimasi.status_inout = "1"
                           ';    
        }elseif($inOut == 'out'){
            $queryEstimasi = ' SELECT * FROM estimasi
                            LEFT JOIN customer ON customer.id_customer = estimasi.id_customer
                            LEFT JOIN jenis_kendaran ON jenis_kendaran.id_jenis = estimasi.id_jenis
                            LEFT JOIN user ON user.id_user = estimasi.id_user
                            WHERE estimasi.tgl_estimasi >= "'.$dateFrom.'" AND estimasi.tgl_estimasi <= "'.$dateTo.'" AND estimasi.status_inout = "0"
                           ';
        }else{
            $queryEstimasi = ' SELECT * FROM estimasi
                            LEFT JOIN customer ON customer.id_customer = estimasi.id_customer
                            LEFT JOIN jenis_kendaran ON jenis_kendaran.id_jenis = estimasi.id_jenis
                            LEFT JOIN user ON user.id_user = estimasi.id_user
                            WHERE estimasi.tgl_estimasi >= "'.$dateFrom.'" AND estimasi.tgl_estimasi <= "'.$dateTo.'" 
                           ';
        }
        $dataEstimasi = $this->Crud->q($queryEstimasi);
        $no = 1;
        foreach ($dataEstimasi as $datas) {

            if($datas->nomor_wo == NULL){
                $wo = "-";
            }else{
                $wo = $datas->nomor_wo;
            }

            if($datas->tgl_estimasi == NULL){
                $tgl_estimasi = "-";
            }else{
                $tgl_estimasi = date('d-M-Y', strtotime($datas->tgl_estimasi));
            }

            if($datas->tgl_masuk == NULL){
                $tgl_masuk = "-";
            }else{
                $tgl_masuk = date('d-M-Y', strtotime($datas->tgl_masuk));
            }

            if($datas->tgl_janji_penyerahan == NULL){
                $tgl_janji_penyerahan = "-";
            }else{
                $tgl_janji_penyerahan = date('d-M-Y', strtotime($datas->tgl_janji_penyerahan));
            }
            
            
            

            $output .= '
                        <tr>
                            <td style="text-align: center;vertical-align: middle;">'.$no++.'</td>
                            <td style="text-align: center;vertical-align: middle;">'.$wo.'</td>
                            <td style="text-align: center;vertical-align: middle;">'.$datas->no_polisi.'</td>
                            <td style="text-align: center;vertical-align: middle;">'.$datas->nama_lengkap.'</td>
                            <td style="text-align: center;vertical-align: middle;">'.$datas->nama_lengkap_user.'</td>
                            <td style="text-align: center;vertical-align: middle;">'.$tgl_estimasi.'</td>
                            <td style="text-align: center;vertical-align: middle;">'.$tgl_masuk.'</td>
                            <td style="text-align: center;vertical-align: middle;">'.$tgl_janji_penyerahan.'</td>
                            <td style="text-align: center;vertical-align: middle;">'.($datas->jenis_customer == "0" ? 'Asuransi - '.$datas->nama_asuransi.'' : 'Tunai').'</td>
                        </tr>
            ';
        }
        $data = array('listEstimasi' => $output);

        echo json_encode($data);
    }

    public function convertTimeLead($n){
        $hours = floor($n / 60);
        $minutes = $n % 60;
        return $hours." Hours ".$minutes." Minutes ";  
    }

    public function loadSummary(){
      error_reporting(0);
        $dateFrom = $this->input->post('dateFrom');
        $dateTo = $this->input->post('dateTo');
        $output = '';
        $querySummary = ' SELECT * FROM summary_lead
                            LEFT JOIN estimasi ON estimasi.id_estimasi = summary_lead.id_estimasi_lead
                            LEFT JOIN summary ON summary.id_estimasi_summary = estimasi.id_estimasi
                            LEFT JOIN user ON user.id_user = estimasi.tim_teknisi
                            WHERE estimasi.tgl_estimasi >= "'.$dateFrom.'" AND estimasi.tgl_estimasi <= "'.$dateTo.'" 
                           ';
        $dataSummary = $this->Crud->q($querySummary);

        foreach ($dataSummary as $datas) {

            if($datas->nomor_wo == NULL){
                $wo = "-";
            }else{
                $wo = $datas->nomor_wo;
            }
            if($datas->body_repair_lead == NULL){
                $body_repair_lead = "-";
            }else{
                $body_repair_lead = $this->convertTimeLead($datas->body_repair_lead); 
            }
            if($datas->preparation_lead == NULL){
                $preparation_lead = "-";
            }else{
                $preparation_lead = $this->convertTimeLead($datas->preparation_lead); 
            }
            if($datas->masking_lead == NULL){
                $masking_lead = "-";
            }else{
                $masking_lead = $this->convertTimeLead($datas->masking_lead); 
            }
            if($datas->painting_lead == NULL){
                $painting_lead = "-";
            }else{
                $painting_lead = $this->convertTimeLead($datas->painting_lead); 
            }
            if($datas->polishing_lead == NULL){
                $polishing_lead = "-";
            }else{
                $polishing_lead = $this->convertTimeLead($datas->polishing_lead); 
            }
            if($datas->re_assembling_lead == NULL){
                $re_assembling_lead = "-";
            }else{
                $re_assembling_lead = $this->convertTimeLead($datas->re_assembling_lead); 
            }
            if($datas->washing_lead == NULL){
                $washing_lead = "-";
            }else{
                $washing_lead = $this->convertTimeLead($datas->washing_lead); 
            }
            if($datas->final_inspection_lead == NULL){
                $final_inspection_lead = "-";
            }else{
                $final_inspection_lead = $this->convertTimeLead($datas->final_inspection_lead); 
            }
            if($datas->total_lead == NULL){
                $total_lead = "-";
            }else{
                $total_lead = $this->convertTimeLead($datas->total_lead); 
            }
            
            $tombolHistory = '
              <a style="margin-right: 5px;" type="button" class="lead_record btn bg-orange btn-xs waves-effect" href="'.base_url("pimpinan/historyLead/".$datas->id_lead).'"><i class="material-icons">history</i></a>
          ';
            

            $output .= '
                        <tr>
                            <td style="text-align: center;vertical-align: middle;">'.$wo.'</td>
                            <td style="text-align: center;vertical-align: middle;">'.$datas->no_polisi.'</td>
                            <td style="text-align: center;vertical-align: middle;">'.$datas->nama_lengkap_user.'</td>
                            <td style="text-align: center;vertical-align: middle;">'.$body_repair_lead.'</td>
                            <td style="text-align: center;vertical-align: middle;">'.$preparation_lead.'</td>
                            <td style="text-align: center;vertical-align: middle;">'.$masking_lead.'</td>
                            <td style="text-align: center;vertical-align: middle;">'.$painting_lead.'</td>
                            <td style="text-align: center;vertical-align: middle;">'.$polishing_lead.'</td>
                            <td style="text-align: center;vertical-align: middle;">'.$re_assembling_lead.'</td>
                            <td style="text-align: center;vertical-align: middle;">'.$washing_lead.'</td>
                            <td style="text-align: center;vertical-align: middle;">'.$final_inspection_lead.'</td>
                            <td style="text-align: center;vertical-align: middle;">'.$total_lead.'</td>
                            <td style="text-align: center;vertical-align: middle;">'.$tombolHistory.'</td>
                        </tr>
            ';
        }

        $data = array('listSummary'    => $output,
                    );

        echo json_encode($data);

    }

    public function loadJasa(){
      error_reporting(0);
      $jenis_kendaraan = $this->input->post('jenis_kendaraan');
      $output = '';
      $query = "
            SELECT * FROM item
            LEFT JOIN jenis_kendaran ON jenis_kendaran.id_jenis = item.id_jenis
            WHERE item.tipe_item = '1' AND item.id_jenis = '".$jenis_kendaraan."'
        ";

        $dataJasa = $this->Crud->q($query);
        $posisi = '';
        $no = 1;
        $harga = 0;

        $head .= '<thead>
                    <tr>
                        <th style="text-align: center;vertical-align: middle;">No</th>
                        <th style="text-align: center;vertical-align: middle;">Nama Jasa</th>
                        <th style="text-align: center;vertical-align: middle;">Harga Jasa</th>
                        <th style="text-align: center;vertical-align: middle;">Posisi Jasa</th>
                        <th style="text-align:center;vertical-align: middle;">Jenis Mobil</th>
                        <th style="text-align: center;vertical-align: middle;">Action</th>
                    </tr>
                  </thead>
                  <tbody>
          ';

        foreach($dataJasa as $datas){
          if($datas->posisi_jasa == 1){
            $posisi = 'Outer Panel';
          }else if($datas->posisi_jasa == 2){
            $posisi = 'Inner Panel';
          }else if($datas->posisi_jasa == 3){
            $posisi = 'Extention/Cover';
          }else if($datas->posisi_jasa == 4){
            $posisi = 'Glasses & Optional';
          }else if($datas->posisi_jasa == 5){
            $posisi = 'Special';
          }else{
            $posisi = 'Spot Repair';
          }

          $harga = "Rp " . number_format($datas->harga_item,0,',','.');

          $tombolEdit = '
              <a style="margin-right: 5px;" href="'.base_url("pimpinan/jasaUpdate/".$datas->id_item).'" class="btn bg-orange waves-effect btn-xs"><i class="material-icons">mode_edit</i></a>
          ';

          $tombolHapus = '
              <a style="margin-right: 5px;" href="'.base_url("pimpinan/jasaHapus/".$datas->id_item).'" class="btn btn-danger waves-effect btn-xs"><i class="material-icons">delete_forever</i></a>
          ';

           $output .= '
                        <tr>
                            <td style="text-align: center;vertical-align: middle;">'.$no++.'</td>
                            <td style="text-align: center;vertical-align: middle;">'.$datas->nama_item.'</td>
                            <td style="text-align: center;vertical-align: middle;">'.$harga.'</td>
                            <td style="text-align: center;vertical-align: middle;">'.$posisi.'</td>
                            <td style="text-align: center;vertical-align: middle;">'.$datas->nama_jenis.'</td>
                            <td style="text-align: center;vertical-align: middle;">'.$tombolEdit.' '.$tombolHapus.'</td>
                        </tr>
            ';
        }

        $footer .= '
                    </tbody>
            ';

        $data = array('headJasa'    => $head,
                      'listJasa'    => $output,
                      'footerJasa'  => $footer
                    );

        echo json_encode($data);

    }



    public function getDataChart(){
        $year = $this->input->post('yearParts');
        //$id_user = $this->session->userdata('id_user');
        $id_user = 19;
        $done = 0;
        $all = 0;
        $yearNow = date('Y');
        $dataCountLight = array();
        $dataCountMedium = array();
        $dataCountHeavy = array();
        $dataPersenD = array();
        $dataPersenA = array();
        $dataPersen = array();

        if($year !== NULL ){
            $yearNow = $year;
            for($i = 0 ; $i < 13; $i++){
                $j = $i+1;
                $queryD = 'SELECT COUNT(*) as totDone FROM estimasi
                    WHERE status_inout = "1" AND done_order="2" AND YEAR(tgl_estimasi) = "'.$yearNow.'" AND MONTH(tgl_estimasi) = "'.$j.'" AND (jenis_estimasi = "1" OR jenis_estimasi = "2")
                ';
                $queryA = 'SELECT COUNT(*) as totAll FROM estimasi
                    WHERE status_inout = "1" AND (done_order="2" OR done_order="1" OR done_order="0") AND YEAR(tgl_estimasi) = "'.$yearNow.'" AND MONTH(tgl_estimasi) = "'.$j.'" AND (jenis_estimasi = "1" OR jenis_estimasi = "2")
                ';

                $tempD = $this->Crud->q($queryD);
                $tempA = $this->Crud->q($queryA);

                foreach ($tempD as $value) {
                    $dataPersenD[$i] = (int)$value->totDone;
                    $done = $dataPersenD[$i];
                }
                foreach ($tempA as $value) {
                    $dataPersenA[$i] = (int)$value->totAll;
                    $all = $dataPersenA[$i];
                }

                if($all == 0){
                    $dataPersen[] = 0;
                }else{
                    $dataPersen[] = ($done/$all)*100;
                }

            }
            $data = array (
                           'dataPersen'     => $dataPersen,
                        );

        }else{
            for($i = 0 ; $i < 13; $i++){
                $j = $i+1;
                $queryD = 'SELECT COUNT(*) as totDone FROM estimasi
                    WHERE status_inout = "1" AND done_order="2" AND YEAR(tgl_estimasi) = "'.$yearNow.'" AND MONTH(tgl_estimasi) = "'.$j.'" AND (jenis_estimasi = "1" OR jenis_estimasi = "2")
                ';
                $queryA = 'SELECT COUNT(*) as totAll FROM estimasi
                    WHERE status_inout = "1" AND (done_order="2" OR done_order="1" OR done_order="0") AND YEAR(tgl_estimasi) = "'.$yearNow.'" AND MONTH(tgl_estimasi) = "'.$j.'" AND (jenis_estimasi = "1" OR jenis_estimasi = "2")
                ';

                $tempD = $this->Crud->q($queryD);
                $tempA = $this->Crud->q($queryA);

                foreach ($tempD as $value) {
                    $dataPersenD[$i] = (int)$value->totDone;
                    $done = $dataPersenD[$i];
                }
                foreach ($tempA as $value) {
                    $dataPersenA[$i] = (int)$value->totAll;
                    $all = $dataPersenA[$i];
                }

                if($all == 0){
                    $dataPersen[] = 0;
                }else{
                    $dataPersen[] = ($done/$all)*100;
                }

            }
            $data = array (
                           'dataPersen'     => $dataPersen,
                        );
        }

        echo json_encode($data);
    }

    public function getDataChartTeknisi($id){
        $year = $this->input->post('yearTeknisi');
        $id_user = $id;
        //$id_user = 19;
        //$id_user = $this->input->post('id_teknisi');
        $yearNow = date('Y');

        
        $dataCountLight = array();
        $totLight = 0;
        $wipLight = array();
        $totWipLight = 0;

        $dataCountMedium = array();
        $totMedium = 0;
        $wipMedium = array();
        $totWipMedium = 0;

        $dataCountHeavy = array();
        $totHeavy = 0;
        $wipHeavy = array();
        $totWipHeavy = 0;

        $persenLight = array();
        $persenMedium = array();
        $persenHeavy = array();
        
        if($year !== NULL ){
            $yearNow = $year;
            for($i = 0 ; $i < 13; $i++){
                $j = $i+1;
                $queryCountLight = 'SELECT COUNT(*) as totLight FROM estimasi
                                    WHERE kategori_jasa = "0" AND status_inout = "1" AND jenis_estimasi != "1" 
                                    AND status_produksi = "9" AND tim_teknisi = "'.$id_user.'"
                                    AND YEAR(tgl_masuk) = "'.$yearNow.'" AND MONTH(tgl_masuk) = "'.$j.'"';
                $queryWipLight =    'SELECT COUNT(*) as totWipLight FROM estimasi
                                    WHERE kategori_jasa = "0" AND status_inout = "1" AND jenis_estimasi != "1"
                                    AND status_produksi <= "9" AND status_produksi >= "0" AND tim_teknisi = "'.$id_user.'"
                                    AND YEAR(tgl_masuk) = "'.$yearNow.'" AND MONTH(tgl_masuk) = "'.$j.'"';

                $queryCountMedium = 'SELECT COUNT(*) as totMedium FROM estimasi
                                    WHERE kategori_jasa = "1" AND status_inout = "1" AND jenis_estimasi != "1" 
                                    AND status_produksi = "9" AND tim_teknisi = "'.$id_user.'"
                                    AND YEAR(tgl_masuk) = "'.$yearNow.'" AND MONTH(tgl_masuk) = "'.$j.'"';
                $queryWipMedium =   'SELECT COUNT(*) as totWipMedium FROM estimasi
                                    WHERE kategori_jasa = "1" AND status_inout = "1" AND jenis_estimasi != "1"
                                    AND status_produksi <= "9" AND status_produksi >= "0" AND tim_teknisi = "'.$id_user.'"
                                    AND YEAR(tgl_masuk) = "'.$yearNow.'" AND MONTH(tgl_masuk) = "'.$j.'"';

                $queryCountHeavy = 'SELECT COUNT(*) as totHeavy FROM estimasi
                                    WHERE kategori_jasa = "2" AND status_inout = "1" AND jenis_estimasi != "1" 
                                    AND status_produksi = "9" AND tim_teknisi = "'.$id_user.'"
                                    AND YEAR(tgl_masuk) = "'.$yearNow.'" AND MONTH(tgl_masuk) = "'.$j.'"';
                $queryWipHeavy =    'SELECT COUNT(*) as totWipHeavy FROM estimasi
                                    WHERE kategori_jasa = "2" AND status_inout = "1" AND jenis_estimasi != "1"
                                    AND status_produksi <= "9" AND status_produksi >= "0" AND tim_teknisi = "'.$id_user.'"
                                    AND YEAR(tgl_masuk) = "'.$yearNow.'" AND MONTH(tgl_masuk) = "'.$j.'"';
                
                $tempLight = $this->Crud->q($queryCountLight);
                $tempWipLight = $this->Crud->q($queryWipLight);

                $tempMedium = $this->Crud->q($queryCountMedium);
                $tempWipMedium = $this->Crud->q($queryWipMedium);

                $tempHeavy = $this->Crud->q($queryCountHeavy);
                $tempWipHeavy = $this->Crud->q($queryWipHeavy);
                
                foreach ($tempLight as $value) {
                    $dataCountLight[$i] = (int)$value->totLight; 
                    $totLight = $dataCountLight[$i];
                }
                foreach($tempWipLight as $value){
                    $wipLight[$i] = (int)$value->totWipLight;
                    $totWipLight = $wipLight[$i];
                }

                if($totWipLight == 0){
                    $persenLight[] = 0;
                }else{
                    $persenLight[] = ($totLight/$totWipLight)*100;
                }


                foreach ($tempMedium as $value) {
                    $dataCountMedium[$i] = (int)$value->totMedium;
                    $totMedium = $dataCountMedium[$i];
                }
                foreach($tempWipMedium as $value){
                    $wipMedium[$i] = (int)$value->totWipMedium;
                    $totWipMedium = $wipMedium[$i];
                }
                if($totWipMedium == 0){
                    $persenMedium[] = 0;
                }else{
                    $persenMedium[] = ($totMedium/$totWipMedium)*100;
                }


                foreach ($tempHeavy as $value) {
                    $dataCountHeavy[$i] = (int)$value->totHeavy; 
                    $totHeavy = $dataCountHeavy[$i];
                }

                foreach($tempWipHeavy as $value){
                    $wipHeavy[$i] = (int)$value->totWipHeavy;
                    $totWipHeavy = $wipHeavy[$i];
                }

                if($totWipHeavy == 0){
                    $persenHeavy[] = 0;
                }else{
                    $persenHeavy[] = ($totHeavy/$totWipHeavy)*100;
                }
                
                $data = array ('dataCountLight'  => $persenLight,
                               'dataCountMedium' => $persenMedium,
                               'dataCountHeavy'  => $persenHeavy,
                       
                               );
            }
        }else{
            for($i = 0 ; $i < 13; $i++){
                $j = $i+1;
                $queryCountLight = 'SELECT COUNT(*) as totLight FROM estimasi
                                    WHERE kategori_jasa = "0" AND status_inout = "1" AND jenis_estimasi != "1" 
                                    AND status_produksi = "9" AND tim_teknisi = "'.$id_user.'"
                                    AND YEAR(tgl_masuk) = "'.$yearNow.'" AND MONTH(tgl_masuk) = "'.$j.'"';
                $queryWipLight =    'SELECT COUNT(*) as totWipLight FROM estimasi
                                    WHERE kategori_jasa = "0" AND status_inout = "1" AND jenis_estimasi != "1"
                                    AND status_produksi <= "9" AND status_produksi >= "0" AND tim_teknisi = "'.$id_user.'"
                                    AND YEAR(tgl_masuk) = "'.$yearNow.'" AND MONTH(tgl_masuk) = "'.$j.'"';

                $queryCountMedium = 'SELECT COUNT(*) as totMedium FROM estimasi
                                    WHERE kategori_jasa = "1" AND status_inout = "1" AND jenis_estimasi != "1" 
                                    AND status_produksi = "9" AND tim_teknisi = "'.$id_user.'"
                                    AND YEAR(tgl_masuk) = "'.$yearNow.'" AND MONTH(tgl_masuk) = "'.$j.'"';
                $queryWipMedium =   'SELECT COUNT(*) as totWipMedium FROM estimasi
                                    WHERE kategori_jasa = "1" AND status_inout = "1" AND jenis_estimasi != "1"
                                    AND status_produksi <= "9" AND status_produksi >= "0" AND tim_teknisi = "'.$id_user.'"
                                    AND YEAR(tgl_masuk) = "'.$yearNow.'" AND MONTH(tgl_masuk) = "'.$j.'"';

                $queryCountHeavy = 'SELECT COUNT(*) as totHeavy FROM estimasi
                                    WHERE kategori_jasa = "2" AND status_inout = "1" AND jenis_estimasi != "1" 
                                    AND status_produksi = "9" AND tim_teknisi = "'.$id_user.'"
                                    AND YEAR(tgl_masuk) = "'.$yearNow.'" AND MONTH(tgl_masuk) = "'.$j.'"';
                $queryWipHeavy =    'SELECT COUNT(*) as totWipHeavy FROM estimasi
                                    WHERE kategori_jasa = "2" AND status_inout = "1" AND jenis_estimasi != "1"
                                    AND status_produksi <= "9" AND status_produksi >= "0" AND tim_teknisi = "'.$id_user.'"
                                    AND YEAR(tgl_masuk) = "'.$yearNow.'" AND MONTH(tgl_masuk) = "'.$j.'"';
                
                $tempLight = $this->Crud->q($queryCountLight);
                $tempWipLight = $this->Crud->q($queryWipLight);

                $tempMedium = $this->Crud->q($queryCountMedium);
                $tempWipMedium = $this->Crud->q($queryWipMedium);

                $tempHeavy = $this->Crud->q($queryCountHeavy);
                $tempWipHeavy = $this->Crud->q($queryWipHeavy);
                
                foreach ($tempLight as $value) {
                    $dataCountLight[$i] = (int)$value->totLight; 
                    $totLight = $dataCountLight[$i];
                }
                foreach($tempWipLight as $value){
                    $wipLight[$i] = (int)$value->totWipLight;
                    $totWipLight = $wipLight[$i];
                }

                if($totWipLight == 0){
                    $persenLight[] = 0;
                }else{
                    $persenLight[] = ($totLight/$totWipLight)*100;
                }


                foreach ($tempMedium as $value) {
                    $dataCountMedium[$i] = (int)$value->totMedium;
                    $totMedium = $dataCountMedium[$i];
                }
                foreach($tempWipMedium as $value){
                    $wipMedium[$i] = (int)$value->totWipMedium;
                    $totWipMedium = $wipMedium[$i];
                }
                if($totWipMedium == 0){
                    $persenMedium[] = 0;
                }else{
                    $persenMedium[] = ($totMedium/$totWipMedium)*100;
                }


                foreach ($tempHeavy as $value) {
                    $dataCountHeavy[$i] = (int)$value->totHeavy; 
                    $totHeavy = $dataCountHeavy[$i];
                }

                foreach($tempWipHeavy as $value){
                    $wipHeavy[$i] = (int)$value->totWipHeavy;
                    $totWipHeavy = $wipHeavy[$i];
                }

                if($totWipHeavy == 0){
                    $persenHeavy[] = 0;
                }else{
                    $persenHeavy[] = ($totHeavy/$totWipHeavy)*100;
                }
                
                $data = array ('dataCountLight'  => $persenLight,
                               'dataCountMedium' => $persenMedium,
                               'dataCountHeavy'  => $persenHeavy,
                       
                               );
            }
        }
        
        echo json_encode($data);
    }

    public function getDataChartForeman(){
        $year = $this->input->post('yearForeman');
        //$id_user = $this->session->userdata('id_user');
        $yearNow = date('Y');

        
        $dataCountLight = array();
        $totLight = 0;
        $wipLight = array();
        $totWipLight = 0;

        $dataCountMedium = array();
        $totMedium = 0;
        $wipMedium = array();
        $totWipMedium = 0;

        $dataCountHeavy = array();
        $totHeavy = 0;
        $wipHeavy = array();
        $totWipHeavy = 0;

        $persenLight = array();
        $persenMedium = array();
        $persenHeavy = array();
        
        if($year !== NULL ){
            $yearNow = $year;
            for($i = 0 ; $i < 13; $i++){
                $j = $i+1;
                $queryCountLight = 'SELECT COUNT(*) as totLight FROM estimasi
                                    WHERE kategori_jasa = "0" AND status_inout = "1" AND jenis_estimasi != "1" 
                                    AND status_produksi = "9"
                                    AND YEAR(tgl_masuk) = "'.$yearNow.'" AND MONTH(tgl_masuk) = "'.$j.'"';
                $queryWipLight =    'SELECT COUNT(*) as totWipLight FROM estimasi
                                    WHERE kategori_jasa = "0" AND status_inout = "1" AND jenis_estimasi != "1"
                                    AND status_produksi <= "9" AND status_produksi >= "0"
                                    AND YEAR(tgl_masuk) = "'.$yearNow.'" AND MONTH(tgl_masuk) = "'.$j.'"';

                $queryCountMedium = 'SELECT COUNT(*) as totMedium FROM estimasi
                                    WHERE kategori_jasa = "1" AND status_inout = "1" AND jenis_estimasi != "1" 
                                    AND status_produksi = "9"
                                    AND YEAR(tgl_masuk) = "'.$yearNow.'" AND MONTH(tgl_masuk) = "'.$j.'"';
                $queryWipMedium =   'SELECT COUNT(*) as totWipMedium FROM estimasi
                                    WHERE kategori_jasa = "1" AND status_inout = "1" AND jenis_estimasi != "1"
                                    AND status_produksi <= "9" AND status_produksi >= "0"
                                    AND YEAR(tgl_masuk) = "'.$yearNow.'" AND MONTH(tgl_masuk) = "'.$j.'"';

                $queryCountHeavy = 'SELECT COUNT(*) as totHeavy FROM estimasi
                                    WHERE kategori_jasa = "2" AND status_inout = "1" AND jenis_estimasi != "1" 
                                    AND status_produksi = "9"
                                    AND YEAR(tgl_masuk) = "'.$yearNow.'" AND MONTH(tgl_masuk) = "'.$j.'"';
                $queryWipHeavy =    'SELECT COUNT(*) as totWipHeavy FROM estimasi
                                    WHERE kategori_jasa = "2" AND status_inout = "1" AND jenis_estimasi != "1"
                                    AND status_produksi <= "9" AND status_produksi >= "0"
                                    AND YEAR(tgl_masuk) = "'.$yearNow.'" AND MONTH(tgl_masuk) = "'.$j.'"';
                
                $tempLight = $this->Crud->q($queryCountLight);
                $tempWipLight = $this->Crud->q($queryWipLight);

                $tempMedium = $this->Crud->q($queryCountMedium);
                $tempWipMedium = $this->Crud->q($queryWipMedium);

                $tempHeavy = $this->Crud->q($queryCountHeavy);
                $tempWipHeavy = $this->Crud->q($queryWipHeavy);
                
                foreach ($tempLight as $value) {
                    $dataCountLight[$i] = (int)$value->totLight; 
                    $totLight = $dataCountLight[$i];
                }
                foreach($tempWipLight as $value){
                    $wipLight[$i] = (int)$value->totWipLight;
                    $totWipLight = $wipLight[$i];
                }

                if($totWipLight == 0){
                    $persenLight[] = 0;
                }else{
                    $persenLight[] = ($totLight/$totWipLight)*100;
                }


                foreach ($tempMedium as $value) {
                    $dataCountMedium[$i] = (int)$value->totMedium;
                    $totMedium = $dataCountMedium[$i];
                }
                foreach($tempWipMedium as $value){
                    $wipMedium[$i] = (int)$value->totWipMedium;
                    $totWipMedium = $wipMedium[$i];
                }
                if($totWipMedium == 0){
                    $persenMedium[] = 0;
                }else{
                    $persenMedium[] = ($totMedium/$totWipMedium)*100;
                }


                foreach ($tempHeavy as $value) {
                    $dataCountHeavy[$i] = (int)$value->totHeavy; 
                    $totHeavy = $dataCountHeavy[$i];
                }

                foreach($tempWipHeavy as $value){
                    $wipHeavy[$i] = (int)$value->totWipHeavy;
                    $totWipHeavy = $wipHeavy[$i];
                }

                if($totWipHeavy == 0){
                    $persenHeavy[] = 0;
                }else{
                    $persenHeavy[] = ($totHeavy/$totWipHeavy)*100;
                }
                
                $data = array ('dataCountLight'  => $persenLight,
                               'dataCountMedium' => $persenMedium,
                               'dataCountHeavy'  => $persenHeavy,
                       
                               );
            }
        }else{
            for($i = 0 ; $i < 13; $i++){
                $j = $i+1;
                $queryCountLight = 'SELECT COUNT(*) as totLight FROM estimasi
                                    WHERE kategori_jasa = "0" AND status_inout = "1" AND jenis_estimasi != "1" 
                                    AND status_produksi = "9"
                                    AND YEAR(tgl_masuk) = "'.$yearNow.'" AND MONTH(tgl_masuk) = "'.$j.'"';
                $queryWipLight =    'SELECT COUNT(*) as totWipLight FROM estimasi
                                    WHERE kategori_jasa = "0" AND status_inout = "1" AND jenis_estimasi != "1"
                                    AND status_produksi <= "9" AND status_produksi >= "0"
                                    AND YEAR(tgl_masuk) = "'.$yearNow.'" AND MONTH(tgl_masuk) = "'.$j.'"';

                $queryCountMedium = 'SELECT COUNT(*) as totMedium FROM estimasi
                                    WHERE kategori_jasa = "1" AND status_inout = "1" AND jenis_estimasi != "1" 
                                    AND status_produksi = "9"
                                    AND YEAR(tgl_masuk) = "'.$yearNow.'" AND MONTH(tgl_masuk) = "'.$j.'"';
                $queryWipMedium =   'SELECT COUNT(*) as totWipMedium FROM estimasi
                                    WHERE kategori_jasa = "1" AND status_inout = "1" AND jenis_estimasi != "1"
                                    AND status_produksi <= "9" AND status_produksi >= "0"
                                    AND YEAR(tgl_masuk) = "'.$yearNow.'" AND MONTH(tgl_masuk) = "'.$j.'"';

                $queryCountHeavy = 'SELECT COUNT(*) as totHeavy FROM estimasi
                                    WHERE kategori_jasa = "2" AND status_inout = "1" AND jenis_estimasi != "1" 
                                    AND status_produksi = "9"
                                    AND YEAR(tgl_masuk) = "'.$yearNow.'" AND MONTH(tgl_masuk) = "'.$j.'"';
                $queryWipHeavy =    'SELECT COUNT(*) as totWipHeavy FROM estimasi
                                    WHERE kategori_jasa = "2" AND status_inout = "1" AND jenis_estimasi != "1"
                                    AND status_produksi <= "9" AND status_produksi >= "0"
                                    AND YEAR(tgl_masuk) = "'.$yearNow.'" AND MONTH(tgl_masuk) = "'.$j.'"';
                
                $tempLight = $this->Crud->q($queryCountLight);
                $tempWipLight = $this->Crud->q($queryWipLight);

                $tempMedium = $this->Crud->q($queryCountMedium);
                $tempWipMedium = $this->Crud->q($queryWipMedium);

                $tempHeavy = $this->Crud->q($queryCountHeavy);
                $tempWipHeavy = $this->Crud->q($queryWipHeavy);
                
                foreach ($tempLight as $value) {
                    $dataCountLight[$i] = (int)$value->totLight; 
                    $totLight = $dataCountLight[$i];
                }
                foreach($tempWipLight as $value){
                    $wipLight[$i] = (int)$value->totWipLight;
                    $totWipLight = $wipLight[$i];
                }

                if($totWipLight == 0){
                    $persenLight[] = 0;
                }else{
                    $persenLight[] = ($totLight/$totWipLight)*100;
                }


                foreach ($tempMedium as $value) {
                    $dataCountMedium[$i] = (int)$value->totMedium;
                    $totMedium = $dataCountMedium[$i];
                }
                foreach($tempWipMedium as $value){
                    $wipMedium[$i] = (int)$value->totWipMedium;
                    $totWipMedium = $wipMedium[$i];
                }
                if($totWipMedium == 0){
                    $persenMedium[] = 0;
                }else{
                    $persenMedium[] = ($totMedium/$totWipMedium)*100;
                }


                foreach ($tempHeavy as $value) {
                    $dataCountHeavy[$i] = (int)$value->totHeavy; 
                    $totHeavy = $dataCountHeavy[$i];
                }

                foreach($tempWipHeavy as $value){
                    $wipHeavy[$i] = (int)$value->totWipHeavy;
                    $totWipHeavy = $wipHeavy[$i];
                }

                if($totWipHeavy == 0){
                    $persenHeavy[] = 0;
                }else{
                    $persenHeavy[] = ($totHeavy/$totWipHeavy)*100;
                }
                
                $data = array ('dataCountLight'  => $persenLight,
                               'dataCountMedium' => $persenMedium,
                               'dataCountHeavy'  => $persenHeavy,
                       
                               );
            }
        }
        
        echo json_encode($data);
    }

     public function getDataChartSa(){
        $year = $this->input->post('year');
        //$id_user = $this->session->userdata('id_user');
        $yearNow = date('Y');
        $dataCountIn = array();
        $dataCountOut = array();
        $dataCountAsuransi = array();
        $dataCountTunai = array();
        if($year !== NULL ){
            $yearNow = $year;
            for($i = 1 ; $i < 13; $i++){
                // $queryCountIn = 'SELECT COUNT(*) as totIn FROM estimasi
                //                  WHERE status_inout = "1" AND id_user = "'.$id_user.'"
                //                  AND YEAR(tgl_estimasi) = "'.$yearNow.'" AND MONTH(tgl_estimasi) = "'.$i.'"';
                // $queryCountOut = 'SELECT COUNT(*) as totOut FROM estimasi
                //                   WHERE status_inout = "0" AND id_user = "'.$id_user.'"
                //                   AND YEAR(tgl_estimasi) = "'.$yearNow.'" AND MONTH(tgl_estimasi) = "'.$i.'"';
                $queryCountAsuransiIn = 'SELECT COUNT(*) as totAsuransiIn FROM estimasi
                                       WHERE jenis_customer = "0"  AND YEAR(tgl_estimasi) = "'.$yearNow.'" 
                                       AND MONTH(tgl_estimasi) = "'.$i.'" 
                                       AND status_inout = "1"';
                $queryCountAsuransiOut = 'SELECT COUNT(*) as totAsuransiOut FROM estimasi
                                       WHERE jenis_customer = "0"  AND YEAR(tgl_estimasi) = "'.$yearNow.'" 
                                       AND MONTH(tgl_estimasi) = "'.$i.'" 
                                       AND status_inout = "0"';
                $queryCountTunaiIn = 'SELECT COUNT(*) as totTunaiIn FROM estimasi
                                       WHERE jenis_customer = "1"  AND YEAR(tgl_estimasi) = "'.$yearNow.'" 
                                       AND MONTH(tgl_estimasi) = "'.$i.'" 
                                      AND status_inout = "1"';
                $queryCountTunaiOut = 'SELECT COUNT(*) as totTunaiOut FROM estimasi
                                       WHERE jenis_customer = "1"  AND YEAR(tgl_estimasi) = "'.$yearNow.'" 
                                       AND MONTH(tgl_estimasi) = "'.$i.'" 
                                      AND status_inout = "0"';
                // $tempIn = $this->Crud->q($queryCountIn);
                // $tempOut = $this->Crud->q($queryCountOut);
                $tempAsuransiIn = $this->Crud->q($queryCountAsuransiIn);
                $tempAsuransiOut = $this->Crud->q($queryCountAsuransiOut);
                $tempTunaiIn = $this->Crud->q($queryCountTunaiIn);
                $tempTunaiOut = $this->Crud->q($queryCountTunaiOut);
                // foreach ($tempIn as $value) {
                //     $dataCountIn[] = (int)$value->totIn; 
                // }
                // foreach ($tempOut as $value) {
                //     $dataCountOut[] = (int)$value->totOut; 
                // }
                foreach ($tempAsuransiIn as $value) {
                    $dataCountAsuransiIn[] = (int)$value->totAsuransiIn; 
                }
                 foreach ($tempAsuransiOut as $value) {
                    $dataCountAsuransiOut[] = (int)$value->totAsuransiOut; 
                }
                foreach ($tempTunaiIn as $value) {
                    $dataCountTunaiIn[] = (int)$value->totTunaiIn; 
                }
                foreach ($tempTunaiOut as $value) {
                    $dataCountTunaiOut[] = (int)$value->totTunaiOut; 
                }
            }
            $data = array ( 'dataCountAsuransiIn' => $dataCountAsuransiIn,
                            'dataCountAsuransiOut' => $dataCountAsuransiOut,
                            'dataCountTunaiIn' => $dataCountTunaiIn,
                            'dataCountTunaiOut' => $dataCountTunaiOut
                            );
        }else{
            for($i = 1 ; $i < 13; $i++){
                $queryCountAsuransiIn = 'SELECT COUNT(*) as totAsuransiIn FROM estimasi
                                       WHERE jenis_customer = "0"  AND YEAR(tgl_estimasi) = "'.$yearNow.'" 
                                       AND MONTH(tgl_estimasi) = "'.$i.'" 
                                       AND status_inout = "1"';
                $queryCountAsuransiOut = 'SELECT COUNT(*) as totAsuransiOut FROM estimasi
                                       WHERE jenis_customer = "0"  AND YEAR(tgl_estimasi) = "'.$yearNow.'" 
                                       AND MONTH(tgl_estimasi) = "'.$i.'" 
                                       AND status_inout = "0"';
                $queryCountTunaiIn = 'SELECT COUNT(*) as totTunaiIn FROM estimasi
                                       WHERE jenis_customer = "1"   AND YEAR(tgl_estimasi) = "'.$yearNow.'" 
                                       AND MONTH(tgl_estimasi) = "'.$i.'" 
                                      AND status_inout = "1"';
                $queryCountTunaiOut = 'SELECT COUNT(*) as totTunaiOut FROM estimasi
                                       WHERE jenis_customer = "1"  AND YEAR(tgl_estimasi) = "'.$yearNow.'" 
                                       AND MONTH(tgl_estimasi) = "'.$i.'" 
                                      AND status_inout = "0"';
               $tempAsuransiIn = $this->Crud->q($queryCountAsuransiIn);
                $tempAsuransiOut = $this->Crud->q($queryCountAsuransiOut);
                $tempTunaiIn = $this->Crud->q($queryCountTunaiIn);
                $tempTunaiOut = $this->Crud->q($queryCountTunaiOut);
                foreach ($tempAsuransiIn as $value) {
                    $dataCountAsuransiIn[] = (int)$value->totAsuransiIn; 
                }
                 foreach ($tempAsuransiOut as $value) {
                    $dataCountAsuransiOut[] = (int)$value->totAsuransiOut; 
                }
                foreach ($tempTunaiIn as $value) {
                    $dataCountTunaiIn[] = (int)$value->totTunaiIn; 
                }
                foreach ($tempTunaiOut as $value) {
                    $dataCountTunaiOut[] = (int)$value->totTunaiOut; 
                }
            }
            $data = array ( 'dataCountAsuransiIn' => $dataCountAsuransiIn,
                            'dataCountAsuransiOut' => $dataCountAsuransiOut,
                            'dataCountTunaiIn' => $dataCountTunaiIn,
                            'dataCountTunaiOut' => $dataCountTunaiOut
                            );
        }
        
        echo json_encode($data);
    }

    public function user(){
        $data = array(  'title'     => 'Daftar User | U-Care',
                        'isi'       => 'admin/dashboard/pimpinan/user',
                        'data'      => $this->Crud->ga('user'),
                        'dataScript'=> 'admin/dataScript/tabel-script' );
                $this->load->view('admin/_layout/wrapper', $data);
    }

    public function userCreate(){
        $data = array(  'title'     => 'Tambah User | U-Care',
                        'isi'       => 'admin/form/formUser',
                        'dataScript'=> 'admin/dataScript/form-script' );
        $this->load->view('admin/_layout/wrapper', $data);
    }

    public function userCreateAction() {
        $input = $this->input->post(NULL, TRUE);

        $data  = array(
                            'nama_lengkap_user'     => $input['nama_lengkap_user'],
                            'no_tlpUser'            => $input['no_tlpUser'],
                            'level'                 => $input['level'],
                            'sub_level'             => $input['sublevel'],
                            'password'              => md5($input['password']),
                            'username'              => $input['username'],
                            'create_at'             => date('Y-m-d h:m;s')
        );
        $this->Crud->i('user', $data);
        redirect(base_url('pimpinan/user'));
    }

    public function userUpdate($id_user=null){
        $where= array('id_user' => $id_user);
        $data = array(  'title'                 => 'Edit User | U-Care',
            'isi'                   => 'admin/form/formPimpinanUpdateUser',
            'data'                  => $this->Crud->gw('user', $where),
            'dataScript'            => 'admin/dataScript/form-script' );
        $this->load->view('admin/_layout/wrapper', $data);
    }

    public function userUpdateAction($pass) {
        $input      = $this->input->post(NULL, TRUE);
        $where      = array('id_user'   => $input['id_user']);

        if($input['level'] == 'qc'){
            if($pass == $input['password']){
                $data       = array(
                            'nama_lengkap_user'     => $input['nama_lengkap_user'],
                            'no_tlpUser'            => $input['no_tlpUser'],
                            'username'              => $input['username'],
                            'level'                 => $input['level'],
                            'sub_level'             => $input['sub_level']                        
                          );
            }else{
                $data       = array(
                            'nama_lengkap_user'     => $input['nama_lengkap_user'],
                            'no_tlpUser'            => $input['no_tlpUser'],
                            'username'              => $input['username'],
                            'password'              => md5($input['password']),
                            'level'                 => $input['level'],
                            'sub_level'             => $input['sub_level']                        
                          );
            }
        } else{
            if($pass == $input['password']){
                $data       = array(
                            'nama_lengkap_user'     => $input['nama_lengkap_user'],
                            'no_tlpUser'            => $input['no_tlpUser'],
                            'username'              => $input['username'],
                            'level'                 => $input['level'],
                            'sub_level'             => NULL                        
                          );
            }else{
                $data       = array(
                            'nama_lengkap_user'     => $input['nama_lengkap_user'],
                            'no_tlpUser'            => $input['no_tlpUser'],
                            'username'              => $input['username'],
                            'password'              => md5($input['password']),
                            'level'                 => $input['level'],
                            'sub_level'             => NULL                       
                          );
            }
        }
        
        $this->Crud->u('user', $data, $where);
        echo 'success';
    }

    public function deleteUser(){
        $input = $this->input->post(NULL, TRUE);
        $where = array('id_user'   => $input['id_user']);

        $this->Crud->d('user', $where);
        echo 'success';
    }

    public function kendaraan(){
        $data = array(  'title'     => 'Daftar Kendaraan | U-Care',
            'isi'       => 'admin/dashboard/pimpinan/kendaraan',
            'data'      => $this->Crud->ga('jenis_kendaran'),
            'dataScript'=> 'admin/dataScript/tabel-script' );
        $this->load->view('admin/_layout/wrapper', $data);
    }


    public function kendaraanCreateAction() {
        $input = $this->input->post(NULL, TRUE);

        $data  = array(
            'nama_jenis'     => $input['nama_jenis'],
        );
        $this->Crud->i('jenis_kendaran', $data);
        $this->session->set_flashdata('info', 'Data Berhasil di Tambahkan');
        redirect(base_url('pimpinan/kendaraan'));
    }

    public function kendaraanUpdateAction($id){
        $input = $this->input->post(NULL, TRUE);
        $where = array('id_jenis' => $id);
        $items = array(
            'nama_jenis'    => $input['nama_jenis'],
        );

        $this->db->update('jenis_kendaran', $items, $where);
        $this->session->set_flashdata('info', 'Data Berhasil di Update');
        redirect(base_url('pimpinan/kendaraan'));
    }

    public function kendaraanHapus($id){
         $where = array(
            'id_jenis' => $id,
        );

        $this->Crud->d('jenis_kendaran', $where);
        $this->Crud->d('jenis_kendaran', $where);
        $this->session->set_flashdata('info', 'Data Berhasil Dihapus');
        redirect('pimpinan/kendaraan');
    }

    public function daftar_asuransi(){
        $data = array(  'title'     => 'Daftar Asuransi | U-Care',
                'isi'       => 'admin/dashboard/pimpinan/daftar_asuransi',
                'data'      => $this->Crud->ga('asuransi'),
                'dataScript'=> 'admin/dataScript/tabel-script' );
                $this->load->view('admin/_layout/wrapper', $data);
    }

    public function asuransiCreateAction(){
        $input = $this->input->post(NULL, TRUE);

        $data  = array(
            'nama_asuransi'     => $input['nama_asuransi'],
        );
        $this->Crud->i('asuransi', $data);
        $this->session->set_flashdata('info', 'Data Berhasil di Tambahkan');
        redirect(base_url('pimpinan/daftar_asuransi'));
    }

    public function asuransiUpdateAction($id){
        $input = $this->input->post(NULL, TRUE);
        $where = array('id_asuransi' => $id);
        $items = array(
            'nama_asuransi'    => $input['nama_asuransi'],
        );

        $this->db->update('asuransi', $items, $where);
        $this->session->set_flashdata('info', 'Data Berhasil di Update');
        redirect(base_url('pimpinan/daftar_asuransi'));
    }

     public function asuransiHapus($id){
         $where = array(
            'id_asuransi' => $id,
        );
        $this->Crud->d('asuransi', $where);
        $this->session->set_flashdata('info', 'Data Berhasil Dihapus');
        redirect('pimpinan/daftar_asuransi');
    }


    public function daftar_warna(){
        $data = array(  'title'     => 'Daftar Estimasi Order | U-Care',
                'isi'       => 'admin/dashboard/pimpinan/daftar_warna',
                'data'      => $this->Crud->ga('color'),
                'dataScript'=> 'admin/dataScript/tabel-script' );
                $this->load->view('admin/_layout/wrapper', $data);
    }

    public function warnaCreateAction(){
        $input = $this->input->post(NULL, TRUE);

        $data = array(
            'code_color'    => $input['code_color'],
            'nama_color'    => $input['nama_color'],
        );

        $this->Crud->i('color', $data);
        $this->session->set_flashdata('info', 'Data Berhasil di Tambahkan');
        redirect(base_url('pimpinan/daftar_warna'));
    }

    public function warnaUpdateAction($id){
        $input = $this->input->post(NULL, TRUE);
        $where = array('id_color' => $id);
        $items = array(
            'nama_color'    => $input['nama_color'],
            'code_color'    => $input['code_color'],
        );

        $this->db->update('color', $items, $where);
        $this->session->set_flashdata('info', 'Data Berhasil di Update');
        redirect(base_url('pimpinan/daftar_warna'));
    }

    public function warnaHapus($id){
         $where = array(
            'id_color' => $id,
        );
        $this->Crud->d('color', $where);
        $this->session->set_flashdata('info', 'Data Berhasil Dihapus');
        redirect('pimpinan/daftar_warna');
    }


    //all about parts
    public function daftar_parts(){
        $data = array(
                    'title'     => 'Daftar Estimasi Order | U-Care',
                    'isi'       => 'admin/dashboard/pimpinan/daftar_parts',
                    'data'      => $this->PartsmanModel->get_estimasi(),
                    'dataScript'=> 'admin/dataScript/tabel-script' );
                $this->load->view('admin/_layout/wrapper', $data);
    }

   public function get_item_json() { //get product data and encode to be JSON object
      header('Content-Type: application/json');
      echo $this->PimpinanModel->get_all_item_parts();
  }

  public function importNewParts(){
        if(isset($_POST['upload'])){//untuk cek apakah sdh terklik actionnya
            if($_FILES['fileImport']['name']){//kondisi file required
                $fileName = explode('.', $_FILES['fileImport']['name']);//memecah ke dalam array untuk cari nama file

                if(end($fileName) == "csv"){//cek ekstensi
                    $handle = fopen($_FILES['fileImport']['tmp_name'], 'r');
                    while($data = fgetcsv($handle)){//fetch data file
                        //mengambil data dari csv ke variabel
                        $nama_item = array();
                        $nomor_parts =  $this->db->escape_str($data[0]);
                        $harga_item = $this->db->escape_str($data[1]);
                        $nama_item1 = $this->db->escape_str($data[2]);
                        array_push($nama_item, $nama_item1);
                        if(isset($data[3])){
                          $nama_item2 = $this->db->escape_str($data[3]);
                          array_push($nama_item, $nama_item2);
                        };
                        if(isset($data[4])){
                          $nama_item3 = $this->db->escape_str($data[4]);
                          array_push($nama_item, $nama_item3);
                        };
                        if(isset($data[5])){
                          $nama_item4 = $this->db->escape_str($data[5]);
                          array_push($nama_item, $nama_item4);
                        };
                        //echo $nomor_parts.' '.$harga_item."<br/>";

                        $nama_parts = implode( ", ", $nama_item );

                        $parts = $this->ExcelModel->get_tambah_parts();

                        if($no_parts == "No. Parts" || $harga_item == "Harga"){

                        }else{
                            if(in_array($nomor_parts, $parts)){
                          
                            }
                            else{
                              $data = [
                              'nama_item'   => $nama_parts,
                              'nomor_parts' => $nomor_parts,
                              'harga_item'  => $harga_item,
                              'tipe_item'   => 2,
                              'id_jenis'    => 80,
                            ];

                            $this->db->insert('item', $data);
                            }
                        }
                    }
                    fclose($handle);//close file after cache
                    $this->session->set_flashdata('not_select', 'Data Sukses Ditambah !');
                    redirect("pimpinan/daftar_parts");
                }else{
                    $this->session->set_flashdata('not_select', 'Gagal !<br><br> Jenis File Bukan .csv.');
                    redirect('pimpinan/daftar_parts');
                }

            }else{//kondisi tidak ada file
                $this->session->set_flashdata('not_select', 'Gagal !<br><br> Pilih File Dengan Cermat.');
                redirect('pimpinan/daftar_parts');
            }
        }
  }

  public function importUpdate(){

        if(isset($_POST['upload'])){//untuk cek apakah sdh terklik actionnya
            if($_FILES['fileImport']['name']){//kondisi file required
                $fileName = explode('.', $_FILES['fileImport']['name']);//memecah ke dalam array untuk cari nama file

                if(end($fileName) == "csv"){//cek ekstensi
                    $handle = fopen($_FILES['fileImport']['tmp_name'], 'r');
                    while($data = fgetcsv($handle)){//fetch data file
                        //mengambil data dari csv ke variabel
                        $nomor_parts =  $this->db->escape_str($data[0]);
                        $harga_item = $this->db->escape_str($data[1]);
                        //echo $nomor_parts.' '.$harga_item."<br/>";

                        $query = "
                            UPDATE item
                            SET nomor_parts = '$nomor_parts',
                            harga_item = '$harga_item'
                            WHERE nomor_parts = '$nomor_parts'
                        ";
                        $this->db->query($query);
                    }
                    fclose($handle);//close file after cache
                    $this->session->set_flashdata('not_select', 'Data Sukses Diupdate !');
                    redirect("pimpinan/daftar_parts");
                }else{
                    $this->session->set_flashdata('not_select', 'Gagal !<br><br> Jenis File Bukan .csv.');
                    redirect('pimpinan/daftar_parts');
                }

            }else{//kondisi tidak ada file
                $this->session->set_flashdata('not_select', 'Gagal !<br><br> Pilih File Dengan Cermat.');
                redirect('pimpinan/daftar_parts');
            }
        }

    }

    public function importUpdateSubs(){

        if(isset($_POST['upload'])){//untuk cek apakah sdh terklik actionnya
            if($_FILES['fileImport']['name']){//kondisi file required
                $fileName = explode('.', $_FILES['fileImport']['name']);//memecah ke dalam array untuk cari nama file

                if(end($fileName) == "csv"){//cek ekstensi
                    $handle = fopen($_FILES['fileImport']['tmp_name'], 'r');
                    while($data = fgetcsv($handle)){//fetch data file
                        //mengambil data dari csv ke variabel
                        $nomor_parts_lama =  $this->db->escape_str($data[0]);
                        $nomor_parts_baru =  $this->db->escape_str($data[1]);
                        $harga_item = $this->db->escape_str($data[2]);
                        //echo $nomor_parts.' '.$harga_item."<br/>";

                        $parts = $this->ExcelModel->get_nomor_parts($nomor_parts_lama);

                        if($parts->nomor_parts !== null){
                        $data = [
                          'nama_item'   => $parts->nama_item,
                          'nomor_parts' => $nomor_parts_baru,
                          'nomor_parts_lama'  => $nomor_parts_lama,
                          'harga_item'  => $harga_item,
                          'tipe_item'   => $parts->tipe_item,
                          'id_jenis'    => $parts->id_jenis,
                          'posisi_jasa' => $parts->posisi_jasa,
                        ];

                        $this->db->insert('item', $data);
                        };
                        

                        // $query = "
                        //     UPDATE item
                        //     SET nomor_parts = '$nomor_parts_baru',
                        //     harga_item = '$harga_item'
                        //     WHERE nomor_parts = '$nomor_parts_lama'
                        // ";
                        // $this->db->query($query);
                    }
                    fclose($handle);//close file after cache
                    $this->session->set_flashdata('not_select', 'Data Sukses Diupdate !');
                    redirect("pimpinan/daftar_parts");
                }else{
                    $this->session->set_flashdata('not_select', 'Gagal !<br><br> Jenis File Bukan .csv.');
                    redirect('pimpinan/daftar_parts');
                }

            }else{//kondisi tidak ada file
                $this->session->set_flashdata('not_select', 'Gagal !<br><br> Pilih File Dengan Cermat.');
                redirect('pimpinan/daftar_parts');
            }
        }

    }

    public function tambahParts(){
        $input = $this->input->post(NULL, TRUE);
        $data = array(
            'nama_item' => $input['nama_item'],
            'nomor_parts' => $input['nomor_parts'],
            'nomor_parts_lama' => $input['nomor_parts_lama'],
            'harga_item' => $input['harga_item'],
            'tipe_item' => 2,
            'id_jenis' => 80,
        );
        $this->Crud->i('item', $data);
        $this->session->set_flashdata('update_sukses', 'Parts Berhasil Ditambahkan !');
        redirect('pimpinan/daftar_parts');
    }

    public function update_parts(){
        $input = $this->input->post(NULL, TRUE);
        $items = array(
            'nama_item' => $input['nama_item'],
            'nomor_parts' => $input['nomor_parts'],
            'nomor_parts_lama' => $input['nomor_parts_lama'],
            'harga_item' => $input['harga_item'],
        );

        $this->db->where('id_item', $input['id_item']);
        $this->db->update('item', $items);

        $this->session->set_flashdata('update_sukses', 'Parts Berhasil Diupdate !');
        redirect('pimpinan/daftar_parts');
    }

    public function delete_parts(){
        $input = $this->input->post(NULL, TRUE);
        $id = $input['id_item'];
        $this->db->where('id_item', $id);
        $this->db->delete('item');

        $this->session->set_flashdata('update_sukses', 'Data Berhasil Dihapus !');
        redirect('pimpinan/daftar_parts');
    }


  //all about jasa
    public function jasa(){
      error_reporting(0);
        $data = array(  'title'     => 'Daftar Jasa | U-Care',
                    'isi'       => 'admin/dashboard/pimpinan/jasa',
                    'dataMobil' =>   $this->Crud->ga('jenis_kendaran'),
                    'dataScript'=> 'admin/dataScript/tabel-script-jasa' 
                );
            
            $this->load->view('admin/_layout/wrapper', $data);
    }

    public function jasaToExcel(){
      $input = $this->input->post(NULL, TRUE);

      $id_jenis = $input['id_jenis'];

      $queryJasa = ' SELECT * FROM item
                            LEFT JOIN jenis_kendaran ON jenis_kendaran.id_jenis = item.id_jenis
                            WHERE item.id_jenis = "'.$id_jenis.'" AND item.tipe_item = "1" 
                           ';


      $data = array(
            'data' => $this->Crud->q($queryJasa),
            //'data' => $this->Crud->ga('detail_estimasi'),
        );
        $this->load->view('admin/dashboard/pimpinan/excel_jasa', $data);
    }

    public function summaryToExcel(){
      $input = $this->input->post(NULL, TRUE);
      $dateFrom = $input['dateFrom'];
      $dateTo = $input['dateTo'];

      $querySummary = ' SELECT * FROM summary_lead
                            LEFT JOIN estimasi ON estimasi.id_estimasi = summary_lead.id_estimasi_lead
                            LEFT JOIN summary ON summary.id_estimasi_summary = estimasi.id_estimasi
                            LEFT JOIN user ON user.id_user = estimasi.tim_teknisi
                            WHERE estimasi.tgl_estimasi >= "'.$dateFrom.'" AND estimasi.tgl_estimasi <= "'.$dateTo.'" 
                           ';
      $data = array(
            'data'  => $this->Crud->q($querySummary),
        );
      $this->load->view('admin/dashboard/pimpinan/excel_summary', $data);
    }

    public function jasaUpdate($id){
        $where= array('id_item' => $id);
        $data = array(  'title'                 => 'Edit Jasa Kendaraan | U-Care',
                        'isi'                   => 'admin/dashboard/pimpinan/updateJasa',
                        'data'                  => $this->Crud->gw('item', $where),
                        'dataScript'            => 'admin/dataScript/tabel-script' );
        $this->load->view('admin/_layout/wrapper', $data);
    }

    public function jasaUpdateAction($id){
        $input = $this->input->post(NULL, TRUE);

        $where = array('id_item' => $id);
        $items = array(
            'nama_item'    => $input['nama_item'],
            'harga_item'    => $input['harga_item'],
        );

        $this->db->update('item', $items, $where);
        $this->session->set_flashdata('info', 'Data Berhasil di Update');
        redirect(base_url('pimpinan/jasa'));
    }

    public function jasaCreateAction(){
        $input = $this->input->post(NULL, TRUE);

        $data = array(
            'nama_item'    => $input['nama_item'],
            'harga_item'    => $input['harga_item'],
            'posisi_jasa'    => $input['posisi_jasa'],
            'id_jenis'    => $input['id_jenis'],
            'tipe_item'     => 1,
        );

        $this->Crud->i('item', $data);
        $this->session->set_flashdata('info', 'Data Berhasil di Tambahkan');
        redirect(base_url('pimpinan/jasa'));
    }

    public function jasaHapus($id){
         $where = array(
            'id_item' => $id,
        );
        $this->Crud->d('item', $where);
        $this->session->set_flashdata('info', 'Data Berhasil Dihapus');
        redirect('pimpinan/jasa');
    }

    public function get_jasa_json() { //get product data and encode to be JSON object
      header('Content-Type: application/json');
      echo $this->PimpinanModel->get_all_item_jasa();
  }

  //all About Summary
  public function summaryLead(){
        $data = array(  'title'     => 'Summary Estimai | U-Care',
                        'isi'       => 'admin/dashboard/pimpinan/summary_lead',
                        'dataScript'        => 'admin/dataScript/tabel-script',
                    );
            
            $this->load->view('admin/_layout/wrapper', $data);
  }

  public function get_summaryLead_json() { //get product data and encode to be JSON object
      header('Content-Type: application/json');
      echo $this->PimpinanModel->get_all_summary_lead();
  }

  public function historyLead($id){
        $queryHistory = ' SELECT *, history_lead.status_produksi as status_produksi_history FROM history_lead
                          LEFT JOIN summary_lead ON summary_lead.id_lead = history_lead.id_lead
                          LEFT JOIN estimasi ON estimasi.id_estimasi = summary_lead.id_estimasi_lead
                          LEFT JOIN customer ON customer.id_customer = estimasi.id_customer
                          LEFT JOIN user ON user.id_user = estimasi.tim_teknisi
                          WHERE history_lead.id_lead = '.$id.'';
        $listHistory  = array();
        $dataHistory = $this->Crud->q($queryHistory);

        foreach($dataHistory as $datas){
            $history = new stdClass();
            $history->id_history        = $datas->id_history;
            $history->status_produksi_history   = $datas->status_produksi_history;
            $history->status_estimasi   = $datas->status_estimasi;
            $history->waktu_history     = $datas->waktu_history;
            $history->ket_history       = $datas->ket_history;
            $history->nomor_wo          = $datas->nomor_wo;
            $history->no_polisi         = $datas->no_polisi;
            $history->nama_lengkap      = $datas->nama_lengkap;
            $history->nama_lengkap_user = $datas->nama_lengkap_user;
            array_push($listHistory, $history);
        }

        $data = array(  
                        'title'         => 'History Estimai | U-Care',
                        'isi'           => 'admin/dashboard/pimpinan/history_lead',
                        'listHistory'   => $listHistory,
                        'dataScript'    => 'admin/dataScript/tabel-script',
                );
            
        $this->load->view('admin/_layout/wrapper', $data);
  }


    public function atBodyRepair(){
        //$id_user = $this->session->userdata('id_user');

        $queryEstimasi  = ' SELECT *, u.nama_lengkap_user as namaSa, uu.nama_lengkap_user as namaTeknisi FROM estimasi
                            LEFT JOIN customer ON customer.id_customer = estimasi.id_customer
                            LEFT JOIN jenis_kendaran ON jenis_kendaran.id_jenis = estimasi.id_jenis
                            LEFT JOIN user u ON u.id_user = estimasi.id_user
                            LEFT JOIN user uu ON uu.id_user = estimasi.tim_teknisi
                            LEFT JOIN summary ON summary.id_estimasi_summary = estimasi.id_estimasi
                            LEFT JOIN summary_lead ON summary_lead.id_estimasi_lead = estimasi.id_estimasi
                            WHERE  estimasi.status_produksi = "1" AND estimasi.is_confirm_teknisi IS NOT NULL';
        $listEstimasi = array();
        $dataEstimasi = $this->Crud->q($queryEstimasi);
        
        foreach($dataEstimasi as $datas){
            $estimasi = new stdClass();
            $estimasi->nomor_wo         = $datas->nomor_wo;
            $estimasi->id_estimasi      = $datas->id_estimasi;
            $estimasi->no_polisi        = $datas->no_polisi;
            $estimasi->nama_lengkap     = $datas->nama_lengkap;
            $estimasi->tgl_masuk        = $datas->tgl_masuk;
            $estimasi->tgl_janji_penyerahan       = $datas->tgl_janji_penyerahan;
            $estimasi->nama_sa          = $datas->namaSa;
            $estimasi->nama_teknisi     = $datas->namaTeknisi;
            $estimasi->kategori_jasa    = $datas->kategori_jasa;
            $estimasi->status_produksi  = $datas->status_produksi;
            $estimasi->body_repair_in   = $datas->body_repair_in;
            $estimasi->body_repair_out  = $datas->body_repair_out;
            $estimasi->body_repair_status  = $datas->body_repair_status;
            $estimasi->id_lead          = $datas->id_lead;
            $estimasi->body_repair_pause    = $datas->body_repair_pause;
            $estimasi->body_repair_start    = $datas->body_repair_start;
            $estimasi->body_repair_note     = $datas->body_repair_note;
            $estimasi->body_repair_lead     = $datas->body_repair_lead;
            $estimasi->total_lead           = $datas->total_lead;
            $estimasi->is_confirm_teknisi = $datas->is_confirm_teknisi;
            $estimasi->ket              = $datas->body_repair_ket;
            array_push($listEstimasi, $estimasi);
        }

        $data = array(
                        'title'             => 'Body Repair | U-Care',
                        'listEstimasi'      => $listEstimasi,
                        'isi'               => 'admin/dashboard/teknisi/atBodyRepair',
                        'dataScript'        => 'admin/dataScript/tabel-script',
                        );
        // var_dump($listEstimasi);
        $this->load->view('admin/_layout/wrapper', $data);
    } 

    public function atPreparation(){
        //$id_user = $this->session->userdata('id_user');
        $queryEstimasi  = ' SELECT *, u.nama_lengkap_user as namaSa, uu.nama_lengkap_user as namaTeknisi FROM estimasi
                            LEFT JOIN customer ON customer.id_customer = estimasi.id_customer
                            LEFT JOIN jenis_kendaran ON jenis_kendaran.id_jenis = estimasi.id_jenis
                            LEFT JOIN user u ON u.id_user = estimasi.id_user
                            LEFT JOIN user uu ON uu.id_user = estimasi.tim_teknisi
                            LEFT JOIN summary ON summary.id_estimasi_summary = estimasi.id_estimasi
                            LEFT JOIN summary_lead ON summary_lead.id_estimasi_lead = estimasi.id_estimasi
                            WHERE  estimasi.status_produksi = "2" AND estimasi.is_confirm_teknisi IS NOT NULL';
        $listEstimasi = array();
        $dataEstimasi = $this->Crud->q($queryEstimasi);
        
        foreach($dataEstimasi as $datas){
            $estimasi = new stdClass();
            $estimasi->nomor_wo         = $datas->nomor_wo;
            $estimasi->id_estimasi      = $datas->id_estimasi;
            $estimasi->no_polisi        = $datas->no_polisi;
            $estimasi->nama_lengkap     = $datas->nama_lengkap;
            $estimasi->tgl_masuk        = $datas->tgl_masuk;
            $estimasi->tgl_janji_penyerahan       = $datas->tgl_janji_penyerahan;
            $estimasi->nama_sa          = $datas->namaSa;
            $estimasi->nama_teknisi     = $datas->namaTeknisi;
            $estimasi->nama_teknisi     = $datas->namaTeknisi;
            $estimasi->kategori_jasa    = $datas->kategori_jasa;
            $estimasi->status_produksi  = $datas->status_produksi;
            $estimasi->preparation_in   = $datas->preparation_in;
            $estimasi->preparation_out  = $datas->preparation_out;
            $estimasi->preparation_status  = $datas->preparation_status;
            $estimasi->id_lead          = $datas->id_lead;
            $estimasi->preparation_pause    = $datas->preparation_pause;
            $estimasi->preparation_start    = $datas->preparation_start;
            $estimasi->preparation_note     = $datas->preparation_note;
            $estimasi->preparation_lead     = $datas->preparation_lead;
            $estimasi->total_lead           = $datas->total_lead;
            $estimasi->is_confirm_teknisi = $datas->is_confirm_teknisi;
            $estimasi->ket              = $datas->preparation_ket;
            array_push($listEstimasi, $estimasi);
        }

        $data = array(
                        'title'             => 'Preparation | U-Care',
                        'listEstimasi'      => $listEstimasi,
                        'isi'               => 'admin/dashboard/teknisi/atPreparation',
                        'dataScript'        => 'admin/dataScript/tabel-script',
                        );
        // var_dump($listEstimasi);
        $this->load->view('admin/_layout/wrapper', $data);
    }
    
    public function atMasking(){
        //$id_user = $this->session->userdata('id_user');
        $queryEstimasi  = ' SELECT *, u.nama_lengkap_user as namaSa, uu.nama_lengkap_user as namaTeknisi FROM estimasi
                            LEFT JOIN customer ON customer.id_customer = estimasi.id_customer
                            LEFT JOIN jenis_kendaran ON jenis_kendaran.id_jenis = estimasi.id_jenis
                            LEFT JOIN user u ON u.id_user = estimasi.id_user
                            LEFT JOIN user uu ON uu.id_user = estimasi.tim_teknisi
                            LEFT JOIN summary ON summary.id_estimasi_summary = estimasi.id_estimasi
                            LEFT JOIN summary_lead ON summary_lead.id_estimasi_lead = estimasi.id_estimasi
                            WHERE  estimasi.status_produksi = "3" AND estimasi.is_confirm_teknisi IS NOT NULL';
        $listEstimasi = array();
        $dataEstimasi = $this->Crud->q($queryEstimasi);
        
        foreach($dataEstimasi as $datas){
            $estimasi = new stdClass();
            $estimasi->nomor_wo         = $datas->nomor_wo;
            $estimasi->id_estimasi      = $datas->id_estimasi;
            $estimasi->no_polisi        = $datas->no_polisi;
            $estimasi->nama_lengkap     = $datas->nama_lengkap;
            $estimasi->tgl_masuk        = $datas->tgl_masuk;
            $estimasi->tgl_janji_penyerahan       = $datas->tgl_janji_penyerahan;
            $estimasi->nama_sa          = $datas->namaSa;
            $estimasi->nama_teknisi     = $datas->namaTeknisi;
            $estimasi->kategori_jasa    = $datas->kategori_jasa;
            $estimasi->status_produksi  = $datas->status_produksi;
            $estimasi->masking_in   = $datas->masking_in;
            $estimasi->masking_out   = $datas->masking_out;
            $estimasi->masking_status   = $datas->masking_status;
            $estimasi->id_lead          = $datas->id_lead;
            $estimasi->masking_pause    = $datas->masking_pause;
            $estimasi->masking_start    = $datas->masking_start;
            $estimasi->masking_note     = $datas->masking_note;
            $estimasi->masking_lead     = $datas->masking_lead;
            $estimasi->total_lead           = $datas->total_lead;
            $estimasi->is_confirm_teknisi = $datas->is_confirm_teknisi;
            $estimasi->ket              = $datas->masking_ket;
            array_push($listEstimasi, $estimasi);
        }

        $data = array(
                        'title'             => 'Masking | U-Care',
                        'listEstimasi'      => $listEstimasi,
                        'isi'               => 'admin/dashboard/teknisi/atMasking',
                        'dataScript'        => 'admin/dataScript/tabel-script',
                        );
        // var_dump($listEstimasi);
        $this->load->view('admin/_layout/wrapper', $data);
    }

    public function atPainting(){
        //$id_user = $this->session->userdata('id_user');
        $queryEstimasi  = ' SELECT *, u.nama_lengkap_user as namaSa, uu.nama_lengkap_user as namaTeknisi FROM estimasi
                            LEFT JOIN customer ON customer.id_customer = estimasi.id_customer
                            LEFT JOIN jenis_kendaran ON jenis_kendaran.id_jenis = estimasi.id_jenis
                            LEFT JOIN user u ON u.id_user = estimasi.id_user
                            LEFT JOIN user uu ON uu.id_user = estimasi.tim_teknisi
                            LEFT JOIN summary ON summary.id_estimasi_summary = estimasi.id_estimasi
                            LEFT JOIN summary_lead ON summary_lead.id_estimasi_lead = estimasi.id_estimasi
                            WHERE  estimasi.status_produksi = "4" AND estimasi.is_confirm_teknisi IS NOT NULL';
        $listEstimasi = array();
        $dataEstimasi = $this->Crud->q($queryEstimasi);
        
        foreach($dataEstimasi as $datas){
            $estimasi = new stdClass();
            $estimasi->nomor_wo         = $datas->nomor_wo;
            $estimasi->id_estimasi      = $datas->id_estimasi;
            $estimasi->no_polisi        = $datas->no_polisi;
            $estimasi->nama_lengkap     = $datas->nama_lengkap;
            $estimasi->tgl_masuk        = $datas->tgl_masuk;
            $estimasi->tgl_janji_penyerahan       = $datas->tgl_janji_penyerahan;
            $estimasi->nama_sa          = $datas->namaSa;
            $estimasi->nama_teknisi     = $datas->namaTeknisi;
            $estimasi->kategori_jasa    = $datas->kategori_jasa;
            $estimasi->status_produksi  = $datas->status_produksi;
            $estimasi->painting_in   = $datas->painting_in;
            $estimasi->painting_out   = $datas->painting_out;
            $estimasi->painting_status   = $datas->painting_status;
            $estimasi->id_lead          = $datas->id_lead;
            $estimasi->painting_pause    = $datas->painting_pause;
            $estimasi->painting_start    = $datas->painting_start;
            $estimasi->painting_note     = $datas->painting_note;
            $estimasi->painting_lead     = $datas->painting_lead;
            $estimasi->total_lead           = $datas->total_lead;
            $estimasi->is_confirm_teknisi = $datas->is_confirm_teknisi;
            $estimasi->ket              = $datas->painting_ket;
            array_push($listEstimasi, $estimasi);
        }

        $data = array(
                        'title'             => 'Painting | U-Care',
                        'listEstimasi'      => $listEstimasi,
                        'isi'               => 'admin/dashboard/teknisi/atPainting',
                        'dataScript'        => 'admin/dataScript/tabel-script',
                        );
        // var_dump($listEstimasi);
        $this->load->view('admin/_layout/wrapper', $data);
    } 

    public function atPolishing(){
        //$id_user = $this->session->userdata('id_user');
        $queryEstimasi  = ' SELECT *, u.nama_lengkap_user as namaSa, uu.nama_lengkap_user as namaTeknisi FROM estimasi
                            LEFT JOIN customer ON customer.id_customer = estimasi.id_customer
                            LEFT JOIN jenis_kendaran ON jenis_kendaran.id_jenis = estimasi.id_jenis
                            LEFT JOIN user u ON u.id_user = estimasi.id_user
                            LEFT JOIN user uu ON uu.id_user = estimasi.tim_teknisi
                            LEFT JOIN summary ON summary.id_estimasi_summary = estimasi.id_estimasi
                            LEFT JOIN summary_lead ON summary_lead.id_estimasi_lead = estimasi.id_estimasi
                            WHERE  estimasi.status_produksi = "5" AND estimasi.is_confirm_teknisi IS NOT NULL';
        $listEstimasi = array();
        $dataEstimasi = $this->Crud->q($queryEstimasi);
        
        foreach($dataEstimasi as $datas){
            $estimasi = new stdClass();
            $estimasi->nomor_wo         = $datas->nomor_wo;
            $estimasi->id_estimasi      = $datas->id_estimasi;
            $estimasi->no_polisi        = $datas->no_polisi;
            $estimasi->nama_lengkap     = $datas->nama_lengkap;
            $estimasi->tgl_masuk        = $datas->tgl_masuk;
            $estimasi->tgl_janji_penyerahan       = $datas->tgl_janji_penyerahan;
            $estimasi->nama_sa          = $datas->namaSa;
            $estimasi->nama_teknisi     = $datas->namaTeknisi;
            $estimasi->kategori_jasa    = $datas->kategori_jasa;
            $estimasi->status_produksi  = $datas->status_produksi;
            $estimasi->polishing_in   = $datas->polishing_in;
            $estimasi->polishing_out   = $datas->polishing_out;
            $estimasi->polishing_status   = $datas->polishing_status;
            $estimasi->id_lead          = $datas->id_lead;
            $estimasi->polishing_pause    = $datas->polishing_pause;
            $estimasi->polishing_start    = $datas->polishing_start;
            $estimasi->polishing_note     = $datas->polishing_note;
            $estimasi->polishing_lead     = $datas->polishing_lead;
            $estimasi->total_lead           = $datas->total_lead;
            $estimasi->is_confirm_teknisi = $datas->is_confirm_teknisi;
            $estimasi->ket              = $datas->polishing_ket;
            array_push($listEstimasi, $estimasi);
        }

        $data = array(
                        'title'             => 'Polishing | U-Care',
                        'listEstimasi'      => $listEstimasi,
                        'isi'               => 'admin/dashboard/teknisi/atPolishing',
                        'dataScript'        => 'admin/dataScript/tabel-script',
                        );
        $this->load->view('admin/_layout/wrapper', $data);
    }

    public function atReAssembling(){
        //$id_user = $this->session->userdata('id_user');
        $queryEstimasi  = ' SELECT *, u.nama_lengkap_user as namaSa, uu.nama_lengkap_user as namaTeknisi FROM estimasi
                            LEFT JOIN customer ON customer.id_customer = estimasi.id_customer
                            LEFT JOIN jenis_kendaran ON jenis_kendaran.id_jenis = estimasi.id_jenis
                            LEFT JOIN user u ON u.id_user = estimasi.id_user
                            LEFT JOIN user uu ON uu.id_user = estimasi.tim_teknisi
                            LEFT JOIN summary ON summary.id_estimasi_summary = estimasi.id_estimasi
                            LEFT JOIN summary_lead ON summary_lead.id_estimasi_lead = estimasi.id_estimasi
                            WHERE  estimasi.status_produksi = "6" AND estimasi.is_confirm_teknisi IS NOT NULL';
        $listEstimasi = array();
        $dataEstimasi = $this->Crud->q($queryEstimasi);
        
        foreach($dataEstimasi as $datas){
            $estimasi = new stdClass();
            $estimasi->nomor_wo         = $datas->nomor_wo;
            $estimasi->id_estimasi      = $datas->id_estimasi;
            $estimasi->no_polisi        = $datas->no_polisi;
            $estimasi->nama_lengkap     = $datas->nama_lengkap;
            $estimasi->tgl_masuk        = $datas->tgl_masuk;
            $estimasi->tgl_janji_penyerahan       = $datas->tgl_janji_penyerahan;
            $estimasi->nama_sa          = $datas->namaSa;
            $estimasi->nama_teknisi     = $datas->namaTeknisi;
            $estimasi->kategori_jasa    = $datas->kategori_jasa;
            $estimasi->status_produksi  = $datas->status_produksi;
            $estimasi->re_assembling_in = $datas->re_assembling_in;
            $estimasi->re_assembling_out = $datas->re_assembling_out;
            $estimasi->re_assembling_status = $datas->re_assembling_status;
            $estimasi->id_lead          = $datas->id_lead;
            $estimasi->re_assembling_pause    = $datas->re_assembling_pause;
            $estimasi->re_assembling_start    = $datas->re_assembling_start;
            $estimasi->re_assembling_note     = $datas->re_assembling_note;
            $estimasi->re_assembling_lead     = $datas->re_assembling_lead;
            $estimasi->total_lead           = $datas->total_lead;
            $estimasi->is_confirm_teknisi = $datas->is_confirm_teknisi;
            $estimasi->ket              = $datas->re_assembling_ket;
            array_push($listEstimasi, $estimasi);
        }

        $data = array(
                        'title'             => 'Re-Assembling | U-Care',
                        'listEstimasi'      => $listEstimasi,
                        'isi'               => 'admin/dashboard/teknisi/atReAssembling',
                        'dataScript'        => 'admin/dataScript/tabel-script',
                        );
        $this->load->view('admin/_layout/wrapper', $data);
    }

    public function atWashing(){
       // $id_user = $this->session->userdata('id_user');
        $queryEstimasi  = ' SELECT *, u.nama_lengkap_user as namaSa, uu.nama_lengkap_user as namaTeknisi FROM estimasi
                            LEFT JOIN customer ON customer.id_customer = estimasi.id_customer
                            LEFT JOIN jenis_kendaran ON jenis_kendaran.id_jenis = estimasi.id_jenis
                            LEFT JOIN user u ON u.id_user = estimasi.id_user
                            LEFT JOIN user uu ON uu.id_user = estimasi.tim_teknisi
                            LEFT JOIN summary ON summary.id_estimasi_summary = estimasi.id_estimasi
                            LEFT JOIN summary_lead ON summary_lead.id_estimasi_lead = estimasi.id_estimasi
                            WHERE estimasi.status_produksi = "7" AND estimasi.is_confirm_teknisi IS NOT NULL';
        $listEstimasi = array();
        $dataEstimasi = $this->Crud->q($queryEstimasi);
        
        foreach($dataEstimasi as $datas){
            $estimasi = new stdClass();
            $estimasi->nomor_wo         = $datas->nomor_wo;
            $estimasi->id_estimasi      = $datas->id_estimasi;
            $estimasi->no_polisi        = $datas->no_polisi;
            $estimasi->nama_lengkap     = $datas->nama_lengkap;
            $estimasi->tgl_masuk        = $datas->tgl_masuk;
            $estimasi->tgl_janji_penyerahan       = $datas->tgl_janji_penyerahan;
            $estimasi->nama_sa          = $datas->namaSa;
            $estimasi->nama_teknisi     = $datas->namaTeknisi;
            $estimasi->kategori_jasa    = $datas->kategori_jasa;
            $estimasi->status_produksi  = $datas->status_produksi;
            $estimasi->washing_in = $datas->wasling_in;
            $estimasi->washing_out = $datas->wasling_out;
            $estimasi->washing_status = $datas->washing_status;
            $estimasi->id_lead          = $datas->id_lead;
            $estimasi->washing_pause    = $datas->washing_pause;
            $estimasi->washing_start    = $datas->washing_start;
            $estimasi->washing_note     = $datas->washing_note;
            $estimasi->washing_lead     = $datas->washing_lead;
            $estimasi->total_lead           = $datas->total_lead;
            $estimasi->is_confirm_teknisi = $datas->is_confirm_teknisi;
            $estimasi->ket              = $datas->wasling_ket;
            array_push($listEstimasi, $estimasi);
        }

        $data = array(
                        'title'             => 'Washing | U-Care',
                        'listEstimasi'      => $listEstimasi,
                        'isi'               => 'admin/dashboard/teknisi/atWashing',
                        'dataScript'        => 'admin/dataScript/tabel-script',
                        );
        // var_dump($listEstimasi);
        $this->load->view('admin/_layout/wrapper', $data);
    }

    public function atFinalInspection(){
       // $id_user = $this->session->userdata('id_user');
        $queryEstimasi  = ' SELECT *, u.nama_lengkap_user as namaSa, uu.nama_lengkap_user as namaTeknisi FROM estimasi
                            LEFT JOIN customer ON customer.id_customer = estimasi.id_customer
                            LEFT JOIN jenis_kendaran ON jenis_kendaran.id_jenis = estimasi.id_jenis
                            LEFT JOIN user u ON u.id_user = estimasi.id_user
                            LEFT JOIN user uu ON uu.id_user = estimasi.tim_teknisi
                            LEFT JOIN summary ON summary.id_estimasi_summary = estimasi.id_estimasi
                            LEFT JOIN summary_lead ON summary_lead.id_estimasi_lead = estimasi.id_estimasi
                            WHERE  estimasi.status_produksi = "8" AND estimasi.is_confirm_teknisi IS NOT NULL';
        $listEstimasi = array();
        $dataEstimasi = $this->Crud->q($queryEstimasi);
        
        foreach($dataEstimasi as $datas){
            $estimasi = new stdClass();
            $estimasi->nomor_wo         = $datas->nomor_wo;
            $estimasi->id_estimasi      = $datas->id_estimasi;
            $estimasi->no_polisi        = $datas->no_polisi;
            $estimasi->nama_lengkap     = $datas->nama_lengkap;
            $estimasi->tgl_masuk        = $datas->tgl_masuk;
            $estimasi->tgl_janji_penyerahan       = $datas->tgl_janji_penyerahan;
            $estimasi->nama_sa          = $datas->namaSa;
            $estimasi->nama_teknisi     = $datas->namaTeknisi;
            $estimasi->kategori_jasa    = $datas->kategori_jasa;
            $estimasi->status_produksi  = $datas->status_produksi;
            $estimasi->final_inspection_in = $datas->final_inspection_in;
            $estimasi->final_inspection_out = $datas->final_inspection_out;
            $estimasi->final_inspection_status = $datas->final_inspection_status;
            $estimasi->id_lead          = $datas->id_lead;
            $estimasi->final_inspection_pause    = $datas->final_inspection_pause;
            $estimasi->final_inspection_start    = $datas->final_inspection_start;
            $estimasi->final_inspection_note     = $datas->final_inspection_note;
            $estimasi->final_inspection_lead     = $datas->final_inspection_lead;
            $estimasi->total_lead           = $datas->total_lead;
            $estimasi->final_inspection_qc = $datas->final_inspection_qc;
            $estimasi->is_confirm_teknisi = $datas->is_confirm_teknisi;
            $estimasi->ket              = $datas->final_inspection_ket;
            $queryCountNotReadyParts = 'SELECT COUNT(*) as totNotReadyParts FROM detail_estimasi 
                                        LEFT JOIN item ON item.id_item = detail_estimasi.id_item
                                        WHERE id_estimasi = "'.$datas->id_estimasi.'" 
                                        AND item.tipe_item = "2" 
                                        AND detail_estimasi.ata IS NULL';
            $dataCountNotReadyParts = $this->Crud->q($queryCountNotReadyParts);
            foreach($dataCountNotReadyParts as $dataCount){
                $estimasi->countNotReadyParts = $dataCount->totNotReadyParts;
            }
            array_push($listEstimasi, $estimasi);
        }

        $data = array(
                        'title'             => 'Final Inspection | U-Care',
                        'listEstimasi'      => $listEstimasi,
                        'isi'               => 'admin/dashboard/teknisi/atFinalInspection',
                        'dataScript'        => 'admin/dataScript/tabel-script',
                        );
        $this->load->view('admin/_layout/wrapper', $data);
    }


     public function customer($id_customer = null) {

        $queryEstimasi  = ' SELECT * FROM estimasi
                            LEFT JOIN customer ON customer.id_customer = estimasi.id_customer
                            LEFT JOIN jenis_kendaran ON jenis_kendaran.id_jenis = estimasi.id_jenis
                            LEFT JOIN summary ON summary.id_estimasi_summary = estimasi.id_estimasi
                            LEFT JOIN user ON user.id_user = estimasi.id_user
                            WHERE estimasi.id_customer = "'.$id_customer.'"';

        $queryCustomer  = ' SELECT * FROM estimasi
                            LEFT JOIN customer ON customer.id_customer = estimasi.id_customer
                            LEFT JOIN jenis_kendaran ON jenis_kendaran.id_jenis = estimasi.id_jenis';

        $queryTimTeknisi = 'SELECT * FROM estimasi 
                            LEFT JOIN user ON user.id_user = estimasi.tim_teknisi
                           ';
        
        $detailEstimasi = array();
        $dataEstimasi = $this->Crud->q($queryEstimasi);
        $dataTimTeknisi = $this->Crud->q($queryTimTeknisi);
        foreach($dataEstimasi as $datas){
            $estimasi = new stdClass();
            // list & detail customer
            $estimasi->id_customer            = $datas->id_customer;
            $estimasi->no_polisi              = $datas->no_polisi;
            $estimasi->nama_customer          = $datas->nama_lengkap;
            $estimasi->no_hp_customer         = $datas->no_hp;
            $estimasi->alamat_customer        = $datas->alamat;
            // track record customer
            $estimasi->jenis_customer         = $datas->jenis_customer;
            $estimasi->tgl_estimasi           = $datas->tgl_estimasi;
            $estimasi->tgl_masuk              = $datas->tgl_masuk;
            $estimasi->tgl_janji_penyerahan   = $datas->tgl_janji_penyerahan;
            $estimasi->tgl_selesai            = $datas->final_inspection_out;
            $estimasi->tgl_penyerahan         = $datas->tgl_penyerahan;
            $estimasi->nama_lengkap_user      = $datas->nama_lengkap_user;
            $estimasi->status_produksi        = $datas->status_produksi;

            $estimasi->id_estimasi            = $datas->id_estimasi;
            
            foreach($dataTimTeknisi as $teknisi){
                if($datas->tim_teknisi == $teknisi->tim_teknisi){
                    $estimasi->nama_teknisi = $teknisi->nama_lengkap_user;
                    $estimasi->id_teknisi   = $teknisi->id_user;
                }
            }
            array_push($detailEstimasi, $estimasi);
        }                    

        if ($id_customer !== null) {

            $where      = array( 'id_customer' => $id_customer );
            $content    = 'admin/dashboard/customer/customerDetail';

            $data = array(  'title'     => 'Detail Customer | U-Care',
                            'isi'       => $content,
                            'data'      => $this->Crud->gw('customer', $where),
                            'dataEstimasi' => $detailEstimasi,
                            'dataScript'=> 'admin/dataScript/tabel-script' );
        }else{
            $data = array(  'title'     => 'Customer | U-Care',
                            'isi'       => 'admin/dashboard/customer/customer',
                            'data'      => $this->Crud->q($queryCustomer),
                            'dataScript'=> 'admin/dataScript/tabel-script' );
        }

        $this->load->view('admin/_layout/wrapper', $data);
    }

    public function get_customer_json() { //get product data and encode to be JSON object
      header('Content-Type: application/json');
      echo $this->PimpinanModel->get_all_customer();
  }

    public function estimasiOut($id_estimasi = null) {
        //$id_user    = $this->session->userdata('id_user');
        $queryEstimasi  = ' SELECT * FROM estimasi
                            LEFT JOIN customer ON customer.id_customer = estimasi.id_customer
                            LEFT JOIN jenis_kendaran ON jenis_kendaran.id_jenis = estimasi.id_jenis
                            LEFT JOIN user ON user.id_user = estimasi.id_user
                            WHERE estimasi.status_inout = "0"';
        if ($id_estimasi !== null) {

            $where      = array( 'id_estimasi' => $id_estimasi );
            $content    = 'admin/dashboard/estimasi/estimasiDetail';

            $data = array(  'title'     => 'Detail Estimasi | U-Care',
                            'isi'       => $content,
                            'data'      => $this->Crud->gw( 'estimasi', $where),
                            'dataScript'=> 'admin/dataScript/tabel-script' );
        }else{
            $data = array(  'title'     => 'Estimasi | U-Care',
                            'isi'       => 'admin/dashboard/estimasi/estimasiOut',
                            'data'      => $this->Crud->q($queryEstimasi),
                            'dataScript'=> 'admin/dataScript/tabel-script' );
        }

        $this->load->view('admin/_layout/wrapper', $data);
    }

    public function get_estimasi_out_json() { //get product data and encode to be JSON object
      header('Content-Type: application/json');
      echo $this->PimpinanModel->get_all_estimasi_out();
  }

    public function estimasiIn($id_estimasi = null) {
        //$id_user    = $this->session->userdata('id_user');
        $queryEstimasi  = ' SELECT * FROM estimasi
                            LEFT JOIN customer ON customer.id_customer = estimasi.id_customer
                            LEFT JOIN jenis_kendaran ON jenis_kendaran.id_jenis = estimasi.id_jenis
                            LEFT JOIN user ON user.id_user = estimasi.id_user
                            WHERE estimasi.status_inout = "1"';
        if ($id_estimasi !== null) {

            $where      = array( 'id_estimasi' => $id_estimasi );
            $content    = 'admin/dashboard/estimasi/estimasiDetail';

            $data = array(  'title'     => 'Detail Estimasi | U-Care',
                            'isi'       => $content,
                            'data'      => $this->Crud->gw( 'estimasi', $where),
                            'dataScript'=> 'admin/dataScript/tabel-script' );
        }else{
            $data = array(  'title'     => 'Estimasi | U-Care',
                            'isi'       => 'admin/dashboard/estimasi/estimasiIn',
                            'data'      => $this->Crud->q($queryEstimasi),
                            'dataScript'=> 'admin/dataScript/tabel-script' );
        }
        $this->load->view('admin/_layout/wrapper', $data);
    }

    public function get_estimasiin_json() { //get product data and encode to be JSON object
      header('Content-Type: application/json');
      echo $this->PimpinanModel->get_all_estimasi_in();
  }

    public function ready($id_estimasi = null){
        //$id_user = $this->session->userdata('id_user');
        if($id_estimasi != null){
            $queryEstimasi  = ' SELECT * FROM estimasi
                                INNER JOIN customer ON customer.id_customer = estimasi.id_customer
                                INNER JOIN jenis_kendaran ON jenis_kendaran.id_jenis = estimasi.id_jenis
                                INNER JOIN user ON user.id_user = estimasi.id_user
                                WHERE estimasi.status_inout = "1" AND estimasi.nomor_wo IS NOT NULL
                                ';
            
            $queryPartsJasa      = 'SELECT * FROM detail_estimasi
                                    LEFT JOIN item ON item.id_item = detail_estimasi.id_item
                                    WHERE detail_estimasi.id_estimasi = "'.$id_estimasi.'"
                                    ORDER BY item.tipe_item';

            
            $data = array(
                            'title'             => 'Teknisi | U-Care',
                            'isi'               => 'admin/dashboard/teknisi/ready_detail',
                            'dataPartsJasa'     => $this->Crud->q($queryPartsJasa),
                            'dataEstimasi'      => $this->Crud->q($queryEstimasi),
                            'dataScript'        => 'admin/dataScript/tabel-script',
                        );
        }else{
            $queryEstimasi  = ' SELECT *, u.nama_lengkap_user as namaSa, uu.nama_lengkap_user as namaTeknisi FROM estimasi
                                LEFT JOIN customer ON customer.id_customer = estimasi.id_customer
                                LEFT JOIN jenis_kendaran ON jenis_kendaran.id_jenis = estimasi.id_jenis
                                LEFT JOIN user u ON u.id_user = estimasi.id_user
                                LEFT JOIN user uu ON uu.id_user = estimasi.tim_teknisi
                                WHERE estimasi.is_confirm_teknisi IS NULL AND estimasi.nomor_wo IS NOT NULL AND estimasi.jenis_estimasi != 1 AND estimasi.status_inout = 1 AND estimasi.tim_teknisi IS NOT NULL
                                ';
            
            $listEstimasi = array();
            $dataEstimasi = $this->Crud->q($queryEstimasi);
            
            foreach($dataEstimasi as $datas){
                $estimasi = new stdClass();
                $estimasi->nomor_wo         = $datas->nomor_wo;
                $estimasi->id_estimasi      = $datas->id_estimasi;
                $estimasi->no_polisi        = $datas->no_polisi;
                $estimasi->nama_lengkap     = $datas->nama_lengkap;
                $estimasi->tgl_masuk        = $datas->tgl_masuk;
                $estimasi->tgl_janji_penyerahan       = $datas->tgl_janji_penyerahan;
                $estimasi->nama_sa          = $datas->namaSa;
                $estimasi->teknisi          = $datas->namaTeknisi;
                $estimasi->kategori_jasa    = $datas->kategori_jasa;
                $estimasi->status_produksi  = $datas->status_produksi;
                
                array_push($listEstimasi, $estimasi);

            }

            $data = array(
                            'title'             => 'Teknisi | U-Care',
                            'listEstimasi'      => $listEstimasi,
                            'isi'               => 'admin/dashboard/teknisi/ready',
                            'dataScript'        => 'admin/dataScript/tabel-script',
                        );
        }
        $this->load->view('admin/_layout/wrapper', $data);
    }

    public function estimasiToExcel(){
      $input = $this->input->post(NULL, TRUE);

      $filter = $input['dateInOut'];
      $dateFrom = $input['dateFrom'];
      $dateTo = $input['dateTo'];

      if($filter == "in"){
         $queryEstimasi = ' SELECT * FROM estimasi
                            LEFT JOIN customer ON customer.id_customer = estimasi.id_customer
                            LEFT JOIN jenis_kendaran ON jenis_kendaran.id_jenis = estimasi.id_jenis
                            LEFT JOIN user ON user.id_user = estimasi.id_user
                            WHERE estimasi.tgl_masuk >= "'.$dateFrom.'" AND estimasi.tgl_masuk <= "'.$dateTo.'" AND estimasi.status_inout = "1"
                           ';    
      }else if($filter == "out"){
        $queryEstimasi = ' SELECT * FROM estimasi
                            LEFT JOIN customer ON customer.id_customer = estimasi.id_customer
                            LEFT JOIN jenis_kendaran ON jenis_kendaran.id_jenis = estimasi.id_jenis
                            LEFT JOIN user ON user.id_user = estimasi.id_user
                            WHERE estimasi.tgl_estimasi >= "'.$dateFrom.'" AND estimasi.tgl_estimasi <= "'.$dateTo.'" AND estimasi.status_inout = "0"
                           ';
      }else{
        $queryEstimasi = ' SELECT * FROM estimasi
                            LEFT JOIN customer ON customer.id_customer = estimasi.id_customer
                            LEFT JOIN jenis_kendaran ON jenis_kendaran.id_jenis = estimasi.id_jenis
                            LEFT JOIN user ON user.id_user = estimasi.id_user
                            WHERE estimasi.tgl_estimasi >= "'.$dateFrom.'" AND estimasi.tgl_estimasi <= "'.$dateTo.'" 
                           ';
      }

      $data = array(
            'data' => $this->Crud->q($queryEstimasi),
            //'data' => $this->Crud->ga('detail_estimasi'),
        );
        $this->load->view('admin/dashboard/pimpinan/excel_sa', $data);
    }

    public function partsToExcel(){
      $input = $this->input->post(NULL, TRUE);

      $filter = $input['dateInOutParts'];
      $dateFrom = $input['dateFromParts'];
      $dateTo = $input['dateToParts'];

      if($filter == "in"){
         $queryEstimasi = ' SELECT * FROM estimasi
                            LEFT JOIN customer ON customer.id_customer = estimasi.id_customer
                            LEFT JOIN user ON user.id_user = estimasi.id_user
                            LEFT JOIN jenis_kendaran ON jenis_kendaran.id_jenis = estimasi.id_jenis
                            WHERE (estimasi.tgl_estimasi >= "'.$dateFrom.'" AND estimasi.tgl_estimasi <= "'.$dateTo.'") AND estimasi.status_inout = "1" AND (estimasi.jenis_estimasi = "1" OR estimasi.jenis_estimasi = "2") AND estimasi.done_order = "2"
                           ';    
      }else if($filter == "out"){
        $queryEstimasi = ' SELECT * FROM estimasi
                            LEFT JOIN customer ON customer.id_customer = estimasi.id_customer
                            LEFT JOIN user ON user.id_user = estimasi.id_user
                            LEFT JOIN jenis_kendaran ON jenis_kendaran.id_jenis = estimasi.id_jenis
                            WHERE (estimasi.tgl_estimasi >= "'.$dateFrom.'" AND estimasi.tgl_estimasi <= "'.$dateTo.'") AND estimasi.status_inout = "1" AND (estimasi.jenis_estimasi = "1" OR estimasi.jenis_estimasi = "2") AND (estimasi.done_order = "1" OR estimasi.done_order = "0")
                           ';
      }else{
        $queryEstimasi = ' SELECT * FROM estimasi
                            LEFT JOIN customer ON customer.id_customer = estimasi.id_customer
                            LEFT JOIN user ON user.id_user = estimasi.id_user
                            LEFT JOIN jenis_kendaran ON jenis_kendaran.id_jenis = estimasi.id_jenis
                            WHERE (estimasi.tgl_estimasi >= "'.$dateFrom.'" AND estimasi.tgl_estimasi <= "'.$dateTo.'") AND estimasi.status_inout = "1" AND (estimasi.jenis_estimasi = "1" OR estimasi.jenis_estimasi = "2") AND (estimasi.done_order = "1" OR estimasi.done_order = "0" OR estimasi.done_order="2")
                           ';
      }

      $data = array(
            'data' => $this->Crud->q($queryEstimasi),
            //'data' => $this->Crud->ga('detail_estimasi'),
        );
        $this->load->view('admin/dashboard/pimpinan/excel_parts', $data);
    }

    public function importJasa(){
      $input = $this->input->post(NULL, TRUE);
      $id_jenis = $input['id_jenis'];

        if(isset($_POST['upload'])){//untuk cek apakah sdh terklik actionnya
            if($_FILES['fileImport']['name']){//kondisi file required
                $fileName = explode('.', $_FILES['fileImport']['name']);//memecah ke dalam array untuk cari nama file

                if(end($fileName) == "csv"){//cek ekstensi
                    $handle = fopen($_FILES['fileImport']['tmp_name'], 'r');
                    while($data = fgetcsv($handle)){//fetch data file
                        //mengambil data dari csv ke variabel
                        $nama_item =  $this->db->escape_str($data[0]);
                        $harga_item = $this->db->escape_str($data[1]);
                        $posisi_jasa = $this->db->escape_str($data[2]);
                        //echo $nomor_parts.' '.$harga_item."<br/>";

                        $query = "
                            INSERT INTO item (id_jenis, nama_item, harga_item, tipe_item, posisi_jasa)
                            VALUES ('$id_jenis', '$nama_item', '$harga_item', '1', '$posisi_jasa')
                        ";
                        $this->db->query($query);
                    }
                    fclose($handle);//close file after cache
                    $this->session->set_flashdata('info', 'Data Sukses Diupdate !');
                    redirect("pimpinan/jasa");
                }else{
                    $this->session->set_flashdata('info', 'Gagal !<br><br> Jenis File Bukan .csv.');
                    redirect('pimpinan/jasa');
                }

            }else{//kondisi tidak ada file
                $this->session->set_flashdata('info', 'Gagal !<br><br> Pilih File Dengan Cermat.');
                redirect('pimpinan/jasa');
            }
        }

    }


  public function importJasaUpdate(){

        if(isset($_POST['upload'])){//untuk cek apakah sdh terklik actionnya
            if($_FILES['fileImport']['name']){//kondisi file required
                $fileName = explode('.', $_FILES['fileImport']['name']);//memecah ke dalam array untuk cari nama file

                if(end($fileName) == "csv"){//cek ekstensi
                    $handle = fopen($_FILES['fileImport']['tmp_name'], 'r');
                    while($data = fgetcsv($handle)){//fetch data file
                        //mengambil data dari csv ke variabel
                        $id =  $this->db->escape_str($data[0]);
                        $nama_item = $this->db->escape_str($data[1]);
                        $harga_item = $this->db->escape_str($data[2]);
                        //echo $nomor_parts.' '.$harga_item."<br/>";

                        $query = "
                            UPDATE item
                            SET nama_item = '$nama_item',
                            harga_item = '$harga_item'
                            WHERE id_item = '$id'
                        ";
                        $this->db->query($query);
                    }
                    fclose($handle);//close file after cache
                    $this->session->set_flashdata('info', 'Data Sukses Diupdate !');
                    redirect("pimpinan/jasa");
                }else{
                    $this->session->set_flashdata('info', 'Gagal !<br><br> Jenis File Bukan .csv.');
                    redirect('pimpinan/jasa');
                }

            }else{//kondisi tidak ada file
                $this->session->set_flashdata('info', 'Gagal !<br><br> Pilih File Dengan Cermat.');
                redirect('pimpinan/jasa');
            }
        }

    }

    public function estimasiHapus($id){
      $where = array(
            'id_estimasi' => $id,
        );
        $this->Crud->d('estimasi', $where);

        $total = $this->PimpinanModel->hitungDetail($id);
        if($total>0){
          $this->Crud->d('detail_estimasi', $where);
        }
        $this->session->set_flashdata('info', 'Data Berhasil Dihapus');
        redirect('pimpinan/estimasiOut'); 
    }


}
