<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Service_advisor extends CI_Controller {

    public function __construct() {
        parent::__construct();
        date_default_timezone_set('Asia/Makassar');
        if($this->session->userdata('status') != "login"){
            redirect('login');
        }
        $method = $this->router->fetch_method();
        if($method != 'delivery' && $method != 'insertTglPenyerahan' && $method != 'printEstimasi' && $method != 'exportEstimasi' && $method != 'get_delivery_readySA_json' && $method != 'get_delivery_doneSA_json' && $method != 'get_delivery_readyPimpinan_json' && $method != 'get_delivery_donePimpinan_json'){
            if($this->session->userdata('level') != "service advisor" && $this->session->userdata('level') != "pimpinan"){
            $level = $this->session->userdata('level');
            $controller = preg_replace("/[^a-z]/", "_", $level);
            redirect($controller);
            }
        }

    }

    public function index(){
        $data = array(  'title'     => 'U-Care',
                        'isi'       => 'admin/dashboard/beranda',
                        'dataScript'=> 'admin/dataScript/beranda-script');
        $this->load->view('admin/_layout/wrapper', $data);
    }

    public function customer($id_customer = null) {

        $queryEstimasi  = ' SELECT * FROM estimasi
                            LEFT JOIN customer ON customer.id_customer = estimasi.id_customer
                            LEFT JOIN jenis_kendaran ON jenis_kendaran.id_jenis = estimasi.id_jenis
                            LEFT JOIN summary ON summary.id_estimasi_summary = estimasi.id_estimasi
                            LEFT JOIN user ON user.id_user = estimasi.id_user
                            WHERE estimasi.id_customer = "'.$id_customer.'"';

        $queryCustomer  = ' SELECT * FROM estimasi
                            LEFT JOIN customer ON customer.id_customer = estimasi.id_customer
                            LEFT JOIN jenis_kendaran ON jenis_kendaran.id_jenis = estimasi.id_jenis';

        $queryTimTeknisi = 'SELECT * FROM estimasi 
                            LEFT JOIN user ON user.id_user = estimasi.tim_teknisi
                           ';
        
        $detailEstimasi = array();
        $dataEstimasi = $this->Crud->q($queryEstimasi);
        $dataTimTeknisi = $this->Crud->q($queryTimTeknisi);
        foreach($dataEstimasi as $datas){
            $estimasi = new stdClass();
            // list & detail customer
            $estimasi->id_customer            = $datas->id_customer;
            $estimasi->no_polisi              = $datas->no_polisi;
            $estimasi->nama_customer          = $datas->nama_lengkap;
            $estimasi->no_hp_customer         = $datas->no_hp;
            $estimasi->alamat_customer        = $datas->alamat;
            // track record customer
            $estimasi->jenis_customer         = $datas->jenis_customer;
            $estimasi->tgl_estimasi           = $datas->tgl_estimasi;
            $estimasi->tgl_masuk              = $datas->tgl_masuk;
            $estimasi->tgl_janji_penyerahan   = $datas->tgl_janji_penyerahan;
            $estimasi->tgl_selesai            = $datas->final_inspection_out;
            $estimasi->tgl_penyerahan         = $datas->tgl_penyerahan;
            $estimasi->nama_lengkap_user      = $datas->nama_lengkap_user;
            $estimasi->status_produksi        = $datas->status_produksi;

            $estimasi->id_estimasi            = $datas->id_estimasi;
            
            foreach($dataTimTeknisi as $teknisi){
                if($datas->tim_teknisi == $teknisi->tim_teknisi){
                    $estimasi->nama_teknisi = $teknisi->nama_lengkap_user;
                    $estimasi->id_teknisi   = $teknisi->id_user;
                }
            }
            array_push($detailEstimasi, $estimasi);
        }                    

        if ($id_customer !== null) {

            $where      = array( 'id_customer' => $id_customer );
            $content    = 'admin/dashboard/customer/customerDetail';

	        $data = array(  'title'     => 'Detail Customer | U-Care',
	                        'isi'       => $content,
	                        'data'      => $this->Crud->gw('customer', $where),
                            'dataEstimasi' => $detailEstimasi,
                            'dataScript'=> 'admin/dataScript/tabel-script' );
        }else{
            $data = array(  'title'     => 'Customer | U-Care',
                            'isi'       => 'admin/dashboard/customer/customer',
                            'data'      => $this->Crud->q($queryCustomer),
                            'dataScript'=> 'admin/dataScript/tabel-script' );
        }

        $this->load->view('admin/_layout/wrapper', $data);
    }

    public function get_customer_json() { //get product data and encode to be JSON object
      header('Content-Type: application/json');
      echo $this->PimpinanModel->get_all_customer();
  }

    // read
    public function customerCreate() {
        $data = array(  'title'     => 'Tambah Customer | U-Care',
                        'isi'       => 'admin/form/formCustomer',
                        'dataScript'=> 'admin/dataScript/form-script' );
        $this->load->view('admin/_layout/wrapper', $data);
    }

    // create
    public function customerCreateAction() {
        $input = $this->input->post(NULL, TRUE);

        $items = array(
                        'nama_lengkap'   => $input['nama_lengkap'],
                        'no_hp'    => $input['no_hp'],
                        'alamat' => $input['alamat'],
                      );
        $this->Crud->i('customer', $items);

        redirect(base_url('service_advisor/customer'));
    }

    // read
    public function customerUpdate($id_customer) {
        $where      = array( 'id_customer' => $id_customer );
        $data = array(  'title'     => 'Edit Data Customer | Confie-id',
                        'isi'       => 'admin/form/formCustomerUpdate',
                        'data'      => $this->Crud->gw( 'customer', $where ),
                        'dataScript'=> 'admin/dataScript/form-script' );

        $this->load->view('admin/_layout/wrapper', $data);
    }

    // update
    public function customerUpdateAction($id_customer) {
        $input = $this->input->post(NULL, TRUE);
        $where = array( 'id_customer' => $id_customer );

        $items = array(
                        'nama_lengkap'   => $input['nama_lengkap'],
                        'no_hp'    => $input['no_hp'],
                        'alamat' => $input['alamat'],
                      );

        $this->Crud->u('customer', $items, $where );

        redirect(base_url('service_advisor/customer'));
    }

    // delete
    public function customerDelete($id_customer) {
        $where = array( 'id_customer' => $id_customer );
        $this->Crud->d('customer', $where);
        redirect(base_url('service_advisor/customer'));
    }

    public function estimasiAll($id_estimasi = null){
        //$id_user    = $this->session->userdata('id_user');
        $queryEstimasi  = ' SELECT * FROM estimasi
                            LEFT JOIN customer ON customer.id_customer = estimasi.id_customer
                            LEFT JOIN jenis_kendaran ON jenis_kendaran.id_jenis = estimasi.id_jenis
                            LEFT JOIN user ON user.id_user = estimasi.id_user
                            ';
        if ($id_estimasi !== null) {

            $where      = array( 'id_estimasi' => $id_estimasi );
            $content    = 'admin/dashboard/estimasi/estimasiDetail';

            $data = array(  'title'     => 'Detail Estimasi | U-Care',
                            'isi'       => $content,
                            'data'      => $this->Crud->gw( 'estimasi', $where),
                            'dataScript'=> 'admin/dataScript/tabel-script' );
        }else{
            $data = array(  'title'     => 'Estimasi | U-Care',
                            'isi'       => 'admin/dashboard/estimasi/estimasiIn',
                            'data'      => $this->Crud->q($queryEstimasi),
                            'dataScript'=> 'admin/dataScript/tabel-script' );
        }
        $this->load->view('admin/_layout/wrapper', $data);
    }

    public function get_estimasiAll_json() { //get product data and encode to be JSON object
      header('Content-Type: application/json');
      echo $this->PimpinanModel->get_all_estimasi_all();
  }

    public function estimasiOut($id_estimasi = null) {
        $id_user    = $this->session->userdata('id_user');
        $queryEstimasi  = ' SELECT * FROM estimasi
                            LEFT JOIN customer ON customer.id_customer = estimasi.id_customer
                            LEFT JOIN jenis_kendaran ON jenis_kendaran.id_jenis = estimasi.id_jenis
                            LEFT JOIN user ON user.id_user = estimasi.id_user
                            WHERE estimasi.status_inout = "0" AND estimasi.id_user = "'.$id_user.'"';
        if ($id_estimasi !== null) {

            $where      = array( 'id_estimasi' => $id_estimasi );
            $content    = 'admin/dashboard/estimasi/estimasiDetail';

            $data = array(  'title'     => 'Detail Estimasi | U-Care',
                            'isi'       => $content,
                            'data'      => $this->Crud->gw( 'estimasi', $where),
                            'dataScript'=> 'admin/dataScript/tabel-script' );
        }else{
            $data = array(  'title'     => 'Estimasi | U-Care',
                            'isi'       => 'admin/dashboard/estimasi/estimasiOut',
                            'data'      => $this->Crud->q($queryEstimasi),
                            'dataScript'=> 'admin/dataScript/tabel-script' );
        }

        $this->load->view('admin/_layout/wrapper', $data);
    }

    public function get_estimasi_out_json() { //get product data and encode to be JSON object
      header('Content-Type: application/json');
      echo $this->PimpinanModel->get_all_estimasi_outSA();
  }

     public function estimasiIn($id_estimasi = null) {
        $id_user    = $this->session->userdata('id_user');
        $queryEstimasi  = ' SELECT * FROM estimasi
                            LEFT JOIN customer ON customer.id_customer = estimasi.id_customer
                            LEFT JOIN jenis_kendaran ON jenis_kendaran.id_jenis = estimasi.id_jenis
                            LEFT JOIN user ON user.id_user = estimasi.id_user
                            WHERE estimasi.status_inout = "1" AND estimasi.id_user = "'.$id_user.'"';
        if ($id_estimasi !== null) {

            $where      = array( 'id_estimasi' => $id_estimasi );
            $content    = 'admin/dashboard/estimasi/estimasiDetail';

            $data = array(  'title'     => 'Detail Estimasi | U-Care',
                            'isi'       => $content,
                            'data'      => $this->Crud->gw( 'estimasi', $where),
                            'dataScript'=> 'admin/dataScript/tabel-script' );
        }else{
            $data = array(  'title'     => 'Estimasi | U-Care',
                            'isi'       => 'admin/dashboard/estimasi/estimasiIn',
                            'data'      => $this->Crud->q($queryEstimasi),
                            'dataScript'=> 'admin/dataScript/tabel-script' );
        }
        $this->load->view('admin/_layout/wrapper', $data);
    }

    public function get_estimasiin_json() { //get product data and encode to be JSON object
      header('Content-Type: application/json');
      echo $this->PimpinanModel->get_all_estimasi_inSA();
  }

    // read
    public function estimasiCreate() {
        $query = 'SELECT * FROM jenis_kendaran ORDER BY nama_jenis ASC';
        $data = array(  'title'     => 'Tambah Estimasi | U-Care',
                        'isi'       => 'admin/form/formEstimasi',
                        'data'      => $this->Crud->q($query),
                        'dataColor'      => $this->Crud->ga('color'),
                        'dataScript'=> 'admin/dataScript/tabel-script' );
        $this->load->view('admin/_layout/wrapperv2', $data);
    }

    // create
    public function estimasiCreateAction() {
        $input      = $this->input->post(NULL, TRUE);
        $newCustomer = $this->input->post('newCustomer');
        $id_user    = $this->session->userdata('id_user');
        $date_now   = date('Y-m-d');
        if($input['newCustomer'] == 'Yes'){
            $itemsCustomer = array(
                                'nama_lengkap'  => $input['nama_lengkap'],
                                'no_hp'         => $input['no_hp'],
                                'alamat'        => $input['alamat'],
                              );
            $id_customer    = $this->Crud->i2('customer', $itemsCustomer);
            
            $itemsEstimasi  = array ('id_user'      => $id_user,     
                                     'id_customer'  => $id_customer,
                                     'id_jenis'     => $input['id_jenis'],
                                     'id_color'     => $input['id_color'],
                                     'tgl_estimasi'=> $date_now,
                                     'no_polisi'    => $input['no_polisi'],
                                     'no_rangka'    => $input['no_rangka']
                                    );

            $id_estimasi    = $this->Crud->i2('estimasi', $itemsEstimasi);
            $dataTimeProcess = array('id_estimasi' => $id_estimasi,
                                     'time_body_repair' => '1.5',
                                     'time_preparation' => '1.5',
                                     'time_masking' => '0.5',
                                     'time_painting' => '1.5',
                                     'time_polishing' => '1',
                                     'time_re_assembling' => '1',
                                     'time_washing' => '0.5',
                                     'time_final_inspection' => '0.5'
                );
            $this->Crud->i('time_process', $dataTimeProcess);
            $dataKedatanganParts = array('id_estimasi' => $id_estimasi,
                                         'depo'        => '0',
                                         'tam'         => '0',
                                         'pabrik'      => '0',
                                        );
            $this->Crud->i('kedatangan_parts', $dataKedatanganParts);
            redirect(base_url('service_advisor/addEstimasi/').$id_estimasi);
        
        }else{
            
            $where = array ('id_customer' => $input['idCustomer']);
            
            $dataCustomer = $this->Crud->gw('customer', $where);
            
            foreach ($dataCustomer as $datas){
                $id_customer = $datas->id_customer;
            }

            $itemsEstimasi  = array ('id_user'      => $id_user,     
                                     'id_customer'  => $id_customer,
                                     'id_jenis'     => $input['id_jenis'],
                                     'id_color'     => $input['id_color'],
                                     'tgl_estimasi'=> $date_now,
                                     'no_polisi'    => $input['no_polisi'],
                                     'no_rangka'    => $input['no_rangka']
                                    );

            $id_estimasi = $this->Crud->i2('estimasi', $itemsEstimasi);
            $dataTimeProcess = array('id_estimasi' => $id_estimasi,
                                    'time_body_repair' => '1.5',
                                    'time_preparation' => '1.5',
                                    'time_masking' => '0.5',
                                    'time_painting' => '1.5',
                                    'time_polishing' => '1',
                                    'time_re_assembling' => '1',
                                    'time_washing' => '0.5',
                                    'time_final_inspection' => '0.5'
                                );
            $this->Crud->i('time_process', $dataTimeProcess);
            $dataKedatanganParts = array('id_estimasi' => $id_estimasi,
                'depo'        => '0',
                'tam'         => '0',
                'pabrik'      => '0',
            );
            $this->Crud->i('kedatangan_parts', $dataKedatanganParts);
            redirect(base_url('service_advisor/addEstimasi/').$id_estimasi);
        }

        
    }

    public function addEstimasi($id_estimasi = null){
        if($id_estimasi !== null){
            $queryEstimasi  = ' SELECT * FROM estimasi
                                LEFT JOIN customer ON customer.id_customer = estimasi.id_customer
                                LEFT JOIN jenis_kendaran ON jenis_kendaran.id_jenis = estimasi.id_jenis
                                LEFT JOIN color ON color.id_color = estimasi.id_color
                                WHERE id_estimasi = "'.$id_estimasi.'"';
            $where = array('id_estimasi' => $id_estimasi);
            $dataEstimasiCheck = $this->Crud->qq($queryEstimasi);


            if($dataEstimasiCheck->num_rows() > 0){
                $dataEstimasi = $this->Crud->q($queryEstimasi);
                foreach ($dataEstimasi as $datas){
                    $id_jenis = $datas->id_jenis;
                }
                $queryJasa  = ' SELECT * FROM item WHERE id_jenis = "'.$id_jenis.'" AND harga_item != 0';
                // $queryParts = ' SELECT * FROM item WHERE tipe_item = "2" AND id_jenis != "0"';
                $data       = array ('title'              => 'Tambah Estimasi | U-Care',
                                     'isi'                => 'admin/form/formAddEstimasi',
                                     'dataEstimasi'       => $dataEstimasi,
                                     'dataTimeProcess'    => $this->Crud->gw('time_process', $where),
                                     'dataKedatanganParts' => $this->Crud->gw('kedatangan_parts', $where),
                                     'dataJasa'           => $this->Crud->q($queryJasa),
                                     'dataColor'          => $this->Crud->ga('color'),
                                     'dataAsuransi'       => $this->Crud->ga('asuransi'),
                                     'dataKendaraan'      => $this->Crud->ga('jenis_kendaran'),
                                     // 'dataParts'          => $this->Crud->q($queryParts),
                                     'dataScript'         => 'admin/dataScript/tabel-script' );
            }else{
                redirect(base_url('service_advisor/error404'));
            }
        }else{
            redirect(base_url('service_advisor/estimasi'));
        }
        $this->load->view('admin/_layout/wrapperv2', $data);
    }

    public function error404(){
        echo "Page is not Found !";
    }
    public function searchCustomer(){
        $keywords = $this->input->post('keywords');
        $query  = ' SELECT * FROM customer
                    LEFT JOIN estimasi ON customer.id_customer = estimasi.id_customer
                    LEFT JOIN jenis_kendaran ON jenis_kendaran.id_jenis = estimasi.id_jenis
                    LEFT JOIN color ON color.id_color = estimasi.id_color
                    WHERE estimasi.no_polisi LIKE "%'.$keywords.'%"';

        $data = $this->Crud->q($query);
        echo json_encode($data); 
    }

    public function checkRangka(){
        $no_rangka = $this->input->post('no_rangka');
        $exists = $this->Crud->no_rangka_exists($no_rangka);
        if($exists){
            echo "No Rangka Sudah Ada!!";
        }else{
            echo "";
        }
      }

    public function submitEstimasi(){
        $input              = $this->input->post(NULL, TRUE);
        $id_estimasi        = $input['id_estimasi'];
        $jenis_customer     = $input['jenis_customer'];
        $nama_asuransi      = $input['nama_asuransi'];

        $whereEstimasi  = array('id_estimasi' => $id_estimasi);
        $queryCountJasa = 'SELECT * FROM detail_estimasi 
                           LEFT JOIN item ON item.id_item = detail_estimasi.id_item
                           WHERE detail_estimasi.id_estimasi = "'.$id_estimasi.'"
                           AND (item.tipe_item = "1" OR item.tipe_item = "3" )';

        $queryCountParts = 'SELECT * FROM detail_estimasi 
                            LEFT JOIN item ON item.id_item = detail_estimasi.id_item
                            WHERE detail_estimasi.id_estimasi = "'.$id_estimasi.'"
                            AND (item.tipe_item = "2" OR item.tipe_item = "4" )';
        $countJasa  = $this->Crud->qq($queryCountJasa);
        $countParts = $this->Crud->qq($queryCountParts);
        
        
        if($countJasa->num_rows() >= 1 && $countParts->num_rows() >= 1){
            $jenis_estimasi = '2';
        }elseif($countJasa->num_rows() >= 1){
            $jenis_estimasi = '0';
        }else{
            $jenis_estimasi = '1';
        }

        if($countJasa->num_rows() >=1 && $countJasa->num_rows() <= 3 ){
            $kategori_jasa = 0;
        }elseif($countJasa->num_rows() >=4 && $countJasa->num_rows() <= 6){
            $kategori_jasa = 1;
        }elseif($countJasa->num_rows() >=7){
            $kategori_jasa = 2;
        }else{
            $kategori_jasa = null;
        }
        $items = array('jenis_customer' => $jenis_customer,
                       'nama_asuransi'  => $nama_asuransi,
                       'jenis_estimasi' => $jenis_estimasi,
                       'kategori_jasa'  => $kategori_jasa,
                        );
        
        if($this->Crud->u('estimasi', $items, $whereEstimasi)){
            $status = 'failed';
        }else {
            $status = 'success';
        }
        
        echo $status;
    }

    public function printEstimasi($id_estimasi = null){
        if($id_estimasi !== null){
            $where = array('id_estimasi' => $id_estimasi);
            $queryEstimasi  = ' SELECT * FROM estimasi
                                LEFT JOIN customer ON customer.id_customer = estimasi.id_customer
                                LEFT JOIN jenis_kendaran ON jenis_kendaran.id_jenis = estimasi.id_jenis
                                LEFT JOIN user ON user.id_user = estimasi.id_user
                                LEFT JOIN color ON color.id_color = estimasi.id_color
                                LEFT JOIN summary ON summary.id_estimasi_summary = estimasi.id_estimasi
                                WHERE id_estimasi = "'.$id_estimasi.'"';

            $queryItem      = ' SELECT *  FROM detail_estimasi
                                LEFT JOIN item ON item.id_item = detail_estimasi.id_item
                                WHERE detail_estimasi.id_estimasi = "'.$id_estimasi.'" 
                                ORDER BY item.tipe_item ASC
                              ';
            $queryLtJasa = 'SELECT SUM(lt_jasa) as ltJasa  FROM detail_estimasi
                            LEFT JOIN item ON item.id_item = detail_estimasi.id_item
                            WHERE detail_estimasi.id_estimasi = "'.$id_estimasi.'"';


            $dataEstimasiCheck = $this->Crud->qq($queryEstimasi);
            $dataEstimasi = $this->Crud->q($queryEstimasi);
            $isEdit = true;
            foreach($dataEstimasi as $datas){
                $no_polisi = $datas->no_polisi;
                if($datas->final_inspection_out){
                    $isEdit = false;
                }
            }
            if($dataEstimasiCheck->num_rows() > 0){


                $data       = array ('title'        => 'Print "'.$no_polisi.'" U-Care',
                                     'isi'          => 'admin/dashboard/estimasi/printEstimasi',
                                     'dataEstimasi' => $this->Crud->q($queryEstimasi),
                                     'dataItem'     => $this->Crud->q($queryItem),
                                     'ltJasa'       => $this->Crud->q($queryLtJasa),
                                     'dataTimeProcess' => $this->Crud->gw('time_process', $where),
                                     'dataKedatanganParts' => $this->Crud->gw('kedatangan_parts', $where),
                                     'isEdit'       => $isEdit,
                                     'dataScript'   => 'admin/dataScript/tabel-script' );
            }else{
                    redirect(base_url('service_advisor/error404'));
                }
        }else{
            redirect(base_url('service_advisor/estimasi'));
        }
        $this->load->view('admin/_layout/wrapperv2', $data);
    }

    public function printEstimasiAll($id_estimasi = null){
        if($id_estimasi !== null){
            $where = array('id_estimasi' => $id_estimasi);
            $queryEstimasi  = ' SELECT * FROM estimasi
                                LEFT JOIN customer ON customer.id_customer = estimasi.id_customer
                                LEFT JOIN jenis_kendaran ON jenis_kendaran.id_jenis = estimasi.id_jenis
                                LEFT JOIN user ON user.id_user = estimasi.id_user
                                LEFT JOIN color ON color.id_color = estimasi.id_color
                                LEFT JOIN summary ON summary.id_estimasi_summary = estimasi.id_estimasi
                                WHERE id_estimasi = "'.$id_estimasi.'"';

            $queryItem      = ' SELECT *  FROM detail_estimasi
                                LEFT JOIN item ON item.id_item = detail_estimasi.id_item
                                WHERE detail_estimasi.id_estimasi = "'.$id_estimasi.'" 
                                ORDER BY item.tipe_item ASC
                              ';
            $queryLtJasa = 'SELECT SUM(lt_jasa) as ltJasa  FROM detail_estimasi
                            LEFT JOIN item ON item.id_item = detail_estimasi.id_item
                            WHERE detail_estimasi.id_estimasi = "'.$id_estimasi.'"';


            $dataEstimasiCheck = $this->Crud->qq($queryEstimasi);
            $dataEstimasi = $this->Crud->q($queryEstimasi);
            $isEdit = false;
            foreach($dataEstimasi as $datas){
                $no_polisi = $datas->no_polisi;
            }
            if($dataEstimasiCheck->num_rows() > 0){


                $data       = array ('title'        => 'Print "'.$no_polisi.'" U-Care',
                                     'isi'          => 'admin/dashboard/estimasi/printEstimasi',
                                     'dataEstimasi' => $this->Crud->q($queryEstimasi),
                                     'dataItem'     => $this->Crud->q($queryItem),
                                     'ltJasa'       => $this->Crud->q($queryLtJasa),
                                     'dataTimeProcess' => $this->Crud->gw('time_process', $where),
                                     'dataKedatanganParts' => $this->Crud->gw('kedatangan_parts', $where),
                                     'isEdit'       => $isEdit,
                                     'dataScript'   => 'admin/dataScript/tabel-script' );
            }else{
                    redirect(base_url('service_advisor/error404'));
                }
        }else{
            redirect(base_url('service_advisor/estimasi'));
        }
        $this->load->view('admin/_layout/wrapperv2', $data);
    }

    public function changeStatus(){
        $input              = $this->input->post(NULL, TRUE);
        $id_estimasi        = $input['id_estimasi'];
        $tgl_janji_penyerahan    = $input['tgl_janji_penyerahan'];
        $nomor_wo    = $input['nomor_wo'];

        $where = array('id_estimasi' => $id_estimasi);
        $items = array('status_inout' => '1',
                       'tgl_janji_penyerahan' => $tgl_janji_penyerahan,
                       'nomor_wo' => $nomor_wo
                        );
        if(!$this->Crud->u('estimasi', $items, $where)){
            $status = 'success';
        }else {
            $status = 'failed';
        }
        echo json_encode(array('status' => $status));

    }

    private function rp($angka){
        $hasil_rupiah = "Rp " . number_format($angka,0,',','.');
        return $hasil_rupiah;
    }

    public function fetchJasa(){
        $id_estimasi = $this->input->post('id_estimasi');
        $where = array('id_estimasi' => $id_estimasi);
        $queryAddedJasa = ' SELECT * FROM detail_estimasi
                                LEFT JOIN item ON item.id_item = detail_estimasi.id_item
                                WHERE detail_estimasi.id_estimasi = "'.$id_estimasi.'" 
                                AND (item.tipe_item = "1" OR item.tipe_item = "3")
                                ORDER BY detail_estimasi.id_detail ASC
                              ';

        $dataFetchJasa = $this->Crud->q($queryAddedJasa);
        $dataEstimasi = $this->Crud->gw('estimasi', $where);
        $dataJasa = '';
        $subTotalJasa = 0;
        foreach($dataFetchJasa as $datas){
            $subTotalJasa += (int)$datas->harga_item;
             $dataJasa .='
                        <tr>
                            <td>'.substr($datas->nama_item, 0, 3).'</td>
                            <td>'.substr($datas->nama_item,4).'</td>
                            <td width="10px"><input type="number" style="text-align:center;"class="form-control countLT" value="'.$datas->lt_jasa.'" data-row="'.$datas->id_item.'"/> </td>
                            <td>'.$this->rp($datas->harga_item).'</td>
                            <td width="10" style="text-align:center">
                            <button type="button" class="btn btn-danger btn-xs waves-effect removeJasa" data-row="'.$datas->id_item.'" '.($datas->ptm_processed !== NULL ? 'style="display:none"':'').'><i class="material-icons">delete_forever</i></button></td>
                            
                        </tr>
                        ';
        }

        foreach ($dataEstimasi as $datas) {
            $diskonJasa = $datas->diskon_jasa;
            $approvalDiskonJasa = $datas->approval_diskon_jasa;
        }
        $data = array('dataJasa' => $dataJasa,
                      'subTotalJasa' => $subTotalJasa,
                      'diskonJasa' => $diskonJasa,
                      'approvalDiskonJasa' => $approvalDiskonJasa,
                      'dataFetchJasa' => $dataFetchJasa
                  );
        echo json_encode($data);
    }

    public function addJasa(){
        $id_estimasi = $this->input->post('id_estimasi');
        $id_item = $this->input->post('id_item');

        $items = array('id_estimasi' => $id_estimasi,
                       'id_item'     => $id_item,
                    );

        $checkJasa = $this->Crud->cw('detail_estimasi', $items);
        if($checkJasa >= 1){
            echo "failed";
        }else{
            $items = array('id_estimasi' => $id_estimasi,
                           'id_item'     => $id_item,
                           'lt_jasa'     => "1",
                          );
            $data = $this->Crud->i('detail_estimasi', $items);
        }

        echo "success";
    }

    public function removeJasa(){
        $id_estimasi = $this->input->post('id_estimasi');
        $id_item = $this->input->post('id_item');

        $where = array('id_estimasi' => $id_estimasi,
                       'id_item'     => $id_item,
                    );
        $data = $this->Crud->d('detail_estimasi', $where);

        echo "success deleted";
    }

    public function diskonJasa(){
        $id_estimasi = $this->input->post('id_estimasi');
        $diskon_jasa = $this->input->post('diskon_jasa');

        $where = array('id_estimasi' => $id_estimasi);
        $items = array('diskon_jasa'     => $diskon_jasa);
        $data = $this->Crud->u('estimasi', $items, $where);

        echo "diskon updated";
    }

    public function fetchParts(){
        $id_estimasi = $this->input->post('id_estimasi');
        $where = array('id_estimasi' => $id_estimasi);
        $queryAddedParts = ' SELECT * FROM detail_estimasi
                            LEFT JOIN item ON item.id_item = detail_estimasi.id_item
                            WHERE detail_estimasi.id_estimasi = "'.$id_estimasi.'" AND (item.tipe_item = "2" OR item.tipe_item = "4")
                            ORDER BY detail_estimasi.id_detail ASC
                              ';

        $dataFetchParts = $this->Crud->q($queryAddedParts);
        $dataEstimasi = $this->Crud->gw('estimasi', $where);
        $dataParts = '';
        $subTotalParts = 0;
        foreach($dataFetchParts as $datas){
            $subTotalParts += ((int)$datas->harga_item*(int)$datas->qty);
            $dataParts .='
                        <tr>
                            <td>'.$datas->nomor_parts.'</td>
                            <td>'.$datas->nama_item.'</td>
                            <td>'.$this->rp($datas->harga_item).'</td>
                            <td><input type="number" class="form-control countQty" value="'.$datas->qty.'" data-row="'.$datas->id_item.'"/> </td>
                            <td>'.$this->rp($datas->harga_item*$datas->qty).'</td>
                            <td width="10" style="text-align:center">
                             
                            <button type="button" class="btn btn-danger btn-xs waves-effect removeParts" data-row="'.$datas->id_item.'" '.($datas->eta !== NULL ? 'style="display:none"':'').'><i class="material-icons">delete_forever</i></button>
                            </td>
                        </tr>
                        ';
        }

        foreach ($dataEstimasi as $datas) {
            $diskonParts = $datas->diskon_parts;
            $approvalDiskonParts = $datas->approval_diskon_parts;
        }
        $data = array('dataParts' => $dataParts,
                      'subTotalParts' => $subTotalParts,
                      'diskonParts' => $diskonParts,
                      'approvalDiskonParts' => $approvalDiskonParts,
                      'dataFetchParts' => $dataFetchParts
                  );
        echo json_encode($data);
    }

    public function addParts(){
        $id_estimasi = $this->input->post('id_estimasi');
        $id_item = $this->input->post('id_item');
        $whereEstimasi = array('id_estimasi' => $id_estimasi);

        $dataEstimasi = $this->Crud->gw('estimasi', $whereEstimasi);
        $status_inout = '';
        foreach($dataEstimasi as $datas){
            $status_inout = $datas->status_inout;
        }
        
        $itemsDetail = array('id_estimasi'  => $id_estimasi,
                             'id_item'      => $id_item,
                            );

        $checkParts = $this->Crud->cw('detail_estimasi', $itemsDetail);
        if($checkParts >= 1){
            echo "failed";
        }else{
            $data = $this->Crud->i('detail_estimasi', $itemsDetail);
            if($status_inout == '1'){
                $itemsEstimasi  = array('is_order' => "1"); 
                $this->Crud->u('estimasi', $itemsEstimasi, $whereEstimasi);
            }
            echo "success";
        }
    }

    public function diskonParts(){
        $id_estimasi = $this->input->post('id_estimasi');
        $diskon_parts = $this->input->post('diskon_parts');

        $where = array('id_estimasi' => $id_estimasi);
        $items = array('diskon_parts'     => $diskon_parts);
        $data = $this->Crud->u('estimasi', $items, $where);

        echo "diskon updated";
    }

    public function qtyParts(){
        $id_estimasi = $this->input->post('id_estimasi');
        $qty = $this->input->post('qty');
        $id_item = $this->input->post('id_item');

        $where = array('id_estimasi' => $id_estimasi,
                       'id_item' => $id_item);
        $items = array('qty'     => $qty);
        $data = $this->Crud->u('detail_estimasi', $items, $where);

        echo "quantity updated";
    }

    public function ltJasa(){
        $id_estimasi = $this->input->post('id_estimasi');
        $lt_jasa = $this->input->post('lt');
        $id_item = $this->input->post('id_item');

        $where = array('id_estimasi' => $id_estimasi,
                       'id_item' => $id_item);
        $items = array('lt_jasa'     => $lt_jasa);
        $data = $this->Crud->u('detail_estimasi', $items, $where);

        echo "LTJasa updated";
    }

    public function produksi(){
        $id_user = $this->session->userdata('id_user');
        $queryEstimasi  = ' SELECT * FROM estimasi
                            LEFT JOIN customer ON customer.id_customer = estimasi.id_customer
                            LEFT JOIN jenis_kendaran ON jenis_kendaran.id_jenis = estimasi.id_jenis
                            LEFT JOIN user ON user.id_user = estimasi.id_user
                            WHERE estimasi.tgl_masuk IS NOT NULL
                            AND estimasi.id_user = "'.$id_user.'"';

        $queryCountJasa =  'SELECT *, COUNT(*) as countJasa FROM detail_estimasi 
                            LEFT JOIN item on item.id_item = detail_estimasi.id_item
                            LEFT JOIN estimasi on estimasi.id_estimasi = detail_estimasi.id_estimasi
                            WHERE item.tipe_item = "1" AND estimasi.status_inout = "1"
                            GROUP BY detail_estimasi.id_estimasi ORDER BY detail_estimasi.id_estimasi ASC';

        $queryTimTeknisi = 'SELECT * FROM estimasi 
                            LEFT JOIN user ON user.id_user = estimasi.tim_teknisi
                            ';
        
        $listEstimasi = array();
        $dataEstimasi = $this->Crud->q($queryEstimasi);
        $countJasa    = $this->Crud->q($queryCountJasa);
        $dataTimTeknisi = $this->Crud->q($queryTimTeknisi);
        foreach($dataEstimasi as $datas){
            $estimasi = new stdClass();
            $estimasi->no_wo = $datas->nomor_wo; 
            $estimasi->id_estimasi      = $datas->id_estimasi;
            $estimasi->no_polisi        = $datas->no_polisi;
            $estimasi->nama_lengkap     = $datas->nama_lengkap;
            $estimasi->tgl_masuk        = $datas->tgl_masuk;
            $estimasi->nama_sa          = $datas->nama_lengkap_user;
            $estimasi->kategori_jasa    = $datas->kategori_jasa;
            $estimasi->status_produksi  = $datas->status_produksi;
            
            foreach($dataTimTeknisi as $teknisi){
                if($datas->tim_teknisi == $teknisi->tim_teknisi){
                    $estimasi->nama_teknisi = $teknisi->nama_lengkap_user;
                }
            }
            array_push($listEstimasi, $estimasi);

        }
        $data       = array ('title'        => 'Monitoring Produksi | U-Care',
                             'isi'          => 'admin/dashboard/estimasi/monitoringProduksi',
                             'listEstimasi' => $listEstimasi,
                             'dataScript'   => 'admin/dataScript/tabel-script',
                         );
       // var_dump($dataEstimasi);
       $this->load->view('admin/_layout/wrapper', $data);
    }

    public function parts(){
        $id_user    = $this->session->userdata('id_user');
        $queryEstimasi  = ' SELECT * FROM estimasi
                            LEFT JOIN customer ON customer.id_customer = estimasi.id_customer
                            LEFT JOIN jenis_kendaran ON jenis_kendaran.id_jenis = estimasi.id_jenis
                            LEFT JOIN user ON user.id_user = estimasi.id_user
                            WHERE estimasi.status_inout = "1" AND (estimasi.jenis_estimasi = "1" OR estimasi.jenis_estimasi = "2") AND estimasi.id_user = "'.$id_user.'"
                           ';
        $queryCountParts = 'SELECT id_estimasi, COUNT(*) as countParts FROM detail_estimasi 
                            LEFT JOIN item on item.id_item = detail_estimasi.id_item
                            WHERE item.tipe_item = "2"
                            GROUP BY id_estimasi ORDER BY id_estimasi ASC';

        $queryCountParts2 = 'SELECT id_estimasi, COUNT(ata) as countParts2 FROM detail_estimasi 
                             LEFT JOIN item on item.id_item = detail_estimasi.id_item
                             WHERE item.tipe_item = "2"
                             GROUP BY id_estimasi ORDER BY id_estimasi ASC' ;
        $queryTimTeknisi  = 'SELECT * FROM estimasi 
                             LEFT JOIN user ON user.id_user = estimasi.tim_teknisi';
 

        $listEstimasi = array();
        $dataEstimasi = $this->Crud->q($queryEstimasi);
        $countTotalParts = $this->Crud->q($queryCountParts);
        $countParts = $this->Crud->q($queryCountParts2);
        $dataTimTeknisi   = $this->Crud->q($queryTimTeknisi);
        foreach ($dataEstimasi as $satuanEstimasi) {
            $estimasi = new stdClass();
            $estimasi->no_wo = $satuanEstimasi->nomor_wo;
            $estimasi->id = $satuanEstimasi->id_estimasi;
            $estimasi->no_polisi = $satuanEstimasi->no_polisi;
            $estimasi->nama_lengkap = $satuanEstimasi->nama_lengkap;
            $estimasi->tgl_estimasi = $satuanEstimasi->tgl_estimasi;
            $estimasi->tgl_janji_penyerahan = $satuanEstimasi->tgl_janji_penyerahan;
            foreach ($countTotalParts as $totalParts) {
                if ($satuanEstimasi->id_estimasi == $totalParts->id_estimasi){
                    $estimasi->totalParts = $totalParts->countParts;
                }
            }
             foreach ($countParts as $totalParts) {
                if ($satuanEstimasi->id_estimasi == $totalParts->id_estimasi){
                    $estimasi->totalReadyParts = $totalParts->countParts2;
                }
            }
            foreach($dataTimTeknisi as $teknisi){
                if($satuanEstimasi->tim_teknisi == $teknisi->tim_teknisi){
                    $estimasi->nama_teknisi = $teknisi->nama_lengkap_user;
                }
            }
            array_push($listEstimasi, $estimasi);
        }
        
        $data       = array ('title'        => 'Monitoring Parts | U-Care',
                             'isi'          => 'admin/dashboard/estimasi/monitoringParts',
                             'dataScript'   => 'admin/dataScript/tabel-script',
                             'listEstimasi' => $listEstimasi
                         );
       $this->load->view('admin/_layout/wrapper', $data);
    }

    public function detailParts($id_estimasi = null){
        $queryEstimasi  = ' SELECT * FROM estimasi
                            INNER JOIN customer ON customer.id_customer = estimasi.id_customer
                            INNER JOIN jenis_kendaran ON jenis_kendaran.id_jenis = estimasi.id_jenis
                            INNER JOIN color ON color.id_color = estimasi.id_color
                            INNER JOIN user ON user.id_user = estimasi.id_user
                            WHERE estimasi.id_estimasi = "'.$id_estimasi.'"
                           ';
        $queryParts     = 'SELECT * FROM detail_estimasi
                           LEFT JOIN item ON item.id_item = detail_estimasi.id_item
                           WHERE item.tipe_item= "2" AND detail_estimasi.id_estimasi = "'.$id_estimasi.'"';
        $queryTeknisi   = 'SELECT * FROM estimasi
                           INNER JOIN user ON user.id_user = estimasi.tim_teknisi
                           WHERE estimasi.id_estimasi = "'.$id_estimasi.'"';
        if ($id_estimasi !== null) {

            $where      = array( 'id_estimasi' => $id_estimasi );
            $content    = 'admin/dashboard/estimasi/detailParts';

            $data = array(  'title'             => 'Detail Parts | U-Care',
                            'isi'               => $content,
                            'dataParts'         => $this->Crud->q($queryParts),
                            'dataEstimasi'      => $this->Crud->q($queryEstimasi),
                            'dataTeknisi'       => $this->Crud->q($queryTeknisi),
                            'dataScript'        => 'admin/dataScript/tabel-script' );
        }else{
           redirect(base_url('service_advisor/parts'));
        }

        $this->load->view('admin/_layout/wrapper', $data);
    }

    public function detailJasa($id_estimasi = null){
        $queryEstimasi  = ' SELECT * FROM estimasi
                            INNER JOIN customer ON customer.id_customer = estimasi.id_customer
                            INNER JOIN jenis_kendaran ON jenis_kendaran.id_jenis = estimasi.id_jenis
                            INNER JOIN user ON user.id_user = estimasi.id_user
                            WHERE estimasi.id_estimasi = "'.$id_estimasi.'"
                           ';
        $queryJasa     = 'SELECT * FROM summary
                          LEFT JOIN estimasi ON estimasi.id_estimasi = summary.id_estimasi_summary
                          WHERE  summary.id_estimasi_summary = "'.$id_estimasi.'"';

        $queryTimTeknisi = 'SELECT * FROM estimasi 
                            LEFT JOIN user ON user.id_user = estimasi.tim_teknisi
                            WHERE estimasi.id_estimasi = "'.$id_estimasi.'"';

        if ($id_estimasi !== null) {

            $where      = array( 'id_estimasi' => $id_estimasi );
            $content    = 'admin/dashboard/estimasi/detailJasa';

            $data = array(  'title'        => 'Detail Parts | U-Care',
                            'isi'          => $content,
                            'dataJasa'    => $this->Crud->q($queryJasa),
                            'dataEstimasi' => $this->Crud->q($queryEstimasi),
                            'dataTeknisi' => $this->Crud->q($queryTimTeknisi),
                            'dataScript'   => 'admin/dataScript/tabel-script' );
        }else{
           redirect(base_url('service_advisor/parts'));
        }

        $this->load->view('admin/_layout/wrapper', $data);
    }

    public function optJasa(){
        $id_estimasi= $this->input->post('id_estimasi');
        $nama_item = $this->input->post('nama_item');
        $harga_item = $this->input->post('harga_item');
        $LT_item = $this->input->post('LT_item');
        $selectKCGC = $this->input->post('selectKCGC');
        
        $countOptJasa = 0;
        $queryCountOptJasa = 'SELECT COUNT(*) as totalOptJasa FROM detail_estimasi
                              INNER JOIN item ON detail_estimasi.id_item = item.id_item
                              WHERE detail_estimasi.id_estimasi = "'.$id_estimasi.'" AND item.tipe_item = "1" AND id_jenis = "0"
                             ';
        
        $dataCountOptJasa = $this->Crud->q($queryCountOptJasa);
        foreach($dataCountOptJasa as $datas){
            $countOptJasa = $datas->totalOptJasa;
        }

//        if($countOptJasa >= 3) {
//            echo "failed";
//        }else{
            $dataItem = array('tipe_item' => '1',
                              'nama_item' => $selectKCGC.' '.$nama_item,
                              'harga_item' => $harga_item,
                              'id_jenis' => "0"
                            );

            $returnIdItem = $this->Crud->i2('item', $dataItem);

            $dataDetailEstimasi = array('id_estimasi' => $id_estimasi,
                                        'id_item'     => $returnIdItem,
                                        'qty'         => '1',
                                        'harga'       => $harga_item,
                                        'lt_jasa'     => $LT_item
                                        );
            $this->Crud->i('detail_estimasi', $dataDetailEstimasi);
            echo "success";
//        }
    }

    public function optParts(){
        $id_estimasi    = $this->input->post('id_estimasi');
        $where          = array('id_estimasi' => $id_estimasi);
        $nomor_parts  = $this->input->post('nomor_parts');
        $nama_item  = $this->input->post('nama_item');
        $harga_item = $this->input->post('harga_item');
        $qty = $this->input->post('qty');
        $countOptParts = 0;
        $queryCountOptParts = 'SELECT COUNT(*) as totalOptParts FROM detail_estimasi
                              INNER JOIN item ON detail_estimasi.id_item = item.id_item
                              WHERE detail_estimasi.id_estimasi = "'.$id_estimasi.'" AND item.tipe_item = "2" AND id_jenis="0"
                             ';

        $dataCountOptParts = $this->Crud->q($queryCountOptParts);
        foreach($dataCountOptParts as $datas){
            $countOptParts = $datas->totalOptParts;
        }

//        if($countOptParts >= 3){
//            echo "failed";
//        }else{
            $dataItem = array( 'tipe_item' => '2',
                               'nomor_parts' => $nomor_parts,
                               'nama_item' => $nama_item,
                               'harga_item' => $harga_item,
                               'id_jenis' => "0"
                            );

            $returnIdItem = $this->Crud->i2('item', $dataItem);

            $dataDetailEstimasi = array('id_estimasi' => $id_estimasi,
                                        'id_item'     => $returnIdItem,
                                        'qty'         => $qty,
                                        'harga'       => $harga_item,
                                        );
            $this->Crud->i('detail_estimasi', $dataDetailEstimasi);
            $dataEstimasi = $this->Crud->gw('estimasi', $where);
            $status_inout = '';
            foreach($dataEstimasi as $datas){
                $status_inout = $datas->status_inout;
            }
            if($status_inout == '1'){
                $itemsEstimasi  = array('is_order' => "1"); 
                $this->Crud->u('estimasi', $itemsEstimasi, $where);
            }
            echo "success";
//            }
    }

    public function getDataChart(){
        $year = $this->input->post('year');
        $id_user = $this->session->userdata('id_user');
        $yearNow = date('Y');
        $dataCountIn = array();
        $dataCountOut = array();
        $dataCountAsuransi = array();
        $dataCountTunai = array();
        if($year !== NULL ){
            $yearNow = $year;
            for($i = 1 ; $i < 13; $i++){
                // $queryCountIn = 'SELECT COUNT(*) as totIn FROM estimasi
                //                  WHERE status_inout = "1" AND id_user = "'.$id_user.'"
                //                  AND YEAR(tgl_estimasi) = "'.$yearNow.'" AND MONTH(tgl_estimasi) = "'.$i.'"';
                // $queryCountOut = 'SELECT COUNT(*) as totOut FROM estimasi
                //                   WHERE status_inout = "0" AND id_user = "'.$id_user.'"
                //                   AND YEAR(tgl_estimasi) = "'.$yearNow.'" AND MONTH(tgl_estimasi) = "'.$i.'"';
                $queryCountAsuransiIn = 'SELECT COUNT(*) as totAsuransiIn FROM estimasi
                                       WHERE jenis_customer = "0" AND id_user = "'.$id_user.'" AND YEAR(tgl_estimasi) = "'.$yearNow.'" 
                                       AND MONTH(tgl_estimasi) = "'.$i.'" 
                                       AND status_inout = "1"';
                $queryCountAsuransiOut = 'SELECT COUNT(*) as totAsuransiOut FROM estimasi
                                       WHERE jenis_customer = "0" AND id_user = "'.$id_user.'" AND YEAR(tgl_estimasi) = "'.$yearNow.'" 
                                       AND MONTH(tgl_estimasi) = "'.$i.'" 
                                       AND status_inout = "0"';
                $queryCountTunaiIn = 'SELECT COUNT(*) as totTunaiIn FROM estimasi
                                       WHERE jenis_customer = "1"  AND id_user = "'.$id_user.'" AND YEAR(tgl_estimasi) = "'.$yearNow.'" 
                                       AND MONTH(tgl_estimasi) = "'.$i.'" 
                                      AND status_inout = "1"';
                $queryCountTunaiOut = 'SELECT COUNT(*) as totTunaiOut FROM estimasi
                                       WHERE jenis_customer = "1"  AND id_user = "'.$id_user.'" AND YEAR(tgl_estimasi) = "'.$yearNow.'" 
                                       AND MONTH(tgl_estimasi) = "'.$i.'" 
                                      AND status_inout = "0"';
                // $tempIn = $this->Crud->q($queryCountIn);
                // $tempOut = $this->Crud->q($queryCountOut);
                $tempAsuransiIn = $this->Crud->q($queryCountAsuransiIn);
                $tempAsuransiOut = $this->Crud->q($queryCountAsuransiOut);
                $tempTunaiIn = $this->Crud->q($queryCountTunaiIn);
                $tempTunaiOut = $this->Crud->q($queryCountTunaiOut);
                // foreach ($tempIn as $value) {
                //     $dataCountIn[] = (int)$value->totIn; 
                // }
                // foreach ($tempOut as $value) {
                //     $dataCountOut[] = (int)$value->totOut; 
                // }
                foreach ($tempAsuransiIn as $value) {
                    $dataCountAsuransiIn[] = (int)$value->totAsuransiIn; 
                }
                 foreach ($tempAsuransiOut as $value) {
                    $dataCountAsuransiOut[] = (int)$value->totAsuransiOut; 
                }
                foreach ($tempTunaiIn as $value) {
                    $dataCountTunaiIn[] = (int)$value->totTunaiIn; 
                }
                foreach ($tempTunaiOut as $value) {
                    $dataCountTunaiOut[] = (int)$value->totTunaiOut; 
                }
            }
            $data = array ( 'dataCountAsuransiIn' => $dataCountAsuransiIn,
                            'dataCountAsuransiOut' => $dataCountAsuransiOut,
                            'dataCountTunaiIn' => $dataCountTunaiIn,
                            'dataCountTunaiOut' => $dataCountTunaiOut
                            );
        }else{
            for($i = 1 ; $i < 13; $i++){
                $queryCountAsuransiIn = 'SELECT COUNT(*) as totAsuransiIn FROM estimasi
                                       WHERE jenis_customer = "0" AND id_user = "'.$id_user.'" AND YEAR(tgl_estimasi) = "'.$yearNow.'" 
                                       AND MONTH(tgl_estimasi) = "'.$i.'" 
                                       AND status_inout = "1"';
                $queryCountAsuransiOut = 'SELECT COUNT(*) as totAsuransiOut FROM estimasi
                                       WHERE jenis_customer = "0" AND id_user = "'.$id_user.'" AND YEAR(tgl_estimasi) = "'.$yearNow.'" 
                                       AND MONTH(tgl_estimasi) = "'.$i.'" 
                                       AND status_inout = "0"';
                $queryCountTunaiIn = 'SELECT COUNT(*) as totTunaiIn FROM estimasi
                                       WHERE jenis_customer = "1"  AND id_user = "'.$id_user.'" AND YEAR(tgl_estimasi) = "'.$yearNow.'" 
                                       AND MONTH(tgl_estimasi) = "'.$i.'" 
                                      AND status_inout = "1"';
                $queryCountTunaiOut = 'SELECT COUNT(*) as totTunaiOut FROM estimasi
                                       WHERE jenis_customer = "1"  AND id_user = "'.$id_user.'" AND YEAR(tgl_estimasi) = "'.$yearNow.'" 
                                       AND MONTH(tgl_estimasi) = "'.$i.'" 
                                      AND status_inout = "0"';
               $tempAsuransiIn = $this->Crud->q($queryCountAsuransiIn);
                $tempAsuransiOut = $this->Crud->q($queryCountAsuransiOut);
                $tempTunaiIn = $this->Crud->q($queryCountTunaiIn);
                $tempTunaiOut = $this->Crud->q($queryCountTunaiOut);
                foreach ($tempAsuransiIn as $value) {
                    $dataCountAsuransiIn[] = (int)$value->totAsuransiIn; 
                }
                 foreach ($tempAsuransiOut as $value) {
                    $dataCountAsuransiOut[] = (int)$value->totAsuransiOut; 
                }
                foreach ($tempTunaiIn as $value) {
                    $dataCountTunaiIn[] = (int)$value->totTunaiIn; 
                }
                foreach ($tempTunaiOut as $value) {
                    $dataCountTunaiOut[] = (int)$value->totTunaiOut; 
                }
            }
            $data = array ( 'dataCountAsuransiIn' => $dataCountAsuransiIn,
                            'dataCountAsuransiOut' => $dataCountAsuransiOut,
                            'dataCountTunaiIn' => $dataCountTunaiIn,
                            'dataCountTunaiOut' => $dataCountTunaiOut
                            );
        }
        
        echo json_encode($data);
    }

    /*
    | this function used for two feature. To Delivery and Delivery Complate. 
    | using one view with params for differentiator
    | to apply this function, please check out nav.php and delivery.php
    */
    public function delivery($params = null){
        $id_user = $this->session->userdata('id_user');
        $level = $this->session->userdata('level');
        if($level == 'service advisor' || $level == 'teknisi'){
            if($level == 'service advisor'){
                $atributQuery = 'AND estimasi.id_user = '.$id_user.'';
            }else{
                $atributQuery = 'AND estimasi.tim_teknisi = "'.$id_user.'"';
            }
            if ($params == 'ready'){
                $queryEstimasi = ' SELECT * FROM estimasi
                            LEFT JOIN customer ON customer.id_customer = estimasi.id_customer
                            LEFT JOIN jenis_kendaran ON jenis_kendaran.id_jenis = estimasi.id_jenis
                            LEFT JOIN user ON user.id_user = estimasi.id_user
                            LEFT JOIN summary ON summary.id_estimasi_summary = estimasi.id_estimasi
                            WHERE estimasi.status_produksi = "9" '.$atributQuery.'
                            AND estimasi.tgl_penyerahan IS NULL
                           ';
            }elseif ($params == 'done'){
                $queryEstimasi = ' SELECT * FROM estimasi
                            LEFT JOIN customer ON customer.id_customer = estimasi.id_customer
                            LEFT JOIN jenis_kendaran ON jenis_kendaran.id_jenis = estimasi.id_jenis
                            LEFT JOIN user ON user.id_user = estimasi.id_user
                            LEFT JOIN summary ON summary.id_estimasi_summary = estimasi.id_estimasi
                            WHERE estimasi.tgl_penyerahan IS NOT NULL '.$atributQuery.'
                           ';
            }else{
                show_404();
            }
        }elseif ($level == 'ptm' || $level == 'qc' || $level == 'pimpinan'){
            if ($params == 'ready'){
                $queryEstimasi = ' SELECT * FROM estimasi
                            LEFT JOIN customer ON customer.id_customer = estimasi.id_customer
                            LEFT JOIN jenis_kendaran ON jenis_kendaran.id_jenis = estimasi.id_jenis
                            LEFT JOIN user ON user.id_user = estimasi.id_user
                            LEFT JOIN summary ON summary.id_estimasi_summary = estimasi.id_estimasi
                            WHERE estimasi.status_produksi = "9" AND estimasi.tgl_penyerahan IS NULL
                           ';
            }elseif ($params == 'done'){
                $queryEstimasi = ' SELECT * FROM estimasi
                            LEFT JOIN customer ON customer.id_customer = estimasi.id_customer
                            LEFT JOIN jenis_kendaran ON jenis_kendaran.id_jenis = estimasi.id_jenis
                            LEFT JOIN user ON user.id_user = estimasi.id_user
                            LEFT JOIN summary ON summary.id_estimasi_summary = estimasi.id_estimasi
                            WHERE estimasi.tgl_penyerahan IS NOT NULL
                           ';
            }else{
                show_404();
            }
        }
        $listEstimasi = array();
        $dataEstimasi = $this->Crud->q($queryEstimasi);
        foreach ($dataEstimasi as $datas) {
            $estimasi = new stdClass();
            $estimasi->id = $datas->id_estimasi;
            $estimasi->no_wo = $datas->nomor_wo;
            $estimasi->no_polisi = $datas->no_polisi;
            $estimasi->nama_customer = $datas->nama_lengkap;
            $estimasi->nama_sa = $datas->nama_lengkap_user;
            $estimasi->no_telp       = $datas->no_hp;
            $estimasi->kategori_jasa = $datas->kategori_jasa;
            $estimasi->type_customer = $datas->jenis_customer;
            $estimasi->nama_asuransi = $datas->nama_asuransi;
            $estimasi->tgl_estimasi  = $datas->tgl_estimasi;
            $estimasi->tgl_masuk  = $datas->tgl_masuk;
            $estimasi->tgl_janji_penyerahan  = $datas->tgl_janji_penyerahan;
            $estimasi->ket_penyerahan = $datas->ket_penyerahan;
            $estimasi->tgl_penyerahan  = $datas->tgl_penyerahan;
            $whereTeknisi = array('id_user' => $datas->tim_teknisi);
            $dataTeknisi = $this->Crud->gw('user',$whereTeknisi);
            foreach($dataTeknisi as $teknisi){
                $estimasi->nama_teknisi = $teknisi->nama_lengkap_user;
            }
            $whereSummary = array('id_estimasi_summary' => $datas->id_estimasi);
            $dataSummary = $this->Crud->gw('summary', $whereSummary);
            foreach ($dataSummary as $summary) {
                $estimasi->tgl_selesai = $summary->final_inspection_out;
            }
            array_push($listEstimasi, $estimasi);
        }
        
        $data       = array ('title'            => 'Estimasi Ready to Delivery | U-Care',
                             'isi'              => 'admin/dashboard/estimasi/delivery',
                             'listEstimasi'     => $listEstimasi,
                             'dataScript'       => 'admin/dataScript/tabel-script',
                         );
       $this->load->view('admin/_layout/wrapper', $data);

    }

    public function get_delivery_readySA_json() { //get product data and encode to be JSON object
      header('Content-Type: application/json');
      echo $this->PimpinanModel->get_all_delivery_readySA();
    }

    public function get_delivery_doneSA_json() { //get product data and encode to be JSON object
      header('Content-Type: application/json');
      echo $this->PimpinanModel->get_all_delivery_doneSA();
    }

    public function get_delivery_readyPimpinan_json() { //get product data and encode to be JSON object
      header('Content-Type: application/json');
      echo $this->PimpinanModel->get_all_delivery_readyPimpinan();
    }

    public function get_delivery_donePimpinan_json() { //get product data and encode to be JSON object
      header('Content-Type: application/json');
      echo $this->PimpinanModel->get_all_delivery_donePimpinan();
    }

    public function insertTglPenyerahan(){
        $id_estimasi = $this->input->post('id_estimasi');
        $tgl_penyerahan = $this->input->post('tgl_penyerahan');
        $ket_penyerahan = $this->input->post('ket_penyerahan');
        $items = array('tgl_penyerahan' => $tgl_penyerahan,
                        'ket_penyerahan' => $ket_penyerahan,
                        );
        $where = array('id_estimasi' => $id_estimasi);
        $this->Crud->u('estimasi', $items, $where);
        echo 'success';
    }

    public function profile(){
        $data = array(
                        'title'     => 'Profile Service Advisor | U-Care',
                        'isi'       => 'admin/form/formSaProfile',
                        'dataScript'=> 'admin/dataScript/form-script'
                      );
        $this->load->view('admin/_layout/wrapper', $data);
    }

    //06 Profile
    public function userUpdateAction(){
        $input = $this->input->post(NULL, TRUE); //get all post with xss filter

        //deteck with password or not
        if($input['new_pass']==NULL){
            $items = array(//save all post in array
                'nama_lengkap_user' => $input['nama_lengkap'],
                'no_tlpUser' => $input['no_tlpUser'],
                'username' => $input['username'],
            );
        }else{
            $items = array(//save all post in array
                'nama_lengkap_user' => $input['nama_lengkap'],
                'no_tlpUser' => $input['no_tlpUser'],
                'username' => $input['username'],
                'password' => md5($input['new_pass']),
            );
        }

        $where = array(//get id for models Crud params w
            "id_user" => $this->session->userdata('id_user'),
        );

        //update session
        $sess = array(
            "nama_user" => $input['nama_lengkap'],
            "username" => $input['username'],
            "no_telp" => $input['no_tlpUser'],
        );
        $this->session->set_userdata($sess);

        $this->Crud->u('user', $items, $where ); //save in database
        $this->session->set_flashdata('update_sukses', 'Update Berhasil !'); //for notif it is succes

        redirect('service_advisor/profile');
    }

    public function searchParts(){
        $keywords = $this->input->post('keywords');
        $where1 = array('nomor_parts' => $keywords);
        $where2 = array('nomor_parts_lama' => $keywords);
        $listParts = $this->db->where($where1)->or_where($where2)->get('item')->result();
        $output = '';
        foreach($listParts as $items){
            $output .='
                       <tr>
                            <td>'.$items->nomor_parts.'</td>
                            <td>'.$items->nomor_parts_lama.'</td>
                            <td>'.$items->nama_item.'</td>
                            <td>'.$this->rp($items->harga_item).'</td>
                            <td><button type="button" class="btn bg-orange btn-xs waves-effect addParts" data-row="'.$items->id_item.'" id="parts_'.$items->id_item.'"><i class="material-icons">add_circle</i></button>
                            </td>
                        </tr>
                    '; 
        }
        $data = array('listEstimasi' => $output);
        echo json_encode($data);
    }

    public function listEstimasiDashboard(){
        $dateFrom = $this->input->post('dateFrom');
        $dateTo = $this->input->post('dateTo');
        $inOut = $this->input->post('inOut');
        $id_user = $this->session->userdata('id_user');
        $output = '';
        if($inOut == 'in'){
            $queryEstimasi = ' SELECT * FROM estimasi
                            LEFT JOIN customer ON customer.id_customer = estimasi.id_customer
                            LEFT JOIN jenis_kendaran ON jenis_kendaran.id_jenis = estimasi.id_jenis
                            LEFT JOIN user ON user.id_user = estimasi.id_user
                            WHERE estimasi.tgl_masuk >= "'.$dateFrom.'" AND estimasi.tgl_masuk <= "'.$dateTo.'" AND estimasi.status_inout = "1" AND estimasi.id_user = "'.$id_user.'"
                           ';    
        }elseif($inOut == 'out'){
            $queryEstimasi = ' SELECT * FROM estimasi
                            LEFT JOIN customer ON customer.id_customer = estimasi.id_customer
                            LEFT JOIN jenis_kendaran ON jenis_kendaran.id_jenis = estimasi.id_jenis
                            LEFT JOIN user ON user.id_user = estimasi.id_user
                            WHERE estimasi.tgl_estimasi >= "'.$dateFrom.'" AND estimasi.tgl_estimasi <= "'.$dateTo.'" AND estimasi.status_inout = "0" AND estimasi.id_user = "'.$id_user.'"
                           ';
        }else{
            $queryEstimasi = ' SELECT * FROM estimasi
                            LEFT JOIN customer ON customer.id_customer = estimasi.id_customer
                            LEFT JOIN jenis_kendaran ON jenis_kendaran.id_jenis = estimasi.id_jenis
                            LEFT JOIN user ON user.id_user = estimasi.id_user
                            WHERE estimasi.tgl_estimasi >= "'.$dateFrom.'" AND estimasi.tgl_estimasi <= "'.$dateTo.'" AND estimasi.id_user = "'.$id_user.'"
                           ';
        }
        $dataEstimasi = $this->Crud->q($queryEstimasi);
        foreach ($dataEstimasi as $datas) {
            $output .= '
                        <tr>
                            <td>'.$datas->nomor_wo.'</td>
                            <td>'.$datas->no_polisi.'</td>
                            <td>'.$datas->nama_lengkap.'</td>
                            <td>'.$datas->tgl_estimasi.'</td>
                            <td>'.$datas->tgl_masuk.'</td>
                            <td>'.$datas->tgl_janji_penyerahan.'</td>
                            <td>'.($datas->jenis_customer == "0" ? 'Asuransi - '.$datas->nama_asuransi.'' : 'Tunai').'</td>
                        </tr>
            ';
        }
        $data = array('listEstimasi' => $output);

        echo json_encode($data);
    }

    public function uploadParts(){
        $this->load->library('excel');
        $id_estimasi = $this->input->post('id_estimasi');
        $data = array();
        if(isset($_FILES["file"]["name"]))
        {
            $path = $_FILES["file"]["tmp_name"];
            $object = PHPExcel_IOFactory::load($path);
            foreach($object->getWorksheetIterator() as $worksheet)
            {
                $highestRow = $worksheet->getHighestRow();
                $highestColumn = $worksheet->getHighestColumn();
                for($row=1; $row<=$highestRow; $row++)
                {
                    $nomor_parts = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
                    // $data = array('nomor_parts' => $nomor_parts);
                    // $parts = new stdClass();
                    $queryParts = 'SELECT * FROM item WHERE nomor_parts = "'.$nomor_parts.'" AND tipe_item = "2"';
                    $dataParts = $this->Crud->q($queryParts);
                    foreach($dataParts as $datas){
                        $parts = new stdClass();
                        $parts->id = $datas->id_item;
                        array_push($data, $parts);
                    }
                }
            }
            foreach($data as $toInsert){
                $itemsDetail = array('id_estimasi'  => $id_estimasi,
                                 'id_item'      => $toInsert->id,
                                 'qty'          => "1",
                                );
                $this->Crud->i('detail_estimasi', $itemsDetail);
            }
            echo 'success';
        }
    }

    public function updateTimeProcess(){
        $id_estimasi = $this->input->post('id_estimasi');
        $whereTimeProcess = array('id_estimasi' => $id_estimasi);
        $dataTimeProcess = array(
                                'time_body_repair' => $this->input->post('lt1'),
                                'time_preparation' => $this->input->post('lt2'),
                                'time_masking' => $this->input->post('lt3'),
                                'time_painting' => $this->input->post('lt4'),
                                'time_polishing' => $this->input->post('lt5'),
                                'time_re_assembling' => $this->input->post('lt6'),
                                'time_washing' => $this->input->post('lt7'),
                                'time_final_inspection' => $this->input->post('lt8')
                            );
        $this->Crud->u('time_process', $dataTimeProcess, $whereTimeProcess);
        echo "success";
    }

    public function updateKedatanganParts(){
        $id_estimasi = $this->input->post('id_estimasi');
        $whereKedatanganParts = array('id_estimasi' => $id_estimasi);
        $dataKedatanganParts = array('depo' => $this->input->post('depo'),
                                     'tam'  => $this->input->post('tam'),
                                     'pabrik' => $this->input->post('pabrik')
                                    );
        $this->Crud->u('kedatangan_parts', $dataKedatanganParts, $whereKedatanganParts);
        echo "success";
    }

    public function getDataCustomer(){
        $id_estimasi = $this->input->post('id_estimasi');
        $queryEstimasi = 'SELECT * FROM estimasi 
                          JOIN customer ON customer.id_customer = estimasi.id_customer
                          WHERE estimasi.id_estimasi = "'.$id_estimasi.'"';
        $dataEstimasi = $this->Crud->q($queryEstimasi);
        $data = array('dataEstimasi' => $dataEstimasi);
        echo json_encode($data);
    }

    public function saveDataCustomer(){
        $id_customer = $this->input->post('id_customer');
        $nama_customer = $this->input->post('nama_customer');
        $no_telp = $this->input->post('no_telp');
        $alamat = $this->input->post('alamat');
        $where = array('id_customer' => $id_customer);
        $dataCustomer = array('nama_lengkap'    => $nama_customer,
                              'no_hp'           => $no_telp,
                              'alamat'          => $alamat
            );
        $this->Crud->u('customer', $dataCustomer, $where);
    }

    public function saveDataKendaraan(){
        $id_estimasi = $this->input->post('id_estimasi');
        $no_polisi = $this->input->post('no_polisi');
        $jenis_kendaraan = $this->input->post('jenis_kendaraan');
        $warna = $this->input->post('warna');
        $no_rangka = $this->input->post('no_rangka');
        $where = array('id_estimasi'        => $id_estimasi);
        $dataEstimasi = array('no_polisi'    => $no_polisi,
                              'id_jenis'     => $jenis_kendaraan,
                              'id_color'     => $warna,
                              'no_rangka'    => $no_rangka
        );
        $this->Crud->u('estimasi', $dataEstimasi, $where);
    }

    public function exportEstimasi($id_estimasi = null){
        if($id_estimasi !== null){
            $where = array('id_estimasi' => $id_estimasi);
            $queryEstimasi  = ' SELECT * FROM estimasi
                                LEFT JOIN customer ON customer.id_customer = estimasi.id_customer
                                LEFT JOIN jenis_kendaran ON jenis_kendaran.id_jenis = estimasi.id_jenis
                                LEFT JOIN user ON user.id_user = estimasi.id_user
                                LEFT JOIN color ON color.id_color = estimasi.id_color
                                LEFT JOIN summary ON summary.id_estimasi_summary = estimasi.id_estimasi
                                WHERE id_estimasi = "'.$id_estimasi.'"';

            $queryItem      = ' SELECT *  FROM detail_estimasi
                                LEFT JOIN item ON item.id_item = detail_estimasi.id_item
                                WHERE detail_estimasi.id_estimasi = "'.$id_estimasi.'" 
                                ORDER BY item.tipe_item ASC
                              ';
            $queryLtJasa = 'SELECT SUM(lt_jasa) as ltJasa  FROM detail_estimasi
                            LEFT JOIN item ON item.id_item = detail_estimasi.id_item
                            WHERE detail_estimasi.id_estimasi = "'.$id_estimasi.'"';


            $dataEstimasiCheck = $this->Crud->qq($queryEstimasi);
            $dataEstimasi = $this->Crud->q($queryEstimasi);
            $isEdit = true;
            foreach($dataEstimasi as $datas){
                $no_polisi = $datas->no_polisi;
                if($datas->final_inspection_out){
                    $isEdit = false;
                }
            }
            if($dataEstimasiCheck->num_rows() > 0){


                $data       = array ('title'        => 'Print "'.$no_polisi.'" U-Care',
                    'isi'          => 'admin/dashboard/estimasi/exportEstimasi',
                    'dataEstimasi' => $this->Crud->q($queryEstimasi),
                    'dataItem'     => $this->Crud->q($queryItem),
                    'ltJasa'       => $this->Crud->q($queryLtJasa),
                    'dataTimeProcess' => $this->Crud->gw('time_process', $where),
                    'dataKedatanganParts' => $this->Crud->gw('kedatangan_parts', $where),
                    'isEdit'       => $isEdit,
                    'dataScript'   => 'admin/dataScript/tabel-script' );
            }else{
                redirect(base_url('service_advisor/error404'));
            }
        }else{
            redirect(base_url('service_advisor/estimasi'));
        }
        $this->load->view('admin/_layout/wrapperv2', $data);
    }
}
