<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
	/*
	| -------------------------------------------------------------------
	| ADMIN CONTROLLER
	| -------------------------------------------------------------------
	| USAHAKAN BERIKAN KOMENTAR UNTUK SETIAP FUNGSI AGAR BISA DI MENGERTI
	| DENGAN CEPAT (^_^)9
    | sdfdsf
	|
	| MAIN INFORMATION :
	| $data 		=> digunakan untuk membungkus informasi - informasi terkait page tertentu
	| dataScript 	=> digunakan untuk memasukkan file javascript yang hanya digunakan pada
	| 				halaman tertentu , (untuk optimasi) di view/nama_folder/dataScript
	*/
    public function __construct() {
        parent::__construct();
        date_default_timezone_set('Asia/Makassar');
        if($this->session->userdata('status') != "login"){
            redirect('login');
        }
        $this->load->model('Crud');

    }
    
    public function index() {
        $data = array(  'title'             => 'U-Care Dashboard',
                        'isi'               => 'admin/dashboard/beranda',
                    	'dataScript'        => 'admin/dataScript/beranda-script');
        $this->load->view('admin/_layout/wrapper', $data);
    }

    public function user($id_user=null){
        $queryUser  = ' SELECT * FROM user WHERE id_user = "'.$id_user.'"';

        if ($id_user !== null) {
            $where      = array( 'id_user'      => $id_user );
            $content    = 'admin/dashboard/admin_page/userDetail';

            $data = array(  'title'             => 'Detail User | U-Care',
                            'isi'               => $content,
                            'data'              => $this->Crud->gw('user', $where),
                            'dataUser'          => $this->Crud->q($queryUser),
                            'dataScript'        => 'admin/dataScript/tabel-script' );
        }else{

          // $data = array('username' => $this->input->post('username', TRUE),
          //               'password' => $this->encrypt->decode(md5($this->input->post('password', TRUE)),$key)
          //               );

            $data = array(  'title'             => 'User | U-Care',
                            'isi'               => 'admin/dashboard/admin_page/user',
                            'data'              => $this->Crud->ga('user'),
                            'dataScript'        => 'admin/dataScript/tabel-script' );
        }

        $this->load->view('admin/_layout/wrapper', $data);

    }

    public function userCreate(){
        $data = array(  'title'                 => 'Tambah User | U-Care',
                        'isi'                   => 'admin/form/formUser',
                        'dataScript'            => 'admin/dataScript/form-script' );
        $this->load->view('admin/_layout/wrapper', $data);
    }

    public function userCreateAction() {
        $input = $this->input->post(NULL, TRUE);

        $data       = array(
                        'nama_lengkap_user'     => $input['nama_lengkap_user'],
                        'no_tlpUser'            => $input['no_tlpUser'],
                        'level'                 => $input['level'],
                        'password'              => md5($input['password']),
                        'username'              => $input['username'],
                        'create_at'             => date('Y-m-d h:m;s')
                      );
        $this->Crud->i('user', $data);
        echo 'success';
    }

    public function userUpdate($id_user=null){
        $where= array('id_user' => $id_user);
        $data = array(  'title'                 => 'Tambah User | U-Care',
                        'isi'                   => 'admin/form/formUpdateUser',
                        'data'                  => $this->Crud->gw('user', $where),
                        'dataScript'            => 'admin/dataScript/form-script' );
        $this->load->view('admin/_layout/wrapper', $data);
    }

    public function userUpdateAction() {
        $input      = $this->input->post(NULL, TRUE);
        $where      = array('id_user'   => $input['id_user']);
        $data       = array(
                        'nama_lengkap_user'     => $input['nama_lengkap_user'],
                        'no_tlpUser'            => $input['no_tlpUser'],
                        'username'              => $input['username'],
                        'password'              => md5($input['password']),
                        'level'                 => $input['level']                        
                      );
        $this->Crud->u('user', $data, $where);
        echo 'success';
    }

    public function deleteUser(){
        $input = $this->input->post(NULL, TRUE);
        $where = array('id_user'   => $input['id_user']);

        $this->Crud->d('user', $where);
        echo 'success';
        // redirect(base_url('service_advisor/customer'));
    }

    public function profile(){
        $data = array(
                        'title'             => 'Partsman | U-Care',
                        'isi'               => 'admin/form/formAdminProfile',
                        'dataScript'        => 'admin/dataScript/beranda-script'
                      );
        $this->load->view('admin/_layout/wrapper', $data);
    }

    //06 Profile
    public function profileUpdateAction(){
        $input = $this->input->post(NULL, TRUE); //get all post with xss filter

        //deteck with password or not
        if($input['new_pass']==NULL){
            $items = array(//save all post in array
                'nama_lengkap_user'     => $input['nama_lengkap'],
                'username'              => $input['username'],
            );
        }else{
            $items = array(//save all post in array
                'nama_lengkap_user'     => $input['nama_lengkap'],
                'username'              => $input['username'],
                'password'              => md5($input['new_pass']),
            );
        }

        $where = array(//get id for models Crud params w
            "id_user" => $this->session->userdata('id_user'),
        );

        //update session
        $sess = array(
            "nama_user"     => $input['nama_lengkap'],
            "username"      => $input['username'],
        );
        $this->session->set_userdata($sess);

        $this->Crud->u('user', $items, $where ); //save in database
        $this->session->set_flashdata('update_sukses', 'Update Berhasil !'); //for notif it is succes

        redirect('partsman/profile');
    }

    public function jenis_kendaraan($id_jenis = NULL){
        if($id_jenis == NULL){
            //kondiis tampilkan semua daftar
            $data = array(  'title'         => 'U-Care Dashboard',
                        'isi'               => 'admin/dashboard/admin_page/daftar_kendaraan',
                        'data'              => $this->Crud->ga('jenis_kendaran'),
                        //ini query ambil data semua $this->Crud->ga('nama_tabel')
                        'dataScript'        => 'admin/dataScript/tabel-script');
            $this->load->view('admin/_layout/wrapper', $data);
        }else{//kondisi untuk update
            $items = array('id_jenis' => $id_jenis);
            $data = array(  'title'             => 'Tambah jenis kendaran | U-Care',
                        'isi'                   => 'admin/form/formUpdateKendaraan',
                        'data'                  => $this->Crud->gw('jenis_kendaran', $items),
                        'dataScript'            => 'admin/dataScript/form-script' );
            $this->load->view('admin/_layout/wrapper', $data);
        }

    }

    public function hapusJenis($id_jenis){
        $where = array('id_jenis' => $id_jenis);
        $this->Crud->d('jenis_kendaran', $where); //ini query delete
        //$this->Crud->d('nama_tabel', PK yg mau dihapus)

        $this->session->set_flashdata('delete_kendaraan', 'Data Telah Dihapus');
        redirect('admin/jenis_kendaraan');
    }
    
    public function jenisCreate(){
        $data = array(  'title'                 => 'Tambah jenis kendaran | U-Care',
                        'isi'                   => 'admin/form/formTambahKendaraan',
                        'dataScript'            => 'admin/dataScript/form-script' );
        $this->load->view('admin/_layout/wrapper', $data);
    }

    public function actionTambahKendaraan(){
        $input = $this->input->post(NULL, TRUE);
        $items = array(
            'nama_jenis' => $input['nama_jenis'],
        );

        $this->db->insert('jenis_kendaran', $items);//ini query tambah data
        $this->session->set_flashdata('add_kendaraan', 'Data Telah Ditambahkan');
        redirect('admin/jenis_kendaraan');
    }  

    public function actionUpdateKendaraan($id_jenis){
        $input = $this->input->post(NULL, TRUE);//ambil semua input text yg ada di view
        $where = array(//primary key yang mau di update
            'id_jenis' => $id_jenis, 
            //'ini kolom database' => 'ini nilai yang mau disamakan/input'
        );

        $items = array(//ini item yang mau di input
            'nama_jenis' => $input['nama_jenis'], 
        );

        $this->db->update('jenis_kendaran', $items, $where);
        $this->session->set_flashdata('update_kendaraan', 'Data Telah Diupdate');
        redirect('admin/jenis_kendaraan');
    }

    public function jasa($id_item = NULL){
        if($id_item == NULL){
            $where = array('tipe_item' => 1);
            $data = array('title'           => 'Daftar Jasa',
                        'isi'               => 'admin/dashboard/admin_page/jasa',
                        'data'              => $this->Crud->gw('item', $where),
                        //ini query ambil data semua $this->Crud->ga('nama_tabel')
                        'dataScript'        => 'admin/dataScript/tabel-script');
            $this->load->view('admin/_layout/wrapper', $data);

        }else{
            $where = array('id_item'      => $id_item);

            $data = array('title'           => 'Edit Jasa',
                        'isi'               => 'admin/form/formEditJasa',
                        'data'              => $this->Crud->gw('item', $where),
                        'dataScript'        => 'admin/dataScript/tabel-script');
            $this->load->view('admin/_layout/wrapper', $data);
        }
    
    }

    public function actionEditJasa($id_item){
        $input = $this->input->post(NULL, TRUE);//ambil semua input text yg ada di view
        $where = array(//primary key yang mau di update
            'id_item' => $id_item, 
            //'ini kolom database' => 'ini nilai yang mau disamakan/input'
        );

        $items = array(//ini item yang mau di input
            'nama_item' => $input['nama_item'], 
            'harga_item' => $input['harga_item'], 
        );

        $this->db->update('item', $items, $where);
        $this->session->set_flashdata('edit_jasa','Data Telah Diedit');
        redirect('admin/jasa');
    }

    public function hapusJasa($id_item){
        $where = array('id_item' => $id_item);
        $this->Crud->d('item', $where); //ini query delete
        //$this->Crud->d('nama_tabel', PK yg mau dihapus)

        $this->session->set_flashdata('delete_jasa','Data Telah Dihapus');
        redirect('admin/jasa');
    }

    public function parts($id_item = NULL){
        if($id_item == NULL){
            $where = array('tipe_item'      => 2);
            $data = array('title'           => 'Daftar Parts',
                        'isi'               => 'admin/dashboard/admin_page/parts',
                        'data'              => $this->Crud->gw('item', $where),
                        //ini query ambil data semua $this->Crud->ga('nama_tabel')
                        // 'toDeliveryNotif'   => $this->toDeliveryNotif(),
                        'dataScript'        => 'admin/dataScript/tabel-script');
            $this->load->view('admin/_layout/wrapper', $data);
        }else{
            $where = array('id_item'        => $id_item);
            $data = array('title'           => 'Edit Parts',
                        'isi'               => 'admin/form/formEditParts',
                        'data'              => $this->Crud->gw('item', $where),
                        // 'toDeliveryNotif'   => $this->toDeliveryNotif(),
                        'dataScript'        => 'admin/dataScript/tabel-script');
            $this->load->view('admin/_layout/wrapper', $data);
        }
    }

    public function actionEditParts($id_item){
        $input = $this->input->post(NULL, TRUE);//ambil semua input text yg ada di view
        $where = array(//primary key yang mau di update
            'id_item' => $id_item, 
            //'ini kolom database' => 'ini nilai yang mau disamakan/input'
        );

        $items = array(//ini item yang mau di input
            'nama_item' => $input['nama_item'], 
            'harga_item' => $input['harga_item'], 
        );
        $this->db->update('item', $items, $where);
        $this->session->set_flashdata('edit_parts', 'Data Telah Diedit');
        redirect('admin/parts');
    }
     public function hapusParts($id_item){
        $where = array('id_item' => $id_item);
        $this->Crud->d('item', $where); //ini query delete
        //$this->Crud->d('nama_tabel', PK yg mau dihapus)

        $this->session->set_flashdata('delete_parts', 'Data Telah Dihapus');
        redirect('admin/parts');
    }
    
}
