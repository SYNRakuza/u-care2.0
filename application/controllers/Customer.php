<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends CI_Controller {

    public function __construct() {
        parent::__construct();
        date_default_timezone_set('Asia/Makassar');
        if($this->session->userdata('status') != "login"){
            redirect('login');
        }
        $this->load->model('Crud');

    }

    public function index(){
        $this->estimasiOut();
    }

    public function estimasiOut($id_estimasi = null) {
        $id_user    = $this->session->userdata('id_user');
        $queryEstimasi  = ' SELECT * FROM estimasi
                            LEFT JOIN customer ON customer.id_customer = estimasi.id_customer
                            LEFT JOIN jenis_kendaran ON jenis_kendaran.id_jenis = estimasi.id_jenis
                            LEFT JOIN user ON user.id_user = estimasi.id_user
                            WHERE estimasi.status_inout = "0" AND estimasi.id_user = "'.$id_user.'"';
        if ($id_estimasi !== null) {

            $where      = array( 'id_estimasi' => $id_estimasi );
            $content    = 'admin/dashboard/estimasi/estimasiDetail';

            $data = array(  'title'     => 'Detail Estimasi | U-Care',
                            'isi'       => $content,
                            'data'      => $this->Crud->gw( 'estimasi', $where),
                            'dataScript'=> 'admin/dataScript/tabel-script' );
        }else{
            $data = array(  'title'     => 'Estimasi | U-Care',
                            'isi'       => 'admin/dashboard/estimasi/estimasiOut',
                            'data'      => $this->Crud->q($queryEstimasi),
                            'dataScript'=> 'admin/dataScript/tabel-script' );
        }

        $this->load->view('admin/_layout/wrapper', $data);
    }
    
   
    
}
