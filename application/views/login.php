<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Login | Admin U-care</title>
    <!-- Favicon-->
    <link rel="icon" href="<?php echo base_url() ?>/assets/logo.png" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="<?php echo base_url() ?>/assets/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="<?php echo base_url() ?>/assets/plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="<?php echo base_url() ?>/assets/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="<?php echo base_url() ?>/assets/css/style.css" rel="stylesheet">
</head>
<style type="text/css">
    html { 
      background: url(<?php echo base_url()?>/assets/landingpage/img/bg-default.png) no-repeat center center fixed; 
      -webkit-background-size: cover;
      -moz-background-size: cover;
      -o-background-size: cover;
      background-size: cover;
    }
    .logo {
        padding-top: 10px;
    }

    .btn {
        text-align: center; 
        height: 45px;
    }
    small {
        font-size: 100%;
    }
</style>
<body class="login-page" style="background-color: green">
    <div class="login-box">
        <div class="logo">
            <a href="javascript:void(0);">U-Care</a>
            <small>Body & Paint Kalla Toyota Urip Sumohardjo</small>
        </div>
        <div class="card">
            <div class="body">
                <form id="sign_in" method="POST" action="<?php echo base_url('login/cekLogin'); ?>">
                    <div class="msg">Silahkan Login untuk Mengakses Dashboard Anda !</div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">person</i>
                        </span>
                        <div class="form-line">
                            <input type="text" maxlength="20" class="form-control" name="username" placeholder="Username..." required>
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line">
                            <input type="password" maxlength="12" class="form-control" name="password" placeholder="Password..." required>
                        </div>
                    </div>
                    <div class="row" style="text-align: center;">
                        <div class="col-xs-12" style="text-align: center;">
                            <label class="check" style="color: orange;text-align: center;">
                                <?php
                                    $info = $this->session->flashdata('info');
                                    if(!empty($info)){
                                    echo $info;
                                    }
                                ?>
                            </label>
                            <button class="btn btn-block bg-orange btn-lg waves-effect" id="button_sign_in" type="submit"><b>LOGIN</b></button>
                        </div>
                    </div>
                </form>
                <div class="footer-logo" style="text-align: center;">
                     <img src="<?php echo base_url()?>assets/img/kalla_toyota.png" alt="Logo U-Care" width="75" style="margin-bottom: 10px; margin-right: 10px;" >
                     <img src="<?php echo base_url()?>assets/img/logo.png" alt="Logo U-Care" width="50" style="margin-bottom: 10px;" >
                </div>
            </div>
        </div>
    </div>
    <!-- Jquery Core Js -->
    <script src="<?php echo base_url() ?>/assets/plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="<?php echo base_url() ?>/assets/plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="<?php echo base_url() ?>/assets/plugins/node-waves/waves.js"></script>

    <!-- Validation Plugin Js -->
    <script src="<?php echo base_url() ?>/assets/plugins/jquery-validation/jquery.validate.js"></script>

    <!-- Custom Js -->
    <script src="<?php echo base_url() ?>/assets/js/admin.js"></script>
    <script src="<?php echo base_url() ?>/assets/js/pages/examples/sign-in.js"></script>
    <script type="text/javascript">
    	$('#button_sign_in').on('click' , function(e) {
    		e.preventdefault();
    	})
    </script>
</body>

</html>