    
    <!-- Bootstrap Core Js -->
    <script src="<?=base_url('assets/plugins/bootstrap/js/bootstrap.js')?>"></script>

    <!-- Select Plugin Js -->
    <script src="<?=base_url('assets/')?>plugins/bootstrap-select/js/bootstrap-select.js"></script>


    <!-- Multi Select Plugin Js -->
    <script src="<?=base_url('assets/')?>plugins/multi-select/js/jquery.multi-select.js"></script>

    <!-- Moment Plugin Js -->
    <script src="<?=base_url('assets/')?>plugins/momentjs/moment.js"></script>
    
    <!-- Bootstrap Material Datetime Picker Plugin Js -->
    <script src="<?=base_url('assets/')?>plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>
    
    <!-- Slimscroll Plugin Js -->
    <script src="<?=base_url('assets/')?>plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
    
    <!-- Waves Effect Plugin Js -->
    <script src="<?=base_url('assets/')?>plugins/node-waves/waves.js"></script>

    <!-- notif Js -->
    <script src="<?=base_url('assets/')?>js/notif.js"></script>

    <!-- teknisi Js -->
    <script src="<?=base_url('assets/')?>js/teknisi.js"></script>

    <!-- application Js -->
    <script src="<?=base_url('assets/')?>js/application.js"></script>

    <?php if ($dataScript) $this->load->view($dataScript); ?>
</body>

</html>