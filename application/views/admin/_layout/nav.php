<?php $controller = $this->uri->segment(1); $modul = $this->uri->segment(2); $params = $this->uri->segment(3); ?>
<!-- #Top Bar -->
<section>
    <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar">
        <!-- User Info -->
        <!-- <?php if($this->session->userdata('level') == 'ptm'):?>
        <div class="user-info">
            <div class="image">
                <img src="<?=base_url('assets/')?>images/user.png" width="48" height="48" alt="User" />
            </div>
            <div class="info-container">
                <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <?php echo $this->session->userdata('nama_user'); ?>
                </div>
                <div class="email">Pembagi Tugas Mekanik</div>
                <div class="btn-group user-helper-dropdown">
                    <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                    <ul class="dropdown-menu pull-right">
                        <li><a href="javascript:void(0);"><i class="material-icons">person</i>Profile</a></li>
                        <li><a href="javascript:void(0);"><i class="material-icons">input</i>Sign Out</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <?php endif; ?> -->
        <!-- END IF FOR USER INFO -->
        <!-- #User Info -->
        <!-- Menu -->
        <div class="menu">
            <ul class="list">
                <!-- <li class="header">MAIN NAVIGATION</li>
                <li class="<?= ($modul == '' ? 'active' : '') ?>">
                    <a href="<?php echo base_url('tes') ?>">
                        <i class="material-icons">home</i>
                        <span>Home</span>
                    </a>
                </li> -->

                <!-- Ini untuk navigasi dropdown -->
                <!-- <li class="<?= ($modul == 'libraryMember' ? 'active' : '') ?>">
                    <a href="javascript:void(0);" class="menu-toggle">
                        <i class="material-icons">library_books</i>
                        <span>Drop Down</span>
                    </a>
                    <ul class="ml-menu">
                        <li>
                            <a href="#">Sub 1</a>
                        </li>
                        <li>
                            <a href="#">Sub 2</a>
                        </li>
                    </ul>
                </li> -->

                <!-- MENUS FOR ADMIN -->

                <?php if($this->session->userdata('level') == 'admin'):?>
                <li class="<?= ($modul == '' ? 'active' : '') ?>">
                    <a href="javascript:void(0);">
                        <i class="material-icons">home</i>
                        <span>Dashboard</span>
                    </a>
                </li>
                <li class="<?= ($modul == 'daftar_parts' || $modul == 'jenis_kendaraan' || $modul == 'daftar_jasa' || $modul == 'jenisCreate' || $modul == 'updateJenis' || $modul == 'jasa' || $modul == 'parts' ? 'active' : '') ?>">
                    <a href="javascript:void(0);" class="menu-toggle">
                        <i class="material-icons">assignment</i>
                        <span>MASTER DATA</span>
                    </a>
                    <ul class="ml-menu">
                        <li class="<?= ($modul == 'parts' ? 'active' : '') ?>">
                            <a href="<?= base_url() ?>admin/parts">Parts</a>
                        </li>
                        <li class="<?= ($modul == 'jasa' ? 'active' : '') ?>">
                            <a href="<?= base_url() ?>admin/jasa">Jasa</a>
                        </li>
                        <li class="<?= ($modul == 'jenis_kendaraan' || $modul == 'jenisCreate' || $modul == 'updateJenis' ? 'active' : '') ?>">
                            <a href="<?= base_url ('')?>admin/jenis_kendaraan">Jenis Kendaraan</a>
                        </li>
                    </ul>
                </li>
                <!-- <li class="<?= ($modul == 'customer' || $modul == 'customerCreate' ? 'active' : '') ?>">
                    <a href="javascript:void(0);" class="menu-toggle">
                        <i class="material-icons">person</i>
                        <span>USER</span>
                    </a>
                    <ul class="ml-menu">
                        <li class="<?= ($modul == 'customerCreate' ? 'active' : '') ?>">
                            <a href="javascript:void(0);">New User</a>
                        </li>
                         <li class="<?= ($modul == 'customer' ? 'active' : '') ?>">
                            <a href="javascript:void(0);">List User</a>
                        </li>
                    </ul>
                </li> -->
                <li class="<?= ($modul == 'userCreate' || $modul == 'user' ? 'active' : '') ?>">
                    <a href="javascript:void(0);" class="menu-toggle">
                        <i class="material-icons">account_box</i>
                        <span>User</span>
                    </a>
                    <ul class="ml-menu">
                        <li class="<?= ($modul == 'userCreate' ? 'active' : '') ?>">
                            <a href="<?=site_url()?>admin/userCreate">Add User</a>
                        </li>
                         <li class="<?= ($modul == 'user' ? 'active' : '') ?>">
                            <a href="<?=site_url()?>admin/user">List User</a>
                        </li>
                    </ul>
                </li>
                <!-- <li class="<?= ($modul == 'notifCreate' ? 'active' : '') ?>">
                    <a href="<?=site_url()?>admin/notifCreate">
                        <i class="material-icons">notifications</i>
                        <span>Notification</span>
                    </a>
                </li> -->
                <li class="<?= ($modul == 'profile' ? 'active' : '') ?>">
                    <a href="<?php echo base_url('admin/profile'); ?>">
                        <i class="material-icons">account_circle</i>
                        <span>Profile</span>
                    </a>
                </li>
                <!-- END MENUS FOR ADMIN -->

                <!-- MENUS FOR SERVICE ADVISOR -->

                <?php elseif($this->session->userdata('level') == 'service advisor'):?>
                <li class="<?= ($modul == '' ? 'active' : '') ?>">
                    <a href="<?=site_url()?>service_advisor">
                        <i class="material-icons">home</i>
                        <span>Dashboard</span>
                    </a>
                </li>
                <li class="<?= ($modul == 'customer' ? 'active' : '') ?>">
                    <a href="<?=site_url()?>service_advisor/customer">
                        <i class="material-icons">group</i>
                        <span>List Customer</span>
                    </a>
                </li>
                <li class="<?= ($modul == 'estimasiOut' || $modul == 'estimasiIn' || $modul == 'estimasiAll' ? 'active' : '') ?>">
                    <a href="javascript:void(0);" class="menu-toggle">
                        <i class="material-icons">developer_board</i>
                        <span>Estimasi</span>
                    </a>
                    <ul class="ml-menu">
                        <li class="<?= ($modul == 'estimasiCreate' ? 'active' : '') ?>">
                            <a href="<?=site_url()?>service_advisor/estimasiCreate" target="_blank">New Estimasi</a>
                        </li>
                        <li class="<?= ($modul == 'estimasiOut' ? 'active' : '') ?>">
                            <a href="<?=site_url()?>service_advisor/estimasiOut">Estimasi Out</a>
                        </li>
                        <li class="<?= ($modul == 'estimasiIn' ? 'active' : '') ?>">
                            <a href="<?=site_url()?>service_advisor/estimasiIn">Estimasi In</a>
                        </li>
                        <li class="<?= ($modul == 'estimasiAll' ? 'active' : '') ?>">
                            <a href="<?=site_url()?>service_advisor/estimasiAll">All Estimasi</a>
                        </li>
                    </ul>
                </li>
                 <li class="<?= ($modul == 'parts' || $modul == 'monitoringProduksi' ? 'active' : '') ?>">
                    <a href="javascript:void(0);" class="menu-toggle">
                        <i class="material-icons">remove_red_eye</i>
                        <span>Monitoring</span>
                    </a>
                    <ul class="ml-menu">
                        <li class="<?= ($modul == 'parts' ? 'active' : '') ?>">
                            <a href="<?=site_url()?>service_advisor/parts">Parts</a>
                        </li>
                         <li class="<?= ($modul == 'monitoringProduksi' ? 'active' : '') ?>">
                            <a href="<?=site_url()?>service_advisor/monitoringProduksi">Produksi</a>
                        </li>
                    </ul>
                </li>
                <li class="<?= ($modul == 'delivery' && $params == 'ready' ? 'active' : '') ?>">
                    <a href="<?=site_url()?>sa/delivery/ready">
                        <i class="material-icons">done</i>
                        <span>Ready To Delivery</span>
                    </a>
                </li>
                <li class="<?= ($modul == 'delivery' && $params == 'done' ? 'active' : '') ?>">
                    <a href="<?=site_url()?>sa/delivery/done">
                        <i class="material-icons">done_all</i>
                        <span>Delivery Complete</span>
                    </a>
                </li>
                <!-- <li class="<?= ($modul == 'notifCreate' ? 'active' : '') ?>">
                    <a href="<?=site_url()?>service_advisor/notifCreate">
                        <i class="material-icons">notifications</i>
                        <span>Notification</span>
                    </a>
                </li> -->
                <li class="<?= ($modul == 'profile' ? 'active' : '') ?>">
                    <a href="<?php echo base_url('service_advisor/profile'); ?>">
                        <i class="material-icons">account_circle</i>
                        <span>PROFILE</span>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url(); ?>login/logout" data-close="true">
                        <i class="material-icons">power_settings_new</i>
                        <span>LOG OUT</span>
                    </a>
                </li>
                <!-- END MENUS FOR SERVICE ADVISOR -->

                <!-- Untuk Halaman Partsman -->
                <?php elseif($this->session->userdata('level') == 'partsman'):?>

                <li class="<?= ($modul == '' ? 'active' : '') ?>">
                    <a href="<?=site_url()?>partsman">
                        <i class="material-icons">home</i>
                        <span>Dashboard</span>
                    </a>
                </li>

                <li class="<?= (($modul == 'list_estimasi' || $modul == 'proses_order') ? 'active' : '') ?>">
                    <a href="<?php echo base_url('partsman/list_estimasi') ?>" data-toggle="tooltip" title="Daftar Estimasi In">
                        <i class="material-icons">library_add</i>
                        <span>Daftar Parts Estimasi</span>
                    </a>
                </li>
                <li class="<?= ($modul == 'ordering' ? 'active' : '') ?>">
                    <a href="<?php echo base_url('partsman/ordering') ?>" data-toggle="tooltip" title="Daftar Barang Yang Akan Diorder">
                        <i class="material-icons">add_shopping_cart</i>
                        <span>Ordering</span>
                    </a>
                </li>
                <li class="<?= ($modul == 'list_order' || $modul == 'updatePart' ? 'active' : '') ?>">
                    <a href="<?php echo base_url('partsman/list_order') ?>" data-toggle="tooltip" title="Daftar Orderan Belum Selesai">
                        <i class="material-icons">library_books</i>
                        <span>Daftar Order</span>
                    </a>
                </li>
                <li class="<?= ($modul == 'monitor_parts' ? 'active' : '') ?>">
                    <a href="<?php echo base_url('partsman/monitor_parts'); ?>" data-toggle="tooltip" title="Monitoring Kedatangan Parts">
                        <i class="material-icons">view_list</i>
                        <span>B/O Parts</span>
                    </a>
                </li>
                <!-- <li class="<?= ($modul == 'reordering_parts' || $modul== 'proses_reorder' ? 'active' : '') ?>">
                    <a href="<?php echo base_url('partsman/reordering_parts') ?>" data-toggle="tooltip" title="Daftar Barang Yang Akan Kembali di Order atau Orderan yang ditambah">
                        <i class="material-icons">autorenew</i>
                        <span>Daftar Order Ulang</span>
                    </a>
                </li> -->
                <li class="<?= ($modul == 'history_order' ? 'active' : '') ?>">
                    <a href="<?php echo base_url('partsman/history_order') ?>" data-toggle="tooltip" title="Daftar Orderan Selesai">
                        <i class="material-icons">access_time</i>
                        <span>Riwayat Order</span>
                    </a>
                </li>
                <li class="<?= ($modul == 'daftar_parts' ? 'active' : '') ?>">
                    <a href="<?php echo base_url('partsman/daftar_parts'); ?>" data-toggle="tooltip" title="Daftar Parts Kalla Toyota">
                        <i class="material-icons">build</i>
                        <span>Daftar Parts</span>
                    </a>
                </li>
                <!-- <li class="<?= ($modul == 'notifCreate' ? 'active' : '') ?>">
                    <a href="<?=site_url()?>partsman/notifCreate">
                        <i class="material-icons">notifications</i>
                        <span>Notification</span>
                    </a>
                </li> -->
                <li class="<?= ($modul == 'profile' ? 'active' : '') ?>">
                    <a href="<?php echo base_url('partsman/profile'); ?>" data-toggle="tooltip" title="Edit Data Pribadi">
                        <i class="material-icons">account_box</i>
                        <span>Profile</span>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url(); ?>login/logout" data-close="true">
                        <i class="material-icons">power_settings_new</i>
                        <span>LOG OUT</span>
                    </a>
                </li>
                <!-- Selesai Menu Partsman -->

                <!-- MULAI Menu TEKNISI -->
                <?php elseif($this->session->userdata('level') == 'teknisi' || $this->session->userdata('level') == 'qc'):?>
                <?php if($this->session->userdata('level') == 'teknisi'):?>
                <li class="<?= ($modul == '' ? 'active' : '') ?>">
                    <a href="<?=site_url()?>teknisi">
                        <i class="material-icons">home</i>
                        <span>Dashboard</span>
                    </a>
                </li>
                <li class="<?= ($modul == 'ready' ? 'active' : '') ?>">
                   <a href="<?=site_url()?>teknisi/ready">
                       <i class="material-icons">touch_app</i>
                       <span>Ready</span>
                   </a>
                </li>
                <?php endif;?>
                <li class="<?= ($modul == 'atBodyRepair' || $modul == 'atPreparation'
                                || $modul == 'atMasking' || $modul == 'atPolishing'
                                || $modul == 'atReAssembling' || $modul == 'atWashing'
                                || $modul == 'atFinalInspection' || $modul == 'atPainting' ? 'active' : '') ?>">
                    <a href="javascript:void(0);" class="menu-toggle">
                        <i class="material-icons">loop</i>
                        <span>On Process</span>
                    </a>
                    <ul class="ml-menu">
                        <?php if($this->session->userdata('level') == 'teknisi'):?>
                        <li class="<?= ($modul == 'atBodyRepair' ? 'active' : '') ?>">
                            <a href="<?=site_url()?>teknisi/atBodyRepair">Body Repair</a>
                        </li>
                        <li class="<?= ($modul == 'atPreparation' ? 'active' : '') ?>">
                            <a href="<?=site_url()?>teknisi/atPreparation">Preparation</a>
                        </li>
                        <li class="<?= ($modul == 'atMasking' ? 'active' : '') ?>">
                            <a href="<?=site_url()?>teknisi/atMasking">Masking</a>
                        </li>
                        <li class="<?= ($modul == 'atPainting' ? 'active' : '') ?>">
                            <a href="<?=site_url()?>teknisi/atPainting">Painting</a>
                        </li>
                        <li class="<?= ($modul == 'atPolishing' ? 'active' : '') ?>">
                            <a href="<?=site_url()?>teknisi/atPolishing">Polishing</a>
                        </li>
                        <li class="<?= ($modul == 'atReAssembling' ? 'active' : '') ?>">
                            <a href="<?=site_url()?>teknisi/atReAssembling">Re-Assembling</a>
                        </li>
                        <li class="<?= ($modul == 'atWashing' ? 'active' : '') ?>">
                            <a href="<?=site_url()?>teknisi/atWashing">Washing</a>
                        </li>
                        <li class="<?= ($modul == 'atFinalInspection' ? 'active' : '') ?>">
                            <a href="<?=site_url()?>teknisi/atFinalInspection">Final Inspection</a>
                        </li>
                        <?php endif;?>
                        <?php if($this->session->userdata('level') == 'qc' && $this->session->userdata('sub_level') == 'body repair'):?>
                        <li class="<?= ($modul == 'atBodyRepair' ? 'active' : '') ?>">
                            <a href="<?=site_url()?>qc/atBodyRepair">Body Repair</a>
                        </li>
                        <?php elseif($this->session->userdata('level') == 'qc' && $this->session->userdata('sub_level') == 'preparation'):?>
                        <li class="<?= ($modul == 'atPreparation' ? 'active' : '') ?>">
                            <a href="<?=site_url()?>qc/atPreparation">Preparation</a>
                        </li>
                        <?php elseif($this->session->userdata('level') == 'qc' && $this->session->userdata('sub_level') == 'masking'):?>
                        <li class="<?= ($modul == 'atMasking' ? 'active' : '') ?>">
                            <a href="<?=site_url()?>qc/atMasking">Masking</a>
                        </li>
                        <?php elseif($this->session->userdata('level') == 'qc' && $this->session->userdata('sub_level') == 'painting'):?>
                        <li class="<?= ($modul == 'atPainting' ? 'active' : '') ?>">
                            <a href="<?=site_url()?>qc/atPainting">Painting</a>
                        </li>
                        <?php elseif($this->session->userdata('level') == 'qc' && $this->session->userdata('sub_level') == 'polishing'):?>
                        <li class="<?= ($modul == 'atPolishing' ? 'active' : '') ?>">
                            <a href="<?=site_url()?>qc/atPolishing">Polishing</a>
                        </li>
                        <?php elseif($this->session->userdata('level') == 'qc' && $this->session->userdata('sub_level') == 're-assembling'):?>
                        <li class="<?= ($modul == 'atReAssembling' ? 'active' : '') ?>">
                            <a href="<?=site_url()?>qc/atReAssembling">Re-Assembling</a>
                        </li>
                        <?php elseif($this->session->userdata('level') == 'qc' && $this->session->userdata('sub_level') == 'washing'):?>
                        <li class="<?= ($modul == 'atWashing' ? 'active' : '') ?>">
                            <a href="<?=site_url()?>qc/atWashing">Washing</a>
                        </li>
                        <?php elseif($this->session->userdata('level') == 'qc' && $this->session->userdata('sub_level') == 'final inspection'):?>
                        <li class="<?= ($modul == 'atFinalInspection' ? 'active' : '') ?>">
                            <a href="<?=site_url()?>qc/atFinalInspection">Final Inspection</a>
                        </li>
                        <?php endif;?>
                    </ul>
                </li>
                <?php if($this->session->userdata('level') == 'teknisi'):?>
                <li class="<?= ($modul == 'monitoringParts' ? 'active' : '') ?>">
                   <a href="<?=site_url()?>teknisi/monitoringParts">
                       <i class="material-icons">remove_red_eye</i>
                       <span>Monitoring Parts</span>
                   </a>
                </li>
                <li class="<?= ($modul == 'delivery' && $params == 'ready' ? 'active' : '') ?>">
                   <a href="<?=site_url()?>teknisi/delivery/ready">
                       <i class="material-icons">done</i>
                       <span>Ready To Delivery</span>
                   </a>
                </li>
                <li class="<?= ($modul == 'delivery' && $params == 'done' ? 'active' : '') ?>">
                   <a href="<?=site_url()?>teknisi/delivery/done">
                       <i class="material-icons">done_all</i>
                       <span>Delivery Complete</span>
                   </a>
                </li>
            <?php elseif ($this->session->userdata('level') == 'qc'): ?>
                <!-- <li class="<?= ($modul == 'monitoringParts' ? 'active' : '') ?>">
                   <a href="<?=site_url()?>qc/monitoringParts">
                       <i class="material-icons">remove_red_eye</i>
                       <span>Monitoring Parts</span>
                   </a>
                </li> -->

                <li class="<?= ($modul == 'monitoringParts' || $modul == 'monitoringProduksi' ? 'active' : '') ?>">
                    <a href="javascript:void(0);" class="menu-toggle">
                        <i class="material-icons">remove_red_eye</i>
                        <span>Monitoring</span>
                    </a>
                    <ul class="ml-menu">
                        <li class="<?= ($modul == 'monitoringParts' ? 'active' : '') ?>">
                            <a href="<?=site_url()?>qc/monitoringParts">Parts</a>
                        </li>
                         <li class="<?= ($modul == 'monitoringProduksi' ? 'active' : '') ?>">
                            <a href="<?=site_url()?>foreman/monitoringProduksi">Produksi</a>
                        </li>
                    </ul>
                </li>

                <li class="<?= ($modul == 'delivery' && $params == 'ready' ? 'active' : '') ?>">
                   <a href="<?=site_url()?>foreman/delivery/ready">
                       <i class="material-icons">done</i>
                       <span>Ready To Delivery</span>
                   </a>
                </li>
                <li class="<?= ($modul == 'delivery' && $params == 'done' ? 'active' : '') ?>">
                   <a href="<?=site_url()?>foreman/delivery/done">
                       <i class="material-icons">done_all</i>
                       <span>Delivery Complete</span>
                   </a>
                </li>
            <?php endif; ?>
                <li class="<?= ($modul == 'profile' ? 'active' : '') ?>">
                    <a href="<?php echo base_url('teknisi/profile'); ?>">
                        <i class="material-icons">account_circle</i>
                        <span>PROFILE</span>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url(); ?>login/logout" data-close="true">
                        <i class="material-icons">power_settings_new</i>
                        <span>LOG OUT</span>
                    </a>
                </li>

                <!-- Menu Pimpinan (Kacab + Kabeng) -->
                <?php elseif($this->session->userdata('level') == 'pimpinan'):?>

                    <li class="<?= ($controller == 'pimpinan' && $modul == '' || $modul == 'service_advisor' || $modul == 'ptm' || $modul == 'teknisi' || $modul == 'partsman' || $modul == 'foreman') ? 'active' : '' ?>">
                    <a href="javascript:void(0);" class="menu-toggle">
                        <i class="material-icons">home</i>
                        <span>Dashboard</span>
                    </a>
                    <ul class="ml-menu">
                        <li class="<?= ($modul == "service_advisor" || $modul == '') ? 'active' : '' ?>">
                            <a href="<?= base_url('pimpinan/service_advisor') ?>">Service Advisor</a>
                        </li>
                        <li class="<?= ($modul == "ptm") ? 'active' : '' ?>">
                            <a href="<?= base_url('pimpinan/ptm') ?>">Pembagi Tugas Mekanik</a>
                        </li>
                        <li class="<?= ($modul == "teknisi") ? 'active' : '' ?>">
                            <a href="<?= base_url('pimpinan/teknisi') ?>">Teknisi</a>
                        </li>
                        <li class="<?= ($modul == "partsman") ? 'active' : '' ?>">
                            <a href="<?= base_url('pimpinan/partsman') ?>">Partsman</a>
                        </li>
                        <li class="<?= ($modul == "foreman") ? 'active' : '' ?>">
                            <a href="<?= base_url('pimpinan/foreman') ?>">Foreman</a>
                        </li>
                    </ul>
                </li>

                <li class="<?= ($modul == 'user' || $modul == 'kendaraan'  || $modul == 'daftar_warna' || $modul == 'daftar_asuransi' || $modul == 'daftar_parts' || $modul == 'daftar_jasa' || $modul == 'userCreate' || $modul == 'userUpdate' || $modul == 'kendaraanCreate' || $modul == 'kendaraanUpdate' || $modul == 'jasa' || $modul == 'summaryLead' || $modul == 'jasaUpdate' )  ? 'active' : '' ?>">
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">queue</i>
                            <span>Master Data</span>
                        </a>
                        <ul class="ml-menu">
                            <li  class="<?= ($controller == 'pimpinan') && ($modul == 'user' || $modul == 'userCreate' || $modul == 'userUpdate') ? 'active' : '' ?>">
                                <a href="<?= base_url() ?>pimpinan/user">User</a>
                            </li>
                            <li class="<?= ($modul == 'kendaraan' || $modul == 'kendaraanCreate' || $modul == 'kendaraanUpdate') ? 'active' : '' ?>">
                                <a href="<?= base_url() ?>pimpinan/kendaraan">Kendaraan</a>
                            </li>
                             <li class="<?= ($controller == 'pimpinan') && ($modul == 'daftar_warna') ? 'active' : '' ?>">
                                <a href="<?= base_url() ?>pimpinan/daftar_warna">Warna Kendaraan</a>
                            </li>
                            <li class="<?= ($controller == 'pimpinan') && ($modul == 'daftar_asuransi') ? 'active' : '' ?>">
                                <a href="<?= base_url() ?>pimpinan/daftar_asuransi">Asuransi</a>
                            </li>
                            <li  class="<?= ($controller == 'pimpinan') && ($modul == 'daftar_parts') ? 'active' : '' ?>">
                                <a href="<?= base_url() ?>pimpinan/daftar_parts">Parts</a>
                            </li>
                            <li class="<?= ($controller == 'pimpinan') && ($modul == 'daftar_jasa' || $modul == 'jasa' || $modul == 'jasaUpdate') ? 'active' : '' ?>">
                                <a href="<?= base_url() ?>pimpinan/jasa">Jasa</a>
                            </li>
                             <li class="<?= ($controller == 'pimpinan') && ($modul == 'summaryLead' || $modul == 'historyLead') ? 'active' : '' ?>">
                                <a href="<?= base_url() ?>pimpinan/summaryLead">Summary Lead</a>
                            </li>
                        </ul>
                </li>
<!--                  <li class="<?= ($controller == 'pimpinan') && ($modul == 'diskon') ? 'active' : '' ?>">
                        <a href="<?= base_url() ?>pimpinan/diskon">
                            <i class="material-icons">loyalty</i>
                            <span>Dicsount Approval</span>
                        </a>
                </li> -->

                <li class="<?= $controller == 'pimpinan' && $modul == 'customer' || $modul == 'customerCreate' || $modul == 'estimasiCreate' || $modul == 'estimasiIn' || $modul == 'estimasiOut' ? 'active' : '' ?>">
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">assignment_ind</i>
                            <span>Service Advisor</span>
                        </a>
                        <ul class="ml-menu">

                            <li class="<?= ($controller == 'pimpinan') && $modul == 'customer' || $modul == 'customerCreate' ? 'active' : '' ?>">
                                <a href="<?= base_url() ?>pimpinan/customer">
                                    <span>List Customer</span>
                                </a>
                            </li>
                            <li class="<?= ($controller == 'pimpinan') && ($modul == 'estimasiCreate' || $modul == 'estimasiIn' || $modul == 'estimasiOut'  ) ? 'active' : '' ?>">
                                <a href="javascript:void(0);" class="menu-toggle">
                                    <span>Estimasi</span>
                                </a>
                                <ul class="ml-menu">
                                    <li class="<?= $controller == 'service_advisor' && ($modul == 'estimasiCreate') ? 'active' : '' ?>">
                                        <a href="<?= base_url() ?>service_advisor/estimasiCreate">New Estimasi</a>
                                    </li>
                                    <li class="<?= $controller == 'pimpinan' && ($modul == 'estimasiOut') ? 'active' : '' ?>">
                                        <a href="<?= base_url() ?>pimpinan/estimasiOut">Estimasi Out</a>
                                    </li>
                                    <li class="<?= $controller == 'pimpinan' && ($modul == 'estimasiIn') ? 'active' : '' ?>">
                                        <a href="<?= base_url() ?>pimpinan/estimasiIn">Estimasi In</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                </li>
                <li class="<?= $controller == 'ptm' ? 'active' : '' ?>">
                         <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">portrait</i>
                            <span>Pembagi Tugas Mekanik</span>
                        </a>
                        <ul class="ml-menu">
                            <li  class="<?= ($controller == 'ptm') && ($modul == 'nextJob' || $modul == 'jobDispatchSelect' || $modul == 'nextJobDetail') ? 'active' : '' ?>">
                                <a href="<?= base_url() ?>ptm/nextJob">
                                    <span>Next Job</span>
                                </a>
                            </li>
                        </ul>
                </li>
                <li class="<?= $controller == 'pimpinan' && ($modul == 'ready' || $modul == 'atBodyRepair' || $modul == 'atPreparation' || $modul == 'atMasking' || $modul == 'atPainting' || $modul == 'atPolishing' || $modul == 'atReAssembling' || $modul == 'atWashing' || $modul == 'atFinalInspection' ) ? 'active' : '' ?>">
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">people</i>
                            <span>Teknisi</span>
                        </a>
                        <ul class="ml-menu">
                            
                            <li  class="<?= $controller == 'pimpinan' && $modul == 'ready' ? 'active' : '' ?>">
                                <a href="<?= base_url() ?>pimpinan/ready">
                                    <span>Ready</span>
                                </a>
                            </li>
                            <li class="<?= $controller == 'pimpinan' && ($modul == 'atBodyRepair' || $modul == 'atPreparation' || $modul == 'atMasking') || $modul == 'atPainting' || $modul == 'atPolishing' || $modul == 'atReAssembling' || $modul == 'atWashing' || $modul == 'atFinalInspection' ? 'active' : '' ?>">
                                <a href="javascript:void(0);" class="menu-toggle">
                                    <span>On Process</span>
                                </a>
                                <ul class="ml-menu">
                                    <li class="<?= $controller == 'pimpinan' && ($modul == 'atBodyRepair') ? 'active' : '' ?>">
                                        <a href="<?= base_url() ?>pimpinan/atBodyRepair">Body Repair</a>
                                    </li>
                                    <li class="<?= $controller == 'pimpinan' && ($modul == 'atPreparation') ? 'active' : '' ?>">
                                        <a href="<?= base_url() ?>pimpinan/atPreparation">Preparation</a>
                                    </li>
                                    <li class="<?= $controller == 'pimpinan' && ($modul == 'atMasking') ? 'active' : '' ?>">
                                        <a href="<?= base_url() ?>pimpinan/atMasking">Masking</a>
                                    </li>
                                    <li class="<?= $controller == 'pimpinan' && ($modul == 'atPainting') ? 'active' : '' ?>">
                                        <a href="<?= base_url() ?>pimpinan/atPainting">Painting</a>
                                    </li>
                                    <li class="<?= $controller == 'pimpinan' && ($modul == 'atPolishing') ? 'active' : '' ?>">
                                        <a href="<?= base_url() ?>pimpinan/atPolishing">Polishing</a>
                                    </li>
                                    <li class="<?= $controller == 'pimpinan' && ($modul == 'atReAssembling') ? 'active' : '' ?>">
                                        <a href="<?= base_url() ?>pimpinan/atReAssembling">Re-Assembling</a>
                                    </li>
                                    <li class="<?= $controller == 'pimpinan' && ($modul == 'atWashing') ? 'active' : '' ?>">
                                        <a href="<?= base_url() ?>pimpinan/atWashing">Washing</a>
                                    </li>
                                    <li class="<?= $controller == 'pimpinan' && ($modul == 'atFinalInspection') ? 'active' : '' ?>">
                                        <a href="<?= base_url() ?>pimpinan/atFinalInspection">Final Inspection</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                </li>
                <li class="<?= $controller == 'qc' ? 'active' : '' ?>">
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">how_to_reg</i>
                            <span>Foreman</span>
                        </a>
                        <ul class="ml-menu">
                            <li class="<?= $controller == 'qc' && ($modul == 'atBodyRepair' || $modul == 'atPreparation' || $modul == 'atMasking') || $modul == 'atPainting' || $modul == 'atPolishing' || $modul == 'atReAssembling' || $modul == 'atWashing' || $modul == 'atFinalInspection' ? 'active' : '' ?>">
                                <a href="javascript:void(0);" class="menu-toggle">
                                    <span>On Process</span>
                                </a>
                                <ul class="ml-menu">
                                    <li class="<?= $controller == 'qc' && ($modul == 'atBodyRepair') ? 'active' : '' ?>">
                                        <a href="<?= base_url() ?>qc/atBodyRepair">Body Repair</a>
                                    </li>
                                    <li class="<?= $controller == 'qc' && ($modul == 'atPreparation') ? 'active' : '' ?>">
                                        <a href="<?= base_url() ?>qc/atPreparation">Preparation</a>
                                    </li>
                                    <li class="<?= $controller == 'qc' && ($modul == 'atMasking') ? 'active' : '' ?>">
                                        <a href="<?= base_url() ?>qc/atMasking">Masking</a>
                                    </li>
                                    <li class="<?= $controller == 'qc' && ($modul == 'atPainting') ? 'active' : '' ?>">
                                        <a href="<?= base_url() ?>qc/atPainting">Painting</a>
                                    </li>
                                    <li class="<?= $controller == 'qc' && ($modul == 'atPolishing') ? 'active' : '' ?>">
                                        <a href="<?= base_url() ?>qc/atPolishing">Polishing</a>
                                    </li>
                                    <li class="<?= $controller == 'qc' && ($modul == 'atReAssembling') ? 'active' : '' ?>">
                                        <a href="<?= base_url() ?>qc/atReAssembling">Re-Assembling</a>
                                    </li>
                                    <li class="<?= $controller == 'qc' && ($modul == 'atWashing') ? 'active' : '' ?>">
                                        <a href="<?= base_url() ?>qc/atWashing">Washing</a>
                                    </li>
                                    <li class="<?= $controller == 'qc' && ($modul == 'atFinalInspection') ? 'active' : '' ?>">
                                        <a href="<?= base_url() ?>qc/atFinalInspection">Final Inspection</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                </li>
                 <li class="<?= ($controller == 'partsman' ? 'active' : '') ?>">
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">commute</i>
                            <span>Partsman</span>
                        </a>
                        <ul class="ml-menu">
                            <li class="<?= $controller == 'partsman' && ($modul == 'list_estimasi' || $modul == 'proses_order' ) ? 'active' : '' ?>">
                                <a href="<?= base_url() ?>partsman/list_estimasi">
                                    <span>Daftar Estimasi</span>
                                </a>
                            </li>
                            <li class="<?= $controller == 'partsman' && ($modul == 'ordering') ? 'active' : '' ?>">
                                <a href="<?= base_url() ?>partsman/ordering">
                                    <span>Ordering</span>
                                </a>
                            </li>
                            <li class="<?= $controller == 'partsman' && ($modul == 'list_order' || $modul == 'updatePart') ? 'active' : '' ?>">
                                <a href="<?= base_url() ?>partsman/list_order">
                                    <span>Daftar Order</span>
                                </a>
                            </li>
                            <li class="<?= $controller == 'partsman' && ($modul == 'monitor_parts') ? 'active' : '' ?>">
                                <a href="<?= base_url() ?>partsman/monitor_parts">
                                    <span>B/O Parts</span>
                                </a>
                            </li>
                            <li class="<?= $controller == 'partsman' && ($modul == 'reordering_parts' || $modul== 'proses_reorder') ? 'active' : '' ?>">
                                <a href="<?= base_url() ?>partsman/reordering_parts">
                                    <span>Daftar Order Ulang</span>
                                </a>
                            </li>
                            <li class="<?= $controller == 'partsman' && ($modul == 'history_order') ? 'active' : '' ?>">
                                <a href="<?= base_url() ?>partsman/history_order">
                                    <span>Riwayat Order</span>
                                </a>
                            </li>
                        </ul>
                </li>
                <li class="<?= ($modul == 'delivery' && $params == 'ready' ? 'active' : '') ?>">
                   <a href="<?=site_url()?>pimpinan/delivery/ready">
                       <i class="material-icons">done</i>
                       <span>Ready To Delivery</span>
                   </a>
                </li>
                <li class="<?= ($modul == 'delivery' && $params == 'done' ? 'active' : '') ?>">
                   <a href="<?=site_url()?>pimpinan/delivery/done">
                       <i class="material-icons">done_all</i>
                       <span>Delivery Complete</span>
                   </a>
                </li>
                <li class="<?= ($modul == 'profile' ? 'active' : '') ?>">
                    <a href="<?php echo base_url('teknisi/profile'); ?>">
                        <i class="material-icons">account_circle</i>
                        <span>Profile</span>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url(); ?>login/logout" data-close="true">
                        <i class="material-icons">power_settings_new</i>
                        <span>Log Out</span>
                    </a>
                </li>


                <!-- Selesai Menu Pimpinan-->

                <!-- MENUS FOR PTM -->
                <?php elseif($this->session->userdata('level') == 'ptm'):?>
                <li class="<?= ($modul == 'workInProcess' || $modul == 'dispatchingUpdateTeam' || $modul == 'dispatching1' || $modul == 'dispatching2' ? 'active' : '') ?>">
                   <a href="<?=site_url()?>ptm/workInProcess">
                       <i class="material-icons">autorenew</i>
                       <span>Work In Process</span>
                   </a>
                </li>
                <li class="<?= ($modul == 'nextJob' || $modul == 'jobDispatchSelect ' ? 'active' : '') ?>">
                   <a href="<?=site_url()?>ptm/nextJob">
                    <!-- href="<?=site_url()?>ptm" -->
                       <i class="material-icons">assignment_late</i>
                       <span>Next Job</span>
                   </a>
                </li>
                 <li class="<?= ($modul == 'monitoringParts' || $modul == 'monitoringProduksi' ? 'active' : '') ?>">
                    <a href="javascript:void(0);" class="menu-toggle">
                        <i class="material-icons">remove_red_eye</i>
                        <span>Monitoring</span>
                    </a>
                    <ul class="ml-menu">
                        <li class="<?= ($modul == 'monitoringParts' ? 'active' : '') ?>">
                            <a href="<?=site_url()?>ptm/monitoringParts">Parts</a>
                        </li>
                         <li class="<?= ($modul == 'monitoringProduksi' ? 'active' : '') ?>">
                            <a href="<?=site_url()?>ptm/monitoringProduksi">Produksi</a>
                        </li>
                    </ul>
                </li>
                <li class="<?= ($modul == 'delivery' && $params == 'ready' ? 'active' : '') ?>">
                   <a href="<?=site_url()?>ptm/delivery/ready">
                       <i class="material-icons">done</i>
                       <span>Ready To Delivery </span>
                   </a>
                </li>
                <li class="<?= ($modul == 'delivery' && $params == 'done' ? 'active' : '') ?>">
                   <a href="<?=site_url()?>ptm/delivery/done">
                       <i class="material-icons">done_all</i>
                       <span>Delivery Complete</span>
                   </a>
                </li>
                <!-- <li class="<?= ($modul == 'notifCreate' ? 'active' : '') ?>">
                    <a href="<?=site_url()?>ptm/notifCreate">
                        <i class="material-icons">notifications</i>
                        <span>Notification</span>
                    </a>
                </li> -->
                <li class="<?= ($modul == 'profile' ? 'active' : '') ?>">
                    <a href="<?php echo base_url('ptm/profile'); ?>">
                        <i class="material-icons">account_circle</i>
                        <span>PROFILE</span>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url(); ?>login/logout" data-close="true">
                        <i class="material-icons">power_settings_new</i>
                        <span>LOG OUT</span>
                    </a>
                </li>

                <?php elseif($this->session->userdata('level') == 'customer'):?>

                    <li class="<?= $controller == 'customer' && $modul == 'customer' || $modul == 'customerCreate' || $modul == 'estimasiCreate' || $modul == 'estimasiIn' || $modul == 'estimasiOut' || $modul == '' ? 'active' : '' ?>">
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">menu</i>
                            <span>Menu</span>
                        </a>
                        
                            
                                <ul class="ml-menu">
                                    <li class="<?= $controller == 'customer' || $controller == 'service_advisor' && ($modul == '' || $modul == 'estimasiOut') ? 'active' : '' ?>">
                                        <a href="<?= base_url() ?>customer/estimasiOut">Estimasi Out</a>
                                    </li>
                                    <li class="<?= $controller == 'service_advisor' && ($modul == 'estimasiCreate') ? 'active' : '' ?>">
                                        <a href="<?= base_url() ?>service_advisor/estimasiCreate">New Estimasi</a>
                                    </li>
                                    
                                </ul>
                            </li>

                <?php endif; ?>
                <!-- END IF FOR NAV SESSION -->
            </ul>
        </div>
        <!-- #Menu -->
        <!-- Footer -->
        <div class="legal">
            <div class="copyright">
                &copy; <a href="javascript:void(0);">U-CARE</a>.
            </div>
            <div class="version">
                <b>Version: </b> 0.1.1
            </div>
        </div>
        <!-- #Footer -->
    </aside>
</section>