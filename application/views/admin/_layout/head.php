<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title><?= $title ?></title>
    <!-- Favicon-->
    <link rel="icon" href="<?php echo base_url() ?>/assets/logo.png" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Jquery Core Js -->
    <script src="<?=base_url('assets/')?>plugins/jquery/jquery.js"></script>


    <!-- Bootstrap Core Css -->
    <link href="<?=base_url('assets/')?>plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Dropzone Css -->
    <link href="<?=base_url('assets/')?>plugins/dropzone/dropzone.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="<?=base_url('assets/')?>plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="<?=base_url('assets/')?>plugins/animate-css/animate.min.css" rel="stylesheet" />

    <!-- Sweetalert Css -->
    <link href="<?=base_url('assets/')?>plugins/sweetalert/sweetalert.css" rel="stylesheet" />

    <!-- Morris Chart Css-->
    <link href="<?=base_url('assets/')?>plugins/morrisjs/morris.css" rel="stylesheet" />

     <!-- Bootstrap Material Datetime Picker Css -->
    <link href="<?=base_url('assets/')?>plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />


    <link rel="stylesheet" type="text/css" href="<?=base_url('assets/')?>plugins/DataTables/datatables.min.css"/>
    
    <!-- Multi Select Css -->
    <link href="<?=base_url('assets/')?>plugins/multi-select/css/multi-select.css" rel="stylesheet">

    <!-- Bootstrap Select Css -->
    <link href="<?=base_url('assets/')?>plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="<?=base_url('assets/')?>css/style.min.css" rel="stylesheet">


    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="<?=base_url('assets/')?>css/themes/all-themes.css" rel="stylesheet" />

    <!-- select2 -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />

</head>
<body class="theme-orange">