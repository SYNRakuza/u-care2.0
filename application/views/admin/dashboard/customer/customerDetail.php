<section class="content">
    <div class="container-fluid">
        <!-- Exportable Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            DETAIL CUSTOMER
                        </h2>
                        <ul class="header-dropdown m-r--5">
                            <li class="dropdown">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">more_vert</i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <?php
                                        foreach ($data as $datas) 
                                        {
                                    ?>      <li><a href="<?php echo site_url('service_advisor/customerUpdate/'.$datas->id_customer.'') ?>">Edit Informasi</a></li>
                                            <li><a href="<?php echo site_url('service_advisor/customerDelete/'.$datas->id_customer.'') ?>">Hapus Customer</a></li>
                                    <?php    
                                        }
                                    ?>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <style type="text/css">
                        /*hilangkan exportable dan menyisakan input search di tabel*/
                        .dt-buttons {
                            display: none;
                        }
                    </style>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                <tbody>
                                    <?php
                                        foreach ($dataEstimasi as $datas) {
                                    ?>
                                        <tr>
                                            <td>ID</td>
                                            <td><?= $datas->id_customer ?></td>
                                        </tr>
                                        <tr>
                                            <td>No. Polisi</td>
                                            <td><?= $datas->no_polisi ?></td>
                                        </tr>
                                         <tr>
                                            <td>Nama</td>
                                            <td><?= $datas->nama_customer ?></td>
                                        </tr>
                                        <tr>
                                            <td>No. Tlp</td>
                                            <td><?= $datas->no_hp_customer ?></td>
                                        </tr>
                                        <tr>
                                            <td>Alamat</td>
                                            <td><?= $datas->alamat_customer ?></td>
                                        </tr>
                                    <?php
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Exportable Table -->

        <!-- Exportable Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            TRACK RECORD ESTIMASI
                        </h2>
                    </div>
                    <style type="text/css">
                        /*hilangkan exportable dan menyisakan input search di tabel*/
                        .dt-buttons {
                            display: none;
                        }
                    </style>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                <thead>
                                    <tr>
                                        <th>Tipe Customer</th>
                                        <th>Tgl. Estimasi</th>
                                        <th>Tgl. Masuk</th>
                                        <th>Tgl. Janji Penyerahan</th>
                                        <th>Tgl. Selesai</th>
                                        <th>Tgl. Penyerahan</th>
                                        <th>Service Advisor</th>
                                        <th>Teknisi</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        foreach ($dataEstimasi as $datas) {
                                    ?>
                                        <tr>
                                        <?php if($datas->jenis_customer == '0'){ ?>
                                            <td>Asuransi</td>
                                        <?php }else{ ?>
                                            <td>Tunai</td>
                                        <?php } ?>
                                            <td><?php echo "".date('d M Y', strtotime($datas->tgl_estimasi)); ?></td>
                                            <td>
                                                <?php if($datas->tgl_masuk !== NULL){
                                                 echo "".date('d M Y', strtotime($datas->tgl_masuk));
                                                }else{
                                                 echo "-";
                                                } ?>            
                                            </td>
                                            <td>
                                                <?php if($datas->tgl_janji_penyerahan !== NULL){
                                                 echo "".date('d M Y', strtotime($datas->tgl_janji_penyerahan));
                                                }else{
                                                 echo "-";
                                                } ?>            
                                            </td>
                                            <td>
                                                <?php if($datas->tgl_selesai !== NULL){
                                                 echo "".date('d M Y', strtotime($datas->tgl_selesai));
                                                }else{
                                                 echo "-";
                                                } ?>            
                                            </td>
                                            <td>
                                                <?php if($datas->tgl_penyerahan !== NULL){
                                                 echo "".date('d M Y', strtotime($datas->tgl_penyerahan));
                                                }else{
                                                 echo "-";
                                                } ?>            
                                            </td>
                                            <td><?= $datas->nama_lengkap_user ?></td>
                                            <td><?= $datas->nama_teknisi ?></td>
                                            <td style="text-align: center;">
                                                <a style="margin-right: 5px;" type="button" class="btn bg-orange btn-xs waves-effect" href="<?= base_url('service_advisor/printEstimasi/').$datas->id_estimasi ?>" target="_blank"><i class="material-icons">open_in_new</i></a>
                                                <a type="button" class="btn btn-danger btn-xs waves-effect" href="javascript:void(0);"><i class="material-icons">delete_forever</i></a>
                                            </td>
                                            
                                        </tr>
                                    <?php
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Exportable Table -->
    </div>
</section>
<script type="text/javascript">
    $('.status').on('click', function(){
        var id_estimasi = $(this).data('row');
        var status_estimasi = $(this).data('status');
        console.log(id_estimasi);
        console.log(status_estimasi);
        swal({
                title: "Are you sure?",
                text: "You will not be able to cancel this Operation !",
                type: "warning",
                showCancelButton: true,
                closeOnConfirm: false,
                showLoaderOnConfirm: true,
            }, function () {
                var data = {
                'id_estimasi'        : id_estimasi,
                'status_estimasi'    : status_estimasi,
                };

                $.ajax({
                    url:"<?php echo base_url();?>service_advisor/changeStatus",
                    method:"POST",
                    crossOrigin : false,
                    data : data,
                    dataType:'json',
                    success: function(result)
                    {
                        if(result.status == 'success'){
                            swal("Good job!", "You clicked the button!", "success");
                            setTimeout(function(){
                                   window.location.reload(1);
                                }, 1000);
                        }else{
                            swal("Error Saving", " ", "warning");
                        }
                        console.log('data');
                        
                    }
                     
                });
            });
    });
</script>
