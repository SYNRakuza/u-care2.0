<section class="content">
    <div class="container-fluid">
        
        <!-- Exportable Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            LIST CUSTOMER
                        </h2>
                        <ul class="header-dropdown m-r--5">
                            <li class="dropdown">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">more_vert</i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="<?= site_url() ?>service_advisor/customerCreate">Tambah Customer</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                     <style type="text/css">
                        /*hilangkan exportable dan menyisakan input search di tabel*/
                        .dt-buttons {
                            display: none;
                        }
                    </style>
                    <div class="body">
                        <div class="table-responsive">
                            <table id="mytable" class="table table-bordered table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th style="text-align: center;vertical-align: middle;">No. Polisi</th>
                                        <th style="text-align: center;vertical-align: middle;">Jenis Kendaraan</th>
                                        <th style="text-align: center;vertical-align: middle;">Nama Customer</th>
                                        <th style="text-align: center;vertical-align: middle;">No. Telpon</th>
                                        <th style="text-align: center;vertical-align: middle;">Tgl. Estimasi</th>
                                        <th style="text-align: center;vertical-align: middle;">Alamat</th>
                                    </tr>
                                </thead>
                                <!-- <tbody>
                                    <?php
                                        foreach ($data as $datas) {
                                    ?>
                                        <tr>
                                            <td data-row="<?= $datas->id_customer ?>"><?= $datas->no_polisi ?></td>
                                            <td data-row="<?= $datas->id_customer ?>"><?= $datas->nama_jenis ?></td>
                                            <td data-row="<?= $datas->id_customer ?>"><?= $datas->nama_lengkap ?></td>
                                            <td data-row="<?= $datas->id_customer ?>"><?= $datas->no_hp ?></td>
                                            <td data-row="<?= $datas->id_customer ?>">
                                                <?php if($datas->tgl_janji_penyerahan !== NULL){
                                                 echo "".date('d M Y', strtotime($datas->tgl_estimasi));
                                                }else{
                                                 echo "-";
                                                } ?>
                                            </td>
                                            <td data-row="<?= $datas->id_customer ?>"><?= $datas->alamat ?></td>
                                        </tr>
                                    <?php
                                        }
                                    ?>
                                </tbody> -->
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Exportable Table -->
    </div>
</section>
<!-- FUNCTION JS FOR CLICKABLE ROW DATATABLE -->
<script type="text/javascript">
   
$(document).ready(function(){

    //setup datatables
    $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
      {
          return {
              "iStart": oSettings._iDisplayStart,
              "iEnd": oSettings.fnDisplayEnd(),
              "iLength": oSettings._iDisplayLength,
              "iTotal": oSettings.fnRecordsTotal(),
              "iFilteredTotal": oSettings.fnRecordsDisplay(),
              "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
              "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
          };
      };

      var table = $("#mytable").dataTable({
          initComplete: function() {
              var api = this.api();
              $('#mytable_filter input')
                  .off('.DT')
                  .on('input.DT', function() {
                      api.search(this.value).draw();
              });
          },
              oLanguage: {
              sProcessing: "loading..."
          },
              pageLength: 15,
              processing: true,
              serverSide: true,
              bLengthChange: false,
              <?php if($this->session->userdata('level') == 'pimpinan'):
              ?>
              ajax: {"url": "<?php echo base_url().'pimpinan/get_customer_json'?>", "type": "POST"},
                    columns: [
                                {"data": "no_polisi"},
                                {"data": "nama_jenis"},
                                {"data": "nama_lengkap"},
                                {"data": "no_hp"},
                                {"data": null, "searchable": false, "render": function(data, row){
                                    if (data.tgl_janji_penyerahan == null) {
                                        return '-';
                                    }
                                    else {
                                        return moment(data.tgl_estimasi).format('D MMM YYYY');
                                    }
                                }},
                                {"data": "alamat"},
                  ],
              <?php elseif($this->session->userdata('level') == 'service advisor'): ?>
              ajax: {"url": "<?php echo base_url().'service_advisor/get_customer_json'?>", "type": "POST"},
                    columns: [
                                {"data": "no_polisi"},
                                {"data": "nama_jenis"},
                                {"data": "nama_lengkap"},
                                {"data": "no_hp"},
                                {"data": null, "searchable": false, "render": function(data, row){
                                    if (data.tgl_janji_penyerahan == null) {
                                        return '-';
                                    }
                                    else {
                                        return moment(data.tgl_estimasi).format('D MMM YYYY');
                                    }
                                }},
                                {"data": "alamat"},
                  ],
              <?php endif; ?>
                order: [],
          rowCallback: function(row, data, iDisplayIndex) {
              var info = this.fnPagingInfo();
              var page = info.iPage;
              var length = info.iLength;
              $('td:eq(0)', row).html();

              $(row).on('click', function() {
                    location.href="<?php echo base_url();?>service_advisor/customer/"+data.id_customer;
                });
          }

      });

      $('#mytable').css({
        'text-align': 'center',
        'vertical-align': 'middle',
      });


});

</script>
