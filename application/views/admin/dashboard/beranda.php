<?php
    $id_user = $this->session->userdata('id_user');
    $user_level = $this->session->userdata('level');
    $yearNow = date('Y');
    // $url = echo base_url('service_advisor/getDataChart');
?>
<section class="content">
    <div class="container-fluid">
        <!-- <div class="block-header">
            <h2>DASHBOARD</h2>
        </div> -->
        <!-- to get id_user for sending to ajax function -->
        <input type="hidden" id="get_user_level" value="<?= $user_level ?>" />
        <input type="hidden" id="get_user" value="<?= $id_user ?>" />
    <?php if($this->session->userdata('level') == 'service advisor'):?>
        <input type="hidden" id="get_url" value="<?= base_url('service_advisor/getDataChart')?>"/>
        <input type="hidden" id="get_url_2" value="<?= base_url('service_advisor/listEstimasiDashboard')?>"/>
    <?php elseif($this->session->userdata('level') == 'teknisi'):?>
        <input type="hidden" id="get_url" value="<?= base_url('teknisi/getDataChart')?>"/>

    <?php else: ?>
        <input type="hidden" id="get_url" value="<?= base_url('partsman/getDataChart')?>"/>
        <input type="hidden" id="get_url_2" value="<?= base_url('partsman/listPartsDashboard')?>"/>
    <?php endif;?>
        <!-- Widgets -->
        <!-- <div class="row clearfix">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box bg-pink hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">playlist_add_check</i>
                    </div>
                    <div class="content">
                        <div class="text">NEW TASKS</div>
                        <div class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box bg-cyan hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">help</i>
                    </div>
                    <div class="content">
                        <div class="text">NEW TICKETS</div>
                        <div class="number count-to" data-from="0" data-to="257" data-speed="1000" data-fresh-interval="20"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box bg-light-green hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">forum</i>
                    </div>
                    <div class="content">
                        <div class="text">NEW COMMENTS</div>
                        <div class="number count-to" data-from="0" data-to="243" data-speed="1000" data-fresh-interval="20"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box bg-orange hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">person_add</i>
                    </div>
                    <div class="content">
                        <div class="text">NEW VISITORS</div>
                        <div class="number count-to" data-from="0" data-to="1225" data-speed="1000" data-fresh-interval="20"></div>
                    </div>
                </div>
            </div>
        </div> -->
        <!-- #END# Widgets -->
        <!-- CPU Usage -->
        <div class="row clearfix">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="card">
                    <div class="header">
                        <div class="row clearfix">
                        <?php if($this->session->userdata('level') == 'service advisor'):?>
                            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                <h2>Chart Estimasi</h2>
                            </div>
                            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                <select id="selectYear" class="form-control">
                                    <option value="2019" style="text-align: center;"<?php if($yearNow == '2019'){ echo "selected";} ?>>2019</option>
                                    <option value="2020" style="text-align: center;"<?php if($yearNow == '2020'){ echo "selected";} ?>>2020</option>
                                    <option value="2021" style="text-align: center;"<?php if($yearNow == '2021'){ echo "selected";} ?>>2021</option>
                                    <option value="2022" style="text-align: center;"<?php if($yearNow == '2022'){ echo "selected";} ?>>2022</option>
                                    <option value="2023" style="text-align: center;"<?php if($yearNow == '2023'){ echo "selected";} ?>>2023</option>
                                </select>
                            </div>
                            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                <select id="selectInOut" class="form-control">
                                    <option value="all" style="text-align: center;">ALL</option>
                                    <option value="in" style="text-align: center;">IN</option>
                                    <option value="out" style="text-align: center;">OUT</option>
                                </select>
                            </div>
                        <?php elseif($this->session->userdata('level') == 'teknisi'):?>
                            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                <h2>Tim Performance</h2>
                            </div>
                            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                <select id="selectYear2" class="form-control">
                                    <option value="2019" style="text-align: center;"<?php if($yearNow == '2019'){ echo "selected";} ?>>2019</option>
                                    <option value="2020" style="text-align: center;"<?php if($yearNow == '2020'){ echo "selected";} ?>>2020</option>
                                    <option value="2021" style="text-align: center;"<?php if($yearNow == '2021'){ echo "selected";} ?>>2021</option>
                                    <option value="2022" style="text-align: center;"<?php if($yearNow == '2022'){ echo "selected";} ?>>2022</option>
                                    <option value="2023" style="text-align: center;"<?php if($yearNow == '2023'){ echo "selected";} ?>>2023</option>
                                </select>
                            </div>

                         <?php else: ?>
                            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                <h2>Service Rate</h2>
                            </div>
                            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                <select id="selectYear2" class="form-control">
                                    <option value="2019" style="text-align: center;"<?php if($yearNow == '2019'){ echo "selected";} ?>>2019</option>
                                    <option value="2020" style="text-align: center;"<?php if($yearNow == '2020'){ echo "selected";} ?>>2020</option>
                                    <option value="2021" style="text-align: center;"<?php if($yearNow == '2021'){ echo "selected";} ?>>2021</option>
                                    <option value="2022" style="text-align: center;"<?php if($yearNow == '2022'){ echo "selected";} ?>>2022</option>
                                    <option value="2023" style="text-align: center;"<?php if($yearNow == '2023'){ echo "selected";} ?>>2023</option>
                                </select>
                            </div>

                        <?php endif; ?>
                        </div>
                    </div>
                    <div class="body">
                        <?php if($this->session->userdata('level') == 'service advisor'):?>
                            <canvas id="myChart"></canvas>
                        <?php elseif($this->session->userdata('level') == 'teknisi') : ?>
                            <canvas id="myChart2"></canvas>
                        <?php else: ?>
                            <canvas id="myChart3"></canvas>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
         <?php if($this->session->userdata('level') == 'service advisor'):?>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="card">
                    <div class="header">
                        <div class="row clearfix">
                            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2" >
                                <h2 style="margin-top: 15px;">Data Estimasi</h2>
                            </div>
                            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                <small>From:</small>
                                <input type="date" id="dateFrom" class="form-control" />
                            </div>
                            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                <small>To:</small>
                                <input type="date" id="dateTo" class="form-control" />
                            </div>
                            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                <small>Estimasi:</small>
                                <select id="dateInOut" class="form-control">
                                    <option value="" style="text-align: center;">--Pilih--</option>
                                    <option value="all" style="text-align: center;">ALL</option>
                                    <option value="in" style="text-align: center;">IN</option>
                                    <option value="out" style="text-align: center;">OUT</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-info">
                                <thead>
                                    <tr>
                                        <th>No. WO</th>
                                        <th>No. Polisi</th>
                                        <th>Nama Customer</th>
                                        <th>Tgl. Estimasi</th>
                                        <th>Tgl. Masuk</th>
                                        <th>Tgl. Janji Penyerahan</th>
                                        <th>Type Customer</th>
                                    </tr>
                                </thead>
                                <tbody id="dataListEstimasi">

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        <?php elseif($this->session->userdata('level') == 'partsman'): ?>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="card">
                    <div class="header">
                        <div class="row clearfix">
                            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2" >
                                <h2 style="margin-top: 15px;">Data Estimasi</h2>
                            </div>
                            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                <small>From:</small>
                                <input type="date" id="dateFrom" class="form-control" />
                            </div>
                            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                <small>To:</small>
                                <input type="date" id="dateTo" class="form-control" />
                            </div>
                            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                <small>Status Parts:</small>
                                <select id="dateInOut" class="form-control">
                                    <option value="" style="text-align: center;">--Pilih--</option>
                                    <option value="all" style="text-align: center;">ALL</option>
                                    <option value="in" style="text-align: center;">Parts Done</option>
                                    <option value="out" style="text-align: center;">Parts Not Done</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-info">
                                <thead>
                                    <tr>
                                        <th style="text-align:center;vertical-align: middle;">No.</th>
                                        <th style="text-align: center;vertical-align: middle;">No. WO</th>
                                        <th style="text-align: center;vertical-align: middle;">No. Polisi</th>
                                        <th style="text-align: center;vertical-align: middle;">Nama Customer</th>
                                        <th style="text-align: center;vertical-align: middle;">Tgl. Estimasi</th>
                                        <th style="text-align: center;vertical-align: middle;">Tgl. Janji Penyerahan</th>
                                        <th style="text-align: center;vertical-align: middle;">Type Customer</th>
                                    </tr>
                                </thead>
                                <tbody id="dataListEstimasi">

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        </div>
    </div>
</section>

