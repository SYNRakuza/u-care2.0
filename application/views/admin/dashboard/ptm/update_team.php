<section class="content">
        <!-- Exportable Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            Detail Estimasi
                        </h2>
                    </div>
                    <style type="text/css">
                        /*hilangkan exportable dan menyisakan input search di tabel*/
                        .dt-buttons {
                            display: none;
                        }
                        .opsi{
                            color: #e56e00;
                        }
                        @media(max-width:412px){
                            .btn{
                                width: 100% !important;
                            }
                        }
                    </style>
                    <div class=" card body">
                        <div class="table-responsive">
                            <table class="table">
                                <tbody style="border: hidden;">

                                    <?php
                                        $i = 1;//variabel berapa kali loop
                                        foreach($data3 as $x){
                                            if($i>1){//supaya satu kali loop saja
                                                break;
                                            }
                                    ?>
                                    <tr>
                                        <td class="col-xs-2"><b>ID Estimasi</b></td>
                                        <td class="col-xs-1"><b>:</b></td>
                                        <td><b><?= $x->id_estimasi ?></b></td>
                                    </tr>

                                    <tr>
                                        <td class="col-xs-2"><b>Nama Customer</b></td>
                                        <td class="col-xs-1"><b>:</b></td>
                                        <td><b><?= $x->nama_lengkap ?></b></td>
                                    </tr>

                                    <tr>
                                        <td class="col-xs-2"><b>No. Telepon</b></td>
                                        <td class="col-xs-1"><b>:</b></td>
                                        <td><b><?= $x->no_hp ?></b></td>
                                    </tr>

                                    <tr>
                                        <td class="col-xs-2"><b>Nomor Polisi</b></td>
                                        <td class="col-xs-1"><b>:</b></td>
                                        <td><b><?= $x->no_polisi ?></b></td>
                                    </tr>

                                    <tr>
                                        <td class="col-xs-2"><b>Jenis Kendaraan</b></td>
                                        <td class="col-xs-1"><b>:</b></td>
                                        <td><b><?= $x->nama_jenis ?></b></td>
                                    </tr>

                                    <?php
                                        $i++;
                                    } ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover js-exportable">
                                <tbody>

                                        <tr>
                                            <th  style="text-align: center;">No.</th>
                                            <th  style="text-align: center;">Nama Barang</th>
                                        </tr>

                                        <?php
                                            $i=1;
                                            foreach($data3 as $x){
                                        ?>

                                         <tr>
                                            <td  style="text-align: center;"><?= $i++ ?></td>
                                            <td  style="text-align: center;"><?= $x->nama_item ?></td>
                                        </tr>

                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                        <?php foreach ($data1 as $datas1) { ?>
                        <form method="POST" action="<?= base_url('ptm/dispatchingUpdateTeamAction/'.$datas1->id_estimasi) ?>">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <h5>Ganti Tim Teknisi :</h5>
                                    <select class="form-control show-tick" name="tim_teknisi">
                                        <optgroup label="Tim Saat Ini" class="opsi">
                                            <option value="<?= $datas1->tim_teknisi ?>">-- <?= $datas1->tim_teknisi ?> --</option>
                                        </optgroup>
                                        <optgroup label="Ganti Tim Lain" class="opsi">
                                            <?php foreach ($data2 as $datas2) { ?>
                                            <option value="<?= $datas2->nama_lengkap_user ?>"><?= $datas2->nama_lengkap_user ?></option>
                                            <?php } ?>
                                        </optgroup>
                                    </select>    
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <button type="submit" class="btn btn-lg bg-green waves-effect">
                                    <i class="material-icons">save</i>
                                        <span>UPDATE</span>
                                </button>
                            </div>
                        </form>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Exportable Table -->
</section>
