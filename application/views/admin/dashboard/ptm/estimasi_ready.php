<style type="text/css">
    .badge-heavy {
        border-radius: 10px;
        font-weight: 100;
        font-size: 13px;
        background-color: #FF5722;
    }
    .badge-medium {
        border-radius: 10px;
        font-weight: 100;
        font-size: 13px;
        background-color: #FF9800;
    }
    .badge-light {
        border-radius: 10px;
        font-weight: 100;
        font-size: 13px;
        background-color: #FFC107;
    }
    .table td, th {
        vertical-align: middle !important;
    }
</style>
<section class="content">
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Job Dispatch
                    </h2>
                </div>
                <style type="text/css">
                    /*hilangkan exportable dan menyisakan input search di tabel*/
                    .dt-buttons {
                        display: none;
                    }
                    .text-mid{
                        text-align: center;
                    }
                    th{
                        text-align: center;
                    }
                </style>
                <div class="body">
                    <div>
                        <?php 
                        // foreach ($test as $key) {
                        //     echo $key;
                        // } 
                        // var_dump($test);
                        // echo"".$test;
                        ?>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover js-exportable dataTable">
                                <thead>
                                    <tr>
                                        <!-- <th><?php echo $test;?></th> -->
                                        <th>No. WO</th>
                                        <th>No. Polisi</th>
                                        <th>Nama Customer</th>
                                        <th>Tgl. Janji Penyerahan</th>
                                        <th>Kategori</th>
                                        <th>Nama SA</th>
                                        <th>Status Parts</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>                               
                                <tbody>
                                        <?php
                                            $i=1;
                                            foreach ($data as $datas) {

                                                $kategori = "";
                                                if ($datas->kategori_jasa == '0') {
                                                    $kategori='<span class="badge badge-light">Light</span>';
                                                }elseif ($datas->kategori_jasa == '1') {
                                                    $kategori='<span class="badge badge-medium">Medium</span>';
                                                }else{
                                                    $kategori='<span class="badge badge-heavy">Heavy</span>';
                                                };
                                        ?>
                                            <tr>
                                                <td><?= $datas->no_wo ?></td>
                                                <td><?= $datas->no_polisi ?></td>
                                                <td><?= $datas->nama_customer ?></td>
                                                <td>
                                                    <?php if($datas->tgl_janji_penyerahan !== NULL){
                                                     echo "".date('d M Y', strtotime($datas->tgl_janji_penyerahan));
                                                    }else{
                                                     echo "-";
                                                    } ?>
                                                </td>
                                                <td><?= $kategori ?></td>
                                                <td><?= $datas->nama_sa ?></td>
                                                <td style="vertical-align: middle;">
                                                    <?php if($datas->isParts):?>
                                                    <div class="progress" style="margin-bottom: 0px; height: 12px;">
                                                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: <?= $datas->totalReadyParts/$datas->totalParts*100 ?>%; line-height: 12px; color: black;" id="status_<?= $datas->id_estimasi ?>">
                                                            <?php echo round($datas->totalReadyParts/$datas->totalParts*100)?>%<span class="sr-only"></span>
                                                        </div>
                                                    </div>
                                                <?php else:
                                                    echo "no parts"
                                                    ?>
                                                <?php endif; ?>
                                                </td>
                                                <td>
                                                    <a style="margin-right: 5px;" href="<?php echo base_url().'Ptm/nextJobDetail/'.$datas->id_estimasi; ?>" class="btn bg-orange btn-xs waves-effect"><i class="material-icons">adjust</i>
                                                    </a>
                                                    <button type="button" data-row="<?= $datas->id_estimasi ?>" class="btn btn-xs bg-green waves-effect m-r-20 modalInput"><i class="material-icons">call_received</i></button>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="modal fade" id="defaultModal" tabindex="-1" role="dialog">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <form>
                                    <div class="modal-header bg-orange">
                                        <h4 class="modal-title" id="defaultModalLabel">Tambahkan Data</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <h5>Pilih Tim Teknisi :</h5>
                                                <select class="form-control show-tick" name="tim_teknisi" id="selectTim">
                                                    <!-- <option value="">-- Pilih Team --</option> -->
                                                    <?php foreach ($dataT as $datas2) { ?>
                                                    <option value="<?= $datas2->id_user ?>"><?= $datas2->nama_lengkap_user ?></option>
                                                    <?php } ?>
                                                </select>    
                                            </div>
                                            <div class="form-line">
                                                <h5>Pilih Proses :</h5>
                                                <select class="form-control show-tick" name="tim_teknisi" id="selectProses">
                                                    <!-- <option value="">-- Pilih Proses --</option> -->
                                                    <option value="1">Body Repair</option>
                                                    <option value="2">Preparation</option>
                                                    <option value="3">Masking</option>
                                                    <option value="4">Painting</option>
                                                    <option value="5">Polishing</option>
                                                    <option value="6">Re Assembling</option>
                                                    <option value="7">Washing</option>
                                                    <option value="8">Final Inspection</option>
                                                </select>    
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer form-group form-float">
                                        <button type="button" class="btn btn-sm bg-green waves-effect save">
                                            <i class="material-icons">save</i>
                                            <span>INPUT</span>
                                        </button>
                                        <button type="button" class="btn bg-red btn-sm waves-effect" data-dismiss="modal"><i class="material-icons">cancel</i>
                                            <span>CLOSE</span>
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>                                    
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
//     $('.dataTable').on('click', 'tbody td', function() {
//         var idEstimasi= $(this).data("row");
//         window.location = "<?php echo base_url();?>Ptm/selectTeam/"+idEstimasi;
  
// })
    var id_estimasi = '';
    $('.modalInput').on('click', function(){
        $('#defaultModal').modal('show');
        id_estimasi = $(this).data('row');
        console.log(id_estimasi);
    });
    $('.save').on('click', function(){
            var selectTim = $('#selectTim').val();
            var selectProses = $('#selectProses').val();
            // var id_estimasi = $(this).data('row');
            // var status_estimasi = $(this).data('status');
            // console.log(id_estimasi);
            // console.log(selectTim);
            // console.log(status_estimasi);
            // console.log(tgl_janji_penyerahan);
            // if(tgl_janji_penyerahan == ''){
            //     alert("Input tanggal janji penyerahan !");
            //     return false;
            // }
            if(selectTim == ''){
                alert("Pilih Nama Tim Yang Tersedia !");
                return false;
            }
            if(selectProses == ''){
                alert("Pilih Proses Yang Sesuai !");
                return false;
            }
            swal({
                    title: "Apakah Anda Yakin?",
                    text: "Silahkan Periksa Kembali Data Anda Sebelum Men Submit !",
                    type: "warning",
                    showCancelButton: true,
                    closeOnConfirm: false,
                    showLoaderOnConfirm: true,
                }, function () {
                    var data = {
                    'id_estimasi'          : id_estimasi,
                    'selectTim'            : selectTim,
                    'selectProses'         : selectProses
                    };

                    $.ajax({
                        url:"<?php echo base_url();?>Ptm/ajaxInputModal",
                        method:"POST",
                        // crossOrigin : false,
                        data : data,
                        // dataType:'json',
                        success: function(result)
                        {
                            console.log(result);
                            if(result == 'success'){
                                swal("Selamat!", "Estimasi Anda Siap Diproses!", "success");
                                setTimeout(function(){
                                       window.location.reload(1);
                                    }, 1000);
                            }else{
                                swal("Error Saving", " ", "warning");
                            }
                            console.log('data');
                            
                        }
                         
                    });
                });
        });
</script>
