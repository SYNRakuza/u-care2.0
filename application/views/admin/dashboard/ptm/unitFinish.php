<section class="content">
    <div class="container-fluid">
        
        <!-- Exportable Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            Status Delivery
                        </h2>
                    </div>
                     <style type="text/css">
                        /*hilangkan exportable dan menyisakan input search di tabel*/
                        .dt-buttons {
                            display: none;
                        }
                        .middle{
                            text-align: center;
                        }
                        th{
                            text-align: center;
                        }
                        .kosong{
                            background:red;
                            color: white;
                            -webkit-animation: flash linear 2s infinite;
                            animation: flash linear 2s infinite;
                        }
                        @-webkit-keyframes flash {
                           from {background-color: #ff0a0a;}
                           to {background-color: orange;}
                        }
                        @keyframes flash {
                            from {background-color: #ff0a0a;}
                            to {background-color: orange;}
                        }
                    </style>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable js-exportable middle">
                                <thead>
                                    <tr style="background: green; color: white">
                                        <th>No. WO</th>
                                        <th>No. Polisi</th>
                                        <th>Nama Customer</th>
                                        <th>Nama SA</th>
                                        <th>Tim Teknisi</th>
                                        <th>Kategori</th>
                                        <th>Tgl. Masuk</th>
                                        <th>Tgl. Perjanjian</th>
                                        <th>Tgl. Selesai</th>
                                        <th>Tgl. Penyerahan</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $ii = 0;
                                        foreach ($dataEstimasi as $datas) {
                                            $ii++;
                                            $kategori = "";

                                            if ($datas->kategori_jasa == '0') {
                                                $kategori="Light";
                                            }elseif ($datas->kategori_jasa == '1') {
                                                $kategori="Medium";
                                            }else{
                                                $kategori="Heavy";
                                            };
                                    ?>
                                    <tr>
                                        <td><?= $datas->nomor_wo ?></td>
                                        <td><?= $datas->no_polisi ?></td>
                                        <td><?= $datas->nama_lengkap ?></td>
                                        <td><?= $datas->nama_lengkap_user ?></td>
                                        <td><?= $datas->nama_teknisi ?></td>
                                        <td><?= $kategori ?></td>
                                        <td>
                                            <?php if($datas->tgl_masuk !== NULL){
                                             echo "".date('d M Y', strtotime($datas->tgl_masuk));
                                            }else{
                                             echo "-";
                                            } ?> 
                                        </td>
                                        <td>
                                            <?php if($datas->tgl_janji_penyerahan !== NULL){
                                             echo "".date('d M Y', strtotime($datas->tgl_janji_penyerahan));
                                            }else{
                                             echo "-";
                                            } ?> 
                                        </td>
                                        <td>
                                            <?php if($datas->final_inspection_out !== NULL){
                                             echo "".date('d M Y', strtotime($datas->final_inspection_out));
                                            }else{
                                             echo "-";
                                            } ?> 
                                        </td>
                                        <td>
                                            <?php if($datas->tgl_penyerahan !== NULL){
                                             echo "".date('d M Y', strtotime($datas->tgl_penyerahan));
                                            }else{
                                             echo "-";
                                            } ?> 
                                        </td>
                                        <td>
                                            <a style="margin-right: 5px;" type="button" class="btn bg-orange btn-xs waves-effect" href="<?= base_url('ptm/detailProduksi/').$datas->id_estimasi ?>"><i class="material-icons">adjust</i></a>
                                        </td>
                                    </tr>
                                    <?php
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Exportable Table -->
    </div>
</section>

