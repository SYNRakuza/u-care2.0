<?php
    $controller = $this->uri->segment(1); $modul = $this->uri->segment(2); $params = $this->uri->segment(3);
?>
 <style type="text/css">
    /*hilangkan exportable dan menyisakan input search di tabel*/
    .dt-buttons {
        display: none;
    }
    .middle{
        text-align: center;
    }
    th{
        text-align: center;
    }
    .kosong{
        background:red;
        color: white;
        -webkit-animation: flash linear 2s infinite;
        animation: flash linear 2s infinite;
    }
    .proses{
        width: 13%;
    }
    .width-med{
        width: 20%;
    }
    .badge-heavy {
        border-radius: 10px;
        font-weight: 100;
        font-size: 13px;
        background-color: #FF5722;
    }
    .badge-medium {
        border-radius: 10px;
        font-weight: 100;
        font-size: 13px;
        background-color: #FF9800;
    }
    .badge-light {
        border-radius: 10px;
        font-weight: 100;
        font-size: 13px;
        background-color: #FFC107;
    }
    @-webkit-keyframes flash {
       from {background-color: #ff0a0a;}
       to {background-color: orange;}
    }
    @keyframes flash {
        from {background-color: #ff0a0a;}
        to {background-color: orange;}
    }
</style>
<section class="content">
    <div class="container-fluid">
        
        <!-- Exportable Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            Monitoring Produksi
                        </h2>
                    </div>
                    <div class="body">
                        <?php
                            $i = 0; 
                            foreach($dataCount as $datas){
                                $i++;
                            $countJasa[$i] = $datas->countJasa;
                        }?>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable js-exportable middle">
                                <thead>
                                    <tr>
                                        <th>No. WO</th>
                                        <th>No. Polisi</th>
                                        <th>Nama Customer</th>
                                        <?php
                                        if ($this->session->userdata('level') != "service advisor"){
                                        ?>
                                        <th>Nama SA</th>
                                        <?php } ?>
                                        <?php
                                        if ($this->session->userdata('level') != "teknisi"){
                                        ?>
                                        <th>Nama Teknisi</th>
                                        <?php } ?>
                                        <th>Kategori Jasa</th>
                                        <th>Tgl. Masuk</th>
                                        <th>Tgl. Janji Penyerahan</th>
                                        <th>Status Produksi</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $ii = 0;
                                        foreach ($dataEstimasi as $datas) {
                                            $ii++;
                                            $warnaKosong = "";
                                            $kategori = "";

                                                if ($datas->kategori_jasa == '0') {
                                                    $kategori='<span class="badge badge-light">Light</span>';
                                                }elseif ($datas->kategori_jasa == '1') {
                                                    $kategori='<span class="badge badge-medium">Medium</span>';
                                                }else{
                                                    $kategori='<span class="badge badge-heavy">Heavy</span>';
                                                };  

                                                if ($datas->status_produksi =='0' || $datas->is_confirm_teknisi == NULL ) {
                                                    $warnaKosong = "kosong";
                                                };
                                    ?>
                                    <tr>
                                        <td><?= $datas->nomor_wo ?></td>
                                        <td class="width-med"><?= $datas->no_polisi ?></td>
                                        <td><?= $datas->nama_lengkap ?></td>
                                        <?php
                                        if ($this->session->userdata('level') != "service advisor"){
                                        ?>
                                        <td><?= $datas->nama_lengkap_user ?></td>
                                        <?php } ?>
                                        <?php
                                        if ($this->session->userdata('level') != "teknisi"){
                                        ?>
                                        <td><?= $datas->nama_teknisi ?></td>
                                        <?php } ?>
                                        <td><?= $kategori ?></td>
                                        <td>
                                            <?php if($datas->tgl_masuk !== NULL){
                                             echo "".date('d M Y', strtotime($datas->tgl_masuk));
                                            }else{
                                             echo "-";
                                            } ?> 
                                        </td>
                                        <td>
                                            <?php if($datas->tgl_janji_penyerahan !== NULL){
                                             echo "".date('d M Y', strtotime($datas->tgl_janji_penyerahan));
                                            }else{
                                             echo "-";
                                            } ?> 
                                        </td>
                                        <td class="<?php echo $warnaKosong;?> proses">
                                        <?php 
                                          if ($datas->is_confirm_teknisi == NULL) {
                                                echo ">> NOT IN PROCESS <<";
                                            }else{
                                                if($datas->status_produksi == '1'):
                                                      echo "Body Repair";
                                                  elseif($datas->status_produksi == '2'):
                                                      echo "Preparation";
                                                  elseif($datas->status_produksi == '3'):
                                                      echo "Masking";
                                                  elseif($datas->status_produksi == '4'):
                                                      echo "Painting";
                                                  elseif($datas->status_produksi == '5'):
                                                      echo "Polishing";
                                                  elseif($datas->status_produksi == '6'):
                                                      echo "Re Assembling";
                                                  elseif($datas->status_produksi == '7'):
                                                      echo "Washing";
                                                  elseif($datas->status_produksi == '8'):
                                                      echo "Final Inspection";
                                                  endif;
                                            }                                                      
                                            ?>
                                        </td>
                                        <td class="width-med">
                                            <a style="margin-right: 5px;" type="button" class="btn bg-orange btn-xs waves-effect" href="<?= base_url('ptm/detailProduksi/').$datas->id_estimasi ?>"><i class="material-icons">adjust</i></a>
                                            <?php
                                                if ($this->session->userdata('level') == "ptm") {
                                            ?>
                                            <a style="margin-right: 5px;" type="button" class="btn bg-green btn-xs waves-effect modalInput" data-namateknisi ="<?= $datas->nama_teknisi ?>" data-idteknisi ="<?= $datas->id_teknisi ?>" data-row="<?= $datas->id_estimasi ?>"><i class="material-icons">mode_edit</i></a>
                                            <?php } ?>
                                        </td>   
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="modal fade" id="defaultModal" tabindex="-1" role="dialog">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form>
                                        <div class="modal-header bg-orange">
                                            <h4 class="modal-title" id="defaultModalLabel">Tambahkan Data</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <h5>Pilih Tim Teknisi :</h5>
                                                    <select class="form-control show-tick" name="tim_teknisi" id="selectTim" required>
                                                        <option></option>
                                                        <?php foreach ($dataT as $datas) { ?>
                                                            <option value="<?= $datas->id_user ?>">
                                                                <?= $datas->nama_lengkap_user ?>
                                                            </option>
                                                        <?php } ?>
                                                    </select>    
                                                </div>
                                                <h6 id="test1"></h6>
                                            </div>
                                        </div>
                                        <div class="modal-footer form-group form-float">
                                            <button type="button" class="btn btn-sm bg-green waves-effect save">
                                                <i class="material-icons">save</i>
                                                <span>UPDATE</span>
                                            </button>
                                            <button type="button" class="btn bg-red btn-sm waves-effect" data-dismiss="modal"><i class="material-icons">cancel</i>
                                                <span>CANCLE</span>
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Exportable Table -->
    </div>
</section>
<script type="text/javascript">
    var id_estimasi = '';
    $('.modalInput').on('click', function(){
        var id_teknisi = $(this).data('idteknisi');
        var nama_teknisi = $(this).data('namateknisi');
        $('#defaultModal').modal('show');
        document.getElementById("test1").innerHTML = "Current Teknisi : " + nama_teknisi;
        id_estimasi = $(this).data('row');
    });
    $('.save').on('click', function(){
            // var id_estimasi = $(this).data('row');
            // var status_estimasi = $(this).data('status');
            // console.log(status_estimasi);
            // console.log(tgl_janji_penyerahan);
            // if(tgl_janji_penyerahan == ''){
            //     alert("Input tanggal janji penyerahan !");
            //     return false;
            // }
            var selectTim = $('#selectTim').val();

            swal({
                    title: "Apakah Anda Yakin?",
                    text: "Silahkan Periksa Kembali Data Anda Sebelum Men Submit !",
                    type: "warning",
                    showCancelButton: true,
                    closeOnConfirm: false,
                    showLoaderOnConfirm: true,
                }, function () {
                    var data = {
                    'id_estimasi'          : id_estimasi,
                    'selectTim'            : selectTim
                    };

                    $.ajax({
                        url:"<?php echo base_url();?>Ptm/ajaxUpdateModal",
                        method:"POST",
                        // crossOrigin : false,
                        data : data,
                        // dataType:'json',
                        success: function(result)
                        {
                            console.log(result);
                            if(result == 'success'){
                                swal("Selamat!", "Berhasil Mengganti Teknisi!", "success");
                                setTimeout(function(){
                                       window.location.reload(1);
                                    }, 1000);
                            }else{
                                swal("Error Saving", " ", "warning");
                            }
                            console.log('data');
                            
                        }
                         
                    });
                });
        });
</script>

