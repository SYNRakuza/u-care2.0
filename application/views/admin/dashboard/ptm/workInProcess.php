<section class="content">
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Work In Process
                    </h2>
                </div>
                <style type="text/css">
                    /*hilangkan exportable dan menyisakan input search di tabel*/
                    .dt-buttons {
                        display: none;
                    }
                    .text-mid{
                        text-align: center;
                    }
                    th{
                        text-align: center;
                    }
                    .variety{
                        text-align: center; 
                        vertical-align: 
                        middle; 
                        width: 79px;
                    }                   
                </style>
                <div class="body">
                    <!-- <?php foreach ($count as $key) {
                        echo"".$key->total."-".$key->tim_teknisi;?><br />
                        <?php
                    } ?>
                    <table class="table table-bordered table-striped table-hover js-exportable dataTable">
                            <tbody>
                                    <?php foreach($count as $q){?>
                                        <tr>
                                            <td><?php echo"".$q->total;?></td>    
                                        </tr>
                                    <?php } ?>
                                </tbody>
                    </table>  -->
                    <div class="text-mid">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover js-exportable dataTable">
                            	<thead>
                            		<tr role="row">
                            			<!-- <th rowspan="3" style="text-align: center; vertical-align: middle; width: 21px; background: orange; color:white;" class="sorting_disabled" colspan="1">No</th> -->
                            			<th colspan="1" rowspan="3" style="text-align: center; vertical-align: middle; width: 175px; background: orange; color:white;" colspan="1">Nama Teknisi</th>
                            			<th colspan="27" style="text-align: center; vertical-align: middle; background: #17aa14; color:white;" rowspan="1">Job Progress Monitoring
                            			</th>
                            		</tr>
                            		<tr role="row" style="background: orange; color:white;">
                            			<th colspan="3" style="text-align: center; vertical-align: middle" rowspan="1">
                            			Body Repair
                            			</th>
                            			<th colspan="3" style="text-align: center; vertical-align: middle" rowspan="1">
                            			Preparation
                            			</th>
                            			<th colspan="3" style="text-align: center; vertical-align: middle" rowspan="1">
                            			Masking
                            			</th>
                            			<th class="col-lg-1 sorting_disabled dt-body-right" rowspan="1" style="text-align: center; vertical-align: middle; width: 78px;" colspan="3">
                            			Painting
                            			</th>
                            			<th colspan="3" style="text-align: center; vertical-align: middle" rowspan="1">
                            			Polishing
                            			</th>
                            			<th colspan="3" style="text-align: center; vertical-align: middle" rowspan="1">
                            			Re Assembling
                            			</th>
                            			<th class="col-lg-1 sorting_disabled dt-body-right" rowspan="1" style="text-align: center; vertical-align: middle; width: 78px;" colspan="3">
                            			Washing
                            			</th>
                            			<th colspan="3" style="text-align: center; vertical-align: middle" rowspan="1">
                            			Final Inspection
                            			</th>
                            			<th colspan="3" style="text-align: center; vertical-align: middle" rowspan="1">
                            			Total
                            			</th>
                            		</tr>
                            		<tr role="row">
                            			<!-- proses1 -->
                            			<th class="col-lg-1 sorting_disabled dt-body-right variety" rowspan="1" colspan="1">
                            			L
                            			</th>
                            			<th class="col-lg-1 sorting_disabled dt-body-right variety" rowspan="1" colspan="1">
                            			M
                            			</th>
                            			<th class="col-lg-1 sorting_disabled dt-body-right variety" rowspan="1" colspan="1">
                            			H
                            			</th>
                            			<!-- proses2 -->
                            			<th class="col-lg-1 sorting_disabled dt-body-right variety" rowspan="1" colspan="1">
                            			L
                            			</th>
                            			<th class="col-lg-1 sorting_disabled dt-body-right variety" rowspan="1" colspan="1">
                            			M
                            			</th>
                            			<th class="col-lg-1 sorting_disabled dt-body-right variety" rowspan="1" colspan="1">
                            			H
                            			</th>
                            			<!-- proses3 -->
                            			<th class="col-lg-1 sorting_disabled dt-body-right variety" rowspan="1" colspan="1">
                            			L
                            			</th>
                            			<th class="col-lg-1 sorting_disabled dt-body-right variety" rowspan="1" colspan="1">
                            			M
                            			</th>
                            			<th class="col-lg-1 sorting_disabled dt-body-right variety" rowspan="1" colspan="1">
                            			H
                            			</th>
                            			<!-- proses4 -->
                            			<th class="col-lg-1 sorting_disabled dt-body-right variety" rowspan="1" colspan="1">
                            			L
                            			</th>
                            			<th class="col-lg-1 sorting_disabled dt-body-right variety" rowspan="1" colspan="1">
                            			M
                            			</th>
                            			<th class="col-lg-1 sorting_disabled dt-body-right variety" rowspan="1" colspan="1">
                            			H
                            			</th>
                            			<!-- proses5 -->
                            			<th class="col-lg-1 sorting_disabled dt-body-right variety" rowspan="1" colspan="1">
                            			L
                            			</th>
                            			<th class="col-lg-1 sorting_disabled dt-body-right variety" rowspan="1" colspan="1">
                            			M
                            			</th>
                            			<th class="col-lg-1 sorting_disabled dt-body-right variety" rowspan="1" colspan="1">
                            			H
                            			</th>
                            			<!-- proses6 -->
                            			<th class="col-lg-1 sorting_disabled dt-body-right variety" rowspan="1" colspan="1">
                            			L
                            			</th>
                            			<th class="col-lg-1 sorting_disabled dt-body-right variety" rowspan="1" colspan="1">
                            			M
                            			</th>
                            			<th class="col-lg-1 sorting_disabled dt-body-right variety" rowspan="1" colspan="1">
                            			H
                            			</th>
                            			<!-- proses7 -->
                            			<th class="col-lg-1 sorting_disabled dt-body-right variety" rowspan="1" colspan="1">
                            			L
                            			</th>
                            			<th class="col-lg-1 sorting_disabled dt-body-right variety" rowspan="1" colspan="1">
                            			M
                            			</th>
                            			<th class="col-lg-1 sorting_disabled dt-body-right variety" rowspan="1" colspan="1">
                            			H
                            			</th>
                            			<!-- proses8 -->
                            			<th class="col-lg-1 sorting_disabled dt-body-right variety" rowspan="1" colspan="1">
                            			L
                            			</th>
                            			<th class="col-lg-1 sorting_disabled dt-body-right variety" rowspan="1" colspan="1">
                            			M
                            			</th>
                            			<th class="col-lg-1 sorting_disabled dt-body-right variety" rowspan="1" colspan="1">
                            			H
                            			</th>
                            			<!-- total -->
                            			<th class="col-lg-1 sorting_disabled dt-body-right variety" rowspan="1" colspan="1">
                            			L
                            			</th>
                            			<th class="col-lg-1 sorting_disabled dt-body-right variety" rowspan="1" colspan="1">
                            			M
                            			</th>
                            			<th class="col-lg-1 sorting_disabled dt-body-right variety" rowspan="1" colspan="1">
                            			H
                            			</th>	
                            		</tr>
                            	</thead>
                            	<tbody>
                            		<!-- <?php var_dump($hitung) ?> -->
                                    <?php
                                        function sembarang($count){
                                            if ($count == NULL) {
                                                return "0";
                                            }else{
                                                return $count;
                                            }
                                        }

                                        foreach ($hitung as $key) {
                                            // var_dump($key);
                                    ?>
                                    <tr>
                                        <td>
                                            <?= $key->nama_lengkap_user ?>
                                        </td>
                                        <td><?= sembarang($key->bodyPaintLight) ?></td>
                                        <td><?= sembarang($key->bodyPaintMedium) ?></td>
                                        <td><?= sembarang($key->bodyPaintHeavy) ?></td>
                                        <td><?= sembarang($key->preparationLight) ?></td>
                                        <td><?= sembarang($key->preparationMedium) ?></td>
                                        <td><?= sembarang($key->preparationHeavy) ?></td>
                                        <td><?= sembarang($key->maskingLight) ?></td>
                                        <td><?= sembarang($key->maskingMedium) ?></td>
                                        <td><?= sembarang($key->maskingHeavy) ?></td>
                                        <td><?= sembarang($key->paintingLight) ?></td>
                                        <td><?= sembarang($key->paintingMedium) ?></td>
                                        <td><?= sembarang($key->paintingHeavy) ?></td>
                                        <td><?= sembarang($key->polishingLight) ?></td>
                                        <td><?= sembarang($key->polishingMedium) ?></td>
                                        <td><?= sembarang($key->polishingHeavy) ?></td>
                                        <td><?= sembarang($key->reassemblingLight) ?></td>
                                        <td><?= sembarang($key->reassemblingMedium) ?></td>
                                        <td><?= sembarang($key->reassemblingHeavy) ?></td>
                                        <td><?= sembarang($key->washingLight) ?></td>
                                        <td><?= sembarang($key->washingMedium) ?></td>
                                        <td><?= sembarang($key->washingHeavy) ?></td>
                                        <td><?= sembarang($key->finalInspectionLight) ?></td>
                                        <td><?= sembarang($key->finalInspectionMedium) ?></td>
                                        <td><?= sembarang($key->finalInspectionHeavy) ?></td>
                                        <td><?= sembarang($key->totalLight) ?></td>
                                        <td><?= sembarang($key->totalMedium) ?></td>
                                        <td><?= sembarang($key->totalHeavy) ?></td>
                                    </tr>
                                    <?php } ?>
                            	</tbody>
                            </table>
                        </div>
                    </div>                    
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
</script>