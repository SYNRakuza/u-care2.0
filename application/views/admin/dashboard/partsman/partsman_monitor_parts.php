<section class="content">
    <div class="container-fluid">

        <!-- Exportable Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            Daftar Order
                        </h2>

                    </div>
                     <style type="text/css">
                        /*hilangkan exportable dan menyisakan input search di tabel*/
                        .dt-buttons {
                            display: none;
                        }
                    </style>
                    <div class="body">
                        <div class="table-responsive">
                            <form method="POST" action="<?= base_url('partsman/proses_order') ?>">
                            <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                <h6 style="color: orange;">
                                    <?php
                                        $info = $this->session->flashdata('order_sukses');
                                        if(!empty($info)){
                                            echo $info;
                                        }
                                        $info1 = $this->session->flashdata('history_sukses');
                                            if(!empty($info1)){
                                                echo $info1;
                                            }
                                    ?>
                        </h6>
                                <thead>
                                    <tr>
                                        <th  style="text-align: center;vertical-align: middle;">No. WO</th>
                                        <th  style="text-align: center;vertical-align: middle;">No. Polisi</th>
                                        <th  style="text-align: center;vertical-align: middle;">Nama Customer</th>
                                        <th  style="text-align: center;vertical-align: middle;">Nama SA</th>
                                        <th  style="text-align: center;vertical-align: middle;">Nama Teknisi</th>
                                        <th  style="text-align: center;vertical-align: middle;">Tanggal Estimasi</th>
                                        <th  style="text-align: center;vertical-align: middle;">Tgl. Janji Penyerahan</th>
                                        <th  style="text-align: center;vertical-align: middle;">Complete %</th>
                                        <th  style="text-align: center;vertical-align: middle;">Action</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <?php
                                        $i = 1;
                                        foreach($listEstimasi as $x){

                                    ?>

                                    <tr>
                                        <td  style="text-align: center;vertical-align: middle;"><?= $x->nomor_wo ?></td>
                                        <td  style="text-align: center;vertical-align: middle;"><?= $x->no_polisi ?></td>
                                        <td  style="text-align: center;vertical-align: middle;"><?= $x->nama_lengkap ?></td>
                                        <td  style="text-align: center;vertical-align: middle;"><?= $x->nama_lengkap_user ?></td>
                                        <td  style="text-align: center;vertical-align: middle;">
                                            <?php
                                                if($x->namaTeknisi == '' || $x->namaTeknisi == NULL){
                                                    echo "-";
                                                }else{
                                                    echo $x->namaTeknisi;
                                                }
                                            ?>
                                        </td>
                                        <td  style="text-align: center;vertical-align: middle;"><?= date('d-M-Y', strtotime($x->tgl_estimasi));?></td>
                                        <td  style="text-align: center;vertical-align: middle;"><?= date('d-M-Y', strtotime($x->tgl_janji_penyerahan));?></td>
                                        <td  style="text-align: center;vertical-align: middle;">
                                            <?php
                                                if($x->done_order == 2){
                                                    echo "Done <br>";
                                            ?>
                                                    <div class="progress" style="margin-bottom: 0px; height: 12px;">
                                                    <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: <?= $x->totalReadyParts/$x->totalParts*100 ?>%; line-height: 12px; color: black;" id="status_<?= $x->id ?>">
                                                        <?php echo round($x->totalReadyParts/$x->totalParts*100)?>%<span class="sr-only"></span>
                                                    </div>
                                                <?php
                                                }else{
                                                    echo "Waiting";?>
                                                    <div class="progress" style="margin-bottom: 0px; height: 12px;">
                                                    <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: <?= $x->totalReadyParts/$x->totalParts*100 ?>%; line-height: 12px; color: black;" id="status_<?= $x->id ?>">
                                                        <?php echo round($x->totalReadyParts/$x->totalParts*100)?>%<span class="sr-only"></span>
                                                    </div>
                                                <?php } ?>

                                        </td>
                                        <td  style="text-align: center;vertical-align: middle;">
                                            <a href="<?= base_url('partsman/monitor_parts/'.$x->id) ?>" class="btn btn-primary waves-effect">Detail</a>
                                        </td>
                                    </tr>
                                    <?php $i++; } ?>
                                </tbody>
                            </table>

                        </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Exportable Table -->
    </div>
</section>

<!-- FUNCTION JS FOR CLICKABLE ROW DATATABLE
<script type="text/javascript">
    $('.dataTable').on('click', 'tbody td', function() {
        var idEstimasi= $(this).data("row");
        window.location = "<?php echo base_url();?>service_advisor/customer/"+idEstimasi;

})
</script>
-->