<section class="content">
    <div class="container-fluid">

        <!-- Exportable Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            Detail Estimasi
                        </h2>
                    </div>
                     <style type="text/css">
                        /*hilangkan exportable dan menyisakan input search di tabel*/
                        .dt-buttons {
                            display: none;
                        }
                    </style>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <h5>Data Parts</h5>
                                <div class="table-responsive">
                                    <table class="table table-customer">
                                        <tbody>
                                        <?php
                                            $i=1;
                                        foreach($data as $datas){
                                            if($i>1){
                                                break;
                                            }
                                        ?>
                                            <tr>
                                                <td width="125">Nomor Parts</td>
                                                <td width="1">:</td>
                                                <td><?= $datas->nomor_parts ?></td>
                                            </tr>
                                             <tr>
                                                <td>Nama Parts</td>
                                                <td>:</td>
                                                <td><?= $datas->nama_item ?></td>
                                            </tr>
                                            <tr>
                                                <td>Nomor Parts Lama</td>
                                                <td>:</td>
                                                <td><?= $datas->nomor_parts_lama ?></td>
                                            </tr>
                                            <tr>
                                                <td>Harga</td>
                                                <td>:</td>
                                                <td>Rp. <?= number_format($datas->harga_item) ?></td>
                                            </tr>
                                        <?php
                                            $i++;
                                         }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <style type="text/css">
                            .badge-not {
                                border-radius: 10px;
                                font-weight: 100;
                                font-size: 13px;
                                background-color: red;
                            }
                            .badge-yes {
                                border-radius: 10px;
                                font-weight: 100;
                                font-size: 13px;
                                background-color: green;
                            }
                        </style>
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <h5>Data Parts</h5>
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th style="text-align: center;">No.</th>
                                            <th style="text-align: center;">No. Wo</th>
                                            <th style="text-align: center;">Nama Customer</th>
                                            <th style="text-align: center;">Nomor Polisi</th>
                                            <th style="text-align: center;">Nama SA</th>
                                            <th style="text-align: center;">Tanggal Estimasi</th>
                                            <th style="text-align: center;">Tgl. Janji Penyerahan</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                            $i=1;
                                            foreach($data1 as $x){
                                        ?>

                                         <tr>
                                            <td style="text-align: center;"><?= $i++ ?></td>
                                            <td style="text-align: center;">
                                            <?php
                                                if($x->nomor_wo == NULL){
                                                    echo "-";
                                                }else{
                                                    echo $x->nomor_wo;
                                                }
                                            ?>
                                            </td>
                                            <td style="text-align: center;"><?= $x->nama_lengkap ?></td>
                                            <td style="text-align: center;"><?= $x->no_polisi ?></td>
                                            <td style="text-align: center;"><?= $x->nama_lengkap_user ?></td>
                                            <td style="text-align: center;">
                                            <?php
                                                if($x->tgl_estimasi == NULL){
                                                    echo "-";
                                                }else{
                                                    echo date('d-M-Y', strtotime($x->tgl_estimasi));
                                                }
                                            ?>
                                            </td>
                                            <td style="text-align: center;vertical-align: middle;">
                                            <?php
                                                if($x->tgl_janji_penyerahan == NULL){
                                                    echo "-";
                                                }else{
                                                    echo date('d-M-Y', strtotime($x->tgl_janji_penyerahan));
                                                }
                                            ?>
                                            </td>
                                        </tr>

                                <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Exportable Table -->
    </div>
</section>
<!-- FUNCTION JS FOR CLICKABLE ROW DATATABLE -->
