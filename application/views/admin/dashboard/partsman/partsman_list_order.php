<section class="content">
    <div class="container-fluid">

        <!-- Exportable Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            Daftar Order Parts
                        </h2>

                    </div>
                     <style type="text/css">
                        /*hilangkan exportable dan menyisakan input search di tabel*/
                        .dt-buttons {
                            display: none;
                        }
                    </style>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                <thead>
                                    <tr>
                                        <th  style="text-align: center;vertical-align: middle;">No. WO</th>
                                        <th  style="text-align: center;vertical-align: middle;">No. Polisi</th>
                                        <th  style="text-align: center;vertical-align: middle;">Nama Customer</th>
                                        <th  style="text-align: center;vertical-align: middle;">Nama SA</th>
                                        <th  style="text-align: center;vertical-align: middle;">Nama Teknisi</th>
                                        <th  style="text-align: center;vertical-align: middle;">Tanggal Estimasi</th>
                                        <th  style="text-align: center;vertical-align: middle;">Tgl. Janji Penyerahan</th>
                                        <th  style="text-align: center;vertical-align: middle;">Complete %</th>
                                        <th  style="text-align: center;vertical-align: middle;">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        foreach ($listEstimasi as $datas) {
                                    ?>
                                        <tr>
                                            <td style="text-align: center;vertical-align: middle;"><?= $datas->nomor_wo ?></td>
                                            <td style="text-align: center;vertical-align: middle;"><?= $datas->no_polisi ?></td>
                                            <td style="text-align: center;vertical-align: middle;"><?= $datas->nama_lengkap ?></td>
                                            <td style="text-align: center;vertical-align: middle;"><?= $datas->nama_lengkap_user ?></td>
                                            <td style="text-align: center;vertical-align: middle;">
                                                <?php
                                                    if($datas->namaTeknisi == '' || $datas->namaTeknisi == NULL){
                                                        echo "-";
                                                    }else{
                                                        echo $datas->namaTeknisi;
                                                    }
                                                ?>
                                            </td>
                                            <td style="text-align: center;vertical-align: middle;">
                                                <?php
                                                if($datas->tgl_estimasi == NULL){
                                                    echo "-";
                                                }else{
                                                    echo date('d-M-Y', strtotime($datas->tgl_estimasi));
                                                }
                                            ?>
                                            </td>
                                            <td style="text-align: center;vertical-align: middle;">
                                            <?php
                                                if($datas->tgl_janji_penyerahan == NULL){
                                                    echo "-";
                                                }else{
                                                    echo date('d-M-Y', strtotime($datas->tgl_janji_penyerahan));
                                                }
                                            ?>
                                            </td>
                                            <td style="text-align: center;vertical-align: middle;">
                                                <div class="progress" style="margin-bottom: 0px; height: 12px;">
                                                    <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: <?= $datas->totalReadyParts/$datas->totalParts*100 ?>%; line-height: 12px; color: black;" id="status_<?= $datas->id ?>">
                                                        <?php echo round($datas->totalReadyParts/$datas->totalParts*100)?>%<span class="sr-only"></span>
                                                    </div>
                                                </div>
                                            </td>
                                            <td style="text-align: center;vertical-align: middle;">
                                                <a href="<?= base_url('partsman/list_order/'.$datas->id) ?>" class="btn btn-primary waves-effect">Detail</a>
                                            </td>

                                        </tr>
                                    <?php
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Exportable Table -->
    </div>
</section>

