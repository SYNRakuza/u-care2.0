<section class="content">
    <div class="container-fluid">

        <!-- Exportable Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            Daftar Order Ulang atau Penambahan Parts
                        </h2>

                    </div>
                     <style type="text/css">
                        /*hilangkan exportable dan menyisakan input search di tabel*/
                        .dt-buttons {
                            display: none;
                        }
                    </style>
                    <div class="body">
                        <div class="table-responsive">
                            <form method="POST" action="<?= base_url('partsman/proses_reorder') ?>">
                            <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                <h6 style="color: orange;">
                                    <?php
                                        $info = $this->session->flashdata('order_sukses');
                                        $reorder = $this->session->flashdata('reorder_sukses');
                                        $goToListOrder = $this->session->flashdata('goToListOrder');
                                        if(!empty($info)){
                                            echo $info;
                                        }elseif(!empty($reorder)){
                                            echo $reorder;
                                        }elseif(!empty($goToListOrder)){
                                            echo $goToListOrder;
                                        }
                                    ?>
                        </h6>
                                <thead>
                                    <tr>
                                        <th style="text-align: center;vertical-align: middle;">Order</th>
                                        <th style="text-align: center;vertical-align: middle;">No. WO</th>
                                        <th style="text-align: center;vertical-align: middle;">Nama Customer</th>
                                        <th style="text-align: center;vertical-align: middle;">Nomor Polisi</th>
                                        <th style="text-align: center;vertical-align: middle;">Nama SA</th>
                                        <th style="text-align: center;vertical-align: middle;">Tanggal Estimasi</th>
                                        <th style="text-align: center;vertical-align: middle;">Tgl. Janji Penyerahan</th>
                                        <th style="text-align: center;vertical-align: middle;">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                    $i=1;
                                    foreach($data as $x){

                                ?>
                                    <tr>

                                        <td style="text-align: center;">
                                            <div class="switch">
                                            <label><input type="checkbox" onclick="muncul()" class="tombol" name="chk[]" value="<?= $x->id_estimasi ?>"><span class="lever switch-col-amber"></span></label>
                                            </div>
                                        </td>

                                        <td style="text-align: center;">
                                            <?php
                                                if($x->nomor_wo == NULL){
                                                    echo "-";
                                                }else{
                                                    echo $x->nomor_wo;
                                                }
                                            ?>
                                        </td style="text-align: center;">
                                        <td style="text-align: center;"><?= $x->nama_lengkap ?></td>
                                        <td style="text-align: center;"><?= $x->no_polisi ?></td>
                                        <td style="text-align: center;"><?= $x->nama_lengkap_user ?></td>
                                        <td style="text-align: center;">
                                            <?php
                                                if($x->tgl_estimasi == NULL){
                                                    echo "-";
                                                }else{
                                                    echo date('d-M-Y', strtotime($x->tgl_estimasi));
                                                }
                                            ?>
                                        </td>
                                        <td style="text-align: center;vertical-align: middle;">
                                            <?php
                                                if($x->tgl_janji_penyerahan == NULL){
                                                    echo "-";
                                                }else{
                                                    echo date('d-M-Y', strtotime($x->tgl_janji_penyerahan));
                                                }
                                            ?>
                                        </td>
                                        <td style="text-align: center;">
                                            <a href="<?php echo base_url().'partsman/reordering_parts/'.$x->id_estimasi; ?>" class="btn btn-primary waves-effect">Detail</a>
                                        </td>
                                    </tr>

                                <?php } ?>

                                </tbody>
                            </table>
                            <input type="submit" id="input1" style="display: none;" name="" class="btn btn-warning waves-effect" value="Proses">
                        </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Exportable Table -->
    </div>
</section>

<!-- FUNCTION JS FOR CLICKABLE ROW DATATABLE
<script type="text/javascript">
    $('.dataTable').on('click', 'tbody td', function() {
        var idEstimasi= $(this).data("row");
        window.location = "<?php echo base_url();?>service_advisor/customer/"+idEstimasi;

})
</script>
-->

<script type="text/javascript">

    function muncul(){
        var jumlahCek = document.getElementsByClassName('tombol'); //deteksi cekbox
        var kondisi = false; //bikin kondisi ada atau tidak tercek
        for(var i=0;i<jumlahCek.length;i++){ //loop untuk membuat cek
            if(jumlahCek[i].checked){
                kondisi = true; //kalau ada yg tercek, kondisi di buat true
                break;
            }
        }

        if(kondisi){//kondisi bernilai tru, maka tombol akan ditampilkan
           document.getElementById("input1").style.display = "inline-block";
       }else{
           document.getElementById("input1").style.display = "none";
       }

    }

</script>