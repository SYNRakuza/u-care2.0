<section class="content">
    <div class="container-fluid">
        <!-- Exportable Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            Detail Monitoring Parts
                        </h2>
                        <h6 style="color: orange">
                            <?php
                                 $info = $this->session->flashdata('update_sukses');
                                if(!empty($info)){
                                    echo $info;
                                }
                            ?>
                        </h6>
                    </div>
                    <style type="text/css">
                        /*hilangkan exportable dan menyisakan input search di tabel*/
                        .dt-buttons {
                            display: none;
                        }
                    </style>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="table-responsive">
                                <h5>Data Customer</h5>
                                    <table class="table table-customer">
                                        <tbody>
                                        <?php
                                            $i=1;
                                            $diskonJasa = 0;
                                            $diskonParts = 0;
                                        foreach($data as $datas){
                                            if($i>1){
                                                break;
                                            }
                                        ?>
                                            <tr>
                                                <td width="125">Nama</td>
                                                <td width="1">:</td>
                                                <td><?= $datas->nama_lengkap ?></td>
                                            </tr>
                                             <tr>
                                                <td>No. Telp</td>
                                                <td>:</td>
                                                <td><?= $datas->no_hp ?></td>
                                            </tr>
                                             <tr>
                                                <td>Alamat</td>
                                                <td>:</td>
                                                <td><?= $datas->alamat ?></td>
                                            </tr>
                                            <tr>
                                                <td>Type Customer</td>
                                                <td>:</td>
                                            <?php if($datas->jenis_customer == '0'):?>
                                                <td><span>Asuransi - <?= $datas->nama_asuransi ?></span></td>
                                            <?php else: ?>
                                                <td><span>Tunai</span></td>
                                            <?php endif;?>
                                            </tr>
                                        <?php
                                            $i++;
                                         }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <h5>Data Kendaraan</h5>
                                <div class="table-responsive">
                                    <table class="table table-customer">
                                        <tbody>
                                        <?php
                                        $i=1;
                                        foreach($data as $datas){
                                            if($i>1){
                                                break;
                                            }
                                            $diskonJasa = $datas->diskon_jasa;
                                            $diskonParts = $datas->diskon_parts;
                                        ?>
                                            <tr>
                                                <td width="155">Jenis Kendaraan</td>
                                                <td width="1">:</td>
                                                <td><?= $datas->nama_jenis ?></td>
                                            </tr>
                                            <tr>
                                                <td>No. Polisi</td>
                                                <td>:</td>
                                                <td><?= $datas->no_polisi ?></td>
                                            </tr>
                                            <tr>
                                                <td>Warna</td>
                                                <td>:</td>
                                                <td><?= $datas->code_color.'- '.$datas->nama_color ?></td>
                                            </tr>
                                            <tr>
                                                <td>No. Rangka</td>
                                                <td>:</td>
                                                <td><?= $datas->no_rangka ?></td>
                                            </tr>

                                        <?php $i++; }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <h5>Data Parts</h5>
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th style="text-align: center;vertical-align: middle;">No.</th>
                                            <th style="text-align: center;vertical-align: middle;">No. Parts</th>
                                            <th style="text-align: center;vertical-align: middle;">Nama Parts</th>
                                            <th style="text-align: center;vertical-align: middle;" >Jumlah</th>
                                            <th style="text-align: center;vertical-align: middle;">Status</th>
                                            <th style="text-align: center;vertical-align: middle;">Estimated Time Arrived</th>
                                            <th style="text-align: center;vertical-align: middle;">Actual Time Arrived</th>
                                            <th style="text-align: center;vertical-align: middle;">Tanggal Pengambilan</th>
                                            <th style="text-align: center;vertical-align: middle;">Pengambil</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                            $i=1;
                                            $j=0;
                                            foreach($data1 as $x){
                                                $id_est = $x->id_estimasi;
                                                $j++;

                                        ?>

                                         <tr>
                                            <td style="text-align: center;vertical-align: middle;"><?= $i++ ?></td>
                                            <td style="text-align: center;vertical-align: middle;"><?= $x->nomor_parts ?></td>
                                            <td style="text-align: center;vertical-align: middle;"><?= $x->nama_item ?></td>
                                            <td style="text-align: center;vertical-align: middle;"><?= $x->qty ?></td>
                                            <td style="text-align: center;vertical-align: middle;">
                                                <?php
                                                    if($x->status_barang == NULL){
                                                        echo "<span style='color:red;'><b>Not Ready</b></span>";
                                                    }else if($x->status_barang == 0){
                                                        echo "<span style='color:green;'><b>Ready</b></span>";
                                                    }else{
                                                        echo "-";
                                                    }
                                                ?>
                                            </td>
                                            <td style="text-align: center;vertical-align: middle;">
                                                <?php
                                                    if($x->eta == NULL){
                                                        echo "-";
                                                    }else{
                                                        echo date('d-M-Y', strtotime($x->eta));
                                                    }
                                                ?>
                                            </td>
                                            <td style="text-align: center;vertical-align: middle;">
                                                <?php
                                                    if($x->ata == NULL){
                                                        echo "-";
                                                    }else{
                                                        $datax = date('d-M-Y', strtotime($x->ata));
                                                        echo $datax;
                                                    }
                                                ?>
                                            </td>
                                            <td style="text-align: center;vertical-align: middle;">
                                                <?php
                                                    if($x->tgl_pengambilan == NULL){
                                                        echo "-";
                                                    }else{
                                                        $datay = date('d-M-Y', strtotime($x->tgl_pengambilan));
                                                        echo $datay;
                                                    }
                                                ?>
                                            </td>
                                            <td style="text-align: center;vertical-align: middle;">
                                                <?php
                                                    if($x->pengambil == NULL){
                                                        echo "-";
                                                    }else{
                                                        echo $x->pengambil;
                                                    }
                                                ?>
                                            </td>

                                        </tr>

                                <?php } ?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Exportable Table -->
    </div>
</section>
<!-- FUNCTION JS FOR CLICKABLE ROW DATATABLE -->
