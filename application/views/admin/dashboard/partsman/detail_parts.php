<section class="content">
    <div class="container-fluid">

        <!-- Exportable Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            Detail Parts
                        </h2>

                    </div>
                     <style type="text/css">
                        /*hilangkan exportable dan menyisakan input search di tabel*/
                        .dt-buttons {
                            display: none;
                        }

                    </style>
                    <div class="body">
                        <div class="table-responsive">
                            <form method="POST">
                                <table class="table">
                                <tbody style="border: hidden;">

                                    <?php
                                        $i = 1;//variabel berapa kali loop
                                        foreach($data as $x){
                                            if($i>1){//supaya satu kali loop saja
                                                break;
                                            }
                                    ?>

                                    <tr>
                                        <td class="col-xs-2"><b>Nomor Parts</b></td>
                                        <td class="col-xs-1"><b>:</b></td>
                                        <td>
                                            <input type="text" class="form-control" name="nomor_parts" placeholder="Masukkan Nomor Parts" value="<?= $x->nomor_parts ?>" maxlength="40" required />
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="col-xs-2"><b>Nomor Parts Lama</b></td>
                                        <td class="col-xs-1"><b>:</b></td>
                                        <td>
                                            <input type="text" class="form-control" name="nomor_parts_lama" placeholder="Masukkan Nomor Parts Lama" value="<?= $x->nomor_parts_lama ?>" maxlength="40" />
                                        </td>
                                    </tr>

                                     <tr>
                                        <td class="col-xs-2"><b>Nama Parts</b></td>
                                        <td class="col-xs-1"><b>:</b></td>
                                        <td>
                                            <input type="text" class="form-control" name="nama_item" placeholder="Masukkan Nama Parts" value="<?= $x->nama_item ?>" maxlength="50" onkeyup="this.value = this.value.toUpperCase()"  required />
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="col-xs-2"><b>Harga Parts</b></td>
                                        <td class="col-xs-1"><b>:</b></td>
                                        <td>
                                            <input type="number" class="form-control" name="harga_item" placeholder="Masukkan Harga Parts" value="<?= $x->harga_item ?>" maxlength="10" required />
                                        </td>
                                    </tr>


                                </tbody>

                            </table>
                            <input type="submit" formaction="<?= base_url('partsman/parts_action/'.$x->id_item) ?>" name="" class="btn btn-warning waves-effect" value="Update">
                                <?php
                                    $i++;
                                } ?>

                        </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Exportable Table -->
    </div>
</section>

<!-- FUNCTION JS FOR CLICKABLE ROW DATATABLE
<script type="text/javascript">
    $('.dataTable').on('click', 'tbody td', function() {
        var idEstimasi= $(this).data("row");
        window.location = "<?php echo base_url();?>service_advisor/customer/"+idEstimasi;

})
</script>
-->