<section class="content">
    <div class="container-fluid">
        <!-- Exportable Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            Proses Order
                        </h2>
                    </div>
                    <style type="text/css">
                        /*hilangkan exportable dan menyisakan input search di tabel*/
                        .dt-buttons {
                            display: none;
                        }
                    </style>
                    <div class="body">
                        <div class="table-responsive">
                            <?php $y=1; ?>
                            <form method="POST" action="<?= base_url('partsman/proses_order_action') ?>">
                            <select name="chk" required>
                                <option value="">--Pilih Tipe Order--</option>
                                <option value="1">Tipe 1</option>
                                <option value="3">Tipe 3</option>
                            </select>
                            <table class="table table-bordered table-striped table-hover">
                                <th colspan="7"><center><h4>Form Pemesanan Parts</h4></center></th>
                                <tbody>

                                        <tr>
                                            <th>No.</th>
                                            <th style="text-align: center;">No. Parts</th>
                                            <th style="text-align: center;">Nama Barang</th>
                                            <th style="text-align: center;">Jumlah</th>
                                            <th style="text-align: center;">Nama Customer</th>
                                            <th style="text-align: center;">No. Polisi</th>
                                            <th style="text-align: center;display: none;visibility: hidden;">Id Detail</th>
                                        </tr>

                                        <?php
                                            $i = 1;
                                            $count = 0;
                                            foreach($data as $x){
                                        ?>

                                         <tr>
                                            <td style="text-align: center;"><?= $i++ ?></td>
                                            <td style="text-align: center;"><?= $x->nomor_parts ?></td>
                                            <td style="text-align: center;"><?= $x->nama_item ?></td>
                                            <td style="text-align: center;"><?= $x->qty ?> Buah</td>
                                            <td style="text-align: center;"><?= $x->nama_lengkap ?></td>
                                            <td style="text-align: center;"><?= $x->no_polisi ?></td>
                                            <td style="text-align: center;display: none;visibility: hidden;">
                                                <input type="number" name="id[]" value="<?= $x->id_detail ?>">
                                            </td>
                                          </tr>
                                        <?php $count++;} ?>

                                        <!--Untuk Get Id_Estimasi supaya bisa update barang sdh done atau tidak KALASI GET ID ESTIMASI-->
                                        <?php foreach($data1 as $y){ ?>
                                        <input style="visibility: hidden;display: none;" type="number" name="est[]" value="<?= $y->id_estimasi ?>" >

                                        <?php } ?>
                                </tbody>
                            </table>
                            <table class="table table-bordered table-striped table-hover" style="visibility: hidden;display: none;">
                                <th colspan="3"><center><h4>Form Nomor WO</h4></center></th>
                                <tbody>

                                        <tr>
                                            <th style="text-align: center;">Nama Customer</th>
                                            <th style="text-align: center;">No. Polisi</th>
                                            <th style="text-align: center;">Nomor WO</th>
                                        </tr>
                                <?php
                                    foreach($data2 as $z){
                                ?>
                                        <tr>
                                            <td style="text-align: center;"><?= $z->nama_lengkap ?></td>
                                            <td style="text-align: center;"><?= $z->no_polisi ?></td>
                                            <td style="text-align: center;">
                                                <?php
                                                    if($z->nomor_wo != NULL){?>

                                                    <input type="text" name="nomor_wo[]" maxlength="12" minlength="8" value="<?= $z->nomor_wo ?>" onkeyup="this.value = this.value.toUpperCase()"  required>

                                                <?php }else{ ?>

                                                <input type="text" name="nomor_wo[]" onkeyup="this.value = this.value.toUpperCase()" maxlength="12" minlength="8" value="<?= $z->nomor_wo ?>" required>
                                            <?php } ?>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                 </tbody>
                             </table>
                            <!--INI EXPORT EXCEL
                            <input type="submit" formaction="<?= base_url('partsman/excel') ?>" name="" class="btn btn-success waves-effect" value="Download Excel">
                            -->

                            <input type="submit" formaction="<?= base_url('partsman/proses_order_action') ?>" name="" class="btn btn-warning waves-effect" value="Order Parts">
                        </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Exportable Table -->

        <!-- Exportable Table -->
    </div>
</section>


