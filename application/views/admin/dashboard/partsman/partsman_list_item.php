<section class="content">
    <div class="container-fluid">

        <!-- Exportable Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            Daftar Parts Estimasi
                        </h2>

                    </div>
                     <style type="text/css">
                        /*hilangkan exportable dan menyisakan input search di tabel*/
                        .dt-buttons {
                            display: none;
                        }
                    </style>
                    <div class="body">
                        <div class="table-responsive">
                            <form method="POST" action="<?= base_url('partsman/proses_order') ?>">
                            <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                <h6 style="color: orange;">
                                    <?php
                                        $info = $this->session->flashdata('order_sukses');
                                        $reorder = $this->session->flashdata('reorder_sukses');
                                        if(!empty($info)){
                                            echo $info;
                                        }elseif(!empty($reorder)){
                                            echo $reorder;
                                        }
                                    ?>
                        </h6>
                                <thead>
                                    <tr>
                                        <th style="text-align: center;vertical-align: middle;">Order</th>
                                        <th style="text-align: center;vertical-align: middle;">Nomor Parts</th>
                                        <th style="text-align: center;vertical-align: middle;">Nama Parts</th>
                                        <th style="text-align: center;vertical-align: middle;">Nomor Parts Lama</th>
                                        <th style="text-align: center;vertical-align: middle;">Quantity</th>
                                        <th style="text-align: center;vertical-align: middle;">Harga Item</th>
                                        <th style="text-align: center;vertical-align: middle;">Total Harga</th>
                                        <th style="text-align: center;vertical-align: middle;">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                    $i=1;
                                    foreach($datas as $data){

                                ?>
                                    <tr>

                                        <td style="text-align: center;">
                                            <div class="switch">
                                            <label><input type="checkbox" onclick="muncul()" class="tombol"  name="chk[]" value="<?= $data->id_item ?>"><span class="lever switch-col-amber"></span></label>
                                            <input type="text" hidden class="tombol"  name="id_estimasi[]" value="<?= $data->id_estimasi ?>">
                                            </div>
                                        </td>
                                        <td style="text-align: center;"><?= $data->nomor_parts ?></td>
                                        <td style="text-align: center;"><?= $data->nama_item ?></td>
                                        <td style="text-align: center;"><?= $data->nomor_parts_lama ?></td>
                                        <td style="text-align: center;"><?= $data->totalQuantity ?></td>
                                        <td style="text-align: center;">Rp.<?= number_format($data->harga_item) ?></td>
                                        <td style="text-align: center;">Rp.<?= number_format($data->totalQuantity*$data->harga_item) ?></td>
                                        <td style="text-align: center;">
                                            <a href="<?php echo base_url().'partsman/list_estimasi/'.$data->id_item; ?>" class="btn btn-primary waves-effect">Detail</a>
                                        </td>
                                    </tr>

                                <?php } ?>

                                </tbody>
                            </table>
                            <input type="submit" id="input1" name="" class="btn btn-warning waves-effect" value="Proses" style="display: none;">
                        </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Exportable Table -->
    </div>
</section>

<!-- FUNCTION JS FOR CLICKABLE ROW DATATABLE
<script type="text/javascript">
    $('.dataTable').on('click', 'tbody td', function() {
        var idEstimasi= $(this).data("row");
        window.location = "<?php echo base_url();?>service_advisor/customer/"+idEstimasi;

})
</script>
-->

<script type="text/javascript">

function muncul(){

var jumlahCek = document.getElementsByClassName('tombol'); //deteksi cekbox
        var kondisi = false; //bikin kondisi ada atau tidak tercek
        for(var i=0;i<jumlahCek.length;i++){ //loop untuk membuat cek
            if(jumlahCek[i].checked){
                kondisi = true; //kalau ada yg tercek, kondisi di buat true
                break;
            }
        }

        if(kondisi){//kondisi bernilai tru, maka tombol akan ditampilkan
           document.getElementById("input1").style.display = "inline-block";
       }else{
           document.getElementById("input1").style.display = "none";
       }



}
</script>