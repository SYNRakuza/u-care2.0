<?php
$date = date('d-m-Y');

header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=daftar_orderan_kembali ".$date.".xls");

header("Pragma: no-cache");

header("Expires: 0");
?>

<table border="1" width="100%">
    <thead>
        <th style="text-align:center;">No.</th>
        <th style="text-align: center;">Nomor WO</th>
        <th style="text-align: center;">Customer</th>
        <th style="text-align: center;">Nomor Parts</th>
        <th style="text-align: center;">Nama Parts</th>
        <th style="text-align: center;">Jumlah</th>
        <th style="text-align: center;">Jenis Mobil</th>
        <th style="text-align: center;">Tanggal Order</th>
        <th style="text-align: center;">Tipe Order</th>
        <th style="text-align: center;">Catatan</th>
    </thead>
    <tbody>
        <?php
            $i=1;
            foreach($data as $x){
        ?>
        <tr>
            <td style="text-align:center;vertical-align: middle;"><?= $i ?></td>
            <td style="text-align: center;vertical-align: middle;"><?= $x->nomor_wo ?></td>
            <td style="text-align: center;vertical-align: middle;"><?= $x->nama_lengkap ?></td>
            <td style="text-align: center;vertical-align: middle;"><?= $x->nomor_parts ?></td>
            <td style="text-align: center;vertical-align: middle;"><?= $x->nama_item ?></td>
            <td style="text-align: center;vertical-align: middle;"><?= $x->qty ?></td>
            <td style="text-align: center;vertical-align: middle;"><?= $x->nama_jenis ?></td>
            <td style="text-align: center;vertical-align: middle;"><?= $x->tgl_order ?></td>
            <td style="text-align: center;vertical-align: middle;"><?= $x->lokasi_order ?></td>
            <td style="text-align: center;vertical-align: middle;">
                <?php
                    if($x->komentar == NULL){
                        echo "-";
                    }else{
                        echo $x->komentar;
                    }
                ?>
            </td>
        </tr>

        <?php $i++; } ?>
    </tbody>
</table>

</div>