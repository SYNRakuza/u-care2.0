<section class="content">
    <div class="container-fluid">
        <!-- Exportable Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            Detail Order Pemesanan
                        </h2>
                        <h6 style="color: orange">
                            <?php
                                 $info = $this->session->flashdata('update_sukses');
                                if(!empty($info)){
                                    echo $info;
                                }
                            ?>
                        </h6>
                    </div>
                    <style type="text/css">
                        /*hilangkan exportable dan menyisakan input search di tabel*/
                        .dt-buttons {
                            display: none;
                        }
                    </style>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="table-responsive">
                                <h5>Data Customer</h5>
                                    <table class="table table-customer">
                                        <tbody>
                                        <?php
                                            $i=1;
                                            $diskonJasa = 0;
                                            $diskonParts = 0;
                                        foreach($data as $datas){
                                            if($i>1){
                                                break;
                                            }
                                        ?>
                                            <tr>
                                                <td width="125">Nama</td>
                                                <td width="1">:</td>
                                                <td><?= $datas->nama_lengkap ?></td>
                                            </tr>
                                             <tr>
                                                <td>No. Telp</td>
                                                <td>:</td>
                                                <td><?= $datas->no_hp ?></td>
                                            </tr>
                                             <tr>
                                                <td>Alamat</td>
                                                <td>:</td>
                                                <td><?= $datas->alamat ?></td>
                                            </tr>
                                             <tr>
                                                <td>Type Customer</td>
                                                <td>:</td>
                                            <?php if($datas->jenis_customer == '0'):?>
                                                <td><span>Asuransi - <?= $datas->nama_asuransi ?></span></td>
                                            <?php else: ?>
                                                <td><span>Tunai</span></td>
                                            <?php endif;?>
                                            </tr>
                                        <?php
                                            $i++;
                                         }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <h5>Data Kendaraan</h5>
                                <div class="table-responsive">
                                    <table class="table table-customer">
                                        <tbody>
                                        <?php
                                        $i=1;
                                        foreach($data as $datas){
                                            if($i>1){
                                                break;
                                            }
                                            $diskonJasa = $datas->diskon_jasa;
                                            $diskonParts = $datas->diskon_parts;
                                        ?>
                                            <tr>
                                                <td width="155">Jenis Kendaraan</td>
                                                <td width="1">:</td>
                                                <td><?= $datas->nama_jenis ?></td>
                                            </tr>
                                            <tr>
                                                <td>No. Polisi</td>
                                                <td>:</td>
                                                <td><?= $datas->no_polisi ?></td>
                                            </tr>
                                            <tr>
                                                <td>Warna</td>
                                                <td>:</td>
                                                <td><?= $datas->code_color.' - '.$datas->nama_color ?></td>
                                            </tr>
                                            <tr>
                                                <td>No. Rangka</td>
                                                <td>:</td>
                                                <td><?= $datas->no_rangka ?></td>
                                            </tr>


                                        <?php $i++; }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <h4>Data Parts</h4>
                                <h5 style="color: orange;">
                                <?php
                                    $info = $this->session->flashdata('update');
                                    if(!empty($info)){
                                        echo $info;
                                    }
                                ?>
                                </h5>
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th style="display: none;visibility: hidden;">ID Kalassi</th>
                                            <th style="text-align: center;vertical-align: middle;">No.</th>
                                            <th style="text-align: center;vertical-align: middle;">No. Order</th>
                                            <th style="text-align: center;vertical-align: middle;">No. Parts</th>
                                            <th style="text-align: center;vertical-align: middle;">Nama Parts</th>
                                            <th style="text-align: center;vertical-align: middle;" >Jumlah</th>
                                            <th style="text-align: center;vertical-align: middle;" >Tipe Order</th>
                                            <th style="text-align: center;vertical-align: middle;">Status</th>
                                            <th style="text-align: center;vertical-align: middle;">Estimated Time Arrived</th>
                                            <th style="text-align: center;vertical-align: middle;">Actual Time Arrived</th>
                                            <th style="text-align: center;vertical-align: middle;">Tanggal Pengambilan</th>
                                            <th style="text-align: center;vertical-align: middle;">Pengambil</th>
                                            <th style="text-align: center;vertical-align: middle;">Reorder</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                            $i=1;
                                            foreach($data1 as $x){
                                        ?>
                            <form method="POST">
                                         <tr>
                                            <td style="visibility: hidden;display: none;">
                                                <input type="number" name="id_detail[]" value="<?= $x->id_detail ?>">
                                            </td>
                                            <td style="text-align: center;vertical-align: middle;"><?= $i++ ?></td>
                                            <td style="text-align: center;vertical-align: middle;">
                                                <?php 
                                                    if($x->no_order !== null){
                                                        echo $x->no_order;
                                                    }else{
                                                        echo "-";
                                                    }
                                                ?>
                                            </td>
                                            <td style="text-align: center;vertical-align: middle;"><?= $x->nomor_parts ?></td>
                                            <td style="text-align: center;vertical-align: middle;"><?= $x->nama_item ?></td>
                                            <td style="text-align: center;vertical-align: middle;"><?= $x->qty ?></td>
                                            <td style="text-align: center;vertical-align: middle;"><?= $x->tipe_order ?></td>
                                            <td style="text-align: center;vertical-align: middle;">
                                                <?php
                                                if($x->status_barang == NULL || $x->status_barang == 1){
                                                    echo "<span style='color:red;'><b>Not Ready</b></span>";
                                                }else if($x->status_barang == 0){
                                                    echo "<span style='color:green;'><b>Ready</b></span>";
                                                }
                                                    /*if($x->status_barang == 0){
                                                        echo "<span style='color:green;'><b>Ready</b></span>";
                                                    }else{
                                                        echo "<span style='color:red;'><b>Not Ready</b></span>";
                                                    }*/
                                                ?>
                                            </td>
                                            <td style="text-align: center;vertical-align: middle;">
                                                <?php
                                                    if($x->eta == NULL){
                                                        echo "<span style='color:red;'><b>Ordering</b></span>";
                                                    }else{
                                                        echo date('d-M-Y', strtotime($x->eta));
                                                    }
                                                ?>
                                            </td>
                                            <td style="text-align: center;vertical-align: middle;">
                                                <?php
                                                if($x->eta == NULL){
                                                    echo "-";
                                                    echo '<input style="max-width: 155px;visibility:hidden;display:none;" type="date" name="ata[]" value="" class="datepicker form-control">';
                                                }else{
                                                ?>
                                                    <input style="max-width: 155px;" type="date" name="ata[]" value="<?= $x->ata ?>" class="datepicker form-control">
                                                <?php } ?>
                                            </td>
                                            <td style="text-align: center;vertical-align: middle;">
                                                <?php
                                                if($x->eta == NULL){
                                                    echo "-";
                                                    echo '<input style="max-width: 155px;visibility:hidden;display:none;" type="date" name="tgl_pengambilan[]" value="" class="datepicker form-control" >';
                                                }else{
                                                ?>
                                                <center>
                                                    <input style="max-width: 155px;" type="date" name="tgl_pengambilan[]" value="<?= $x->tgl_pengambilan ?>" class="datepicker form-control" >
                                                </center>
                                                <?php } ?>

                                            </td>
                                            <td style="text-align: center;vertical-align: middle;">

                                            <?php
                                                if($x->eta == NULL){
                                                    echo "-";
                                                    echo '<input style="display:none;visibility:hidden;" type="text" name="pengambil[]" value="" class="form-control" onkeyup="this.value = this.value.toUpperCase()" placeholder="Nama Pengambil" />';
                                                }else{
                                            ?>
                                                <input type="text" name="pengambil[]" value="<?= $x->pengambil ?>" class="form-control" onkeyup="this.value = this.value.toUpperCase()" placeholder="Nama Pengambil" />
                                            <?php
                                                }
                                            ?>



                                            </td>

                                            <td style="text-align: center;vertical-align: middle;">
                                                <?php
                                                    if($x->eta==NULL){
                                                        echo "-";
                                                    }else{
                                                ?>
                                                <a href="<?= base_url('partsman/reorder/'.$x->id_detail.'/'.$x->id_estimasi) ?>" class="btn bg-orange waves-effect" title = "Reorder Parts"><i class="material-icons">history</i></a>
                                                <?php
                                                 }
                                                 ?>
                                            </td>
                            <?php
                                $id_est = $x->id_estimasi;
                             ?>

                             <?php
                                    if($i<=2){
                                        if(($x->totalReadyParts)/($x->totalParts) == 1){
                            ?>
                                        <a href="<?= base_url('partsman/saveToHistory/'.$id_est)  ?>" class="btn btn-warning waves-effect"><h6>Parts Selesai</h6></a>
                                        <br>
                                        <h6 style="color: black">Klik ini jika semua parts sudah terupdate</h6>
                             <?php           }
                                    }
                            } ?>
                                        </tr>
                                        </tbody>
                                    </table>

                    <input type="submit" formaction="<?= base_url('partsman/updateBulgData/'.$id_est) ?>" class="btn btn-success waves-effect" value="Update All">
                    </form>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Exportable Table -->
    </div>
</section>
<!-- FUNCTION JS FOR CLICKABLE ROW DATATABLE -->
