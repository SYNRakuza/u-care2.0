<section class="content">
    <div class="container-fluid">
        <!-- Exportable Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            Detail Ordering
                        </h2>
                    </div>
                    <style type="text/css">
                        /*hilangkan exportable dan menyisakan input search di tabel*/
                        .dt-buttons {
                            display: none;
                        }
                    </style>
                    <div class="body">
                <div class="row clearfix">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <h5>Data Parts</h5>
                                <div class="table-responsive">
                                    <table class="table table-customer">
                                        <tbody>
                                        <?php
                                            $i=1;
                                            foreach($data as $datas){
                                            if($i>1){
                                                break;
                                            }
                                        ?>
                                            <tr>
                                                <td width="125">Nomor Parts</td>
                                                <td width="1">:</td>
                                                <td><?= $datas->nomor_parts ?></td>
                                            </tr>
                                             <tr>
                                                <td>Nama Parts</td>
                                                <td>:</td>
                                                <td><?= $datas->nama_item ?></td>
                                            </tr>
                                            <tr>
                                                <td>Nomor Parts Lama</td>
                                                <td>:</td>
                                                <td><?= $datas->nomor_parts_lama ?></td>
                                            </tr>
                                            <tr>
                                                <td>Harga</td>
                                                <td>:</td>
                                                <td><?= $datas->harga_item ?></td>
                                            </tr>
                                        <?php
                                            $i++;
                                         }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <h5>Data Parts</h5>
                                <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                <tbody>

                                        <tr>
                                            <th style="text-align: center;vertical-align: middle;">No.</th>
                                            <th style="text-align: center;vertical-align: middle;">Nomor WO</th>
                                            <th style="text-align: center;vertical-align: middle;">Nama Customer</th>
                                            <th style="text-align: center;vertical-align: middle;">Nomor Polisi</th>
                                            <th style="text-align: center;vertical-align: middle;">No. Parts</th>
                                            <th style="text-align: center;vertical-align: middle;">Nama Parts</th>
                                            <th style="text-align: center;vertical-align: middle;">Jumlah</th>
                                            <th style="text-align: center;vertical-align: middle;">Tanggal Order</th>
                                            <th style="text-align: center;vertical-align: middle;">Tipe Order</th>
                                        </tr>

                                        <?php
                                            $i=1;
                                            foreach($data1 as $x){
                                        ?>

                                         <tr>
                                            <td style="text-align: center;vertical-align: middle;"><?= $i++ ?></td>
                                            <td style="text-align: center;vertical-align: middle;"><?= $x->nomor_wo ?></td>
                                            <td style="text-align: center;vertical-align: middle;"><?= $x->nama_lengkap ?></td>
                                            <td style="text-align: center;vertical-align: middle;"><?= $x->no_polisi ?></td>
                                            <td style="text-align: center;vertical-align: middle;"><?= $x->nomor_parts ?></td>
                                            <td style="text-align: center;vertical-align: middle;"><?= $x->nama_item ?></td>
                                            <td style="text-align: center;vertical-align: middle;"><?= $x->qty ?></td>
                                            <td style="text-align: center;vertical-align: middle;"><?= date('d-M-Y', strtotime($x->tgl_order))?></td>
                                            <td style="text-align: center;vertical-align: middle;"><?= $x->lokasi_order ?></td>
                                        </tr>

                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Exportable Table -->

        <!-- Exportable Table -->
    </div>
</section>
