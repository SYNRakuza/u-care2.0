<?php
$date = date('d-m-Y');

header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=daftar_orderan ".$date.".xls");

header("Pragma: no-cache");

header("Expires: 0");
?>

<table border="1" width="100%">
    <thead>
        <th style="text-align:center;">No.</th>
        <th style="text-align: center;">Nomor WO</th>
        <th style="text-align: center;">Customer</th>
        <th style="text-align: center;">Nomor Parts</th>
        <th style="text-align: center;">Nama Parts</th>
        <th style="text-align: center;">Jumlah</th>
        <th style="text-align: center;">Jenis Mobil</th>
        <th style="text-align: center;">Tanggal Order</th>
        <th style="text-align: center;">Tipe Order</th>
    </thead>
    <tbody>
        <?php
            $i=1;
            foreach($data as $x){
        ?>
        <tr>
            <td style="text-align:center;"><?= $i ?></td>
            <td style="text-align: center;"><?= $x->nomor_wo ?></td>
            <td style="text-align: center;"><?= $x->nama_lengkap ?></td>
            <td style="text-align: center;"><?= $x->nomor_parts ?></td>
            <td style="text-align: center;"><?= $x->nama_item ?></td>
            <td style="text-align: center;"><?= $x->qty ?></td>
            <td style="text-align: center;"><?= $x->nama_jenis ?></td>
            <td style="text-align: center;"><?= $x->tgl_order ?></td>
            <td style="text-align: center;"><?= $x->lokasi_order ?></td>
        </tr>

        <?php $i++; } ?>
    </tbody>
</table>

</div>