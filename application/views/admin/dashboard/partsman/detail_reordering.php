<section class="content">
    <div class="container-fluid">

        <!-- Exportable Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            Detail Estimasi
                        </h2>
                    </div>
                     <style type="text/css">
                        /*hilangkan exportable dan menyisakan input search di tabel*/
                        .dt-buttons {
                            display: none;
                        }
                    </style>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <h5>Data Customer</h5>
                                <div class="table-responsive">
                                    <table class="table table-customer">
                                        <tbody>
                                        <?php
                                            $i=1;
                                            $diskonJasa = 0;
                                            $diskonParts = 0;
                                        foreach($data as $datas){
                                            if($i>1){
                                                break;
                                            }
                                        ?>
                                            <tr>
                                                <td width="125">Nama</td>
                                                <td width="1">:</td>
                                                <td><?= $datas->nama_lengkap ?></td>
                                            </tr>
                                             <tr>
                                                <td>No. Telp</td>
                                                <td>:</td>
                                                <td><?= $datas->no_hp ?></td>
                                            </tr>
                                             <tr>
                                                <td>Alamat</td>
                                                <td>:</td>
                                                <td><?= $datas->alamat ?></td>
                                            </tr>
                                              <tr>
                                                <td>Type Customer</td>
                                                <td>:</td>
                                            <?php if($datas->jenis_customer == '0'):?>
                                                <td><span>Asuransi - <?= $datas->nama_asuransi ?></span></td>
                                            <?php else: ?>
                                                <td><span>Tunai</span></td>
                                            <?php endif;?>
                                            </tr>
                                        <?php
                                            $i++;
                                         }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <h5>Data Kendaraan</h5>
                                <div class="table-responsive">
                                    <table class="table table-customer">
                                        <tbody>
                                        <?php
                                        $i=1;
                                        foreach($data as $datas){
                                            if($i>1){
                                                break;
                                            }
                                            $diskonJasa = $datas->diskon_jasa;
                                            $diskonParts = $datas->diskon_parts;
                                        ?>
                                            <tr>
                                                <td width="155">Jenis Kendaraan</td>
                                                <td width="1">:</td>
                                                <td><?= $datas->nama_jenis ?></td>
                                            </tr>
                                            <tr>
                                                <td>No. Polisi</td>
                                                <td>:</td>
                                                <td><?= $datas->no_polisi ?></td>
                                            </tr>
                                            <tr>
                                                <td>Warna</td>
                                                <td>:</td>
                                                <td><?= $datas->code_color.' - '.$datas->nama_color ?></td>
                                            </tr>
                                            <tr>
                                                <td>No. Rangka</td>
                                                <td>:</td>
                                                <td><?= $datas->no_rangka ?></td>
                                            </tr>

                                        <?php $i++; }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <style type="text/css">
                            .badge-not {
                                border-radius: 10px;
                                font-weight: 100;
                                font-size: 13px;
                                background-color: red;
                            }
                            .badge-yes {
                                border-radius: 10px;
                                font-weight: 100;
                                font-size: 13px;
                                background-color: green;
                            }
                        </style>
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <h5>Data Parts</h5>
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th style="text-align: center;">No.</th>
                                            <th style="text-align: center;">No. Parts</th>
                                            <th style="text-align: center;">Nama Parts</th>
                                            <th style="text-align: center;">Jumlah</th>
                                            <th style="text-align: center;">Status</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                            $i=1;
                                            $j = 1;
                                            foreach($data1 as $x){
                                                $id_est = $x->id_estimasi;
                                        ?>

                                         <tr>
                                            <td style="text-align: center;"><?= $i++ ?></td>
                                            <td style="text-align: center;"><?= $x->nomor_parts ?></td>
                                            <td style="text-align: center;"><?= $x->nama_item ?></td>
                                            <td style="text-align: center;"><?= $x->qty ?></td>
                                            <td style="text-align: center;">
                                                <?php
                                                    if($x->eta == NULL){
                                                        echo "<b style='color:red;'>On Ordering</b>";
                                                    }else{
                                                        echo "<b style='color:green;'>Done Ordering</b>";
                                                    }
                                                ?>
                                            </td>

                                        <?php
                                            if($j<2){
                                                if(($x->totalParts)-($x->totalDoneParts) == 0){
                                        ?>
                                            <a href="<?= base_url('partsman/goToListOrder/'.$id_est)  ?>" class="btn btn-warning waves-effect"><h6>Parts Selesai</h6></a>
                                            <br>
                                             <h6 style="color: black">Klik ini jika semua parts sudah diorder</h6>
                                        <?php
                                                }else{

                                                }
                                            }
                                        ?>


                                        </tr>

                                <?php $j++;

                            } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Exportable Table -->
    </div>
</section>
<!-- FUNCTION JS FOR CLICKABLE ROW DATATABLE -->
