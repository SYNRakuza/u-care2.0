<section class="content">
    <div class="container-fluid">

        <!-- Exportable Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            Ordering
                        </h2>

                    </div>
                     <style type="text/css">
                        /*hilangkan exportable dan menyisakan input search di tabel*/
                        .dt-buttons {
                            display: none;
                        }
                    </style>
                    <div class="body">
                        <div class="table-responsive">
                            <form method="POST" action="<?= base_url('partsman/proses_ordering') ?>">
                            <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                <h6 style="color: orange;">
                                    <?php
                                        $info = $this->session->flashdata('order_sukses');
                                        if(!empty($info)){
                                            echo $info;
                                        }
                                    ?>
                        </h6>
                                <thead>
                                    <tr>
                                        <th style="text-align: center;vertical-align: middle;">Order</th>
                                        <th style="text-align: center;vertical-align: middle;">No. WO</th>
                                        <th style="text-align: center;vertical-align: middle;">No. Parts</th>
                                        <th style="text-align: center;vertical-align: middle;">Nama Parts</th>
                                        <th style="text-align: center;vertical-align: middle;">No. Parts Lama</th>
                                        <th style="text-align: center;vertical-align: middle;">Jumlah</th>
                                        <th style="text-align: center;vertical-align: middle;">Tgl. Order</th>
                                        <th style="text-align: center;vertical-align: middle;">Tipe Order</th>
                                        <th style="text-align: center;vertical-align: middle;">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                    $i=1;
                                    foreach($data as $x){
                                ?>
                                    <tr>

                                        <td style="text-align: center;vertical-align: middle;">
                                            <div class="switch">
                                            <label><input type="checkbox" onclick="muncul()" class="tombol" name="chk[]" value="<?= $x->id_detail_estimasi ?>"><span class="lever switch-col-amber"></span></label>
                                            <input type="text" hidden class="tombol"  name="id_estimasi[]" value="<?= $x->id_estimasi ?>">
                                            </div>
                                        </td>
                                        <td style="text-align: center;vertical-align: middle;"><?= $x->nomor_wo ?></td>
                                        <td style="text-align: center;vertical-align: middle;"><?= $x->nomor_parts ?></td>
                                        <td style="text-align: center;vertical-align: middle;"><?= $x->nama_item ?></td>
                                        <td style="text-align: center;vertical-align: middle;"><?= $x->nomor_parts_lama ?></td>
                                        <td style="text-align: center;vertical-align: middle;"><?= $x->totalQuantity ?></td>
                                        <td style="text-align: center;vertical-align: middle;">
                                            <?php
                                                if($x->tgl_order == NULL){
                                                    echo "-";
                                                }else{
                                                    echo date('d-M-Y', strtotime($x->tgl_order));
                                                }
                                            ?>
                                        </td>
                                        <td style="text-align: center;vertical-align: middle;"><?= $x->lokasi_order ?></td>
                                        <td style="text-align: center;vertical-align: middle;">
                                            <a href="<?php echo base_url().'partsman/ordering/'.$x->id_item; ?>" class="btn btn-primary waves-effect">Detail</a>
                                        </td>
                                    </tr>

                                <?php } ?>

                                </tbody>
                            </table>
                            <div class="row">
                                <div class="col-lg-5">
                                    <div class="row">
                                        <div class="col-sm-3" id="input1Body" style="display: none;">
                                            <input type="submit" id="input1" formaction="<?= base_url('partsman/excel') ?>" name="" class="btn btn-success waves-effect" value="Export Excel" onclick="munculLagi()">
                                        </div>
                                        <div class="col-sm-2" id="input2Body" style="display: none;">
                                            <input type="submit" id="input2" name="" class="btn btn-warning waves-effect" onclick="verify()" disabled value="Proses">
                                        </div>
                                        <div class="col-sm-4" id="input3Body" style="display: none;">
                                            <input type="text" id="input3" placeholder="Nomor Order" name="no_order" onkeyup="checkNoOrder()" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Exportable Table -->
    </div>
</section>

<!-- FUNCTION JS FOR CLICKABLE ROW DATATABLE
<script type="text/javascript">
    $('.dataTable').on('click', 'tbody td', function() {
        var idEstimasi= $(this).data("row");
        window.location = "<?php echo base_url();?>service_advisor/customer/"+idEstimasi;

})
</script>
-->

<script type="text/javascript">

function muncul(){
    var jumlahCek = document.getElementsByClassName('tombol'); //deteksi cekbox
        var kondisi = false; //bikin kondisi ada atau tidak tercek
        for(var i=0;i<jumlahCek.length;i++){ //loop untuk membuat cek
            if(jumlahCek[i].checked){
                kondisi = true; //kalau ada yg tercek, kondisi di buat true
                break;
            }
        }

        if(kondisi){//kondisi bernilai tru, maka tombol akan ditampilkan
           document.getElementById("input1Body").style.display = "inline-block";
       }else{
           document.getElementById("input1Body").style.display = "none";
       }
}

function munculLagi(){
    document.getElementById("input2Body").style.display = "inline-block";
    document.getElementById("input3Body").style.display = "inline-block";
}

function checkNoOrder(){
    if(document.getElementById("input3").value.length > 0) { 
        document.getElementById('input2').disabled = false; 
    } else { 
        document.getElementById('input2').disabled = true;
    }
}

</script>