<section class="content">
    <div class="container-fluid">

        <!-- Exportable Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            PENGERJAAN MOBIL
                        </h2>
                    </div>
                     <style type="text/css">
                        /*hilangkan exportable dan menyisakan input search di tabel*/
                        .dt-buttons {
                            display: none;
                        }
                    </style>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                <thead>
                                    <tr>
                                        <th>ID Estimasi</th>
                                        <th>Nomor Polisi</th>
                                        <th>Detail Summary</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php  
                                    foreach($data as $x) {
                                 ?>     
                                      <tr>
                                        <td ><?= $x->id_estimasi ?></td>
                                        <td ><?= $x->no_polisi ?></td>
                                       <td>
                                                <a type="button" class="btn btn-info btn-xs waves-effect" href="<?php echo site_url ('Teknisi/summary/'.$x->id_estimasi.'') ?>">DETAIL</a>
                                            </td>
                                    </tr> 
                                 <?php } 
                                 ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Exportable Table -->
    </div>
</section>

<!-- FUNCTION JS FOR CLICKABLE ROW DATATABLE -->
<!-- <script type="text/javascript">
    $('.dataTable').on('click', 'tbody td', function() {
        var idEstimasi= $(this).data("row");
        window.location = "<?php echo base_url();?>teknisi/profile/"+idEstimasi;

})
</script> -->
