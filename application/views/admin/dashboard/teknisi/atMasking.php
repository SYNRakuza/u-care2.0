<section class="content">
    <div class="container-fluid">
        
        <!-- Exportable Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                           ESTIMASI ON MASKING
                        </h2>
                    </div>
                     <style type="text/css">
                        /*hilangkan exportable dan menyisakan input search di tabel*/
                        .dt-buttons {
                            display: none;
                        }
                        .badge-heavy {
                            border-radius: 10px;
                            font-weight: 100;
                            font-size: 13px;
                            background-color: #FF5722;
                        }
                        .badge-medium {
                            border-radius: 10px;
                            font-weight: 100;
                            font-size: 13px;
                            background-color: #FF9800;
                        }
                        .badge-light {
                            border-radius: 10px;
                            font-weight: 100;
                            font-size: 13px;
                            background-color: #FFC107;
                        }
                    </style>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                <thead>
                                    <tr>
                                        <th>No. WO</th>
                                        <th>No. Polisi</th>
                                        <th>Nama Customer</th>
                                        <th>Tgl Masuk</th>
                                        <th>Tgl Janji Penyerahan</th>
                                        <th>Nama SA</th>
                                        <th>Nama Teknisi</th>
                                        <th>Kategori Jasa</th>
                                        <th>Clock On</th>
                                        <th>Clock Pause</th>
                                        <th>Clock Off</th>
                                        <th>Lead Time</th>
                                         <?php if($this->session->userdata('level') == "qc" || $this->session->userdata('level') == "pimpinan"): ?>
                                        <th>Quality Control</th>
                                    <?php endif; ?>
                                        <th>Ket.</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        foreach ($listEstimasi as $datas) {
                                        if($datas->masking_start !== NULL):
                                            $start_from = date("H:i | d M Y", strtotime($datas->masking_start));
                                            $lead_start = date("Y-m-d H:i:s", strtotime($datas->masking_start));
                                        else:
                                            $start_from = date("H:i | d M Y", strtotime($datas->masking_in));
                                            $lead_start = date("Y-m-d H:i:s", strtotime($datas->masking_in));
                                        endif;
                                        if($datas->masking_pause !== NULL):
                                            $last_pause = date("H:i | d M Y", strtotime($datas->masking_pause));;
                                        else:
                                            $last_pause = "-";
                                        endif;
                                        if($datas->masking_lead !== NULL):
                                            $hours = floor($datas->masking_lead / 60);
                                            $minutes = $datas->masking_lead % 60;
                                            $totalLead = $hours." Hours ".$minutes." Minutes ";
                                        else:
                                            $totalLead = "-";
                                        endif;
                                    ?>
                                        <tr>
                                            <td><?= $datas->nomor_wo ?></td>
                                            <td><?= $datas->no_polisi ?></td>
                                            <td><?= $datas->nama_lengkap ?></td>
                                            <td style="text-align: center;">
                                              <?php if($datas->tgl_masuk !== NULL){
                                                echo "".date('d M Y', strtotime($datas->tgl_masuk));
                                              }else{
                                                echo "-";
                                              } ?>
                                            </td>
                                            <td style="text-align: center;">
                                              <?php if($datas->tgl_janji_penyerahan !== NULL){
                                                echo "".date('d M Y', strtotime($datas->tgl_janji_penyerahan));
                                              }else{
                                                echo "-";
                                              } ?>
                                            </td>
                                            <td><?= $datas->nama_sa ?></td>
                                            <td><?= $datas->nama_teknisi?></td>
                                            <td align="center">
                                                <?php if($datas->kategori_jasa == '0'){ ?>
                                                    <span class="badge badge-light">Light</span>
                                                <?php }elseif($datas->kategori_jasa  == '1'){ ?>
                                                    <span class="badge badge-medium">Medium</span>
                                                <?php }elseif ($datas->kategori_jasa == '2' ){ ?>
                                                    <span class="badge badge-heavy">Heavy</span>
                                                <?php }else{
                                                     echo "-";
                                                 }?>
                                            </td>
                                            <td>
                                                <?php if($datas->masking_in !== NULL){
                                                    echo "".date('d M Y', strtotime($datas->masking_in));
                                                    }else{ ?>
                                                        <button style="margin-right: 5px;" type="button" class="btn bg-green btn-xs waves-effect clockOn" data-idlead ="<?=$datas->id_lead ?>" data-row="3" data-id="<?= $datas->id_estimasi ?>" data-url="<?= base_url('teknisi/clockOn')?>" <?= ($datas->is_confirm_teknisi == NULL ? 'disabled' : '') ?>>ON</button>
                                            <?php } ?>
                                            </td>
                                            <td>
                                            <?php if($datas->masking_status == 'pause'): ?>
                                            <button type="button" class="btn bg-lime btn-xs waves-effect clockStart" data-idlead ="<?=$datas->id_lead ?>" data-note="<?= $datas->masking_note ?>" data-pause="<?= $last_pause ?>" data-row="3" data-id="<?= $datas->id_estimasi ?>"<?= ($datas->masking_in == NULL || $datas->masking_out !== NULL ? 'disabled' : '')?>>
                                                 <i class=" material-icons">play_arrow</i>
                                            </button>
                                            <br>
                                            <?php else: ?>
                                            <button type="button" class="btn bg-grey btn-xs waves-effect clockPause" data-idlead ="<?=$datas->id_lead ?>" data-total ="<?=$datas->total_lead?>" data-leadstart="<?= $lead_start ?>" data-lead="<?= $datas->masking_lead ?>" data-start="<?= $start_from ?>" data-row="3" data-id="<?= $datas->id_estimasi ?>"<?= ($datas->masking_in == NULL || $datas->masking_out !== NULL ? 'disabled' : '')?>>
                                                <i class="material-icons">pause</i>
                                            </button>
                                            <?php endif ?>
                                            </td>
                                            <td>
                                                <?php if($datas->masking_out !== NULL){
                                                    echo "".date('d M Y', strtotime($datas->masking_out));
                                                    }else{ ?>
                                                        <button style="margin-right: 5px;" type="button" class="btn bg-red btn-xs waves-effect clockOff" data-idlead ="<?=$datas->id_lead ?>" data-total ="<?=$datas->total_lead?>" data-leadstart="<?= $lead_start ?>" data-lead="<?= $datas->masking_lead ?>" data-row="3" data-id="<?= $datas->id_estimasi ?>" data-url="<?= base_url('teknisi/clockOff')?>"<?= ($datas->masking_in == NULL || $datas->masking_status == 'pause' ? 'disabled' : '') ?>>OFF</button>
                                                    <?php } ?>
                                            </td>
                                            <td><?= $totalLead ?></td>
                                            <?php if($this->session->userdata('level') == "qc" || $this->session->userdata('level') == "pimpinan"): ?>
                                            <td>
                                                <button style="margin-right: 5px;" type="button" class="btn bg-green btn-xs waves-effect qControl" data-idlead ="<?=$datas->id_lead ?>" data-row="3" data-id="<?= $datas->id_estimasi ?>"<?= ($datas->masking_out == NULL || $datas->masking_in == NULL ? 'disabled' : '') ?>>OK</button>
                                            </td>
                                        <?php endif; ?>
                                            <td><?= $datas->ket ?></td>
                                            <td style="text-align: center;">
                                                <a style="margin-right: 5px;" type="button" class="btn bg-orange btn-xs waves-effect" href="<?= base_url('teknisi/on_process/').$datas->id_estimasi ?>"><i class="material-icons">adjust</i></a>
                                            </td>
                                        </tr>
                                    <?php
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade tgl_modal" id="smallModal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="smallModalLabel">Form Quality Control</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-line">
                            <input name="group1" type="radio" id="radio_4" class="with-gap" value="1" checked="checked" />
                            <label for="radio_4">OK</label>
                           <input name="group1" type="radio" class="with-gap" id="radio_3" value="0" />
                            <label for="radio_3">REDO</label>
                        </div>
                        <br />
                        <div class="form-line">
                            <textarea rows="4" class="form-control no-resize" name="message" placeholder="Keterangan..." id="ket"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        
                        <button type="button" class="btn bg-orange waves-effect save" data-url="<?php echo base_url();?>teknisi/qualityControl">SAVE</button>
                        
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="pauseModal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="smallModalLabel">Are You Sure Want to Pause ?</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-line">
                            <label>You can left note below</label>
                        </div>
                        <div class="form-line">
                            <textarea rows="4" class="form-control no-resize" name="message" placeholder=". . . " id="note"></textarea>
                        </div>
                            <h5 id="start_from"></h5>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Cancel</button>
                         <button type="button" class="btn bg-orange waves-effect pause" data-url="<?php echo base_url();?>teknisi/clockPause">OK</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="startModal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="smallModalLabel">Are You Sure Want to Start ?</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-line">
                            <label>Note</label>
                        </div>
                        <div class="form-line">
                            <textarea rows="4" class="form-control no-resize" name="message" readonly id="note_pause"></textarea>
                        </div>
                            <h5 id="last_pause"></h5>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Cancel</button>
                         <button type="button" class="btn bg-orange waves-effect start" data-url="<?php echo base_url();?>teknisi/clockStart">OK</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

