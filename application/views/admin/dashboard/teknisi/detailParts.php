<section class="content">
    <div class="container-fluid">
        
        <!-- Exportable Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            Detail Parts Estimasi
                        </h2>
                    </div>
                     <style type="text/css">
                        /*hilangkan exportable dan menyisakan input search di tabel*/
                        .dt-buttons {
                            display: none;
                        }
                        h5{
                            color: #009688;
                        }
                    </style>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="table-responsive">
                                    <h5>Data Customer</h5>
                                    <table class="table table-customer">
                                        <tbody>
                                        <?php 
                                            $diskonJasa = 0;
                                            $diskonParts = 0;
                                        foreach($dataEstimasi as $datas){
                                        ?>  
                                            <tr style="display: none;">
                                                <td width="125"></td>
                                                <td width="1"></td>
                                                <td id="id_estimasi"><?= $datas->id_estimasi ?></td>
                                            </tr>
                                            <tr style="display: none;">
                                                <td width="125"></td>
                                                <td width="1"></td>
                                                <td id="id_customer"><?= $datas->id_customer ?></td>
                                            </tr>
                                            <tr>
                                                <td width="125">Nama</td>
                                                <td width="1">:</td>
                                                <td><?= $datas->nama_lengkap ?></td>
                                            </tr>
                                             <tr>
                                                <td>No. Telp</td>
                                                <td>:</td>
                                                <td><?= $datas->no_hp ?></td>
                                            </tr>
                                            <tr>
                                                <td>Alamat</td>
                                                <td>:</td>
                                                <td><?= $datas->alamat ?></td>
                                            </tr>
                                            <tr>
                                                <td>Tipe Customer</td>
                                                <td>:</td>
                                                <!-- <td><?= $datas->jenis_customer ?></td> -->
                                                <?php if($datas->jenis_customer == '0'){ ?>
                                                    <td>Asuransi</td>
                                                <?php }else{ ?>
                                                    <td>Tunai</td>
                                                <?php } ?>
                                            </tr>
                                        <?php }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="table-responsive">
                                    <h5>Data Kendaraan</h5>
                                    <table class="table table-customer">
                                        <tbody>
                                            <?php foreach($dataEstimasi as $datas){
                                                $diskonJasa = $datas->diskon_jasa;
                                                $diskonParts = $datas->diskon_parts;
                                            ?>  
                                            <tr>
                                                <td>No. Polisi</td>
                                                <td>:</td>
                                                <td><?= $datas->no_polisi ?></td>
                                            </tr>
                                            <tr>
                                                <td width="155">Jenis Kendaraan</td>
                                                <td width="1">:</td>
                                                <td><?= $datas->nama_jenis ?></td>
                                            </tr>
                                            <tr>
                                                <td width="155">Warna</td>
                                                <td width="1">:</td>
                                                <td><?= $datas->code_color.' - '.$datas->nama_color ?></td>
                                            </tr>
                                            <tr>
                                                <td width="155">No. Rangka</td>
                                                <td width="1">:</td>
                                                <td><?= $datas->no_rangka ?></td>
                                            </tr>
                                            <?php }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <style type="text/css">
                            .badge-not {
                                border-radius: 10px;
                                font-weight: 100;
                                font-size: 13px;
                                background-color: red;
                            }
                            .badge-yes {
                                border-radius: 10px;
                                font-weight: 100;
                                font-size: 13px;
                                background-color: green;
                            }
                            th{
                                text-align: center;
                            }
                        </style>
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <h5>Pemesanan Parts</h5>
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                            <th>No.</th>
                                            <th>No. Parts</th>
                                            <th>Nama Parts</th>
                                            <th>Status</th>
                                            <th>ETA</th>
                                            <th>ATA</th>
                                            <th>Tgl Diambil</th>
                                            <th>Pengambil</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                            $i=1;
                                            foreach($dataParts as $x){
                                        ?>

                                         <tr>
                                            <td style="text-align: center;"><?= $i++ ?></td>
                                            <td style="text-align: center;"><?= $x->nomor_parts ?></td>
                                            <td style="text-align: center;"><?= $x->nama_item ?></td>
                                            <td style="text-align: center;">
                                                <?php
                                                    if($x->ata == NULL){
                                                        ?>
                                                        <span class="badge badge-not">Not Ready</span>
                                                    
                                                <?php }else{ ?>
                                                        <span class="badge badge-yes">Ready</span>
                                                    
                                                <?php  }
                                                ?>
                                            </td>
                                            <td  style="text-align: center;">
                                                <?php
                                                    if($x->eta == NULL){
                                                        echo "-";
                                                    }else{
                                                        $datax = date('d M Y', strtotime($x->eta));
                                                        echo $datax;
                                                    }
                                                ?>
                                            </td>
                                            <td  style="text-align: center;">
                                                <?php
                                                    if($x->ata == NULL){
                                                        echo "-";
                                                    }else{
                                                        $datax = date('d M Y', strtotime($x->ata));
                                                        echo $datax;
                                                    }
                                                ?>
                                            </td>
                                            <td  style="text-align: center;">
                                                <?php
                                                    if($x->tgl_pengambilan == NULL){
                                                        echo "-";
                                                    }else{
                                                        $datay = date('d/m/Y', strtotime($x->tgl_pengambilan));
                                                        echo $datay;
                                                    }
                                                ?>
                                            </td>
                                            <td  style="text-align: center;">
                                                <?php
                                                    if($x->pengambil == NULL){
                                                        echo "-";
                                                    }else{
                                                        echo $x->pengambil;
                                                    }
                                                ?>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Exportable Table -->
    </div>
</section>
<!-- FUNCTION JS FOR CLICKABLE ROW DATATABLE -->
