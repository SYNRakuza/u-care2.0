<section class="content">
    <div class="container-fluid">
        
        <!-- Exportable Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                          <?= $titleCard ?>
                        </h2>
                    </div>
                     <style type="text/css">
                        /*hilangkan exportable dan menyisakan input search di tabel*/
                        .dt-buttons {
                            display: none;
                        }
                        .badge-heavy {
                            border-radius: 10px;
                            font-weight: 100;
                            font-size: 13px;
                            background-color: #FF5722;
                        }
                        .badge-medium {
                            border-radius: 10px;
                            font-weight: 100;
                            font-size: 13px;
                            background-color: #FF9800;
                        }
                        .badge-light {
                            border-radius: 10px;
                            font-weight: 100;
                            font-size: 13px;
                            background-color: #FFC107;
                        }
                    </style>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                <thead>
                                    <tr>
                                        <th>No. WO</th>
                                        <th>No. Polisi</th>
                                        <th>Nama Customer</th>
                                        <th>Nama SA</th>
                                        <th>Tgl Masuk</th>
                                        <th>Tgl Keluar</th>
                                        <th>Lama Pengerjaan</th>
                                        <th>Kategori Jasa</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        foreach ($listEstimasi as $datas) {
                                            $dateIn = strtotime($datas->tgl_masuk);
                                            $dateOut = strtotime($datas->tgl_done);
                                            $dateDiff = round(($dateOut - $dateIn)/(60*60*24));
                                    ?>
                                        <tr>
                                            <td><?= $datas->nomor_wo ?></td>
                                            <td><?= $datas->no_polisi ?></td>
                                            <td><?= $datas->nama_lengkap ?></td>
                                            <td><?= $datas->nama_sa ?></td>
                                            <td style="text-align: center;">
                                              <?php if($datas->tgl_masuk !== NULL){
                                                echo "".date('d M Y', strtotime($datas->tgl_masuk));
                                              }else{
                                                echo "-";
                                              } ?>
                                            </td>
                                            <td style="text-align: center;">
                                              <?php if($datas->tgl_done !== NULL){
                                                echo "".date('d M Y', strtotime($datas->tgl_done));
                                              }else{
                                                echo "-";
                                              } ?>
                                            </td>
                                            <td style="text-align: center;"><?= $dateDiff ?> Hari</td>
                                            <td align="center">
                                                <?php if($datas->kategori_jasa == '0'){ ?>
                                                    <span class="badge badge-light">Light</span>
                                                <?php }elseif($datas->kategori_jasa  == '1'){ ?>
                                                    <span class="badge badge-medium">Medium</span>
                                                <?php }elseif ($datas->kategori_jasa == '2' ){ ?>
                                                    <span class="badge badge-heavy">Heavy</span>
                                                <?php }else{
                                                     echo "-";
                                                 }?>
                                            </td>
                                            <td style="text-align: center;">
                                                <a style="margin-right: 5px;" type="button" class="btn bg-orange btn-xs waves-effect" href="<?= base_url('teknisi/done/').$datas->id_estimasi ?>"><i class="material-icons">adjust</i></a>
                                            </td>
                                            
                                        </tr>
                                    <?php
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Exportable Table -->
        <div class="modal fade tgl_modal" id="smallModal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="smallModalLabel">Tahapan Produksi</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-line">
                           <div class="form-group">
                                <div class="form-control  show-tick">
                                    <select name="id_jenis" id="tahap_produksi">
                                        <option value="">-- pilih tahap produksi --</option>
                                        <option value="1">Body Paint</option>
                                        <option value="2">Preparation</option>
                                        <option value="3">Masking</option>
                                        <option value="4">Painting</option>
                                        <option value="5">Polesing</option>
                                        <option value="6">Re Assembling</option>
                                        <option value="7">Washing</option>
                                        <option value="8">Final Inspection</option>
                                        <option value="9">Done</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn bg-orange waves-effect save" data-url="<?php echo base_url('teknisi/update_tahap_produksi')?>">SAVE</button>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

