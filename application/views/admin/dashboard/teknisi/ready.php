<section class="content">
    <div class="container-fluid">
        
        <!-- Exportable Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                           ESTIMASI READY
                        </h2>
                    </div>
                     <style type="text/css">
                        /*hilangkan exportable dan menyisakan input search di tabel*/
                        .dt-buttons {
                            display: none;
                        }
                        .badge-heavy {
                            border-radius: 10px;
                            font-weight: 100;
                            font-size: 13px;
                            background-color: #FF5722;
                        }
                        .badge-medium {
                            border-radius: 10px;
                            font-weight: 100;
                            font-size: 13px;
                            background-color: #FF9800;
                        }
                        .badge-light {
                            border-radius: 10px;
                            font-weight: 100;
                            font-size: 13px;
                            background-color: #FFC107;
                        }
                    </style>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                <thead>
                                    <tr>
                                        <th>No. WO</th>
                                        <th>No. Polisi</th>
                                        <th>Nama Customer</th>
                                        <th>Tgl Masuk</th>
                                        <th>Tgl Janji Penyerahan</th>
                                        <th>Nama SA</th>
                                        <?php
                                            if($this->session->userdata('level') == 'pimpinan'){
                                                echo "
                                                    <th>Nama Teknisi</th>
                                                ";
                                            }
                                        ?>
                                        <th>Kategori Jasa</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        foreach ($listEstimasi as $datas) {
                                    ?>
                                        <tr>
                                            <td><?= $datas->nomor_wo ?></td>
                                            <td><?= $datas->no_polisi ?></td>
                                            <td><?= $datas->nama_lengkap ?></td>
                                            <td style="text-align: center;">
                                              <?php if($datas->tgl_masuk !== NULL){
                                                echo "".date('d M Y', strtotime($datas->tgl_masuk));
                                              }else{
                                                echo "-";
                                              } ?>
                                            </td>
                                            <td style="text-align: center;">
                                              <?php if($datas->tgl_janji_penyerahan !== NULL){
                                                echo "".date('d M Y', strtotime($datas->tgl_janji_penyerahan));
                                              }else{
                                                echo "-";
                                              } ?>
                                            </td>
                                            <td><?= $datas->nama_sa ?></td>
                                             <?php
                                                if($this->session->userdata('level') == 'pimpinan'){
                                            ?>
                                            <td><?= $datas->teknisi ?></td>
                                            <?php
                                                }
                                            ?>
                                            <td align="center">
                                                <?php if($datas->kategori_jasa == '0'){ ?>
                                                    <span class="badge badge-light">Light</span>
                                                <?php }elseif($datas->kategori_jasa  == '1'){ ?>
                                                    <span class="badge badge-medium">Medium</span>
                                                <?php }elseif ($datas->kategori_jasa == '2' ){ ?>
                                                    <span class="badge badge-heavy">Heavy</span>
                                                <?php }else{
                                                     echo "-";
                                                 }?>
                                            </td>
                                            <td style="text-align: center;">
                                                <a style="margin-right: 5px;" type="button" class="btn bg-orange btn-xs waves-effect" href="<?= base_url('teknisi/ready/').$datas->id_estimasi ?>"><i class="material-icons">adjust</i></a>
                                            </td>
                                            
                                        </tr>
                                    <?php
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Exportable Table -->
    </div>
</section>

