<section class="content">
    <div class="container-fluid">
        
        <!-- Exportable Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            DETAIL ESTIMASI
                        </h2>
                    </div>
                     <style type="text/css">
                        /*hilangkan exportable dan menyisakan input search di tabel*/
                        .dt-buttons {
                            display: none;
                        }
                    </style>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="">
                                <h5>Data Customer</h5>
                                    <table class="table table-customer">
                                        <tbody>
                                        <?php  
                                        foreach($dataEstimasi as $datas){
                                        ?>  
                                            <tr style="display: none;">
                                                <td width="125"></td>
                                                <td width="1"></td>
                                                <td id="id_estimasi"><?= $datas->id_estimasi ?></td>
                                            </tr>
                                            <tr style="display: none;">
                                                <td width="125"></td>
                                                <td width="1"></td>
                                                <td id="id_customer"><?= $datas->id_customer ?></td>
                                            </tr>
                                            <tr>
                                                <td width="125">Nama</td>
                                                <td width="1">:</td>
                                                <td><?= $datas->nama_lengkap ?></td>
                                            </tr>
                                             <tr>
                                                <td>No. Telp</td>
                                                <td>:</td>
                                                <td><?= $datas->no_hp ?></td>
                                            </tr>
                                             <tr>
                                                <td>Alamat</td>
                                                <td>:</td>
                                                <td><?= $datas->alamat ?></td>
                                            </tr>
                                        <?php }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <h5>Data Kendaraan</h5>
                                <div class="">
                                    <table class="table table-customer">
                                        <tbody>
                                        <?php foreach($dataEstimasi as $datas){   
                                        ?>  
                                            <tr>
                                                <td width="155">Jenis Kendaraan</td>
                                                <td width="1">:</td>
                                                <td><?= $datas->nama_jenis ?></td>
                                            </tr>
                                            <tr>
                                                <td>No. Polisi</td>
                                                <td>:</td>
                                                <td><?= $datas->no_polisi ?></td>
                                            </tr>
                                            <tr>
                                                <td>Kategori Customer</td>
                                                <td>:</td>
                                            <?php if($datas->jenis_customer == '0'):?>
                                                <td><span>Asuransi - <?= $datas->nama_asuransi ?></span></td>
                                            <?php else: ?>
                                                <td><span>Tunai</span></td>
                                            <?php endif;?>
                                            </tr>
                                        <?php }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <style type="text/css">
                                .badge-not {
                                    border-radius: 10px;
                                    font-weight: 100;
                                    font-size: 13px;
                                    background-color: red;
                                }
                                .badge-yes {
                                    border-radius: 10px;
                                    font-weight: 100;
                                    font-size: 13px;
                                    background-color: green;
                                }
                            </style>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <h5>Items</h5>
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>No.</th>
                                                <th>Nama Items</th>
                                                <th>Jenis Items</th>
                                                <th>Status</th>
                                                <th>ETA</th>
                                                <th>ATA</th>
                                                <th>Tgl Diambil</th>
                                                <th>Pengambil</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                            $i=1;
                                            foreach($dataPartsJasa as $x){
                                        ?>
                                         <tr>
                                            <td  style="text-align: center;"><?= $i++ ?></td>
                                            <td><?= $x->nama_item ?></td>
                                            <td>
                                                <?php if($x->tipe_item == '1'){
                                                        echo "Jasa";   
                                                    }else if($x->tipe_item == '2'){
                                                        echo "Parts";
                                                    } ?>
                                            </td>
                                            <td  style="text-align: center;">
                                                <?php
                                                    if($x->ata == NULL){
                                                        if($x->tipe_item == "1" || $x->tipe_item == "3"){
                                                            echo "-";
                                                        }else{ ?>
                                                            <span class="badge badge-not">Not Ready</span> 
                                                       <?php } ?>                                  
                                                <?php }else{ ?>
                                                        <span class="badge badge-yes">Ready</span>
                                                    
                                                <?php  }
                                                ?>
                                            </td>
                                            <td  style="text-align: center;">
                                               <?php if($x->tipe_item == "1" || $x->tipe_item == "3"){
                                                            echo "-";
                                                        }else{ 
                                                            echo date('d M Y', strtotime($x->eta));
                                                        } ?>

                                            </td>
                                            <td  style="text-align: center;">
                                                <?php
                                                    if($x->ata == NULL){
                                                        echo "-";
                                                    }else{
                                                        $datax = date('d M Y', strtotime($x->ata));
                                                        echo $datax;
                                                    }
                                                ?>
                                            </td>
                                            <td  style="text-align: center;">
                                                <?php
                                                    if($x->tgl_pengambilan == NULL){
                                                        echo "-";
                                                    }else{
                                                        $datay = date('d M Y', strtotime($x->tgl_pengambilan));
                                                        echo $datay;
                                                    }
                                                ?>
                                            </td>
                                            <td  style="text-align: center;">
                                                <?php
                                                    if($x->pengambil == NULL){
                                                        echo "-";
                                                    }else{
                                                        echo $x->pengambil;
                                                    }
                                                ?>
                                            </td>
                                        </tr>

                                <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: center;">   
                                <?php foreach($dataEstimasi as $datas){?>
                                <button type="button" class="btn bg-orange waves-effect done" data-row="<?= $datas->id_estimasi ?>" data-url="<?php echo base_url('teknisi/go_process')?>" data-redirect="<?php echo base_url('teknisi/ready')?>"><i class="material-icons">loop</i>
                                <span>PROCESS</span>
                                </button> 
                            <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Exportable Table -->
        <div class="modal fade tgl_modal" id="smallModal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="smallModalLabel">Tahapan Produksi</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-line">
                           <div class="form-group">
                                <div class="form-control  show-tick">
                                    <select name="id_jenis" id="tahap_produksi">
                                        <option value="1">Body Paint</option>
                                        <option value="2">Preparation</option>
                                        <option value="3">Masking</option>
                                        <option value="4">Painting</option>
                                        <option value="5">Polishing</option>
                                        <option value="6">Re Assembling</option>
                                        <option value="7">Washing</option>
                                        <option value="8">Final Inspection</option>
                                        <option value="9">Done</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn bg-orange waves-effect save">SAVE</button>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- FUNCTION JS FOR CLICKABLE ROW DATATABLE -->
