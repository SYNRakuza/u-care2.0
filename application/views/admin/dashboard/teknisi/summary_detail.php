<section class="content">
    <div class="container-fluid">

        <!-- Exportable Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            DETAIL SUMMARY
                        </h2>
                    </div>
                     <style type="text/css">
                        /*hilangkan exportable dan menyisakan input search di tabel*/
                        .dt-buttons {
                            display: none;
                        }
                    </style>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                <tbody>
                                    <?php
                                        foreach ($data as $datas) {
                                    ?>
                                        <tr>
                                            <td>ID</td>
                                            <td><?= $datas->id_estimasi ?></td>
                                        </tr>
                                         <tr>
                                            <td>Nama</td>
                                            <td><?= $datas->nama_lengkap ?></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                No Polisi
                                            </td>
                                            <td>
                                                <?=$datas->no_polisi?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>No. Tlp</td>
                                            <td><?= $datas->no_hp?></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Nama SA
                                            </td>
                                            <td>
                                                <?= $datas->nama_lengkap?>
                                            </td>
                                        </tr>
                                       <!--  <tr>
                                            <td>Alamat</td>
                                            <td><?= $datas->alamat ?></td>
                                        </tr> -->
                                    <?php
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                            <div class="body">
                        <?php foreach ($data as $datad) {
                        ?>
                        <form method="POST" action="<?= base_url('Teknisi/summary/'.$datad->id_estimasi)?>">
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="card">
                                        <div class="header">
                                            <h2>
                                                Update Teknisi
                                                <!-- <small>Taken from <a href="https://silviomoreto.github.io/bootstrap-select/" target="_blank">silviomoreto.github.io/bootstrap-select</a></small> -->
                                            </h2>
                                            <ul class="header-dropdown m-r--5">
                                                <li class="dropdown">
                                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                                        <i class="material-icons">more_vert</i>
                                                    </a>
                                                  <!--   <ul class="dropdown-menu pull-right">
                                                        <li><a href="javascript:void(0);">Action</a></li>
                                                        <li><a href="javascript:void(0);">Another action</a></li>
                                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                                    </ul> -->
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="body">
                                            <div class="table-responsive">
                                                <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                                    <thead>
                                                        <tr>
                                                            <th >Prosess</th>
                                                            <th >On</th>
                                                            <th>Off</th>
                                                        </tr>
                                                    </thead>

                                                    <tbody>
                                                        <?php foreach ($data1 as $x ) {
                                                            
                                                         ?>
                                                        <tr>
                                                            <td>Body Repair</td>
                                                            <td><?=$x->body_repair_in?></td>
                                                            <td><?=$x->body_repair_out?></td>
                                                        </tr>
                                                         <tr>
                                                            <td>Preparation</td>
                                                            <td><?=$x->preparation_in?></td>
                                                            <td><?=$x->preparation_out?></td>
                                                        </tr>
                                                         <tr>
                                                            <td>Masking</td>
                                                            <td><?=$x->masking_in?></td>
                                                            <td><?=$x->masking_out?></td>
                                                        </tr>
                                                         <tr>
                                                            <td>Painting</td>
                                                            <td><?=$x->painting_in?></td>
                                                            <td><?=$x->painting_out?></td>
                                                        </tr>
                                                         <tr>
                                                            <td>Polishing</td>
                                                            <td><?=$x->polishing_in?></td>
                                                            <td><?=$x->polishing_out?></td>
                                                        </tr>
                                                         <tr>
                                                            <td>Re-assembling</td>
                                                            <td><?=$x->re_assembling_in?></td>
                                                            <td><?=$x->re_assembling_out?></td>
                                                        </tr>
                                                         <tr>
                                                            <td>Washing</td>
                                                            <td><?=$x->wasling_in?></td>
                                                            <td><?=$x->wasling_out?></td>
                                                        </tr>
                                                         <tr>
                                                            <td>Final inspection</td>
                                                            <td>final_inspection_in</td>
                                                            <td>final_inspection_out</td>
                                                        </tr>
                                                          <?php } ?>
                                                    </tbody>
                                                </table>       
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- #END# Advanced Select -->
                            <!--                             <?php
                            foreach ($data as $datas) {
                            ?>
                            <div class="form-group" style="display: none">
                                <div class="form-line">
                                    <input type="text" class="form-control" name="id_estimasi" placeholder="Password" value="<?= $datas->id_estimasi ?>" />
                                </div>
                            </div>
                            <?php } ?> -->
                            
                        </form>
                        <?php } ?>
                    </div>                
                    </div>
            </div>
        </div>
        <!-- #END# Exportable Table -->
    </div>
</section>

<!-- FUNCTION JS FOR CLICKABLE ROW DATATABLE -->
<!-- <script type="text/javascript">
    $('.dataTable').on('click', 'tbody td', function() {
        var idEstimasi= $(this).data("row");
        window.location = "<?php echo base_url();?>teknisi/profile/"+idEstimasi;

})
</script> -->
