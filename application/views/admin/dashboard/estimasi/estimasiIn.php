<?php $modul = $this->uri->segment(2); ?>
<section class="content">
    <div class="container-fluid">
        
        <!-- Exportable Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <?php if($modul === 'estimasiAll'): ?>
                        <h2>
                            LIST ALL ESTIMASI
                        </h2>
                        <?php else: ?>
                        <h2>
                            LIST ESTIMASI IN
                        </h2>
                        <?php endif; ?>
                        <?php if($modul !== 'estimasiAll') {
                        ?>
                        <ul class="header-dropdown m-r--5">
                            <li class="dropdown">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">more_vert</i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="<?= site_url() ?>service_advisor/estimasiCreate">Tambah Estimasi</a></li>
                                </ul>
                            </li>
                        </ul>
                        <?php
                            }
                        ?>
                    </div>
                     <style type="text/css">
                        /*hilangkan exportable dan menyisakan input search di tabel*/
                        .dt-buttons {
                            display: none;
                        }
                    </style>
                    <div class="body">
                        <div class="table-responsive">
                            <!-- <table id="mytable" class="table table-bordered table-striped table-hover dataTable js-exportable"> -->
                            <table id="mytable" class="table table-bordered table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th style="text-align: center;vertical-align: middle;">No. WO</th>
                                        <th style="text-align: center;vertical-align: middle;">No. Polisi</th>
                                        <th style="text-align: center;vertical-align: middle;">Nama Customer</th>
                                        <?php
                                            if($this->session->userdata('level') == 'pimpinan' || $modul == 'estimasiAll'){
                                        ?>
                                            <th style="text-align: center;vertical-align: middle;">Nama SA</th>
                                        <?php
                                            }
                                        ?>
                                        <th style="text-align: center;vertical-align: middle;">Tgl. Estimasi</th>
                                        <th style="text-align: center;vertical-align: middle;">Tgl. Masuk</th>
                                        <th style="text-align: center;vertical-align: middle;">Tgl. Janji Penyerahan</th>
                                        <th style="text-align: center;vertical-align: middle;">Type Customer</th>
                                        <?php if($modul === 'estimasiAll'): ?>
                                        <th style="text-align: center;vertical-align: middle;">Jenis Estimasi</th>
                                        <?php endif; ?>
                                        <th style="text-align: center;vertical-align: middle;">Action</th>
                                    </tr>
                                </thead>
                                <!-- <tbody>
                                    <?php
                                        foreach ($data as $datas) {
                                        if($datas->status_inout == '0'):
                                            $status = 'Out';
                                        elseif($datas->status_inout == '1'):
                                            $status = 'In';
                                        endif;
                                    ?>
                                        <tr>
                                            <td style="text-align: center;vertical-align: middle;"><?= $datas->nomor_wo ?></td>
                                            <td style="text-align: center;vertical-align: middle;"><?= $datas->no_polisi ?></td>
                                            <td style="text-align: center;vertical-align: middle;"><?= $datas->nama_lengkap ?></td>
                                            <?php
                                            if($this->session->userdata('level') == 'pimpinan' || $modul == 'estimasiAll'){
                                            ?>
                                                <td style="text-align: center;vertical-align: middle;"><?= $datas->nama_lengkap_user ?></td>
                                            <?php
                                                }
                                            ?>
                                            <td style="text-align: center;vertical-align: middle;"><?php echo "".date('d M Y', strtotime($datas->tgl_estimasi)); ?></td>
                                        <?php if($datas->tgl_masuk !== NULL):?>    
                                            <td style="text-align: center;vertical-align: middle;"><?php echo "".date('d M Y', strtotime($datas->tgl_masuk)); ?></td>
                                        <?php else: ?>
                                            <td style="text-align: center;vertical-align: middle;"></td>
                                        <?php endif; ?>
                                            <td style="text-align: center;vertical-align: middle;">
                                                <?php if($datas->status_inout == '1') { ?>
                                                <?php echo "".date('d M Y', strtotime($datas->tgl_janji_penyerahan)); ?>
                                                <?php } ?>
                                                </td>
                                        <?php if($datas->jenis_customer == '0'){ ?>
                                            <td style="text-align: center;vertical-align: middle;">Asuransi - <?= $datas->nama_asuransi ?></td>
                                        <?php }else{ ?>
                                            <td style="text-align: center;vertical-align: middle;">Tunai</td>
                                        <?php } ?>
                                        <?php if($modul === 'estimasiAll'): ?>
                                            <td style="text-align: center;vertical-align: middle;"><?= $status ?></td>
                                        <?php else: ?>
                                            <td style="text-align: center;vertical-align: middle;">
                                                <a style="margin-right: 5px;" type="button" class="btn bg-orange btn-xs waves-effect" href="<?= base_url('service_advisor/printEstimasi/').$datas->id_estimasi ?>" target="_blank"><i class="material-icons">print</i></a>
                                            </td>    
                                        <?php endif; ?>
                                        </tr>
                                    <?php
                                        }
                                    ?>
                                </tbody> -->
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Exportable Table -->
    </div>
</section>
<!-- FUNCTION JS FOR CLICKABLE ROW DATATABLE -->

<script type="text/javascript">

$(document).ready(function(){

    //setup datatables
    $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
      {
          return {
              "iStart": oSettings._iDisplayStart,
              "iEnd": oSettings.fnDisplayEnd(),
              "iLength": oSettings._iDisplayLength,
              "iTotal": oSettings.fnRecordsTotal(),
              "iFilteredTotal": oSettings.fnRecordsDisplay(),
              "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
              "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
          };
      };

      var table = $("#mytable").dataTable({
          
              oLanguage: {
              sProcessing: "loading..."
          },
              pageLength: 15,
              processing: true,
              serverSide: true,
              bLengthChange: false,
              <?php if($this->session->userdata('level') == 'service advisor' && $modul == 'estimasiAll'):
              ?>
              ajax: {"url": "<?php echo base_url().'service_advisor/get_estimasiAll_json'?>", "type": "POST"},
                    columns: [
                                {"data": "nomor_wo"},
                                {"data": "no_polisi"},
                                {"data": "nama_lengkap"},
                                {"data": "nama_lengkap_user"},
                                {"data": "tgl_estimasi", render: $.fn.dataTable.render.moment( 'D MMM YYYY' )},
                                {"data": "tgl_masuk", render: $.fn.dataTable.render.moment( 'D MMM YYYY' )},
                                {"data": "tgl_janji_penyerahan", render: $.fn.dataTable.render.moment( 'D MMM YYYY' )},
                                {"data": null, 'searchable': false, 'orderable': false, render: function(data) {
                                    if (data.jenis_customer == '0') {
                                        return 'Asuransi - '+data.nama_asuransi;
                                    }
                                    else {
                                        return 'Tunai';
                                    }
                                }},
                                {"data": "status_inout", render: function(data) { 
                                    if (data === "0") {
                                        return 'Out'; 
                                        }
                                    else {
                                        return 'In';
                                        }
                                    } 
                                },
                                {"data": "view", 'orderable': false},
                  ],
                  order: [ 4 , 'desc'],
              <?php elseif($this->session->userdata('level') == 'service advisor' && $modul != 'estimasiAll'): ?>
                    ajax: {"url": "<?php echo base_url().'service_advisor/get_estimasiin_json'?>", "type": "POST"},
                    columns: [
                                {"data": "nomor_wo"},
                                {"data": "no_polisi"},
                                {"data": "nama_lengkap"},
                                {"data": "tgl_estimasi", render: $.fn.dataTable.render.moment( 'D MMM YYYY' )},
                                {"data": "tgl_masuk", render: $.fn.dataTable.render.moment( 'D MMM YYYY' )},
                                {"data": "tgl_janji_penyerahan", render: $.fn.dataTable.render.moment( 'D MMM YYYY' )},
                                {"data": null, 'searchable': false, 'orderable': false, render: function(data) {
                                    if (data.jenis_customer == '0') {
                                        return 'Asuransi - '+data.nama_asuransi;
                                    }
                                    else {
                                        return 'Tunai';
                                    }
                                    // if (data === '') {
                                    //     return 'Tunai';
                                    // }
                                    // else {
                                    //     return 'Asuransi - '+ data;
                                    // }
                                }},
                                {"data": "view", 'orderable': false},
                                                //render harga dengan format angka
                                // {"data": "harga_item", "searchable": false, render: $.fn.dataTable.render.number('.', '.', '')},
                  ],
                  order: [ 3 , 'desc'],
              <?php elseif($this->session->userdata('level') == 'pimpinan'): ?>
              ajax: {"url": "<?php echo base_url().'pimpinan/get_estimasiin_json'?>", "type": "POST"},
                    columns: [
                                {"data": "nomor_wo"},
                                {"data": "no_polisi"},
                                {"data": "nama_lengkap"},
                                {"data": "nama_lengkap_user"},
                                {"data": "tgl_estimasi", render: $.fn.dataTable.render.moment( 'D MMM YYYY' )},
                                {"data": "tgl_masuk", render: $.fn.dataTable.render.moment( 'D MMM YYYY' )},
                                {"data": "tgl_janji_penyerahan", render: $.fn.dataTable.render.moment( 'D MMM YYYY' )},
                                {"data": null, 'searchable': false, 'orderable': false, render: function(data, row) {
                                    if (data.jenis_customer == '0') {
                                        return 'Asuransi - '+data.nama_asuransi;
                                    }
                                    else {
                                        return 'Tunai';
                                    }
                                }},
                                {"data": "view", 'orderable': false},
                                // {"data": "status"},
                                                //render harga dengan format angka
                                // {"data": "harga_item", "searchable": false, render: $.fn.dataTable.render.number('.', '.', '')},
                  ],
                  order: [ 4 , 'desc'],
              <?php endif; ?>
          rowCallback: function(row, data, iDisplayIndex) {
              var info = this.fnPagingInfo();
              var page = info.iPage;
              var length = info.iLength;
              $('td:eq(0)', row).html();
          }

      });

      $('#mytable').css({
        'text-align': 'center',
        'vertical-align': 'middle',
      });

});

</script>