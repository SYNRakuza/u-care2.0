<?php
    $controller = $this->uri->segment(1); $modul = $this->uri->segment(2); $params = $this->uri->segment(3); $level = $this->session->userdata('level');
    function kategoryJasa($kategori){
        if($kategori == '0'){
            return '<span class="badge badge-light">Light</span>';
        }elseif($kategori == '1'){
            return '<span class="badge badge-medium">Medium</span>';
        }else{
            return '<span class="badge badge-heavy">Heavy</span>';
        }
    }

    function typeCustomer($type,$asuransi){
        if($type == '0'){
            return 'ASURANSI - '.$asuransi.'';
        }else{
            return 'TUNAI';
        }
    }

    function formatDate($date){
        $dataDate = date('d M Y', strtotime($date));
        return $dataDate;
    }

    function alertDelivery($date){
        if($date < date('Y-m-d')){
            return 'class="color-alert"';
        }else{
            return null;
        }
    }
?>
<style type="text/css">
    /*hilangkan exportable dan menyisakan input search di tabel*/
    .dt-buttons {
        display: none;
    }
    .badge-heavy {
        border-radius: 10px;
        font-weight: 100;
        font-size: 13px;
        background-color: #FF5722;
    }
    .badge-medium {
        border-radius: 10px;
        font-weight: 100;
        font-size: 13px;
        background-color: #FF9800;
    }
    .badge-light {
        border-radius: 10px;
        font-weight: 100;
        font-size: 13px;
        background-color: #FFC107;
    }
    .color-alert {
        background:red;
        color: white;
        -webkit-animation: flash linear 2s infinite;
        animation: flash linear 2s infinite;
    }
    .none-alert {
        background:white !important;
        color: #555 !important;
    }
    @-webkit-keyframes flash {
       from {background-color: #ff0a0a;}
       to {background-color: orange;}
    }
    @keyframes flash {
        from {background-color: #ff0a0a;}
        to {background-color: orange;}
    }
</style>
<section class="content">
    <div class="container-fluid">

        <!-- Exportable Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            <?php if($params == 'done'):?>
                                DELIVERY COMPLETE
                            <?php elseif ($params == 'ready'): ?>
                                READY TO DELIVERY
                            <?php endif; ?>
                        </h2>
                    </div>
                     <style type="text/css">
                        /*hilangkan exportable dan menyisakan input search di tabel*/
                        .dt-buttons {
                            display: none;
                        }
                        .kosong{
                            background:red;
                            color: white;
                            -webkit-animation: flash linear 2s infinite;
                            animation: flash linear 2s infinite;
                        }
                        @-webkit-keyframes flash {
                           from {background-color: #ff0a0a;}
                           to {background-color: orange;}
                        }
                        @keyframes flash {
                            from {background-color: #ff0a0a;}
                            to {background-color: orange;}
                        }
                    </style>
                    <div class="body">
                        <div class="table-responsive">
                            <table id="mytable" class="table table-bordered table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>No. WO</th>
                                        <th>No. Polisi</th>
                                        <th>Nama Customer</th>
                                        <th>No. Telp</th>
                                        <?php if($this->session->userdata('level') == 'teknisi' || $this->session->userdata('level') == 'qc' || $this->session->userdata('level') == 'ptm' || $this->session->userdata('level') == 'pimpinan' ):?>
                                        <th>Nama SA</th>
                                        <?php endif; ?>
                                        <?php if($this->session->userdata('level') == 'service advisor' || $this->session->userdata('level') == 'pimpinan' || $this->session->userdata('level') == 'qc' || $this->session->userdata('level') == 'ptm' ):?>
                                        <th>Nama Teknisi</th>
                                        <?php endif; ?>
                                        <th>Kategori Jasa</th>
                                        <th>Type Customer</th>
                                        <th>Tgl. Estimasi</th>
                                        <th>Tgl. Masuk</th>
                                        <th>Tgl. Janji Penyerahan</th>
                                        <th class="none-alert">Tgl. Selesai</th>
                                        <?php if($params == 'done'):?>
                                        <th>Tgl. Penyerahan</th>
                                        <?php endif; ?>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <!-- \<tbody>
                                    <?php
                                        foreach ($listEstimasi as $datas) {
                                    ?>
                                        <tr>
                                            <td><?= $datas->no_wo ?></td>
                                            <td><?= $datas->no_polisi ?></td>
                                            <td><?= $datas->nama_customer ?></td>
                                            <td><?= $datas->no_telp ?></td>
                                            <?php if($this->session->userdata('level') == 'teknisi' || $this->session->userdata('level') == 'qc' || $this->session->userdata('level') == 'ptm' || $this->session->userdata('level') == 'pimpinan' ):?>
                                            <td><?= $datas->nama_sa ?></td>
                                            <?php endif; ?>
                                            <?php if($this->session->userdata('level') == 'service advisor' || $this->session->userdata('level') == 'qc' || $this->session->userdata('level') == 'ptm' ):?>
                                            <td><?= $datas->nama_teknisi ?></td>
                                            <?php endif; ?>
                                            <td><?= kategoryJasa($datas->kategori_jasa) ?></td>
                                            <td><?= typeCustomer($datas->type_customer, $datas->nama_asuransi) ?></td>
                                            <td><?= formatDate($datas->tgl_estimasi) ?></td>
                                            <td><?= formatDate($datas->tgl_masuk) ?></td>
                                            <td><?= formatDate($datas->tgl_janji_penyerahan) ?></td>
                                            <td <?= ($params == 'ready' ? ''.alertDelivery($datas->tgl_selesai).'' : '' )?>><?= formatDate($datas->tgl_selesai) ?></td>
                                            <?php if($params == 'done'):?>
                                            <td><?= formatDate($datas->tgl_penyerahan) ?></td>
                                            <?php endif; ?>
                                            <td style="text-align: center;white-space: nowrap">
                                                <?php if($params == 'ready'): ?>
                                                <a type="button" class="btn btn-success btn-xs waves-effect doIn" href="javascript:void(0);" data-row="<?= $datas->id?>" data-status="0"><i class="material-icons">call_received</i></a>
                                            <?php endif; ?>
                                                <a style="margin-right: 5px;" type="button" class="btn bg-orange btn-xs waves-effect" href="<?= base_url('service_advisor/printEstimasi/').$datas->id ?>" target="_blank"><i class="material-icons">print</i></a>
                                            </td>
                                        </tr>
                                    <?php
                                        }
                                    ?>
                                </tbody> -->
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Exportable Table -->

        <?php if($params == 'done'):?>
        <div class="modal fade tgl_modal" id="smallModal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="smallModalLabel">Ket. Penyerahan</h4>
                    </div>
                    <div class="modal-body">
                       <table class="table">
                                <tbody style="border: hidden;">
                                    <tr>
                                        <td>
                                             <textarea type="text" rows="5" class="form-control" id="ket_penyerahan" placeholder="" value="" readonly style=" resize: none; "></textarea>
                                        </td>
                                    </tr>
                                </tbody>

                            </table>
                    </div>
                    <div class="modal-footer">

                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                    </div>
                </div>
            </div>
        </div>
        <?php endif; ?>

        <div class="modal fade tgl_modal" id="smallModal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-l" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="smallModalLabel"></h4>
                    </div>
                    <div class="modal-body">
                       <table class="table">
                                <tbody style="border: hidden;">
                                    <tr>
                                        <td class="col-xs-2"><b>Tgl. Penyerahan</b></td>
                                        <td class="col-xs-1"><b>:</b></td>
                                        <td>
                                            <input type="date" class="form-control" id="tgl_penyerahan" placeholder="" value="" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col-xs-2"><b>Ket. Penyerahan</b></td>
                                        <td class="col-xs-1"><b>:</b></td>
                                        <td>
                                             <textarea type="text" rows="5" class="form-control" id="ket_penyerahan" placeholder="" value="" style=" resize: none; "></textarea>
                                        </td>
                                    </tr>
                                </tbody>

                            </table>
                    </div>
                    <div class="modal-footer">

                        <button type="button" class="btn bg-orange waves-effect save" data-target="<?php echo base_url();?>service_advisor/insertTglPenyerahan">SAVE</button>

                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">

$(document).ready(function(){

    //setup datatables
    $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
      {
          return {
              "iStart": oSettings._iDisplayStart,
              "iEnd": oSettings.fnDisplayEnd(),
              "iLength": oSettings._iDisplayLength,
              "iTotal": oSettings.fnRecordsTotal(),
              "iFilteredTotal": oSettings.fnRecordsDisplay(),
              "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
              "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
          };
      };

      var table = $("#mytable").dataTable({
          initComplete: function() {
              var api = this.api();
              $('#mytable_filter input')
                  .off('.DT')
                  .on('input.DT', function() {
                      api.search(this.value).draw();
              });
          },
              oLanguage: {
              sProcessing: "loading..."
          },
              pageLength: 15,
              processing: true,
              serverSide: true,
              bLengthChange: false,
              <?php if($level == 'service advisor' && $params == 'ready' || $level == 'teknisi' && $params == 'ready'):
              ?>
              ajax: {"url": "<?php echo base_url().'service_advisor/get_delivery_readySA_json'?>", "type": "POST"},
                    columns: [
                                {"data": "nomor_wo"},
                                {"data": "no_polisi"},
                                {"data": "nama_lengkap"},
                                {"data": "no_hp"},
                                {"data": "nama_lengkap_user"},
                                {"data": "kategori_jasa", 'searchable': false, render: function(data) {
                                    if(data == '0'){
                                        return '<span class="badge badge-light">Light</span>';
                                    }else if(data == '1'){
                                        return '<span class="badge badge-medium">Medium</span>';
                                    }else{
                                        return '<span class="badge badge-heavy">Heavy</span>';
                                    }
                                }},
                                {"data": null, 'searchable': false, 'orderable': false, render: function(data) {
                                    if (data.jenis_customer == '0') {
                                        return 'Asuransi - '+data.nama_asuransi;
                                    }
                                    else {
                                        return 'Tunai';
                                    }
                                }},
                                {"data": "tgl_estimasi", render: $.fn.dataTable.render.moment( 'D MMM YYYY' )},
                                {"data": "tgl_masuk", render: $.fn.dataTable.render.moment( 'D MMM YYYY' )},
                                {"data": "tgl_janji_penyerahan", render: $.fn.dataTable.render.moment( 'D MMM YYYY' )},
                                {"data": "final_inspection_out", render: $.fn.dataTable.render.moment( 'YYYY-MM-DD HH:mm:ss', 'D MMM YYYY')},
                                {"data": "view", 'orderable': false},
                    ],
                    columnDefs: [
                                { className: "color-alert", "targets": [ 10 ] }
                    ],
                  order: [ 9 , 'desc'],
              <?php elseif($level == 'service advisor' && $params == 'done' || $level == 'teknisi' && $params == 'done'):
              ?>
              ajax: {"url": "<?php echo base_url().'service_advisor/get_delivery_doneSA_json'?>", "type": "POST"},
                    columns: [
                                {"data": "nomor_wo"},
                                {"data": "no_polisi"},
                                {"data": "nama_lengkap"},
                                {"data": "no_hp"},
                                {"data": "nama_lengkap_user"},
                                {"data": "kategori_jasa", 'searchable': false, render: function(data) {
                                    if(data == '0'){
                                        return '<span class="badge badge-light">Light</span>';
                                    }else if(data == '1'){
                                        return '<span class="badge badge-medium">Medium</span>';
                                    }else{
                                        return '<span class="badge badge-heavy">Heavy</span>';
                                    }
                                }},
                                {"data": null, 'searchable': false, 'orderable': false, render: function(data) {
                                    if (data.jenis_customer == '0') {
                                        return 'Asuransi - '+data.nama_asuransi;
                                    }
                                    else {
                                        return 'Tunai';
                                    }
                                }},
                                {"data": "tgl_estimasi", render: $.fn.dataTable.render.moment( 'D MMM YYYY' )},
                                {"data": "tgl_masuk", render: $.fn.dataTable.render.moment( 'D MMM YYYY' )},
                                {"data": "tgl_janji_penyerahan", render: $.fn.dataTable.render.moment( 'D MMM YYYY' )},
                                {"data": "final_inspection_out", render: $.fn.dataTable.render.moment( 'YYYY-MM-DD HH:mm:ss', 'D MMM YYYY')},
                                {"data": "tgl_penyerahan", render: $.fn.dataTable.render.moment( 'YYYY-MM-DD HH:mm:ss', 'D MMM YYYY')},
                                {"data": "view", 'orderable': false},
                    ],
                  order: [ 11 , 'desc'],
              <?php elseif($level == 'pimpinan' && $params == 'ready' || $level == 'ptm' && $params == 'ready' || $level == 'qc' && $params == 'ready'):
              ?>
              ajax: {"url": "<?php echo base_url().'service_advisor/get_delivery_readyPimpinan_json'?>", "type": "POST"},
                    columns: [
                                {"data": "nomor_wo"},
                                {"data": "no_polisi"},
                                {"data": "nama_lengkap"},
                                {"data": "no_hp"},
                                {"data": "nama_sa"},
                                {"data": "nama_teknisi"},
                                {"data": "kategori_jasa", 'searchable': false, render: function(data) {
                                    if(data == '0'){
                                        return '<span class="badge badge-light">Light</span>';
                                    }else if(data == '1'){
                                        return '<span class="badge badge-medium">Medium</span>';
                                    }else{
                                        return '<span class="badge badge-heavy">Heavy</span>';
                                    }
                                }},
                                {"data": null, 'searchable': false, 'orderable': false, render: function(data) {
                                    if (data.jenis_customer == '0') {
                                        return 'Asuransi - '+data.nama_asuransi;
                                    }
                                    else {
                                        return 'Tunai';
                                    }
                                }},
                                {"data": "tgl_estimasi", render: $.fn.dataTable.render.moment( 'D MMM YYYY' )},
                                {"data": "tgl_masuk", render: $.fn.dataTable.render.moment( 'D MMM YYYY' )},
                                {"data": "tgl_janji_penyerahan", render: $.fn.dataTable.render.moment( 'D MMM YYYY' )},
                                {"data": "final_inspection_out", render: $.fn.dataTable.render.moment( 'YYYY-MM-DD HH:mm:ss', 'D MMM YYYY')},
                                {"data": "view", 'orderable': false},
                    ],
                    columnDefs: [
                                { className: "color-alert", "targets": [ 11 ] }
                    ],
                  order: [ 10, 'desc'],
              <?php elseif($level == 'pimpinan' && $params == 'done' || $level == 'ptm' && $params == 'done' || $level == 'qc' && $params == 'done'):
              ?>
              ajax: {"url": "<?php echo base_url().'service_advisor/get_delivery_donePimpinan_json'?>", "type": "POST"},
                    columns: [
                                {"data": "nomor_wo"},
                                {"data": "no_polisi"},
                                {"data": "nama_lengkap"},
                                {"data": "no_hp"},
                                {"data": "nama_sa"},
                                {"data": "nama_teknisi"},
                                {"data": "kategori_jasa", 'searchable': false, render: function(data) {
                                    if(data == '0'){
                                        return '<span class="badge badge-light">Light</span>';
                                    }else if(data == '1'){
                                        return '<span class="badge badge-medium">Medium</span>';
                                    }else{
                                        return '<span class="badge badge-heavy">Heavy</span>';
                                    }
                                }},
                                {"data": null, 'searchable': false, 'orderable': false, render: function(data) {
                                    if (data.jenis_customer == '0') {
                                        return 'Asuransi - '+data.nama_asuransi;
                                    }
                                    else {
                                        return 'Tunai';
                                    }
                                }},
                                {"data": "tgl_estimasi", render: $.fn.dataTable.render.moment( 'D MMM YYYY' )},
                                {"data": "tgl_masuk", render: $.fn.dataTable.render.moment( 'D MMM YYYY' )},
                                {"data": "tgl_janji_penyerahan", render: $.fn.dataTable.render.moment( 'D MMM YYYY' )},
                                {"data": "final_inspection_out", render: $.fn.dataTable.render.moment( 'YYYY-MM-DD HH:mm:ss', 'D MMM YYYY')},
                                {"data": "tgl_penyerahan", render: $.fn.dataTable.render.moment( 'YYYY-MM-DD HH:mm:ss', 'D MMM YYYY')},
                                {"data": "view", 'orderable': false},
                    ],
                  order: [ 12 , 'desc'],                  
              <?php endif; ?>
          rowCallback: function(row, data, iDisplayIndex) {
              var info = this.fnPagingInfo();
              var page = info.iPage;
              var length = info.iLength;
              $('td:eq(0)', row).html();
          }

      });

      $('#mytable').css({
        'text-align': 'center',
        'vertical-align': 'middle',
      });

    // FUNCTION JS FOR CLICKABLE ROW DATATABLE
    var id_estimasi = '';
    $('#mytable').on('click','.form_in' ,function(){
        $('#smallModal').modal('show');
        id_estimasi = $(this).data('row');
        console.log(id_estimasi);
    });
    $('.save').on('click', function(){
        var target = $(this).data('target');
        // var id_estimasi = $(this).data('row');
        // var status_estimasi = $(this).data('status');
        var tgl_penyerahan = $('#tgl_penyerahan').val();
        var ket_penyerahan = $('#ket_penyerahan').val();
        console.log(id_estimasi);
        // console.log(status_estimasi);
        console.log(tgl_penyerahan);
        if(tgl_penyerahan == ''){
            alert("Input tanggal penyerahan !");
            return false;
        }
        swal({
                title: "Are you sure?",
                text: "You will not be able to cancel this Operation !",
                type: "warning",
                showCancelButton: true,
                closeOnConfirm: false,
                showLoaderOnConfirm: true,
            }, function () {
                var data = {
                'id_estimasi'    : id_estimasi,
                'tgl_penyerahan' : tgl_penyerahan,
                'ket_penyerahan' : ket_penyerahan,
                };

                $.ajax({
                    url:target,
                    method:"POST",
                    crossOrigin : false,
                    data : data,
                    success: function(result)
                    {
                        if(result == 'success'){
                            swal("Good job!", "You clicked the button!", "success");
                            setTimeout(function(){
                                   window.location.reload(1);
                                }, 1000);
                        }else{
                            swal("Error Saving", " ", "warning");
                        }
                        console.log('data');

                    }

                });
            });
    });

});

</script>

