<section class="content">
    <div class="container-fluid">
        
        <!-- Exportable Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            DETAIL NOTIFIKASI
                        </h2>
                    </div>
                     <style type="text/css">
                        /*hilangkan exportable dan menyisakan input search di tabel*/
                        .dt-buttons {
                            display: none;
                        }
                    </style>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="">
                                 <?php 
                                foreach($dataShowNotif as $datas){
                                ?>   
                                <h5>JUDUL : "<?= $datas->title ?>"</h5>
                                    <table class="table">
                                        <tbody>  
                                            <tr>
                                                <td width="100" style="border-bottom: 0px;">Pengirim</td>
                                                <td width="5" style="border-bottom: 0px;">:</td>
                                                <td style="border-bottom: 0px;"><b><?= $datas->created_by ?></b></td>
                                            </tr>
                                             <tr>
                                                <td style="border-top: 0px; border-bottom: 0px;">Pesan</td>
                                                <td style="border-top: 0px; border-bottom: 0px;">:</td>
                                                <td style="border-top: 0px; border-bottom: 0px;"><?= $datas->message ?></td>
                                            </tr>
                                        <?php }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Exportable Table -->
    </div>
</section>
<!-- FUNCTION JS FOR CLICKABLE ROW DATATABLE -->
