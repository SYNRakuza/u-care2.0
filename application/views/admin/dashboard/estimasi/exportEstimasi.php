<?php
$modul = $this->uri->segment(2);
$params = $this->uri->segment(3);
$ltProcessJasa = 0;
$nomor_wo = '';
foreach($dataEstimasi as $datas) {
    $nomor_wo = $datas->nomor_wo;
}
?>
<section class="content">
    <div class="container-fluid">
        <?php
        $string = 'Content-Disposition: attachment; filename=data_estimasi_'.$nomor_wo.'.xls';
        header("Content-type: application/vnd-ms-excel");
        header($string);
        ?>
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <style type="text/css">
                        .content {
                            margin: 4px 0 0 0 !important;
                        }

                        .body {
                            padding: 10px 25px 0px 25px !important;
                        }
                        /*hilangkan exportable dan menyisakan input search di tabel*/
                        .dt-buttons {
                            display: none;
                        }

                        .badge {
                            border-radius: 0.75rem !important;
                        }
                        .badge-active {
                            background-color: orange !important;
                        }
                        .badge-unactive {
                            background-color: green !important;
                        }
                        .table td {
                            vertical-align: middle !important;
                        }
                        .logo {
                            text-align: center !important;
                        }
                        .center-logo {
                            /*display: inline-block !important;*/
                            min-width: 875px !important;
                            margin-bottom: 30px !important;
                            /*margin-left: -31px !important;*/
                        }
                        .judul-estimasi {

                        }
                        .print {
                            text-align: center !important;
                        }
                        .tombol-print {
                            display: inline-block !important;
                            /* min-width: 850px !important;*/
                        }

                        .table-estimasi tbody tr td {
                            padding: 5px !important;
                            font-size: 12px !important;
                        }

                        .table-customer tbody tr td {
                            padding: 5px !important;
                            font-size: 14px !important;
                        }
                    </style>
                    <div class="body">
                        <div class="row clearfix" id="printableArea">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 0px;">
                                <div class="row center-logo">
                                    <div class="col-lg-5 col-md-6 col-sm-6 col-xs-6" style="text-align: center;">
                                        <h4><span">Estimasi Biaya Perbaikan Kendaraan <br />Bengkel Body & Paint Kalla Toyota <br />Cabang Urip Sumohardjo</span></h4>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="margin-bottom: 0px;">
                                <div class="table-mid">
                                    <h4>Data Customer</h4>
                                    <div class="table-responsive">
                                        <table class="table table-customer">
                                            <tbody>
                                            <?php
                                            $diskonJasa = 0;
                                            $diskonParts = 0;
                                            foreach($dataEstimasi as $datas){
                                                ?>
                                                <tr style="display: none;">
                                                    <td width="125"></td>
                                                    <td width="1"></td>
                                                    <td id="id_estimasi"><?= $datas->id_estimasi ?></td>
                                                </tr>
                                                <tr style="display: none;">
                                                    <td width="125"></td>
                                                    <td width="1"></td>
                                                    <td id="id_customer"><?= $datas->id_customer ?></td>
                                                </tr>
                                                <tr>
                                                    <td width="125">Nama</td>
                                                    <td width="1">:</td>
                                                    <td><?= $datas->nama_lengkap ?></td>
                                                </tr>
                                                <tr>
                                                    <td>No. Telp</td>
                                                    <td>:</td>
                                                    <td><?= $datas->no_hp ?></td>
                                                </tr>
                                                <tr>
                                                    <td>Alamat</td>
                                                    <td>:</td>
                                                    <td><?= $datas->alamat ?></td>
                                                </tr>
                                                <tr>
                                                    <td>Type Customer</td>
                                                    <td>:</td>
                                                    <?php if($datas->jenis_customer == '0'):?>
                                                        <td><span>ASURANSI - <?= $datas->nama_asuransi ?></span></td>
                                                    <?php else: ?>
                                                        <td><span>Tunai</span></td>
                                                    <?php endif;?>
                                                </tr>
                                            <?php }
                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="margin-bottom: 0px;">
                                <div class="table-mid">
                                    <h4>Data Kendaraan</h4>
                                    <div class="table-responsive table-customer">
                                        <table class="table">
                                            <tbody>
                                            <?php foreach($dataEstimasi as $datas){
                                                $diskonJasa = $datas->diskon_jasa;
                                                $diskonParts = $datas->diskon_parts;
                                                ?>
                                                <tr>
                                                    <td>No. Polisi</td>
                                                    <td>:</td>
                                                    <td><?= $datas->no_polisi ?></td>
                                                </tr>
                                                <tr>
                                                    <td width="125">Jenis Kendaraan</td>
                                                    <td width="1">:</td>
                                                    <td><?= $datas->nama_jenis ?></td>
                                                </tr>
                                                <tr>
                                                    <td>Warna</td>
                                                    <td>:</td>
                                                    <td><?= $datas->code_color." - ".$datas->nama_color ?></td>
                                                </tr>
                                                <tr>
                                                    <td>No. Rangka</td>
                                                    <td>:</td>
                                                    <td><?= $datas->no_rangka ?></td>
                                                </tr>
                                            <?php }
                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <h4>List Estimasi</h4>
                                <div class="table-responsive">
                                    <table class="table table-striped table-estimasi">
                                        <thead>
                                        <tr>
                                            <th width="5" align="center" >No.</th>
                                            <th width="150" style="text-align: center;">Jenis Item</th>
                                            <th width="350">Nama Item</th>
                                            <th width="150" style="text-align: center;">Harga Satuan</th>
                                            <th width="50" style="text-align: center;">Jumlah</th>
                                            <th width="150" style="text-align: center;">Harga</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        function rupiah($angka){
                                            $hasil_rupiah = " " . number_format($angka,0,',','.');
                                            return $hasil_rupiah;
                                        }
                                        $i = 1;
                                        $countJasa = 0;
                                        $hargaTotJasa = 0;
                                        $hargaTotParts = 0;
                                        foreach ($dataItem as $datas) {
                                            ?>
                                            <tr>
                                                <td align="center" ><?= $i++ ?></td>
                                                <?php if($datas->tipe_item == '1'){
                                                    $countJasa++;
                                                    $hargaTotJasa = $hargaTotJasa + (int)$datas->harga_item;
                                                    ?>
                                                    <td style="text-align: center;"><?= substr($datas->nama_item, 0, 3) ?></td>
                                                <?php }else if($datas->tipe_item == '2') {
                                                    $hargaTotParts = $hargaTotParts + ((int)$datas->harga_item*(int)$datas->qty)
                                                    ?>
                                                    <td style="text-align: center;"><?= $datas->nomor_parts ?></td>
                                                <?php } ?>

                                                <?php if($datas->tipe_item == '1'){ ?>
                                                    <td><?= substr($datas->nama_item,4) ?></td>
                                                <?php }else{ ?>
                                                    <td><?= $datas->nama_item ?></td>
                                                <?php } ?>


                                                <td align="right"><?php echo "".rupiah($datas->harga_item) ?></td>
                                                <td style="text-align: center;"><?= $datas->qty ?></td>
                                                <td align="right"><?php echo "".rupiah((int)$datas->qty*(int)$datas->harga_item) ?></td>

                                            </tr>
                                            <?php
                                        }
                                        $diskonParts = $diskonParts*$hargaTotParts/100;
                                        $diskonJasa  = $diskonJasa*$hargaTotJasa/100;
                                        $hargaPPN = (($hargaTotParts+$hargaTotJasa)-($diskonParts+$diskonJasa))*0.1;
                                        $grandTotal = ($hargaTotJasa+$hargaTotParts-$diskonJasa-$diskonParts+$hargaPPN);

                                        ?>
                                        <tr style="text-align: left;">
                                            <td style="text-align: right;" colspan="5">Total Jasa :</td>
                                            <td align="right"><b><?php echo "".rupiah($hargaTotJasa); ?></b></td>
                                        </tr>
                                        <tr style="text-align: left;">
                                            <td style="text-align: right;" colspan="5">Total Parts :</td>
                                            <td align="right"><b><?php echo "".rupiah($hargaTotParts); ?></b></td>
                                        </tr>
                                        <?php if($diskonJasa !== 0):?>
                                            <tr style="text-align: left;">
                                                <td style="text-align: right;" colspan="5">Diskon Jasa :</td>
                                                <td align="right"><b><?php echo "".rupiah($diskonJasa) ?></b></td>
                                            </tr>
                                        <?php endif ;?>
                                        <?php if($diskonParts !== 0):?>
                                            <tr style="text-align: left;">
                                                <td style="text-align: right;" colspan="5">Diskon Part :</td>
                                                <td align="right"><b><?php echo "".rupiah($diskonParts) ?></b></td>
                                            </tr>
                                        <?php endif ;?>
                                        <tr style="text-align: left;">
                                            <td style="text-align: right;" colspan="5">PPN 10% :</td>
                                            <td align="right"><b><?php echo "".rupiah($hargaPPN) ?></b></td>
                                        </tr>
                                        <tr style="text-align: left;">
                                            <td style="text-align: right;" colspan="5">Grand Total :</td>
                                            <td align="right"><b><?php echo "".rupiah($grandTotal) ?></b></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <style type="text/css">
                                    .note-harga, .note-harga i{
                                        font-size: 10px !important;
                                        margin-bottom: 10px !important;
                                        color: red !important;
                                    }

                                </style>
                                <span class="note-harga">*<i>Estimasi TIDAK MENGIKAT, harga sewaktu-waktu dapat berubah <br />jika ada penambahan jasa/sparepart dan kenaikan harga pada saat perbaikan dilakukan.</i> </span>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: -10px;">
                                <?php foreach($ltJasa as $datas){
                                    $katJasa="";
                                    $ltProcessJasa = $datas->ltJasa;
                                    if($datas->ltJasa >= 1 && $datas->ltJasa <= 3){
                                        $katJasa = "Light";
                                    }elseif ($datas->ltJasa >= 4 && $datas->ltJasa <= 6){
                                        $katJasa = "Medium";
                                    }elseif($datas->ltJasa >=7){
                                        $katJasa = "Heavy";
                                    }else{
                                        $katJasa = "-";
                                    }
                                    ?>
                                    <table>
                                        <tr>
                                            <td>Kategori Perbaikan</td>
                                            <td width="10">:</td>
                                            <td><?= $katJasa ?></td>
                                        </tr>
                                        <tr>
                                            <td>Estimasi Waktu Perbaikan</td>
                                            <td>:</td>
                                            <td><?= $datas->ltJasa ?> Hari</td>
                                        </tr>
                                    </table>
                                    <!--                                    <b>Kategori Perbaikan : --><?//= $katJasa ?><!--</b>-->
                                    <!--                                    <br />-->
                                    <!--                                    <b>Estimasi Waktu Perbaikan : --><?//= $datas->ltJasa ?><!-- </b>-->
                                <?php } ?>
                                <div class="row clearfix">
                                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                                        <table class="table" align="center">
                                            <?php foreach($dataTimeProcess as $datas){ ?>
                                                <tbody>
                                                <tr>
                                                    <td>Body Repair</td>
                                                    <td>Preparation</td>
                                                    <td>Masking</td>
                                                    <td>Painting</td>
                                                    <td>Polishing</td>
                                                    <td>Re Assembling</td>
                                                    <td>Washing</td>
                                                    <td>Final Inspection</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <?= $datas->time_body_repair * $ltProcessJasa ?> Jam
                                                    </td>
                                                    <td>
                                                        <?= $datas->time_preparation * $ltProcessJasa ?> Jam
                                                    </td>
                                                    <td>
                                                        <?= $datas->time_masking * $ltProcessJasa ?> Jam
                                                    </td>
                                                    <td>
                                                        <?= $datas->time_painting * $ltProcessJasa ?> Jam
                                                    </td>
                                                    <td>
                                                        <?= $datas->time_polishing * $ltProcessJasa ?> Jam
                                                    </td>
                                                    <td>
                                                        <?= $datas->time_re_assembling * $ltProcessJasa ?> Jam
                                                    </td>
                                                    <td>
                                                        <?= $datas->time_washing * $ltProcessJasa ?> Jam
                                                    </td>
                                                    <td>
                                                        <?= $datas->time_final_inspection * $ltProcessJasa ?> Jam
                                                    </td>
                                                </tr>
                                                </tbody>
                                            <?php } ?>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: -10px;">
                                <?php foreach($dataKedatanganParts as $datas){ ?>
                                    <table>
                                        <tr>
                                            <td>Estimasi Waktu Pemesanan Parts</td>
                                            <td width="10">:</td>
                                            <td><?= $datas->depo + $datas->tam + $datas->pabrik ?> Hari</td>
                                        </tr>
                                    </table>
                                <?php } ?>
                                <div class="row clearfix">
                                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                                        <table class="table" align="center">
                                            <thead>
                                            <tr>
                                                <td>DEPO</td>
                                                <td>TAM</td>
                                                <td>PABRIK</td>
                                            </tr>
                                            </thead>
                                            <?php foreach($dataKedatanganParts as $datas){ ?>
                                                <tbody>
                                                <tr>
                                                    <td>
                                                        <?= $datas->depo ?> Hari
                                                    </td>
                                                    <td>
                                                        <?= $datas->tam ?> Hari
                                                    </td>
                                                    <td>
                                                        <?= $datas->pabrik ?> Hari
                                                    </td>
                                                </tr>
                                                </tbody>
                                            <?php } ?>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="row clearfix">
                                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                                        <div id="service_advisor" align="left">
                                            <table>
                                                <?php
                                                foreach($dataEstimasi as $datas)
                                                {
                                                    $nama_customer = $datas->nama_lengkap;
                                                    ?>
                                                    <tr>
                                                        <td width="150">Date</td>
                                                        <td width="5%">:</td>
                                                        <td><strong><?php echo "".date('d M Y', strtotime($datas->tgl_estimasi)); ?></strong></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Service Advisor</td>
                                                        <td>:</td>
                                                        <td><strong> <?= $datas->nama_lengkap_user; ?></strong></td>
                                                    </tr>
                                                    <tr>
                                                        <td>No. Telp</td>
                                                        <td>:</td>
                                                        <td><strong> <?= $datas->no_tlpUser; ?></strong></td>
                                                    </tr>
                                                    <?php
                                                }
                                                ?>
                                            </table>
                                            <br />
                                            <br />
                                        </div>
                                    </div>
                                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                                        <div class="table-responsive table-customer">
                                            <table>
                                                <tbody>
                                                <tr>
                                                    <td>Menyetujui,</td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td>Nama Customer</td>
                                                    <td>:</td>
                                                    <td><b><?= $nama_customer ?></b></td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
</section>
