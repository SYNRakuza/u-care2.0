<section class="content">
    <div class="container-fluid">
        
        <!-- Exportable Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            LIST ESTIMASI OUT
                        </h2>
                        <!-- <ul class="header-dropdown m-r--5">
                            <li class="dropdown">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">more_vert</i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="<?= site_url() ?>service_advisor/estimasiCreate">Tambah Estimasi</a></li>
                                </ul>
                            </li>
                        </ul> -->
                    </div>
                     <style type="text/css">
                        /*hilangkan exportable dan menyisakan input search di tabel*/
                        .dt-buttons {
                            display: none;
                        }
                    </style>
                    <div class="body">
                        <div class="table-responsive">
                            <table id="mytable" class="table table-bordered table-striped table-hover">

                                <h6 style="color: orange;">
                                    <?php
                                        $info = $this->session->flashdata('info');
                                        if(!empty($info)){
                                            echo $info;
                                        }
                                    ?>
                                </h6>

                                <thead>
                                    <tr>
                                        <th style="text-align: center;vertical-align: middle;">No. Polisi</th>
                                        <th style="text-align: center;vertical-align: middle;">Nama Customer</th>
                                        <?php
                                            if($this->session->userdata('level') == 'pimpinan'){
                                        ?>
                                        <th style="text-align: center;vertical-align: middle;">Nama SA</th>
                                        <?php
                                            }
                                        ?>
                                        <th style="text-align: center;vertical-align: middle;">Tgl. Estimasi</th>
                                        <th style="text-align: center;vertical-align: middle;">Type Customer</th>
                                        <th style="text-align: center;vertical-align: middle;" class="col-md-2 col-lg-2">Action</th>
                                    </tr>
                                </thead>
                               <!--  <tbody>
                                    <?php
                                        foreach ($data as $datas) {
                                    ?>
                                        <tr>
                                            <td style="text-align: center;vertical-align: middle;"><?= $datas->no_polisi ?></td>
                                            <td style="text-align: center;vertical-align: middle;"><?= $datas->nama_lengkap ?></td>
                                            <?php
                                            if($this->session->userdata('level') == 'pimpinan'){
                                            ?>
                                                <td style="text-align: center;vertical-align: middle;"><?= $datas->nama_lengkap_user ?></td>
                                            <?php
                                                }
                                            ?>
                                            <td style="text-align: center;vertical-align: middle;"><?php echo "".date('d M Y', strtotime($datas->tgl_estimasi)); ?></td>
                                        <?php if($datas->jenis_customer == '0'){ ?>
                                            <td style="text-align: center;vertical-align: middle;"> Asuransi - <?= $datas->nama_asuransi ?></td>
                                        <?php }else{ ?>
                                            <td style="text-align: center;vertical-align: middle;">Tunai</td>
                                        <?php } ?>
                                            <td style="text-align: center;vertical-align: middle;" class="col-md-2 col-lg-2">
                                                <?php if($this->session->userdata('level') == 'service advisor' || $this->session->userdata('level') == 'pimpinan'){ ?>
                                                <a type="button" class="btn btn-success btn-xs waves-effect doIn" href="javascript:void(0);" data-row="<?= $datas->id_estimasi?>" data-status="0"><i class="material-icons">call_received</i></a>
                                            <?php } ?>
                                                <a type="button" class="btn bg-orange btn-xs waves-effect" href="<?= base_url('service_advisor/printEstimasi/').$datas->id_estimasi ?>" target="_blank"><i class="material-icons">print</i></a>
                                                <?php
                                                    if($this->session->userdata('level') == 'pimpinan'){
                                                ?>
                                                    <a data-toggle="modal" data-target="#modal_hapus<?= $datas->id_estimasi ?>" class="btn btn-danger btn-xs waves-effect btn-xs"><i class="material-icons">delete_forever</i></a>
                                                <?php
                                                    }
                                                ?>
                                            </td>
                                        </tr>
                                    <?php
                                        }
                                    ?>
                                </tbody> -->
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Exportable Table -->
        <div class="modal fade tgl_modal" id="smallModal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="smallModalLabel">Form Estimasi In</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-line">
                            <b>Tgl. Janji Penyerahan</b>
                            <input type="date" class="form-control" id="tgl_janji_penyerahan" placeholder="" value="" />
                        </div>
                        <br />
                        <div class="form-line">
                            <b>Nomor WO</b>
                            <input type="text" class="form-control" id="nomor_wo" placeholder="" value="" />
                        </div>
                    </div>
                    <div class="modal-footer">
                        
                        <button type="button" class="btn bg-orange waves-effect save" data-target="<?php echo base_url();?>service_advisor/changeStatus">SAVE</button>
                        
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>




<!-- Modal Hapus -->

<?php

foreach($data as $get){

$id = $get->id_estimasi;

?>

<div id="modal_hapus<?= $id ?>" class="modal fade" role="dialog">
   <div class="modal-dialog">
    <!-- konten modal-->
    <div class="modal-content">
        <!-- heading modal -->
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Hapus Estimasi</h4>
        </div>
        <!-- body modal -->
        <div class="modal-body">
         Apakah Anda Yakin Ingin Menghapus Data Ini ? 
         <br/><br/>  
        <a href="<?= base_url('pimpinan/estimasiHapus/'.$id) ?>" class="btn bg-orange waves-effect">
            <i class="material-icons">delete</i>
                <span>Hapus</span>
        </a>
                        
        </div>
    </div>
   </div>
</div>


<?php } ?>

<!-- End Modal Hapus -->

<script type="text/javascript">

$(document).ready(function(){

    //setup datatables
    $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
      {
          return {
              "iStart": oSettings._iDisplayStart,
              "iEnd": oSettings.fnDisplayEnd(),
              "iLength": oSettings._iDisplayLength,
              "iTotal": oSettings.fnRecordsTotal(),
              "iFilteredTotal": oSettings.fnRecordsDisplay(),
              "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
              "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
          };
      };

      var table = $("#mytable").dataTable({
          // initComplete: function() {
          //     var api = this.api();
          //     $('#mytable_filter input')
          //         .off('.DT')
          //         .on('input.DT', function() {
          //             api.search(this.value).draw();
          //     });
          // },
              oLanguage: {
              sProcessing: "loading..."
          },
              pageLength: 15,
              processing: true,
              serverSide: true,
              bLengthChange: false,
              <?php if($this->session->userdata('level') == 'pimpinan'): ?>
              ajax: {"url": "<?php echo base_url().'pimpinan/get_estimasi_out_json'?>", "type": "POST"},
                    columns: [
                                {"data": "no_polisi"},
                                {"data": "nama_lengkap"},
                                {"data": "nama_lengkap_user"},
                                {"data": "tgl_estimasi", render: $.fn.dataTable.render.moment( 'D MMM YYYY' )},
                                {"data": null, 'searchable': false, 'orderable': false, render: function(data, row) {
                                    if (data.jenis_customer == '0') {
                                        return 'Asuransi - '+data.nama_asuransi;
                                    }
                                    else {
                                        return 'Tunai';
                                    }
                                }},
                                {"data": "view", 'orderable': false},
                                // {"data": "status"},
                                                //render harga dengan format angka
                                // {"data": "harga_item", "searchable": false, render: $.fn.dataTable.render.number('.', '.', '')},
                  ],
                order: [ 3 , 'desc'],
              <?php elseif($this->session->userdata('level') == 'service advisor'): ?>
                ajax: {"url": "<?php echo base_url().'service_advisor/get_estimasi_out_json'?>", "type": "POST"},
                    columns: [
                                {"data": "no_polisi"},
                                {"data": "nama_lengkap"},
                                {"data": "tgl_estimasi", render: $.fn.dataTable.render.moment( 'D MMM YYYY' )},
                                {"data": null, 'searchable': false, 'orderable': false, render: function(data, row) {
                                    if (data.jenis_customer == '0') {
                                        return 'Asuransi - '+data.nama_asuransi;
                                    }
                                    else {
                                        return 'Tunai';
                                    }
                                }},
                                {"data": "view", 'orderable': false},
                                // {"data": "status"},
                                                //render harga dengan format angka
                                // {"data": "harga_item", "searchable": false, render: $.fn.dataTable.render.number('.', '.', '')},
                  ],
                order: [ 2 , 'desc'],
              <?php endif; ?>
          rowCallback: function(row, data, iDisplayIndex) {
              var info = this.fnPagingInfo();
              var page = info.iPage;
              var length = info.iLength;
              $('td:eq(0)', row).html();
          }

      });

      $('#mytable').css({
        'text-align': 'center',
        'vertical-align': 'middle',
      });

// FUNCTION JS MODAL FORM IN
    var id_estimasi = '';
    $('#mytable').on('click','.form_in' ,function(){
        $('#smallModal').modal('show');
        id_estimasi = $(this).data('row');
        console.log(id_estimasi);
    });

    $('.save').on('click', function(){
        var target = $(this).data('target');
        // var id_estimasi = $(this).data('row');
        // var status_estimasi = $(this).data('status');
        var tgl_janji_penyerahan = $('#tgl_janji_penyerahan').val();
        var nomor_wo = $('#nomor_wo').val();
        
        if(tgl_janji_penyerahan == ''){
            alert("Input tanggal janji penyerahan !");
            return false;
        }
        if(nomor_wo == ''){
            alert("Input Nomor WO !");
            return false;
        }
        swal({
                title: "Are you sure?",
                text: "You will not be able to cancel this Operation !",
                type: "warning",
                showCancelButton: true,
                closeOnConfirm: false,
                showLoaderOnConfirm: true,
            }, function () {
                var data = {
                'id_estimasi'          : id_estimasi,
                'tgl_janji_penyerahan' : tgl_janji_penyerahan,
                'nomor_wo' : nomor_wo
                };

                $.ajax({
                    url:target,
                    method:"POST",
                    crossOrigin : false,
                    data : data,
                    dataType:'json',
                    success: function(result)
                    {
                        if(result.status == 'success'){
                            swal("Good job!", "You clicked the button!", "success");
                            setTimeout(function(){
                                   window.location.reload(1);
                                }, 1000);
                        }else{
                            swal("Error Saving", " ", "warning");
                        }
                        console.log('data');
                        
                    }
                     
                });
            });
    });

});

</script>