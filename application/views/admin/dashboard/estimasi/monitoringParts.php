<section class="content">
    <div class="container-fluid">

        <!-- Exportable Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            MONITORING PARTS
                        </h2>
                        <ul class="header-dropdown m-r--5">
                            <li class="dropdown">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">more_vert</i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="<?= site_url() ?>service_advisor/estimasiCreate">Tambah Estimasi</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                     <style type="text/css">
                        /*hilangkan exportable dan menyisakan input search di tabel*/
                        .dt-buttons {
                            display: none;
                        }
                    </style>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                <thead>
                                    <tr>
                                        <th>No. WO</th>
                                        <th>No. Polisi</th>
                                        <th>Nama Customer</th>
                                        <th>Nama Teknisi</th>
                                        <th>Tgl. Masuk</th>
                                        <th>Tgl. Janji Penyerahan</th>
                                        <th>Status Parts</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        foreach ($listEstimasi as $datas) {
                                    ?>
                                        <tr>
                                            <td><?= $datas->no_wo ?></td>
                                            <td><?= $datas->no_polisi ?></td>
                                            <td><?= $datas->nama_lengkap ?></td>
                                            <td><?= $datas->nama_teknisi ?></td>
                                            <td><?php echo "".date('d M Y', strtotime($datas->tgl_estimasi)); ?></td>
                                            <td><?php echo "".date('d M Y', strtotime($datas->tgl_janji_penyerahan)); ?></td>
                                            <td style="vertical-align: middle;">
                                                <div class="progress" style="margin-bottom: 0px; height: 12px;">
                                                    <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: <?= $datas->totalReadyParts/$datas->totalParts*100 ?>%; line-height: 12px; color: black;" id="status_<?= $datas->id ?>">
                                                        <?php echo round($datas->totalReadyParts/$datas->totalParts*100)?>%<span class="sr-only"></span>
                                                    </div>
                                                </div>
                                            </td>
                                            <td style="text-align: center;">
                                                <a style="margin-right: 5px;" type="button" class="btn bg-orange btn-xs waves-effect" href="<?= base_url('service_advisor/detailParts/').$datas->id ?>"><i class="material-icons">adjust</i></a>
                                            </td>
                                        </tr>
                                    <?php
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Exportable Table -->
    </div>
</section>

