<section class="content">
    <div class="container-fluid">

        <!-- Exportable Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            READY TO DELIVERY
                        </h2>
                        <ul class="header-dropdown m-r--5">
                            <li class="dropdown">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">more_vert</i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="<?= site_url() ?>service_advisor/estimasiCreate">Tambah Estimasi</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                     <style type="text/css">
                        /*hilangkan exportable dan menyisakan input search di tabel*/
                        .dt-buttons {
                            display: none;
                        }
                        .kosong{
                            background:red;
                            color: white;
                            -webkit-animation: flash linear 2s infinite;
                            animation: flash linear 2s infinite;
                        }
                        @-webkit-keyframes flash {
                           from {background-color: #ff0a0a;}
                           to {background-color: orange;}
                        }
                        @keyframes flash {
                            from {background-color: #ff0a0a;}
                            to {background-color: orange;}
                        }
                    </style>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                <thead>
                                    <tr>
                                        <th>No. WO</th>
                                        <th>No. Polisi</th>
                                        <th>Nama Customer</th>
                                        <th>Nama SA</th>
                                        <th>No. Telp</th>
                                        <th>Nama Teknisi</th>
                                        <th>Kategori</th>
                                        <th>Type Customer</th>
                                        <th>Tgl. Estimasi</th>
                                        <th>Tgl. Masuk</th>
                                        <th>Tgl. Janji Penyerahan</th>
                                        <th>Tgl. Selesai</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        foreach ($listEstimasi as $datas) {
                                            $now            = date('Y-m-d');
                                            $warnaKosong    = "";

                                            if ($datas->final_inspection_out < $now) {
                                                    $warnaKosong = "kosong";
                                                };
                                    ?>
                                        <tr>
                                            <td><?= $datas->nomor_wo ?></td>
                                            <td><?= $datas->no_polisi ?></td>
                                            <td><?= $datas->nama_lengkap ?></td>
                                            <td><?php echo "".date('d M Y', strtotime($datas->tgl_estimasi)); ?></td>
                                        <?php if($datas->tgl_masuk !== NULL):?>    
                                            <td><?php echo "".date('d-M-Y', strtotime($datas->tgl_masuk)); ?></td>
                                        <?php else: ?>
                                            <td></td>
                                        <?php endif; ?>
                                            <td class="<?php echo $warnaKosong;?>"><?php echo "".date('d M Y', strtotime($datas->tgl_janji_penyerahan)); ?></td>
                                        <?php if($datas->jenis_customer == '0'){ ?>
                                            <td>Asuransi - <?= $datas->nama_asuransi ?></td>
                                        <?php }else{ ?>
                                            <td>Tunai</td>
                                        <?php } ?>
                                            <td style="text-align: center;">
                                                <a type="button" class="btn btn-success btn-xs waves-effect doIn" href="javascript:void(0);" data-row="<?= $datas->id_estimasi?>" data-status="0"><i class="material-icons">call_received</i></a>
                                                <a style="margin-right: 5px;" type="button" class="btn bg-orange btn-xs waves-effect" href="<?= base_url('service_advisor/printEstimasi/').$datas->id_estimasi ?>" target="_blank"><i class="material-icons">print</i></a>
                                            </td>
                                        </tr>
                                    <?php
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Exportable Table -->
        <div class="modal fade tgl_modal" id="smallModal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="smallModalLabel">Tgl. Penyerahan</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-line">
                            <input type="date" class="form-control" id="tgl_penyerahan" placeholder="" value="" />
                        </div>
                    </div>
                    <div class="modal-footer">
                        
                        <button type="button" class="btn bg-orange waves-effect save" data-target="<?php echo base_url();?>service_advisor/insertTglPenyerahan">SAVE</button>
                        
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- FUNCTION JS FOR CLICKABLE ROW DATATABLE -->
<script type="text/javascript">
    var id_estimasi = '';
    $('.doIn').on('click', function(){
        $('#smallModal').modal('show');
        id_estimasi = $(this).data('row');
        console.log(id_estimasi);
    });
    $('.save').on('click', function(){
        var target = $(this).data('target');
        // var id_estimasi = $(this).data('row');
        // var status_estimasi = $(this).data('status');
        var tgl_penyerahan = $('#tgl_penyerahan').val();
        console.log(id_estimasi);
        // console.log(status_estimasi);
        console.log(tgl_penyerahan);
        if(tgl_penyerahan == ''){
            alert("Input tanggal penyerahan !");
            return false;
        }
        swal({
                title: "Are you sure?",
                text: "You will not be able to cancel this Operation !",
                type: "warning",
                showCancelButton: true,
                closeOnConfirm: false,
                showLoaderOnConfirm: true,
            }, function () {
                var data = {
                'id_estimasi'    : id_estimasi,
                'tgl_penyerahan' : tgl_penyerahan,
                };

                $.ajax({
                    url:target,
                    method:"POST",
                    crossOrigin : false,
                    data : data,
                    success: function(result)
                    {
                        if(result == 'success'){
                            swal("Good job!", "You clicked the button!", "success");
                            setTimeout(function(){
                                   window.location.reload(1);
                                }, 1000);
                        }else{
                            swal("Error Saving", " ", "warning");
                        }
                        console.log('data');
                        
                    }
                     
                });
            });
    });
//     $('.dataTable').on('click', 'tbody td', function() {
//         var idCustomer= $(this).data("row");
//         window.location = "<?php echo base_url();?>service_advisor/customer/"+idCustomer;
  
// // })
// $(document).ready(function(){
//     $('button').click(function(e){
//     e.preventDefault();
    
//     console.log('Hi');
//     var classname = $(this).prop('class');
//     console.log(classname);
//     if(classname == 'btn btn-info waves-effect positive')
//         { 
//             //$('button.positive').attr('disabled', false);
            
//             $('button.positive').attr('class', 'btn btn-danger waves-effect negative');
//             $(this).attr('class', 'btn btn-success waves-effect positive2');
//             $('button.negative').attr('disabled', true);
//         }
//         else
//         {
//             $('button.negative').attr('class', 'btn btn-info waves-effect positive');
//             $(this).attr('class', 'btn btn-info waves-effect positive');
//             $('button.positive').attr('disabled', false);
//         }
//     // $('#btn2').prop('disabled', true);
//     // $('#btn3').prop('disabled', true);
//     // $('#btn4').prop('disabled', true);
// });

// });



// // $('.abcd').on('click', function(){
// //     console.log('Hi2');
// //     // $(this).prop('class','btn bg-orange btn-lg waves-effect btn-block');
// //     // $('#btn2').prop('disabled', false);
// //     // $('#btn3').prop('disabled', false);
// //     // $('#btn4').prop('disabled', false);
// // });
</script>
