<section class="content">
    <div class="container-fluid">

        <!-- Exportable Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                  <div class="header">
                      <h2>
                          Dashboard
                      </h2>
                  </div>
                  <hr>

                  
                </div>
            </div>
        </div>
        <!-- #END# Exportable Table -->
    </div>
</section>


<div id="addParts" class="modal fade" role="dialog">
   <div class="modal-dialog">
    <!-- konten modal-->
    <div class="modal-content">
        <!-- heading modal -->
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Tambah Parts</h4>
        </div>
        <!-- body modal -->
        <div class="modal-body">
            <form method="POST" action="<?= base_url()?>pimpinan/tambahParts">
                                <table class="table">
                                <tbody style="border: hidden;">

                                    <tr>
                                        <td class="col-xs-2"><b>Nomor Parts</b></td>
                                        <td class="col-xs-1"><b>:</b></td>
                                        <td>
                                            <input type="text" class="form-control" name="nomor_parts" placeholder="Masukkan Nomor Parts" maxlength="40" required />
                                        </td>
                                    </tr>

                                     <tr>
                                        <td class="col-xs-2"><b>Nama Parts</b></td>
                                        <td class="col-xs-1"><b>:</b></td>
                                        <td>
                                            <input type="text" class="form-control" name="nama_item" placeholder="Masukkan Nama Parts" onkeyup="this.value = this.value.toUpperCase()"  required />
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="col-xs-2"><b>Harga Parts</b></td>
                                        <td class="col-xs-1"><b>:</b></td>
                                        <td>
                                            <input type="number" class="form-control" name="harga_item" placeholder="Masukkan Harga Parts" maxlength="10" required />
                                        </td>
                                    </tr>

                                </tbody>

                            </table>
                            <input type="submit" name="" class="btn btn-warning waves-effect" value="Tambah Parts">
                        </form>
        </div>
        <!-- footer modal
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Tutup Modal</button>
        </div>
        -->
    </div>
   </div>
</div>

<div id="editParts" class="modal fade" role="dialog">
   <div class="modal-dialog">
    <!-- konten modal-->
    <div class="modal-content">
        <!-- heading modal -->
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Tambah Parts</h4>
        </div>
        <!-- body modal -->
        <div class="modal-body">
            <form method="POST" action="<?= base_url()?>pimpinan/update_parts">
                                <table class="table">
                                <tbody style="border: hidden;">
                                   <tr style="display: none;visibility: hidden;">
                                        <td class="col-xs-2"><b>Id Item</b></td>
                                        <td class="col-xs-1"><b>:</b></td>
                                        <td>
                                            <input type="hidden" class="form-control" name="id_item" placeholder="id item" required readonly />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col-xs-2"><b>Nomor Parts</b></td>
                                        <td class="col-xs-1"><b>:</b></td>
                                        <td>
                                            <input type="text" class="form-control" name="nomor_parts" placeholder="Masukkan Nomor Parts" maxlength="40" required />
                                        </td>
                                    </tr>

                                     <tr>
                                        <td class="col-xs-2"><b>Nama Parts</b></td>
                                        <td class="col-xs-1"><b>:</b></td>
                                        <td>
                                            <input type="text" class="form-control" name="nama_item" placeholder="Masukkan Nama Parts" onkeyup="this.value = this.value.toUpperCase()"  required />
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="col-xs-2"><b>Harga Parts</b></td>
                                        <td class="col-xs-1"><b>:</b></td>
                                        <td>
                                            <input type="number" class="form-control" name="harga_item" placeholder="Masukkan Harga Parts" maxlength="10" required />
                                        </td>
                                    </tr>



                                </tbody>

                            </table>
                            <input type="submit" name="" class="btn btn-warning waves-effect" value="Update Parts">
                        </form>
        </div>
        <!-- footer modal
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Tutup Modal</button>
        </div>
        -->
    </div>
   </div>
</div>


   <form id="add-row-form" action="<?php echo base_url().'pimpinan/delete_parts'?>" method="post">
         <div class="modal small fade" id="hapusParts" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                 <h3 id="myModalLabel">Konfirmasi Hapus</h3>

            </div>
            <div class="modal-body">
                <p class="error-text"><i class="fa fa-warning modal-icon"></i><h4>Apakah Anda Yakin Ingin Menghapus ?</h4>
                Setelah Anda Menghapus, Maka Data Akan Hilang.</p>
                <input type="hidden" class="form-control" name="id_item" placeholder="id item" required readonly />
            </div>
            <div class="modal-footer">
              <input type="submit" name="" class="btn btn-warning waves-effect" value="Hapus Parts">
            </div>
        </div>
    </div>
</div>
     </form>

<script type="text/javascript">

$(document).ready(function(){

    //setup datatables
    $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
      {
          return {
              "iStart": oSettings._iDisplayStart,
              "iEnd": oSettings.fnDisplayEnd(),
              "iLength": oSettings._iDisplayLength,
              "iTotal": oSettings.fnRecordsTotal(),
              "iFilteredTotal": oSettings.fnRecordsDisplay(),
              "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
              "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
          };
      };

      var table = $("#mytable").dataTable({
          initComplete: function() {
              var api = this.api();
              $('#mytable_filter input')
                  .off('.DT')
                  .on('input.DT', function() {
                      api.search(this.value).draw();
              });
          },
              oLanguage: {
              sProcessing: "loading..."
          },
              processing: true,
              serverSide: true,
              bLengthChange: false,
              ajax: {"url": "<?php echo base_url().'pimpinan/get_jasa_json'?>", "type": "POST"},
                    columns: [
                                {"data": "nomor_parts"},
                                {"data": "nama_item"},
                                                //render harga dengan format angka
                                {"data": "harga_item", "searchable": false, render: $.fn.dataTable.render.number('.', '.', '')},
                                {"data": "view"},
                  ],
                order: [],
          rowCallback: function(row, data, iDisplayIndex) {
              var info = this.fnPagingInfo();
              var page = info.iPage;
              var length = info.iLength;
              $('td:eq(0)', row).html();
          }

      });


       $('#mytable').on('click','.edit_record',function(){
                        var kode=$(this).data('id');
                        var nama=$(this).data('nama');
                        var harga=$(this).data('harga');
                        var nomor=$(this).data('nomor');
            $('#editParts').modal('show');
                        $('[name="id_item"]').val(kode);
                        $('[name="nama_item"]').val(nama);
                        $('[name="harga_item"]').val(harga);
                        $('[name="nomor_parts"]').val(nomor);
      });

       $('#mytable').on('click','.hapus_record',function(){
            var kode=$(this).data('id');
            $('#hapusParts').modal('show');
            $('[name="id_item"]').val(kode);
      });

      $('#mytable').css({
        'text-align': 'center',
        'vertical-align': 'center',
      });

});

</script>
