<section class="content">
    <div class="container-fluid">

        <!-- Exportable Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            Daftar User
                        </h2>
                        <ul class="header-dropdown m-r--5">
                            <li class="dropdown">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">more_vert</i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li>
                                        <a href="<?=base_url('pimpinan/userCreate') ?>">Tambah User</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                     <style type="text/css">
                        /*hilangkan exportable dan menyisakan input search di tabel*/
                        .dt-buttons {
                            display: none;
                        }
                         table tr td {
                             vertical-align: middle !important;
                         }
                    </style>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable js-exportable" >
                                <thead>
                                    <tr>
                                        <th style="text-align: center;vertical-align: middle;">Nama Lengkap</th>
                                        <th style="text-align: center;vertical-align: middle;">Level</th>
                                        <th style="text-align: center;vertical-align: middle;">Username</th>
                                        <th style="text-align: center;vertical-align: middle;">No. Telepon</th>
                                        <th style="text-align: center;vertical-align: middle;">Last Login</th>
                                        <th style="text-align: center;vertical-align: middle;">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php foreach($data as $datas){ ?>
                                    <tr>
                                        <td style="text-align: center;vertical-align: middle;"><?= $datas->nama_lengkap_user ?></td>
                                        <?php if($datas->level == 'qc'): ?>
                                        <td style="text-align: center;vertical-align: middle;"><?= $datas->level ?> (<?= $datas->sub_level ?>)</td>
                                        <?php else: ?>
                                        <td style="text-align: center;vertical-align: middle;"><?= $datas->level ?></td>
                                        <?php endif ?>
                                        <td style="text-align: center;vertical-align: middle;"><?= $datas->username ?></td>
                                        <td style="text-align: center;vertical-align: middle;">
                                            <?php
                                                if($datas->no_tlpUser == NULL){
                                                    echo "-";
                                                }else{
                                                    echo $datas->no_tlpUser;
                                                }
                                            ?>
                                        </td>
                                        <td style="text-align: center;vertical-align: middle;"><?= date('d-M-Y', strtotime($datas->last_login)).' Pada '.date('H:i', strtotime($datas->last_login)) ?>WITA</td>
                                        <td style="text-align: center;vertical-align: middle;">
                                            <a style="margin-right: 5px;" type="button" data-row="<?= $datas->id_user ?>" href="<?php echo base_url().'pimpinan/userUpdate/'.$datas->id_user; ?>" class="btn bg-orange waves-effect btn-xs modalEdit"><i class="material-icons">mode_edit</i></a>
                                            <button type="button" data-id="<?= $datas->id_user ?>" data-action="<?=base_url('pimpinan/deleteUser') ?>" class="btn btn-danger waves-effect btn-xs js-delete-user"><i class="material-icons">delete_forever</i></button>
                                        </td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Exportable Table -->
    </div>
</section>
