<?php

header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=daftar_summary.xls");

header("Pragma: no-cache");

header("Expires: 0");
?>

<table border="1" width="100%">
    <thead>
        <th style="text-align: center;vertical-align: middle;">No. WO</th>
        <th style="text-align: center;vertical-align: middle;">No. Polisi</th>
        <th style="text-align: center;vertical-align: middle;">Nama Teknisi</th>
        <th style="text-align: center;vertical-align: middle;">Body Repair Lead</th>
        <th style="text-align: center;vertical-align: middle;">Preparation Lead</th>
        <th style="text-align: center;vertical-align: middle;">Masking Lead</th>
        <th style="text-align: center;vertical-align: middle;">Painting Lead</th>
        <th style="text-align: center;vertical-align: middle;">Polishing Lead</th>
        <th style="text-align: center;vertical-align: middle;">Re-Assembling Lead</th>
        <th style="text-align: center;vertical-align: middle;">Washing Lead</th>
        <th style="text-align: center;vertical-align: middle;">Final Inspection Lead</th>
        <th style="text-align: center;vertical-align: middle;">Total</th>
    </thead>
    <tbody>
        <?php
            foreach($data as $datas){
            if($datas->nomor_wo == NULL){
                $wo = "-";
            }else{
                $wo = $datas->nomor_wo;
            }
            if($datas->body_repair_lead == NULL){
                $body_repair_lead = "-";
            }else{
                $body_repair_lead = $datas->body_repair_lead;
            }
            if($datas->preparation_lead == NULL){
                $preparation_lead = "-";
            }else{
                $preparation_lead = $datas->preparation_lead; 
            }
            if($datas->masking_lead == NULL){
                $masking_lead = "-";
            }else{
                $masking_lead = $datas->masking_lead; 
            }
            if($datas->painting_lead == NULL){
                $painting_lead = "-";
            }else{
                $painting_lead = $datas->painting_lead; 
            }
            if($datas->polishing_lead == NULL){
                $polishing_lead = "-";
            }else{
                $polishing_lead = $datas->polishing_lead; 
            }
            if($datas->re_assembling_lead == NULL){
                $re_assembling_lead = "-";
            }else{
                $re_assembling_lead = $datas->re_assembling_lead; 
            }
            if($datas->washing_lead == NULL){
                $washing_lead = "-";
            }else{
                $washing_lead = $datas->washing_lead; 
            }
            if($datas->final_inspection_lead == NULL){
                $final_inspection_lead = "-";
            }else{
                $final_inspection_lead = $datas->final_inspection_lead;
            }
            if($datas->total_lead == NULL){
                $total_lead = "-";
            }else{
                $total_lead = $datas->total_lead;
            }
                
        ?>
        <tr>
            <td style="text-align: center;vertical-align: middle;"><?= $wo ?></td>
            <td style="text-align: center;vertical-align: middle;"><?= $datas->no_polisi ?></td>
            <td style="text-align: center;vertical-align: middle;"><?= $datas->nama_lengkap_user ?></td>
            <td style="text-align: center;vertical-align: middle;"><?= $body_repair_lead ?></td>
            <td style="text-align: center;vertical-align: middle;"><?= $preparation_lead ?></td>
            <td style="text-align: center;vertical-align: middle;"><?= $masking_lead ?></td>
            <td style="text-align: center;vertical-align: middle;"><?= $painting_lead ?></td>
            <td style="text-align: center;vertical-align: middle;"><?= $polishing_lead ?></td>
            <td style="text-align: center;vertical-align: middle;"><?= $re_assembling_lead ?></td>
            <td style="text-align: center;vertical-align: middle;"><?= $washing_lead ?></td>
            <td style="text-align: center;vertical-align: middle;"><?= $final_inspection_lead ?></td>
            <td style="text-align: center;vertical-align: middle;"><?= $total_lead ?></td>
        </tr>

        <?php  } ?>
    </tbody>
</table>

</div>