<?php
  /*  $id_user = $this->session->userdata('id_user');
    $user_level = $this->session->userdata('level');
    $yearNow = date('Y');
    // $url = echo base_url('service_advisor/getDataChart');*/
    $yearNow = date('Y');
    $modul = $this->uri->segment(2);
?>
<section class="content">
    <div class="container-fluid">
        <!-- <div class="block-header">
            <h2>DASHBOARD</h2>
        </div> -->
        <!-- to get id_user for sending to ajax function -->
        <?php if($modul == '' || $modul == 'service_advisor') :?>
        <input type="hidden" id="modul" value="<?= $modul ?>">
        <input type="hidden" id="get_url" value="<?= base_url('pimpinan/getDataChartSa')?>"/>
        <input type="hidden" id="get_url_2" value="<?= base_url('pimpinan/listEstimasiDashboard')?>"/>

        <?php elseif($modul =='teknisi'): ?> 
        <input type="hidden" id="modul" value="<?= $modul ?>">    
<!--         <input type="hidden" id="get_url_teknisi" value="<?= base_url('pimpinan/getDataChartTeknisi')?>"/>
 -->       
        <?php elseif($modul == 'partsman'): ?>
        <input type="hidden" id="modul" value="<?= $modul ?>">    
        <input type="hidden" id="get_url_chart_parts" value="<?= base_url('pimpinan/getDataChart')?>"/>
        <input type="hidden" id="get_url_parts" value="<?= base_url('pimpinan/listPartsDashboard')?>"/>

        <?php elseif($modul =='foreman'): ?> 
        <input type="hidden" id="modul" value="<?= $modul ?>">    
        <input type="hidden" id="get_url_foreman" value="<?= base_url('pimpinan/getDataChartForeman')?>"/>

        <?php endif; ?>

        <div class="row clearfix">


<?php if($modul == '' || $modul == 'service_advisor'): ?>
<!-- Charts SA-->
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="card">
                    <div class="header">
                        <div class="row clearfix">
                            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                <h2>Estimasi</h2>
                            </div>
                            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                <select id="selectYear" class="form-control">
                                    <option value="2019" style="text-align: center;"<?php if($yearNow == '2019'){ echo "selected";} ?>>2019</option>
                                    <option value="2020" style="text-align: center;"<?php if($yearNow == '2020'){ echo "selected";} ?>>2020</option>
                                    <option value="2021" style="text-align: center;"<?php if($yearNow == '2021'){ echo "selected";} ?>>2021</option>
                                    <option value="2022" style="text-align: center;"<?php if($yearNow == '2022'){ echo "selected";} ?>>2022</option>
                                    <option value="2023" style="text-align: center;"<?php if($yearNow == '2023'){ echo "selected";} ?>>2023</option>
                                </select>
                            </div>
                            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                <select id="selectInOut" class="form-control">
                                    <option value="all" style="text-align: center;">ALL</option>
                                    <option value="in" style="text-align: center;">IN</option>
                                    <option value="out" style="text-align: center;">OUT</option>
                                </select>
                            </div>
                       
                        </div>
                    </div>
                    <div class="body">
                            <canvas id="myChart"></canvas>
                    </div>
                </div>
            </div>
<!--End Charts SA-->

<!-- Data Estimasi In/Out-->
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="card">
                    <div class="header">
                        
                        <div class="row clearfix">
                            <form method="POST" action="<?= site_url('pimpinan/estimasiToExcel') ?>">
                            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2" >
                                <h2 style="margin-top: 15px;">Data Estimasi</h2>
                            </div>
                            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                <small>From:</small>
                                <input type="date" id="dateFrom" class="form-control" name="dateFrom" required/>
                            </div>
                            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                <small>To:</small>
                                <input type="date" id="dateTo" class="form-control" name="dateTo" required />
                            </div>
                            
                            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                <small>Estimasi:</small>
                                <select id="dateInOut" class="form-control" name="dateInOut" required>
                                    <option value="" style="text-align: center;">--Pilih--</option>
                                    <option value="all" style="text-align: center;">ALL</option>
                                    <option value="in" style="text-align: center;">IN</option>
                                    <option value="out" style="text-align: center;">OUT</option>
                                </select>
                            </div>

                            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3" >
                                <button type="submit" class="btn bg-deep-orange btn-circle waves-effect">
                                    <i class="material-icons">publish</i>
                                </button>
                            </div>
                            </form>
                        </div>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-info">
                                <thead>
                                    <tr>
                                        <th style="text-align: center;vertical-align: middle;">No.</th>
                                        <th style="text-align: center;vertical-align: middle;">No. WO</th>
                                        <th style="text-align: center;vertical-align: middle;">No. Polisi</th>
                                        <th style="text-align: center;vertical-align: middle;">Nama Customer</th>
                                        <th style="text-align: center;vertical-align: middle;">SA</th>
                                        <th style="text-align: center;vertical-align: middle;">Tgl. Estimasi</th>
                                        <th style="text-align: center;vertical-align: middle;">Tgl. Masuk</th>
                                        <th style="text-align: center;vertical-align: middle;">Tgl. Janji Penyerahan</th>
                                        <th style="text-align: center;vertical-align: middle;">Type Customer</th>
                                    </tr>
                                </thead>
                                <tbody id="dataListEstimasi">

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
<!-- END Data Estimasi In/Out-->

<?php elseif($modul == 'partsman'): ?>
 <!--Charts Parts-->
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="card">
                    <div class="header">
                        <div class="row clearfix">
                        

                            <!-- <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                <h2>Chart Estimasi</h2>
                            </div>
                            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                <select id="selectYear" class="form-control">
                                    <option value="2019" style="text-align: center;"<?php if($yearNow == '2019'){ echo "selected";} ?>>2019</option>
                                    <option value="2020" style="text-align: center;"<?php if($yearNow == '2020'){ echo "selected";} ?>>2020</option>
                                    <option value="2021" style="text-align: center;"<?php if($yearNow == '2021'){ echo "selected";} ?>>2021</option>
                                    <option value="2022" style="text-align: center;"<?php if($yearNow == '2022'){ echo "selected";} ?>>2022</option>
                                    <option value="2023" style="text-align: center;"<?php if($yearNow == '2023'){ echo "selected";} ?>>2023</option>
                                </select>
                            </div>
                            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                <select id="selectInOut" class="form-control">
                                    <option value="all" style="text-align: center;">ALL</option>
                                    <option value="in" style="text-align: center;">IN</option>
                                    <option value="out" style="text-align: center;">OUT</option>
                                </select>
                            </div>



                        
                            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                <h2>Tim Performance</h2>
                            </div>
                            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                <select id="selectYear2" class="form-control">
                                    <option value="2019" style="text-align: center;"<?php if($yearNow == '2019'){ echo "selected";} ?>>2019</option>
                                    <option value="2020" style="text-align: center;"<?php if($yearNow == '2020'){ echo "selected";} ?>>2020</option>
                                    <option value="2021" style="text-align: center;"<?php if($yearNow == '2021'){ echo "selected";} ?>>2021</option>
                                    <option value="2022" style="text-align: center;"<?php if($yearNow == '2022'){ echo "selected";} ?>>2022</option>
                                    <option value="2023" style="text-align: center;"<?php if($yearNow == '2023'){ echo "selected";} ?>>2023</option>
                                </select>
                            </div> -->



                         
                            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                <h2>Service Rate</h2>
                            </div>
                            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                <select id="selectYear2" class="form-control">
                                    <option value="2019" style="text-align: center;"<?php if($yearNow == '2019'){ echo "selected";} ?>>2019</option>
                                    <option value="2020" style="text-align: center;"<?php if($yearNow == '2020'){ echo "selected";} ?>>2020</option>
                                    <option value="2021" style="text-align: center;"<?php if($yearNow == '2021'){ echo "selected";} ?>>2021</option>
                                    <option value="2022" style="text-align: center;"<?php if($yearNow == '2022'){ echo "selected";} ?>>2022</option>
                                    <option value="2023" style="text-align: center;"<?php if($yearNow == '2023'){ echo "selected";} ?>>2023</option>
                                </select>
                            </div>

                        
                        </div>
                    </div>
                    <div class="body">
                            <canvas id="myChart3"></canvas>              
                    </div>
                </div>
</div>
<!--End Chart Parts-->

<!--Data Parts-->
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="card">
                    <div class="header">
                        <div class="row clearfix">
                            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2" >
                                <h2 style="margin-top: 15px;">Data Estimasi</h2>
                            </div>
                            <form method="POST" action="<?= site_url('pimpinan/partsToExcel') ?>">
                            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                <small>From:</small>
                                <input type="date" id="dateFromParts" name="dateFromParts" class="form-control" />
                            </div>
                            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                <small>To:</small>
                                <input type="date" id="dateToParts" class="form-control" name="dateToParts" />
                            </div>
                            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                <small>Status Parts:</small>
                                <select id="dateInOutParts" class="form-control" name="dateInOutParts">
                                    <option value="" style="text-align: center;">--Pilih--</option>
                                    <option value="all" style="text-align: center;">ALL</option>
                                    <option value="in" style="text-align: center;">Parts Done</option>
                                    <option value="out" style="text-align: center;">Parts Not Done</option>
                                </select>
                            </div>

                            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3" >
                                <button type="submit" class="btn bg-deep-orange btn-circle waves-effect">
                                    <i class="material-icons">publish</i>
                                </button>
                            </div>

                        </form>
                        </div>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-info">
                                <thead>
                                    <tr>
                                        <th style="text-align:center;vertical-align: middle;">No.</th>
                                        <th style="text-align: center;vertical-align: middle;">No. WO</th>
                                        <th style="text-align: center;vertical-align: middle;">No. Polisi</th>
                                        <th style="text-align: center;vertical-align: middle;">Nama Customer</th>
                                        <th style="text-align: center;vertical-align: middle;">SA</th>
                                        <th style="text-align: center;vertical-align: middle;">Tgl. Estimasi</th>
                                        <th style="text-align: center;vertical-align: middle;">Tgl. Janji Penyerahan</th>
                                        <th style="text-align: center;vertical-align: middle;">Type Customer</th>
                                    </tr>
                                </thead>
                                <tbody id="dataListEstimasiParts">

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
<!-- End Data Parts-->


 <?php elseif($modul == 'ptm'): ?>

<!--Work In Process-->
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Work In Process
                    </h2>
                </div>
                <style type="text/css">
                    /*hilangkan exportable dan menyisakan input search di tabel*/
                    .dt-buttons {
                        display: none;
                    }
                    .text-mid{
                        text-align: center;
                    }
                    th{
                        text-align: center;
                    }
                    .variety{
                        text-align: center; 
                        vertical-align: 
                        middle; 
                        width: 79px;
                    }                   
                </style>
                <div class="body">
                    <!-- <?php foreach ($count as $key) {
                        echo"".$key->total."-".$key->tim_teknisi;?><br />
                        <?php
                    } ?>
                    <table class="table table-bordered table-striped table-hover js-exportable dataTable">
                            <tbody>
                                    <?php foreach($count as $q){?>
                                        <tr>
                                            <td><?php echo"".$q->total;?></td>    
                                        </tr>
                                    <?php } ?>
                                </tbody>
                    </table>  -->
                    <div class="text-mid">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover js-exportable dataTable">
                                <thead>
                                    <tr role="row">
                                        <!-- <th rowspan="3" style="text-align: center; vertical-align: middle; width: 21px; background: orange; color:white;" class="sorting_disabled" colspan="1">No</th> -->
                                        <th colspan="1" rowspan="3" style="text-align: center; vertical-align: middle; width: 175px; background: orange; color:white;" colspan="1">Nama Teknisi</th>
                                        <th colspan="27" style="text-align: center; vertical-align: middle; background: #17aa14; color:white;" rowspan="1">Job Progress Monitoring
                                        </th>
                                    </tr>
                                    <tr role="row" style="background: orange; color:white;">
                                        <th colspan="3" style="text-align: center; vertical-align: middle" rowspan="1">
                                        Body Repair
                                        </th>
                                        <th colspan="3" style="text-align: center; vertical-align: middle" rowspan="1">
                                        Preparation
                                        </th>
                                        <th colspan="3" style="text-align: center; vertical-align: middle" rowspan="1">
                                        Masking
                                        </th>
                                        <th class="col-lg-1 sorting_disabled dt-body-right" rowspan="1" style="text-align: center; vertical-align: middle; width: 78px;" colspan="3">
                                        Painting
                                        </th>
                                        <th colspan="3" style="text-align: center; vertical-align: middle" rowspan="1">
                                        Polishing
                                        </th>
                                        <th colspan="3" style="text-align: center; vertical-align: middle" rowspan="1">
                                        Re Assembling
                                        </th>
                                        <th class="col-lg-1 sorting_disabled dt-body-right" rowspan="1" style="text-align: center; vertical-align: middle; width: 78px;" colspan="3">
                                        Washing
                                        </th>
                                        <th colspan="3" style="text-align: center; vertical-align: middle" rowspan="1">
                                        Final Inspection
                                        </th>
                                        <th colspan="3" style="text-align: center; vertical-align: middle" rowspan="1">
                                        Total
                                        </th>
                                    </tr>
                                    <tr role="row">
                                        <!-- proses1 -->
                                        <th class="col-lg-1 sorting_disabled dt-body-right variety" rowspan="1" colspan="1">
                                        L
                                        </th>
                                        <th class="col-lg-1 sorting_disabled dt-body-right variety" rowspan="1" colspan="1">
                                        M
                                        </th>
                                        <th class="col-lg-1 sorting_disabled dt-body-right variety" rowspan="1" colspan="1">
                                        H
                                        </th>
                                        <!-- proses2 -->
                                        <th class="col-lg-1 sorting_disabled dt-body-right variety" rowspan="1" colspan="1">
                                        L
                                        </th>
                                        <th class="col-lg-1 sorting_disabled dt-body-right variety" rowspan="1" colspan="1">
                                        M
                                        </th>
                                        <th class="col-lg-1 sorting_disabled dt-body-right variety" rowspan="1" colspan="1">
                                        H
                                        </th>
                                        <!-- proses3 -->
                                        <th class="col-lg-1 sorting_disabled dt-body-right variety" rowspan="1" colspan="1">
                                        L
                                        </th>
                                        <th class="col-lg-1 sorting_disabled dt-body-right variety" rowspan="1" colspan="1">
                                        M
                                        </th>
                                        <th class="col-lg-1 sorting_disabled dt-body-right variety" rowspan="1" colspan="1">
                                        H
                                        </th>
                                        <!-- proses4 -->
                                        <th class="col-lg-1 sorting_disabled dt-body-right variety" rowspan="1" colspan="1">
                                        L
                                        </th>
                                        <th class="col-lg-1 sorting_disabled dt-body-right variety" rowspan="1" colspan="1">
                                        M
                                        </th>
                                        <th class="col-lg-1 sorting_disabled dt-body-right variety" rowspan="1" colspan="1">
                                        H
                                        </th>
                                        <!-- proses5 -->
                                        <th class="col-lg-1 sorting_disabled dt-body-right variety" rowspan="1" colspan="1">
                                        L
                                        </th>
                                        <th class="col-lg-1 sorting_disabled dt-body-right variety" rowspan="1" colspan="1">
                                        M
                                        </th>
                                        <th class="col-lg-1 sorting_disabled dt-body-right variety" rowspan="1" colspan="1">
                                        H
                                        </th>
                                        <!-- proses6 -->
                                        <th class="col-lg-1 sorting_disabled dt-body-right variety" rowspan="1" colspan="1">
                                        L
                                        </th>
                                        <th class="col-lg-1 sorting_disabled dt-body-right variety" rowspan="1" colspan="1">
                                        M
                                        </th>
                                        <th class="col-lg-1 sorting_disabled dt-body-right variety" rowspan="1" colspan="1">
                                        H
                                        </th>
                                        <!-- proses7 -->
                                        <th class="col-lg-1 sorting_disabled dt-body-right variety" rowspan="1" colspan="1">
                                        L
                                        </th>
                                        <th class="col-lg-1 sorting_disabled dt-body-right variety" rowspan="1" colspan="1">
                                        M
                                        </th>
                                        <th class="col-lg-1 sorting_disabled dt-body-right variety" rowspan="1" colspan="1">
                                        H
                                        </th>
                                        <!-- proses8 -->
                                        <th class="col-lg-1 sorting_disabled dt-body-right variety" rowspan="1" colspan="1">
                                        L
                                        </th>
                                        <th class="col-lg-1 sorting_disabled dt-body-right variety" rowspan="1" colspan="1">
                                        M
                                        </th>
                                        <th class="col-lg-1 sorting_disabled dt-body-right variety" rowspan="1" colspan="1">
                                        H
                                        </th>
                                        <!-- total -->
                                        <th class="col-lg-1 sorting_disabled dt-body-right variety" rowspan="1" colspan="1">
                                        L
                                        </th>
                                        <th class="col-lg-1 sorting_disabled dt-body-right variety" rowspan="1" colspan="1">
                                        M
                                        </th>
                                        <th class="col-lg-1 sorting_disabled dt-body-right variety" rowspan="1" colspan="1">
                                        H
                                        </th>   
                                    </tr>
                                </thead>
                                <tbody>
                                    <!-- <?php var_dump($hitung) ?> -->
                                    <?php
                                        function sembarang($count){
                                            if ($count == NULL) {
                                                return "0";
                                            }else{
                                                return $count;
                                            }
                                        }

                                        foreach ($hitung as $key) {
                                            // var_dump($key);
                                    ?>
                                    <tr>
                                        <td>
                                            <?= $key->nama_lengkap_user ?>
                                        </td>
                                        <td><?= sembarang($key->bodyPaintLight) ?></td>
                                        <td><?= sembarang($key->bodyPaintMedium) ?></td>
                                        <td><?= sembarang($key->bodyPaintHeavy) ?></td>
                                        <td><?= sembarang($key->preparationLight) ?></td>
                                        <td><?= sembarang($key->preparationMedium) ?></td>
                                        <td><?= sembarang($key->preparationHeavy) ?></td>
                                        <td><?= sembarang($key->maskingLight) ?></td>
                                        <td><?= sembarang($key->maskingMedium) ?></td>
                                        <td><?= sembarang($key->maskingHeavy) ?></td>
                                        <td><?= sembarang($key->paintingLight) ?></td>
                                        <td><?= sembarang($key->paintingMedium) ?></td>
                                        <td><?= sembarang($key->paintingHeavy) ?></td>
                                        <td><?= sembarang($key->polishingLight) ?></td>
                                        <td><?= sembarang($key->polishingMedium) ?></td>
                                        <td><?= sembarang($key->polishingHeavy) ?></td>
                                        <td><?= sembarang($key->reassemblingLight) ?></td>
                                        <td><?= sembarang($key->reassemblingMedium) ?></td>
                                        <td><?= sembarang($key->reassemblingHeavy) ?></td>
                                        <td><?= sembarang($key->washingLight) ?></td>
                                        <td><?= sembarang($key->washingMedium) ?></td>
                                        <td><?= sembarang($key->washingHeavy) ?></td>
                                        <td><?= sembarang($key->finalInspectionLight) ?></td>
                                        <td><?= sembarang($key->finalInspectionMedium) ?></td>
                                        <td><?= sembarang($key->finalInspectionHeavy) ?></td>
                                        <td><?= sembarang($key->totalLight) ?></td>
                                        <td><?= sembarang($key->totalMedium) ?></td>
                                        <td><?= sembarang($key->totalHeavy) ?></td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>                    
                </div>
            </div>
        </div>
<!--End Work In Process--> 


<?php elseif($modul == 'teknisi'): ?> 
<!--Charts Teknisi-->

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="card">
                    <div class="header">
                        <div class="row clearfix">
                        
                            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                <h2>Team Performance</h2>
                            </div>
                            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                <select id="selectYearTeknisi" class="form-control">
                                    <option value="2019" style="text-align: center;"<?php if($yearNow == '2019'){ echo "selected";} ?>>2019</option>
                                    <option value="2020" style="text-align: center;"<?php if($yearNow == '2020'){ echo "selected";} ?>>2020</option>
                                    <option value="2021" style="text-align: center;"<?php if($yearNow == '2021'){ echo "selected";} ?>>2021</option>
                                    <option value="2022" style="text-align: center;"<?php if($yearNow == '2022'){ echo "selected";} ?>>2022</option>
                                    <option value="2023" style="text-align: center;"<?php if($yearNow == '2023'){ echo "selected";} ?>>2023</option>
                                </select>
                            </div>
                             <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                <select id="selectTeknisi" class="form-control">
                                    <?php foreach($dataTeknisi as $teknisi){ ?>   
                                    <option value="<?= base_url('pimpinan/getDataChartTeknisi/'.$teknisi->id_user)?>" style="text-align: center;"><?= $teknisi->nama_lengkap_user ?></option>
                                <?php } ?>
                                </select>
                            </div    
                        </div>
                    </div>

                    <div class="body">
                            <canvas id="myChart2"></canvas>              
                    </div>
                </div>
</div>

<!--End Charts Teknisi--> 

<?php elseif($modul == 'foreman'): ?>
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="card">
                    <div class="header">
                        <div class="row clearfix">
                        
                            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                <h2>All Team Performance</h2>
                            </div>
                            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                <select id="selectYearForeman" class="form-control">
                                    <option value="2019" style="text-align: center;"<?php if($yearNow == '2019'){ echo "selected";} ?>>2019</option>
                                    <option value="2020" style="text-align: center;"<?php if($yearNow == '2020'){ echo "selected";} ?>>2020</option>
                                    <option value="2021" style="text-align: center;"<?php if($yearNow == '2021'){ echo "selected";} ?>>2021</option>
                                    <option value="2022" style="text-align: center;"<?php if($yearNow == '2022'){ echo "selected";} ?>>2022</option>
                                    <option value="2023" style="text-align: center;"<?php if($yearNow == '2023'){ echo "selected";} ?>>2023</option>
                                </select>
                            </div>     
                        </div>
                    </div>

                    <div class="body">
                            <canvas id="myChart4"></canvas>              
                    </div>
                </div>
</div>


<?php endif; ?>







         




        </div>
    </div>
</section>