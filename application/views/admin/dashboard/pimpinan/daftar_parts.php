<section class="content">
    <div class="container-fluid">

        <!-- Exportable Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            Daftar Parts
                        </h2>
                        <ul class="header-dropdown m-r--5">
                            <li class="dropdown">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">more_vert</i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li>
                                        <a data-toggle='modal' data-target="#addParts">Tambah Parts</a>
                                    </li>
                                    <li>
                                        <a data-toggle='modal' data-target="#addPartsFiles">Tambah Parts Dengan File</a>
                                    </li>
                                    <li>
                                        <a data-toggle="modal" data-target="#myModal">Update Harga Dengan File (Biasa)</a>
                                    </li>
                                    <li>
                                        <a data-toggle="modal" data-target="#myModalSubs">Update Harga Dengan File (Subs. Parts)</a>
                                    </li>
                                    <li></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                     <style type="text/css">
                        /*hilangkan exportable dan menyisakan input search di tabel*/
                        .dt-buttons {
                            display: none;
                        }
                    </style>
                    <div class="body">
                        <div class="table-responsive">
                            <table id="mytable" class="table table-bordered table-striped table-hover" >
                                <h6 style="color: orange;">
                                    <?php
                                        $info = $this->session->flashdata('update_sukses');
                                        $empty = $this->session->flashdata('not_select');
                                        if(!empty($info)){
                                            echo $info;
                                        }else if(!empty($empty)){
                                            echo $empty;
                                        }
                                    ?>
                        </h6>
                                <thead>
                                    <tr>
                                        <th style="text-align: center;vertical-align: middle;">Nomor Parts</th>
                                        <th style="text-align: center;vertical-align: middle;">Nomor Parts Lama</th>
                                        <th style="text-align: center;vertical-align: middle;">Nama Parts</th>
                                        <th style="text-align: center;vertical-align: middle;">Harga<br>(Rp)</th>
                                        <th style="text-align: center;vertical-align: middle;">Action</th>
                                        <!--<th style="text-align: center;vertical-align: middle;">Update</th>-->
                                    </tr>
                                </thead>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Exportable Table -->
    </div>
</section>

<div id="addPartsFiles" class="modal fade" role="dialog">
   <div class="modal-dialog">
    <!-- konten modal-->
    <div class="modal-content">
        <!-- heading modal -->
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Import File Parts Baru</h4>
        </div>
        <!-- body modal -->
        <div class="modal-body">
            <form method="POST" enctype="multipart/form-data" action="<?= base_url('pimpinan/importNewParts') ?>" style="padding-bottom: -30px;">

                <div class="input-group" style="max-width: 500px;">
                    <div class="form-line" >
                         <input type="file" name="fileImport" id="fileParts" class="form-control" required accept=".csv">
                    </div>
                    <span class="input-group-addon">
                        <button type="submit" class="btn bg-deep-orange btn-circle waves-effect waves-circle waves-float" name="upload" style="margin-right: 50px">
                            <i class="material-icons">file_upload</i>
                        </button>
                    </span>
                </div>
                <h6>File yang diizinkan hanya .csv</h6>
                <div class="input-group" style="max-width: 150px;margin-bottom: 5px">
                <h5 style="margin-top: 15px">Format Tabel</h5>
                <span class="input-group-addon" >
                     <a href="<?= base_url('assets/csv/Format Tambah Data Dengan File.csv') ?>" class="btn bg-deep-orange btn-circle waves-effect waves-circle waves-float" style="height: 30px; width: 30px; padding: 4px 8px !important;">
                       <i class="material-icons">get_app</i>
                     </a>
                </span>
                </div>
                    <div class="container">
                        <table class="table-bordered" style="width: 500px;">
                            <thead>
                                <tr>
                                     <th style="text-align: center;vertical-align: middle;">No. Parts, Harga, Nama Parts</th>
                                       </tr>
                                   </thead>
                                       <tbody>
                                         <tr>
                                           <td>92611-00816,160000,PIPE ASSY,EXHAUST</td>
                                         </tr>
                                         <tr>
                                           <td>83320-30560,800000,TRANSMISSION ASSY</td>
                                         </tr>
                                         <tr>
                                           <td>83320-52240,960000,SPRING, COIL, FR</td>
                                         </tr>
                                     </tbody>
                                 </table>
                                 </div>
                <h6> Catatan :</h6>
                <ol style="font-size: 12px">
                  <li>Isi dipisah berdasarkan koma (,)</li>
                  <li>Koma secara berurutan berdasarkan "No. Parts" lalu "Harga" lalau "Nama Parts"</li>
                  <li>Misalnya (92611-00816-83,58396-BZ010-83,16000000, TRANSMISSION ASSY)</li>
                  <li>Diisi pada Kolom A</li>
                </ol>

                </form>
        </div>
        <!-- footer modal
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Tutup Modal</button>
        </div>
        -->
    </div>
   </div>
</div>

<div id="myModal" class="modal fade" role="dialog">
   <div class="modal-dialog">
    <!-- konten modal-->
    <div class="modal-content">
        <!-- heading modal -->
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Import File Update Harga Parts (File Biasa)</h4>
        </div>
        <!-- body modal -->
        <div class="modal-body">
            <form method="POST" enctype="multipart/form-data" action="<?= base_url('pimpinan/importUpdate') ?>" style="padding-bottom: -30px;">

                <div class="input-group" style="max-width: 500px;">
                    <div class="form-line" >
                         <input type="file" name="fileImport" id="fileParts" class="form-control" required accept=".csv">
                    </div>
                    <span class="input-group-addon">
                        <button type="submit" class="btn bg-deep-orange btn-circle waves-effect waves-circle waves-float" name="upload" style="margin-right: 50px">
                            <i class="material-icons">file_upload</i>
                        </button>
                    </span>
                </div>
                <h6>File yang diizinkan hanya .csv</h6>
                <div class="input-group" style="max-width: 150px;margin-bottom: 5px">
                <h5 style="margin-top: 15px">Format Tabel</h5>
                <span class="input-group-addon" >
                     <a href="<?= base_url('assets/csv/Format Update Harga Dengan File (Biasa).csv') ?>" class="btn bg-deep-orange btn-circle waves-effect waves-circle waves-float" style="height: 30px; width: 30px; padding: 4px 8px !important;">
                       <i class="material-icons">get_app</i>
                     </a>
                </span>
                </div>
                    <div class="container">
                        <table class="table-bordered" style="width: 500px;">
                            <thead>
                                <tr>
                                     <th style="text-align: center;vertical-align: middle;">No. Parts,Harga</th>
                                       </tr>
                                   </thead>
                                       <tbody>
                                         <tr>
                                           <td>92611-00816,160000</td>
                                         </tr>
                                         <tr>
                                           <td>83320-30560,800000</td>
                                         </tr>
                                         <tr>
                                           <td>83320-52240,960000</td>
                                         </tr>
                                     </tbody>
                                 </table>
                                 </div>
                <h6> Catatan :</h6>
                <ol style="font-size: 12px">
                  <li>Isi dipisah berdasarkan koma (,)</li>
                  <li>Koma secara berurutan berdasarkan "No. Parts Lama" lalu "No. Parts Baru" lalu "Harga"</li>
                  <li>Misalnya (92611-00816-83,58396-BZ010-83,16000000)</li>
                  <li>Diisi pada Kolom A</li>
                </ol>

                </form>
        </div>
        <!-- footer modal
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Tutup Modal</button>
        </div>
        -->
    </div>
   </div>
</div>

<div id="myModalSubs" class="modal fade" role="dialog">
   <div class="modal-dialog">
    <!-- konten modal-->
    <div class="modal-content">
        <!-- heading modal -->
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Import File Update Harga Parts (File Subs. Parts)</h4>
        </div>
        <!-- body modal -->
        <div class="modal-body">
            <form method="POST" enctype="multipart/form-data" action="<?= base_url('pimpinan/importUpdateSubs') ?>" style="padding-bottom: -30px;">

                <div class="input-group" style="max-width: 500px;">
                    <div class="form-line">
                         <input type="file" name="fileImport" id="fileParts" class="form-control" required accept=".csv">
                    </div>
                    <span class="input-group-addon">
                        <button type="submit" class="btn bg-deep-orange btn-circle waves-effect waves-circle waves-float" name="upload" style="margin-right: 50px">
                            <i class="material-icons">file_upload</i>
                        </button>
                    </span>
                </div>
                <h6>File yang diizinkan hanya .csv</h6>
                <div class="input-group" style="max-width: 150px;margin-bottom: 5px">
                <h5 style="margin-top: 15px">Format Tabel</h5>
                <span class="input-group-addon" >
                     <a href="<?= base_url('assets/csv/Format Update Harga Dengan File (Subs. Parts).csv') ?>" class="btn bg-deep-orange btn-circle waves-effect waves-circle waves-float" style="height: 30px; width: 30px; padding: 4px 8px !important;">
                       <i class="material-icons">get_app</i>
                     </a>
                </span>
                </div>
                    <div class="container">
                        <table class="table-bordered" style="width: 500px;">
                            <thead>
                                <tr>
                                  <th style="text-align: center;vertical-align: middle;">No. Parts Lama,No. Parts Baru,Harga</th>
                                </tr>
                              </thead>
                             <tbody>
                                <tr>
                                  <td>92611-00816,58396-BZ010,160000</td>
                                </tr>
                                <tr>
                                  <td>83320-30560,83320-30570,800000</td>
                                </tr>
                                <tr>
                                  <td>83320-52240,83320-52260,960000</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                <h6> Catatan :</h6>
                <ol style="font-size: 12px">
                  <li>Isi dipisah berdasarkan koma (,)</li>
                  <li>Koma secara berurutan berdasarkan "No. Parts Lama" lalu "No. Parts Baru" lalu "Harga"</li>
                  <li>Misalnya (92611-00816-83,58396-BZ010-83,16000000)</li>
                  <li>Diisi pada Kolom A</li>
                </ol>

                </form>
        </div>
        <!-- footer modal
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Tutup Modal</button>
        </div>
        -->
    </div>
   </div>
</div>

<div id="addParts" class="modal fade" role="dialog">
   <div class="modal-dialog">
    <!-- konten modal-->
    <div class="modal-content">
        <!-- heading modal -->
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Tambah Parts</h4>
        </div>
        <!-- body modal -->
        <div class="modal-body">
            <form method="POST" action="<?= base_url()?>pimpinan/tambahParts">
                                <table class="table">
                                <tbody style="border: hidden;">

                                    <tr>
                                        <td class="col-xs-2"><b>Nomor Parts</b></td>
                                        <td class="col-xs-1"><b>:</b></td>
                                        <td>
                                            <input type="text" class="form-control" name="nomor_parts" placeholder="Masukkan Nomor Parts" maxlength="40" required />
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="col-xs-2"><b>Nomor Parts Lama</b></td>
                                        <td class="col-xs-1"><b>:</b></td>
                                        <td>
                                            <input type="text" class="form-control" name="nomor_parts_lama" placeholder="Masukkan Nomor Parts Lama" maxlength="40" />
                                        </td>
                                    </tr>

                                     <tr>
                                        <td class="col-xs-2"><b>Nama Parts</b></td>
                                        <td class="col-xs-1"><b>:</b></td>
                                        <td>
                                            <input type="text" class="form-control" name="nama_item" placeholder="Masukkan Nama Parts" onkeyup="this.value = this.value.toUpperCase()"  required />
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="col-xs-2"><b>Harga Parts</b></td>
                                        <td class="col-xs-1"><b>:</b></td>
                                        <td>
                                            <input type="number" class="form-control" name="harga_item" placeholder="Masukkan Harga Parts" maxlength="10" required />
                                        </td>
                                    </tr>

                                </tbody>

                            </table>
                            <input type="submit" name="" class="btn btn-warning waves-effect" value="Tambah Parts">
                        </form>
        </div>
        <!-- footer modal
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Tutup Modal</button>
        </div>
        -->
    </div>
   </div>
</div>

<div id="editParts" class="modal fade" role="dialog">
   <div class="modal-dialog">
    <!-- konten modal-->
    <div class="modal-content">
        <!-- heading modal -->
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Edit Parts</h4>
        </div>
        <!-- body modal -->
        <div class="modal-body">
            <form method="POST" action="<?= base_url()?>pimpinan/update_parts">
                                <table class="table">
                                <tbody style="border: hidden;">
                                   <tr style="display: none;visibility: hidden;">
                                        <td class="col-xs-2"><b>Id Item</b></td>
                                        <td class="col-xs-1"><b>:</b></td>
                                        <td>
                                            <input type="hidden" class="form-control" name="id_item" placeholder="id item" required readonly />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col-xs-2"><b>Nomor Parts</b></td>
                                        <td class="col-xs-1"><b>:</b></td>
                                        <td>
                                            <input type="text" class="form-control" name="nomor_parts" placeholder="Masukkan Nomor Parts" maxlength="40" required />
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="col-xs-2"><b>Nomor Parts Lama</b></td>
                                        <td class="col-xs-1"><b>:</b></td>
                                        <td>
                                            <input type="text" class="form-control" name="nomor_parts_lama" placeholder="Masukkan Nomor Parts Lama" maxlength="40" />
                                        </td>
                                    </tr>

                                     <tr>
                                        <td class="col-xs-2"><b>Nama Parts</b></td>
                                        <td class="col-xs-1"><b>:</b></td>
                                        <td>
                                            <input type="text" class="form-control" name="nama_item" placeholder="Masukkan Nama Parts" onkeyup="this.value = this.value.toUpperCase()"  required />
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="col-xs-2"><b>Harga Parts</b></td>
                                        <td class="col-xs-1"><b>:</b></td>
                                        <td>
                                            <input type="number" class="form-control" name="harga_item" placeholder="Masukkan Harga Parts" maxlength="10" required />
                                        </td>
                                    </tr>



                                </tbody>

                            </table>
                            <input type="submit" name="" class="btn btn-warning waves-effect" value="Update Parts">
                        </form>
        </div>
        <!-- footer modal
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Tutup Modal</button>
        </div>
        -->
    </div>
   </div>
</div>


   <form id="add-row-form" action="<?php echo base_url().'pimpinan/delete_parts'?>" method="post">
         <div class="modal small fade" id="hapusParts" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                 <h3 id="myModalLabel">Konfirmasi Hapus</h3>

            </div>
            <div class="modal-body">
                <p class="error-text"><i class="fa fa-warning modal-icon"></i><h4>Apakah Anda Yakin Ingin Menghapus ?</h4>
                Setelah Anda Menghapus, Maka Data Akan Hilang.</p>
                <input type="hidden" class="form-control" name="id_item" placeholder="id item" required readonly />
            </div>
            <div class="modal-footer">
              <input type="submit" name="" class="btn btn-warning waves-effect" value="Hapus Parts">
            </div>
        </div>
    </div>
</div>
     </form>

<script type="text/javascript">

$(document).ready(function(){

    //setup datatables
    $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
      {
          return {
              "iStart": oSettings._iDisplayStart,
              "iEnd": oSettings.fnDisplayEnd(),
              "iLength": oSettings._iDisplayLength,
              "iTotal": oSettings.fnRecordsTotal(),
              "iFilteredTotal": oSettings.fnRecordsDisplay(),
              "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
              "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
          };
      };

      var table = $("#mytable").dataTable({
          initComplete: function() {
              var api = this.api();
              $('#mytable_filter input')
                  .off('.DT')
                  .on('input.DT', function() {
                      api.search(this.value).draw();
              });
          },
              oLanguage: {
              sProcessing: "loading..."
          },
              processing: true,
              serverSide: true,
              bLengthChange: false,
              ajax: {"url": "<?php echo base_url().'pimpinan/get_item_json'?>", "type": "POST"},
                    columns: [
                                {"data": "nomor_parts"},
                                {"data": "nomor_parts_lama", render: function(data) {
                                    if (data == null) {
                                        return '-';
                                    }
                                    else {
                                        return data;
                                    }
                                }},
                                {"data": "nama_item"},
                                                //render harga dengan format angka
                                {"data": "harga_item", "searchable": false, render: $.fn.dataTable.render.number('.', '.', '')},
                                {"data": "view", "orderable": false},
                  ],
                order: [],
          rowCallback: function(row, data, iDisplayIndex) {
              var info = this.fnPagingInfo();
              var page = info.iPage;
              var length = info.iLength;
              $('td:eq(0)', row).html();
          }

      });


       $('#mytable').on('click','.edit_record',function(){
                        var kode=$(this).data('id');
                        var nama=$(this).data('nama');
                        var harga=$(this).data('harga');
                        var nomor=$(this).data('nomor');
                        var nomorlama=$(this).data('nomorlama');
            $('#editParts').modal('show');
                        $('[name="id_item"]').val(kode);
                        $('[name="nama_item"]').val(nama);
                        $('[name="harga_item"]').val(harga);
                        $('[name="nomor_parts"]').val(nomor);
                        $('[name="nomor_parts_lama"]').val(nomorlama);
      });

       $('#mytable').on('click','.hapus_record',function(){
            var kode=$(this).data('id');
            $('#hapusParts').modal('show');
            $('[name="id_item"]').val(kode);
      });

      $('#mytable').css({
        'text-align': 'center',
        'vertical-align': 'middle',
      });

});

</script>
