<section class="content">
    <div class="container-fluid">
        
        <!-- Exportable Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            HISTORY ESTIMASI
                        </h2>
                    </div>
                     <style type="text/css">
                        /*hilangkan exportable dan menyisakan input search di tabel*/
                        .dt-buttons {
                            display: none;
                        }
                    </style>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="">
                                <h5>DATA ESTIMASI</h5>
                                    <table class="table">
                                        <tbody>  
                                        <?php foreach($listHistory as $datas): ?>
                                            <tr>
                                                <td width="125" style="border: none;">No. WO</td>
                                                <td width="1" style="border: none;">:</td>
                                                <td style="border: none;"><?= $datas->nomor_wo ?></td>
                                            </tr>
                                             <tr>
                                                <td style="border: none;">No. Polisi</td>
                                                <td style="border: none;">:</td>
                                                <td style="border: none;"><?= $datas->no_polisi ?></td>
                                            </tr>
                                             <tr>
                                                <td style="border: none;">Nama Customer</td>
                                                <td style="border: none;">:</td>
                                                <td style="border: none;"><?= $datas->nama_lengkap ?></td>
                                            </tr>
                                            <tr>
                                                <td style="border: none;">Nama Teknisi</td>
                                                <td style="border: none;">:</td>
                                                <td style="border: none;"><?= $datas->nama_lengkap_user?></td>
                                            </tr>
                                        <?php 
                                        break;
                                        endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <style type="text/css">
                                .badge-not {
                                    border-radius: 10px;
                                    font-weight: 100;
                                    font-size: 13px;
                                    background-color: red;
                                }
                                .badge-yes {
                                    border-radius: 10px;
                                    font-weight: 100;
                                    font-size: 13px;
                                    background-color: green;
                                }
                            </style>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                        <thead>
                                            <tr>
                                                <th align="center">No.</th>
                                                <th>Status Produksi</th>
                                                <th>Status Estimasi</th>
                                                <th>Waktu</th>
                                                <th>Keterangan</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php 
                                        $no = 1;
                                        foreach($listHistory as $datas){ 
                                        ?>
                                            <tr>
                                                <td align="center"><?= $no++ ?></td>
                                                <td><?= $datas->status_produksi_history ?></td>
                                                <td><?= $datas->status_estimasi ?></td>
                                                <td><?= date("H:i | d M Y", strtotime($datas->waktu_history)) ?></td>
                                                <td align="center"><a style="margin-right: 5px;" type="button" class="btn bg-orange btn-xs waves-effect" data-toggle="modal" data-target="#ketModal<?=$datas->id_history?>"><i class="material-icons">sticky_note_2</i></a>'</td>
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <?php foreach($listHistory as $datas): ?>
                        <div class="modal fade" id="ketModal<?=$datas->id_history?>" tabindex="-1" role="dialog">
                            <div class="modal-dialog modal-sm" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="smallModalLabel">Keterangan</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="form-line">
                                            <textarea rows="4" disabled class="form-control no-resize" name="message" placeholder=". . . " id="note"><?= $datas->ket_history ?></textarea>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn bg-red waves-effect" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php endforeach; ?>
                        <style type="text/css">
                            .icon-in {
                                color: green;
                            }
                            .icon-out {
                                color: red;
                            }
                            .icon-car {
                                vertical-align: middle;
                                color: orange;
                            }
                            .badge-process {
                                border-radius: 20px;
                                font-weight: 100;
                                background-color: orange;
                            }

                        </style>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Exportable Table -->
    </div>
</section>
<!-- FUNCTION JS FOR CLICKABLE ROW DATATABLE -->
