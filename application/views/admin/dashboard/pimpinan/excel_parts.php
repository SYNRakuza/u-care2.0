<?php
$date = date('d-m-Y');

header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=daftar_parts.xls");

header("Pragma: no-cache");

header("Expires: 0");
?>

<table border="1" width="100%">
    <thead>
        <th style="text-align:center;vertical-align: middle;">No.</th>
        <th style="text-align: center;vertical-align: middle;">Nomor WO</th>
        <th style="text-align: center;vertical-align: middle;">No. Polisi</th>
        <th style="text-align: center;vertical-align: middle;">Nama Customer</th>
        <th style="text-align: center;vertical-align: middle;">SA</th>
        <th style="text-align: center;vertical-align: middle;">Tgl. Estimasi</th>
        <th style="text-align: center;vertical-align: middle;">Tgl. Janji Penyerahan</th>
        <th style="text-align: center;vertical-align: middle;">Tipe Customer</th>
    </thead>
    <tbody>
        <?php
            $i=1;
            foreach($data as $x){
                $nomor_wo = $x->nomor_wo;

                if($nomor_wo == NULL){
                    $nomor_wo = "-";
                }

                if($x->jenis_customer == 0){
                    $jenis = "ASURANSI - ".$x->nama_asuransi;
                }else{
                    $jenis = "Tunai";
                }

                if($x->tgl_estimasi == NULL || $x->tgl_estimasi == "0000-00-00" ){
                    $tgl_estimasi = "-";
                }else{
                    $tgl_estimasi = date('d-M-Y', strtotime($x->tgl_estimasi));
                }

                if($x->tgl_janji_penyerahan == NULL || $x->tgl_janji_penyerahan == "0000-00-00" ){
                    $tgl_janji_penyerahan = "-";
                }else{
                    $tgl_janji_penyerahan = date('d-M-Y', strtotime($x->tgl_janji_penyerahan));
                }

                
        ?>
        <tr>
            <td style="text-align: center;vertical-align: middle;"><?= $i ?></td>
            <td style="text-align: center;vertical-align: middle;"><?= $nomor_wo ?></td>
            <td style="text-align: center;vertical-align: middle;"><?= $x->no_polisi ?></td>
            <td style="text-align: center;vertical-align: middle;"><?= $x->nama_lengkap ?></td>
            <td style="text-align: center;vertical-align: middle;"><?= $x->nama_lengkap_user ?></td>
            <td style="text-align: center;vertical-align: middle;"><?= $tgl_estimasi ?></td>
            <td style="text-align: center;vertical-align: middle;"><?= $tgl_janji_penyerahan ?></td>
            <td style="text-align: center;vertical-align: middle;"><?= $jenis ?></td>
        </tr>

        <?php $i++; } ?>
    </tbody>
</table>

</div>