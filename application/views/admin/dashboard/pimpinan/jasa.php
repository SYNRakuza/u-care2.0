<section class="content">
    <div class="container-fluid">
        <!-- Exportable Table -->

        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2 class="col-xs-12 col-sm-12 col-md-2 col-md-2" style="vertical-align: middle;">
                            Daftar Jasa
                        </h2>
                        <br/>
                        <div class="row clearfix">
                            <div class="col-xs-12 col-sm-3 col-md-9 col-lg-9">
                                <input type="hidden" id="get_url" value="<?= base_url('pimpinan/loadJasa') ?>">
                                <form method="POST" action="<?= site_url('pimpinan/jasaToExcel') ?>">
                                    <select id="jenis_kendaraan" class="col-md-4 col-lg-4" name="id_jenis">
                                        <option value="" style="text-align: center;">--Pilih Jenis Kendaraan--</option>
                                        <?php
                                            foreach($dataMobil as $get){
                                        ?>
                                            <option value="<?= $get->id_jenis ?>" text-align="center"><?= $get->nama_jenis ?></option>
                                        <?php } ?>
                                    </select>
                                    <button type="submit" class="btn bg-deep-orange btn-circle waves-effect waves-circle waves-float searching">
                                        <i class="material-icons">publish</i>
                                    </button>                                    
                                </form>
                            </div>
                        </div>
                        <ul class="header-dropdown m-r--5">
                            <li class="dropdown">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">more_vert</i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li>
                                        <a data-toggle='modal' data-target="#addJasa">Tambah Jasa</a>
                                    </li>
                                    <li>
                                        <a data-toggle='modal' data-target="#modalImportBaru">Import Jasa Kendaraan - Baru (Excel)</a>
                                    </li>
                                    <li>
                                        <a data-toggle='modal' data-target="#modalImportUpdate">Update Jasa Kendaraan (Excel)</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <style type="text/css">
                        /*hilangkan exportable dan menyisakan input search di tabel*/
                        .dt-buttons {
                            display: none;
                        }
                        table tr td {
                            vertical-align: middle !important;
                        }
                    </style>
                    <div class="body">
                         <div class="table-responsive">
                                <h6 style="color: orange;">
                                    <?php
                                        $info = $this->session->flashdata('info');
                                        $empty = $this->session->flashdata('not_select');
                                        if(!empty($info)){
                                            echo $info;
                                        }else if(!empty($empty)){
                                            echo $empty;
                                        }
                                    ?>
                                </h6>
                            <table class="table table-bordered table-striped table-hover" id="tabelJasa" >

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Exportable Table -->
    </div>
</section>



<!--Modal Add Jasa-->

<div id="addJasa" class="modal fade" role="dialog">
   <div class="modal-dialog">
    <!-- konten modal-->
    <div class="modal-content">
        <!-- heading modal -->
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Tambah Jasa</h4>
        </div>
        <!-- body modal -->
        <div class="modal-body">
            <form method="POST" action="<?= base_url()?>pimpinan/jasaCreateAction">
                                <table class="table">
                                <tbody style="border: hidden;">

                                     <tr>
                                        <td class="col-xs-2"><b>Nama Jasa</b></td>
                                        <td class="col-xs-1"><b>:</b></td>
                                        <td>
                                            <input type="text" class="form-control" name="nama_item" placeholder="Masukkan Nama Jasa"  required />
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="col-xs-2"><b>Harga Jasa</b></td>
                                        <td class="col-xs-1"><b>:</b></td>
                                        <td>
                                            <input type="number" class="form-control" name="harga_item" placeholder="Masukkan Harga Jasa" required />
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="col-xs-2"><b>Posisi Jasa</b></td>
                                        <td class="col-xs-1"><b>:</b></td>
                                        <td>
                                            <select id="posisi_jasa" name="posisi_jasa" class="col-md-12 col-lg-12">
                                                <option value="">-- Pilih Posisi Jasa --</option>
                                                <option value="1">Outer Panel</option>
                                                <option value="2">Inner Panel</option>
                                                <option value="3">Extention/Cover</option>
                                                <option value="4">Glasses & Optional</option>
                                                <option value="5">Special</option>
                                                <option value="6">Spot Repair</option>
                                            </select>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="col-xs-2"><b>Jenis Mobil</b></td>
                                        <td class="col-xs-1"><b>:</b></td>
                                        <td>
                                            <select id="jenis_kendaraan" class="col-md-12 col-lg-12" name="id_jenis">
                                                <option value="" style="text-align: center;">-- Pilih Jenis Kendaraan --</option>
                                                <?php
                                                    foreach($dataMobil as $get){
                                                ?>
                                                    <option value="<?= $get->id_jenis ?>" text-align="center"><?= $get->nama_jenis ?></option>
                                                <?php } ?>
                                            </select>
                                        </td>
                                    </tr>

                                </tbody>

                            </table>
                            <button type="submit" class="btn bg-orange waves-effect" id="tombol">
                                        <i class="material-icons">save</i>
                                            <span>Tambah</span>
                                    </button>
                        </form>
        </div>
    </div>
   </div>
</div>

<!-- End Modal Add Jasa-->

<div id="modalImportBaru" class="modal fade" role="dialog">
   <div class="modal-dialog">
    <!-- konten modal-->
    <div class="modal-content">
        <!-- heading modal -->
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Import Jasa Kendaraan -Baru (Excel)</h4>
        </div>
        <!-- body modal -->
        <div class="modal-body">
            <form method="POST" enctype="multipart/form-data" action="<?= base_url('pimpinan/importJasa') ?>" style="padding-bottom: -30px;">

                <div class="input-group" style="max-width: 250px;">
                    <div class="form-line">
                         <input type="file" name="fileImport" id="fileParts" class="form-control" required accept=".csv">
                    </div>
                    <br/>
                    <div class="form-line">
                        <select id="jenis_kendaraan" class="col-md-12 col-lg-12" name="id_jenis" required>
                                        <option value="">--Pilih Jenis Kendaraan--</option>
                                        <?php
                                            foreach($dataMobil as $get){
                                        ?>
                                            <option value="<?= $get->id_jenis ?>" text-align="center"><?= $get->nama_jenis ?></option>
                                        <?php } ?>
                        </select>
                    </div>    
                    
                    <span class="input-group-addon">
                        <button type="submit" class="btn bg-deep-orange btn-circle waves-effect waves-circle waves-float" name="upload">
                            <i class="material-icons">file_upload</i>
                        </button>
                    </span>
                </div>
                <h6><b>File yang diizinkan hanya .csv</b></h6>

                </form>
        </div>
        <!-- footer modal
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Tutup Modal</button>
        </div>
        -->
    </div>
   </div>
</div>



<div id="modalImportUpdate" class="modal fade" role="dialog">
   <div class="modal-dialog">
    <!-- konten modal-->
    <div class="modal-content">
        <!-- heading modal -->
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Update Jasa Kendaraan (Excel)</h4>
        </div>
        <!-- body modal -->
        <div class="modal-body">
            <form method="POST" enctype="multipart/form-data" action="<?= base_url('pimpinan/importJasaUpdate') ?>" style="padding-bottom: -30px;">

                <div class="input-group" style="max-width: 250px;">
                    <div class="form-line">
                         <input type="file" name="fileImport" id="fileParts" class="form-control" required accept=".csv">
                    </div>
                    <br/>  
                    
                    <span class="input-group-addon">
                        <button type="submit" class="btn bg-deep-orange btn-circle waves-effect waves-circle waves-float" name="upload">
                            <i class="material-icons">file_upload</i>
                        </button>
                    </span>
                </div>
                <h6><b>File yang diizinkan hanya .csv</b></h6>

                </form>
        </div>
        <!-- footer modal
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Tutup Modal</button>
        </div>
        -->
    </div>
   </div>
</div>