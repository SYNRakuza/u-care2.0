<section class="content">
    <div class="container-fluid">

        <input type="hidden" id="get_url_2" value="<?= base_url('pimpinan/queryListJasa')?>"/>

        <!-- Exportable Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2 class="col-xs-12 col-sm-12 col-md-2 col-md-2" style="vertical-align: middle;">
                            Daftar Jasa
                        </h2>
                        <br/>
                        <div class="row clearfix">
                            <div class="col-xs-12 col-sm-3 col-md-9 col-lg-9">
                                <form method="POST" action="<?= site_url('pimpinan/daftar_jasa') ?>">
                                    <select id="jenis_kendaraan" class="col-md-5 col-lg-5" name="id_jenis">
                                        <option value="" style="text-align: center;">--Pilih Jenis Kendaraan--</option>
                                        <?php
                                            foreach($dataMobil as $get){
                                        ?>
                                            <option value="<?= $get->id_jenis ?>" text-align="center"><?= $get->nama_jenis ?></option>
                                        <?php } ?>
                                    </select>
                                    <button type="submit" class="btn bg-deep-orange btn-circle waves-effect waves-circle waves-float searching">
                                        <i class="material-icons">search</i>
                                    </button>  
                                    <button onclick="exportTableToExcel('tabelJasa')" class="btn bg-deep-orange btn-circle waves-effect">
                                    <i class="material-icons">publish</i>
                                </button>                                  
                                </form>
                                
                            </div>
                        </div>
                        <ul class="header-dropdown m-r--5">
                            <li class="dropdown">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">more_vert</i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li>
                                        <a data-toggle='modal' data-target="#addJasa">Tambah Jasa</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <style type="text/css">
                        /*hilangkan exportable dan menyisakan input search di tabel*/
                        .dt-buttons {
                            display: none;
                        }
                        table tr td {
                            vertical-align: middle !important;
                        }
                    </style>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable js-exportable" id="tabelJasa" >

                                <h6 style="color: orange;">
                                    <?php
                                        $info = $this->session->flashdata('info');
                                        $empty = $this->session->flashdata('not_select');
                                        if(!empty($info)){
                                            echo $info;
                                        }else if(!empty($empty)){
                                            echo $empty;
                                        }
                                    ?>
                                </h6>


                                <thead>
                                <tr>
                                    <th style="text-align: center;vertical-align: middle;">Id</th>
                                    <th style="text-align: center;vertical-align: middle;">Nama Jasa</th>
                                    <th style="text-align: center;vertical-align: middle;">Harga Jasa</th>
                                    <th style="text-align: center;vertical-align: middle;">Posisi Jasa</th>
                                    <th style="text-align:center;vertical-align: middle;">Jenis Mobil</th>
                                    <th style="text-align: center;vertical-align: middle;">Action</th>
                                </tr>
                                </thead>
                                <tbody id="dataListJasa">
                                    <?php foreach($data as $datas) {
                                        $harga = number_format($datas->harga_item,0,',','.');
                                     ?>
                                    <tr>
                                        <td style="text-align: center;vertical-align: middle;"><?= $datas->id_item ?></td>
                                        <td style="text-align: center;vertical-align: middle;"><?= $datas->nama_item ?></td>
                                        <td style="text-align: center;vertical-align: middle;"><?= $harga ?></td>
                                        <td style="text-align: center;vertical-align: middle;">
                                            <?php
                                                $panel =  $datas->posisi_jasa;
                                                if($panel == 1){
                                                    echo "Outer Panel";
                                                }else if($panel == 2){
                                                    echo "Inner Panel";
                                                }else if($panel == 3){
                                                    echo "Extention/Cover";
                                                }else if($panel == 4){
                                                    echo "Glasses & Optional";
                                                }else if($panel == 5){
                                                    echo "Special";
                                                }else if($panel == 6){
                                                    echo "Spot Repair";
                                                }else{
                                                    echo "-";
                                                }
                                            ?>
                                        </td>
                                        <td style="text-align: center;vertical-align: middle;"><?= $datas->nama_jenis ?></td>
                                        
                                        <td  style="text-align: center;vertical-align: middle;">
                                            <a style="margin-right: 5px;" data-toggle="modal" data-target="#modal_edit<?= $datas->id_item ?>" class="btn bg-orange waves-effect btn-xs"><i class="material-icons">mode_edit</i></a>
                                            <a style="margin-right: 5px;" data-toggle="modal" data-target="#modal_hapus<?= $datas->id_item ?>" class="btn btn-danger waves-effect btn-xs"><i class="material-icons">delete_forever</i></a>
                                        </td>
                                     
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Exportable Table -->
    </div>
</section>


<!-- Modal Edit Kendaraan-->

<?php
    foreach($data as $get){
    $id = $get->id_item;
?>

<div id="modal_edit<?= $id ?>" class="modal fade" role="dialog">
   <div class="modal-dialog">
    <!-- konten modal-->
    <div class="modal-content">
        <!-- heading modal -->
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Update Jasa</h4>
        </div>
        <!-- body modal -->
        <div class="modal-body">
            <form method="POST" action="<?= base_url('pimpinan/jasaUpdateAction/'.$id)?>">
                                <table class="table">
                                <tbody style="border: hidden;">

                                     <tr>
                                        <td class="col-xs-2"><b>Nama Jasa</b></td>
                                        <td class="col-xs-1"><b>:</b></td>
                                        <td>
                                            <input type="text" class="form-control" value="<?= $get->nama_item ?>" name="nama_item" placeholder="Masukkan Nama Kendaraan"  required />
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="col-xs-2"><b>Harga Jasa</b></td>
                                        <td class="col-xs-1"><b>:</b></td>
                                        <td>
                                            <input type="number" class="form-control" value="<?= $get->harga_item ?>" name="harga_item" placeholder="Masukkan Nama Kendaraan" required />
                                        </td>
                                    </tr>

                                </tbody>

                            </table>
                            <button type="submit" class="btn bg-orange waves-effect" id="tombol">
                                        <i class="material-icons">save</i>
                                            <span>Update</span>
                                    </button>
                        </form>
        </div>
    </div>
   </div>
</div>

<?php } ?>

<!-- End Modal Edit Kendaraan-->


<!-- Modal Hapus -->

<?php

foreach($data as $get){

$id = $get->id_item;

?>

<div id="modal_hapus<?= $id ?>" class="modal fade" role="dialog">
   <div class="modal-dialog">
    <!-- konten modal-->
    <div class="modal-content">
        <!-- heading modal -->
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Hapus Jasa</h4>
        </div>
        <!-- body modal -->
        <div class="modal-body">
         Apakah Anda Yakin Ingin Menghapus Data Ini ? 
         <br/><br/>  
        <a href="<?= base_url('pimpinan/jasaHapus/'.$id) ?>" class="btn bg-orange waves-effect">
            <i class="material-icons">delete</i>
                <span>Hapus</span>
        </a>
                        
        </div>
    </div>
   </div>
</div>


<?php } ?>

<!-- End Modal Hapus->


<!--Modal Add Jasa-->

<div id="addJasa" class="modal fade" role="dialog">
   <div class="modal-dialog">
    <!-- konten modal-->
    <div class="modal-content">
        <!-- heading modal -->
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Tambah Jasa</h4>
        </div>
        <!-- body modal -->
        <div class="modal-body">
            <form method="POST" action="<?= base_url()?>pimpinan/jasaCreateAction">
                                <table class="table">
                                <tbody style="border: hidden;">

                                     <tr>
                                        <td class="col-xs-2"><b>Nama Jasa</b></td>
                                        <td class="col-xs-1"><b>:</b></td>
                                        <td>
                                            <input type="text" class="form-control" name="nama_item" placeholder="Masukkan Nama Jasa" required />
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="col-xs-2"><b>Harga Jasa</b></td>
                                        <td class="col-xs-1"><b>:</b></td>
                                        <td>
                                            <input type="number" class="form-control" name="harga_item" placeholder="Masukkan Harga Jasa"  required />
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="col-xs-2"><b>Posisi Jasa</b></td>
                                        <td class="col-xs-1"><b>:</b></td>
                                        <td>
                                            <select id="posisi_jasa" name="posisi_jasa" class="col-md-12 col-lg-12">
                                                <option value="">-- Pilih Posisi Jasa --</option>
                                                <option value="1">Outer Panel</option>
                                                <option value="2">Inner Panel</option>
                                                <option value="3">Extention/Cover</option>
                                                <option value="4">Glasses & Optional</option>
                                                <option value="5">Special</option>
                                                <option value="6">Spot Repair</option>
                                            </select>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="col-xs-2"><b>Jenis Mobil</b></td>
                                        <td class="col-xs-1"><b>:</b></td>
                                        <td>
                                            <select id="jenis_kendaraan" class="col-md-12 col-lg-12" name="id_jenis">
                                                <option value="" style="text-align: center;">-- Pilih Jenis Kendaraan --</option>
                                                <?php
                                                    foreach($dataMobil as $get){
                                                ?>
                                                    <option value="<?= $get->id_jenis ?>" text-align="center"><?= $get->nama_jenis ?></option>
                                                <?php } ?>
                                            </select>
                                        </td>
                                    </tr>

                                </tbody>

                            </table>
                            
                        </form>
        </div>
    </div>
   </div>
</div>

<!-- End Modal Add Jasa-->


<script type="text/javascript">
    function exportTableToExcel(tableID, filename = ''){
    var downloadLink;
    var dataType = 'application/vnd.ms-excel';
    var tableSelect = document.getElementById(tableID);
    var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');
    
    // Specify file name
    filename = filename?filename+'.xls':'excel_data.xls';
    
    // Create download link element
    downloadLink = document.createElement("a");
    
    document.body.appendChild(downloadLink);
    
    if(navigator.msSaveOrOpenBlob){
        var blob = new Blob(['\ufeff', tableHTML], {
            type: dataType
        });
        navigator.msSaveOrOpenBlob( blob, filename);
    }else{
        // Create a link to the file
        downloadLink.href = 'data:' + dataType + ', ' + tableHTML;
    
        // Setting the file name
        downloadLink.download = filename;
        
        //triggering the function
        downloadLink.click();
    }
}
</script>