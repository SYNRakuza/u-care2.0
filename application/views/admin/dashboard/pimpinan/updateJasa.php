
<section class="content">
    <div class="container-fluid">
        <!-- Input -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            Edit Jasa Kendaraan
                        </h2>
                    </div>
                    <style type="text/css">
                        input[type=number]::-webkit-inner-spin-button, 
                                input[type=number]::-webkit-outer-spin-button { 
                                  -webkit-appearance: none; 
                                  margin: 0; 
                    </style>
                    <div class="body">

                        <?php
                            foreach($data as $get){
                        ?>

                        <form method="POST" action="<?= site_url('pimpinan/jasaUpdateAction/'.$get->id_item) ?>">
                            <div class="form-group form-float">
                                <div class="form-line">
                                     <input type="text" name="nama_item" value="<?= $get->nama_item ?>" class="form-control"  maxlength="100" required />
                                        <label class="form-label">Nama Jasa*</label>
                                </div>
                            </div>

                            <div class="form-group form-float">
                                <div class="form-line">
                                     <input type="number" name="harga_item" value="<?= $get->harga_item ?>" class="form-control" required />
                                        <label class="form-label">Harga Jasa*</label>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <button type="submit" class="btn bg-orange waves-effect">
                                    <i class="material-icons">save</i>
                                        <span>UPDATE</span>
                                </button>
                            </div>
                        </form>

                    <?php } ?>

                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Exportable Table -->
</section>
