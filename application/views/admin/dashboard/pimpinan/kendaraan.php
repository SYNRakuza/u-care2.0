<section class="content">
    <div class="container-fluid">

        <!-- Exportable Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            Daftar Kendaraan
                        </h2>
                        <ul class="header-dropdown m-r--5">
                            <li class="dropdown">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">more_vert</i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li>
                                        <a data-toggle='modal' data-target="#addKendaraan">Tambah Kendaraan</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <style type="text/css">
                        /*hilangkan exportable dan menyisakan input search di tabel*/
                        .dt-buttons {
                            display: none;
                        }
                        table tr td {
                            vertical-align: middle !important;
                        }
                    </style>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable js-exportable" >

                                <h6 style="color: orange;">
                                    <?php
                                        $info = $this->session->flashdata('info');
                                        $empty = $this->session->flashdata('not_select');
                                        if(!empty($info)){
                                            echo $info;
                                        }else if(!empty($empty)){
                                            echo $empty;
                                        }
                                    ?>
                                </h6>


                                <thead>
                                <tr>
                                    <th style="text-align: center;vertical-align: middle;">Nama Kendaraan</th>
                                    <th style="text-align: center;vertical-align: middle;">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach($data as $datas){ ?>
                                    <tr>
                                        <td  style="text-align: center;vertical-align: middle;"><?= $datas->nama_jenis ?></td>
                                        <td  style="text-align: center;vertical-align: middle;">
                                            <a style="margin-right: 5px;" data-toggle="modal" data-target="#modal_edit<?= $datas->id_jenis ?>" class="btn bg-orange waves-effect btn-xs"><i class="material-icons">mode_edit</i></a>
                                            <a style="margin-right: 5px;" data-toggle="modal" data-target="#modal_hapus<?= $datas->id_jenis ?>" class="btn btn-danger waves-effect btn-xs"><i class="material-icons">delete_forever</i></a>
                                        </td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Exportable Table -->
    </div>
</section>


<!--Modal Add Kendaraan-->

<div id="addKendaraan" class="modal fade" role="dialog">
   <div class="modal-dialog">
    <!-- konten modal-->
    <div class="modal-content">
        <!-- heading modal -->
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Tambah Kendaraan</h4>
        </div>
        <!-- body modal -->
        <div class="modal-body">
            <form method="POST" action="<?= base_url()?>pimpinan/kendaraanCreateAction">
                                <table class="table">
                                <tbody style="border: hidden;">

                                     <tr>
                                        <td class="col-xs-2"><b>Nama Kendaraan</b></td>
                                        <td class="col-xs-1"><b>:</b></td>
                                        <td>
                                            <input type="text" class="form-control" name="nama_jenis" placeholder="Masukkan Nama Kendaraan" onkeyup="this.value = this.value.toUpperCase()"  required />
                                        </td>
                                    </tr>

                                </tbody>

                            </table>
                            <button type="submit" class="btn bg-orange waves-effect" id="tombol">
                                        <i class="material-icons">save</i>
                                            <span>Tambah</span>
                                    </button>
                        </form>
        </div>
    </div>
   </div>
</div>

<!-- End Modal Add Kendaraan-->

<!-- Modal Edit Kendaraan-->

<?php
    foreach($data as $get){
    $id = $get->id_jenis;
?>

<div id="modal_edit<?= $id ?>" class="modal fade" role="dialog">
   <div class="modal-dialog">
    <!-- konten modal-->
    <div class="modal-content">
        <!-- heading modal -->
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Update Kendaraan</h4>
        </div>
        <!-- body modal -->
        <div class="modal-body">
            <form method="POST" action="<?= base_url('pimpinan/kendaraanUpdateAction/'.$id)?>">
                                <table class="table">
                                <tbody style="border: hidden;">

                                     <tr>
                                        <td class="col-xs-2"><b>Nama Kendaraan</b></td>
                                        <td class="col-xs-1"><b>:</b></td>
                                        <td>
                                            <input type="text" class="form-control" value="<?= $get->nama_jenis ?>" name="nama_jenis" placeholder="Masukkan Nama Kendaraan" onkeyup="this.value = this.value.toUpperCase()"  required />
                                        </td>
                                    </tr>

                                </tbody>

                            </table>
                            <button type="submit" class="btn bg-orange waves-effect" id="tombol">
                                        <i class="material-icons">save</i>
                                            <span>Update</span>
                                    </button>
                        </form>
        </div>
    </div>
   </div>
</div>

<?php } ?>

<!-- End Modal Edit Kendaraan-->


<!-- Modal Hapus -->

<?php

foreach($data as $get){

$id = $get->id_jenis;

?>

<div id="modal_hapus<?= $id ?>" class="modal fade" role="dialog">
   <div class="modal-dialog">
    <!-- konten modal-->
    <div class="modal-content">
        <!-- heading modal -->
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Hapus Kendaraan</h4>
        </div>
        <!-- body modal -->
        <div class="modal-body">
         Apakah Anda Yakin Ingin Menghapus Data Ini ? 
         <br/><br/>  
        <a href="<?= base_url('pimpinan/kendaraanHapus/'.$id) ?>" class="btn bg-orange waves-effect">
            <i class="material-icons">delete</i>
                <span>Hapus</span>
        </a>
                        
        </div>
    </div>
   </div>
</div>


<?php } ?>

<!-- End Modal Hapus->