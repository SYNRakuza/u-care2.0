<section class="content">
    <div class="container-fluid">

        <!-- Exportable Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            SUMMARY LEAD ESTIMASI
                        </h2>
                    </div>
                     <style type="text/css">
                        /*hilangkan exportable dan menyisakan input search di tabel*/
                        .dt-buttons {
                            display: none;
                        }
                    </style>
                        <div class="row clearfix">
                            <div class="col-xs-12 col-sm-3 col-md-9 col-lg-10">
                                <input type="hidden" id="get_url" value="<?= base_url('pimpinan/loadSummary') ?>">
                                <form method="POST" action="<?= site_url('pimpinan/summaryToExcel') ?>">
                                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-5">
                                      <small>From:</small>
                                      <input type="date" id="dateFrom" class="form-control" name="dateFrom" required/>
                                    </div>
                                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-5">
                                      <small>To:</small>
                                      <input type="date" id="dateTo" class="form-control" name="dateTo" required />
                                    </div>
                                    <button type="submit" style="margin-top: 16px;" class="btn bg-deep-orange btn-circle waves-effect waves-circle waves-float searching">
                                        <i class="material-icons">download</i>
                                    </button>                                    
                                </form>
                            </div>
                        </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table id="mytable" class="table table-bordered table-striped table-hover" >
                                <thead>
                                    <tr>
                                        <th style="text-align: center;vertical-align: middle;">No. WO</th>
                                        <th style="text-align: center;vertical-align: middle;">No. Polisi</th>
                                        <th style="text-align: center;vertical-align: middle;">Nama Teknisi</th>
                                        <th style="text-align: center;vertical-align: middle;">Body Repair Lead</th>
                                        <th style="text-align: center;vertical-align: middle;">Preparation Lead</th>
                                        <th style="text-align: center;vertical-align: middle;">Masking Lead</th>
                                        <th style="text-align: center;vertical-align: middle;">Painting Lead</th>
                                        <th style="text-align: center;vertical-align: middle;">Polishing Lead</th>
                                        <th style="text-align: center;vertical-align: middle;">Re-Assembling Lead</th>
                                        <th style="text-align: center;vertical-align: middle;">Washing Lead</th>
                                        <th style="text-align: center;vertical-align: middle;">Final Inspection Lead</th>
                                        <th style="text-align: center;vertical-align: middle;">Total</th>
                                        <th style="text-align: center;vertical-align: middle;">History</th>

                                        <!--<th style="text-align: center;vertical-align: middle;">Update</th>-->
                                    </tr>
                                </thead>
                                <tbody id="dataSummary">
                                  
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Exportable Table -->
    </div>
</section>

<script type="text/javascript">

$(document).ready(function(){

    //setup datatables
    $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
      {
          return {
              "iStart": oSettings._iDisplayStart,
              "iEnd": oSettings.fnDisplayEnd(),
              "iLength": oSettings._iDisplayLength,
              "iTotal": oSettings.fnRecordsTotal(),
              "iFilteredTotal": oSettings.fnRecordsDisplay(),
              "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
              "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
          };
      };

      function timeConvert(n) {
        var num = n;
        var hours = (num / 60);
        var rhours = Math.floor(hours);
        var minutes = (hours - rhours) * 60;
        var rminutes = Math.round(minutes);
        return rhours + " hours " + rminutes + " minutes";
      }

      var table = $("#mytable").dataTable({
          initComplete: function() {
              var api = this.api();
              $('#mytable_filter input')
                  .off('.DT')
                  .on('input.DT', function() {
                      api.search(this.value).draw();
              });
          },
              oLanguage: {
              sProcessing: "loading..."
          },
              pageLength: 15,
              processing: true,
              serverSide: true,
              bLengthChange: true,
              destroy: true,
              ajax: {"url": "<?php echo base_url().'pimpinan/get_summaryLead_json'?>", "type": "POST"},
                    columns: [
                                {"data": "nomor_wo"},
                                {"data": "no_polisi"},
                                {"data": "nama_teknisi"},
                                {"data": null, 'searchable': false, 'orderable': false, render: function(data) {
                                    if (data.body_repair_lead == null) {
                                        return '-';
                                    }
                                    else {
                                        return timeConvert(data.body_repair_lead);
                                    }
                                }},
                                {"data": null, 'searchable': false, 'orderable': false, render: function(data) {
                                    if (data.preparation_lead == null) {
                                        return '-';
                                    }
                                    else {
                                        return timeConvert(data.preparation_lead);
                                    }
                                }},
                                {"data": null, 'searchable': false, 'orderable': false, render: function(data) {
                                    if (data.masking_lead == null) {
                                        return '-';
                                    }
                                    else {
                                        return timeConvert(data.masking_lead);
                                    }
                                }},
                                {"data": null, 'searchable': false, 'orderable': false, render: function(data) {
                                    if (data.painting_lead == null) {
                                        return '-';
                                    }
                                    else {
                                        return timeConvert(data.painting_lead);
                                    }
                                }},
                                {"data": null, 'searchable': false, 'orderable': false, render: function(data) {
                                    if (data.polishing_lead == null) {
                                        return '-';
                                    }
                                    else {
                                        return timeConvert(data.polishing_lead);
                                    }
                                }},
                                {"data": null, 'searchable': false, 'orderable': false, render: function(data) {
                                    if (data.re_assembling_lead == null) {
                                        return '-';
                                    }
                                    else {
                                        return timeConvert(data.re_assembling_lead);
                                    }
                                }},
                                {"data": null, 'searchable': false, 'orderable': false, render: function(data) {
                                    if (data.washing_lead == null) {
                                        return '-';
                                    }
                                    else {
                                        return timeConvert(data.washing_lead);
                                    }
                                }},
                                {"data": null, 'searchable': false, 'orderable': false, render: function(data) {
                                    if (data.final_inspection_lead == null) {
                                        return '-';
                                    }
                                    else {
                                        return timeConvert(data.final_inspection_lead);
                                    }
                                }},
                                {"data": null, 'searchable': false, 'orderable': false, render: function(data) {
                                    if (data.total_lead == null) {
                                        return '-';
                                    }
                                    else {
                                        return timeConvert(data.total_lead);
                                    }
                                }},
                                {"data": "view", 'orderable': false},
                  ],
                  order: [ 0 , 'desc'],
          rowCallback: function(row, data, iDisplayIndex) {
              var info = this.fnPagingInfo();
              var page = info.iPage;
              var length = info.iLength;
              $('td:eq(0)', row).html();
          }

      });

    var dateFrom = '';
    var dateTo   = '';
    var url  = $('#get_url').val();

    //Mulai Ajax Summary
    $(document).on('change','#dateFrom', function(){
        dateFrom  = $(this).val();
        dateTo    = $('#dateTo').val();
        if(dateTo == ''){
            return false;
        }
        // console.log(inOut);
        $.ajax({
            url:url,
            method:"POST",
            data:{dateFrom:dateFrom, dateTo:dateTo},
            dataType:'json',
            success:function(data)
            {
                $('#mytable').DataTable().clear().destroy();
                $('#dataSummary').html(data.listSummary);

                $('#mytable').DataTable({
                    processing: true,
                    bLengthChange: false,
                    destroy: true,
                });
            }
        });
    });

    $(document).on('change','#dateTo', function(){
        dateTo    = $(this).val();
        dateFrom  = $('#dateFrom').val();
        if(dateFrom == ''){
            return false;
        }
        $.ajax({
            url:url,
            method:"POST",
            data:{dateFrom:dateFrom, dateTo:dateTo},
            dataType:'json',
            success:function(data)
            {
                $('#mytable').DataTable().clear().destroy();
                $('#dataSummary').html(data.listSummary);

                $('#mytable').DataTable({
                    processing: true,
                    bLengthChange: false,
                    destroy: true,
                });
            }
        });
    });

      $('#mytable').css({
        'text-align': 'center',
        'vertical-align': 'middle',
      });

});

</script>