<?php
$date = date('d-m-Y');

header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=daftar_jasa.xls");

header("Pragma: no-cache");

header("Expires: 0");
?>

<table border="1" width="100%">
    <thead>
        <th style="text-align:center;vertical-align: middle;">ID</th>
        <th style="text-align: center;vertical-align: middle;">Nama Jasa</th>
        <th style="text-align: center;vertical-align: middle;">Harga Jasa</th>
        <th style="text-align: center;vertical-align: middle;">Kode Posisi Jasa</th>
        <th style="text-align: center;vertical-align: middle;">Posisi Jasa</th>
        <th style="text-align: center;vertical-align: middle;">Jenis Mobil</th>
    </thead>
    <tbody>
        <?php
            $posisi = '';
            foreach($data as $x){
            if($x->posisi_jasa == 1){
                $posisi = 'Outer Panel';
              }else if($x->posisi_jasa == 2){
                $posisi = 'Inner Panel';
              }else if($x->posisi_jasa == 3){
                $posisi = 'Extention/Cover';
              }else if($x->posisi_jasa == 4){
                $posisi = 'Glasses & Optional';
              }else if($x->posisi_jasa == 5){
                $posisi = 'Special';
              }else{
                $posisi = 'Spot Repair';
              }
                
        ?>
        <tr>
            <td style="text-align: center;vertical-align: middle;"><?= $x->id_item ?></td>
            <td style="text-align: center;vertical-align: middle;"><?= $x->nama_item ?></td>
            <td style="text-align: center;vertical-align: middle;"><?= $x->harga_item ?></td>
            <td style="text-align: center;vertical-align: middle;"><?= $x->posisi_jasa ?></td>
            <td style="text-align: center;vertical-align: middle;"><?= $posisi ?></td>
            <td style="text-align: center;vertical-align: middle;"><?= $x->nama_jenis ?></td>
        </tr>

        <?php  } ?>
    </tbody>
</table>

</div>