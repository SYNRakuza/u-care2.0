
<section class="content">
    <div class="container-fluid">
        <!-- Input -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            EDIT PARTS
                        </h2>
                    </div>
                    
                    <style type="text/css">
                        input[type=number]::-webkit-inner-spin-button, 
                                input[type=number]::-webkit-outer-spin-button { 
                                  -webkit-appearance: none; 
                                  margin: 0; 
                    </style>
                    
                    <div class="body">
                        <?php
                                foreach($data as $x){
                            ?>
                        <form method="POST" action="<?= base_url('admin/actionEditParts/'.$x->id_item); ?>">
                            <div class="form-group form-float">
                                <div class="form-line">
                                        <input type="text" class="form-control" name="nama_item" placeholder="" value="<?= $x->nama_item ?>" maxlength="50" required onkeyup="this.value = this.value.toUpperCase()" />
                                        <label class="form-label">Nama Parts</label>
                                </div>
                            </div>

                            <div class="form-group form-float">
                                <div class="form-line">
                                        <input type="number" class="form-control" name="harga_item" placeholder="" value="<?= $x->harga_item ?>" maxlength="10" required onkeyup="this.value = this.value.toUpperCase()" />
                                        <label class="form-label">Harga Parts</label>
                                </div>
                            </div>

                            <?php
                                }
                            ?>

                            <div class="form-group">
                                <button type="submit" class="btn bg-blue waves-effect" id="tombol">
                                    <i class="material-icons">save_alt</i>
                                        <span>SAVE</span>
                                </button>
                            </div>

                            <div id="tes"></div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Exportable Table -->
</section>
