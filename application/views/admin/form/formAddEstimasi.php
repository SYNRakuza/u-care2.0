<?php
    function rp($angka){
        $hasil_rupiah = "Rp " . number_format($angka,0,',','.');
        return $hasil_rupiah;
    }
?>
<section class="content">
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header bg-orange" style="text-align: center;">
                            <h2><b>FORM ESTIMASI PERBAIKAN BODY & PAINT</b></h2>
                            <?php
                                $level = $this->session->userdata('level');
                            ?>
                            <input type="hidden" value="<?= $level ?>" id="getLevel">
                        </div>
                        <style type="text/css">
                            .content {
                                margin: 4px 0 0 0 !important;
                            }
                        /*hilangkan exportable dan menyisakan input search di tabel*/
                            .dt-buttons {
                                display: none;
                            }

                            /*.table-mid {
                                text-align: center !important;
                            }

                            .table-mid tr {
                                text-align: left !important;
                            }
                            .table-customer {
                                display: inline-block !important;
                                max-width: 400px !important;
                                min-width: 400px !important;
                            }*/
                        </style>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <div class="table-mid">
                                        <div style="display: inline-block; margin-right:10px;">
                                            <h4>Data Customer</h4>
                                        </div>
                                        <div style="display: inline-block">
                                            <button type="button" class="btn bg-orange btn-xs waves-effect js-edit-customer-data">edit</button>
                                        </div>
                                        <div class="table-responsive table-customer">
                                            <table class="table">
                                                <tbody>
                                                <?php foreach($dataEstimasi as $datas){
                                                ?>  
                                                    <tr style="display: none;">
                                                        <td width="125"></td>
                                                        <td width="1"></td>
                                                        <td id="id_estimasi"><?= $datas->id_estimasi ?></td>
                                                    </tr>
                                                    <tr style="display: none;">
                                                        <td width="125"></td>
                                                        <td width="1"></td>
                                                        <td id="id_customer"><?= $datas->id_customer ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="125">Nama</td>
                                                        <td width="1">:</td>
                                                        <td id="namaCustomer"><?= $datas->nama_lengkap ?></td>
                                                    </tr>
                                                     <tr>
                                                        <td>No. Telp</td>
                                                        <td>:</td>
                                                        <td id="no_telp"><?= $datas->no_hp ?></td>
                                                    </tr>
                                                     <tr>
                                                        <td>Alamat</td>
                                                        <td>:</td>
                                                        <td id="alamat"><?= $datas->alamat ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Kategori Customer</td>
                                                        <td>:</td>
                                                        <td>
                                                            <div class="demo-checkbox">
                                                                <input type="checkbox" id="md_checkbox_36" class="filled-in chk-col-deep-orange katCus" value="0" <?php if($datas->jenis_customer == '0' ){ echo "checked";} ?> />
                                                                <label for="md_checkbox_36">ASURANSI</label>
                                                                <input type="checkbox" id="md_checkbox_37" class="filled-in chk-col-deep-orange katCus" value="1" <?php if($datas->jenis_customer == '1' ){ echo "checked";} ?>/>
                                                                <label for="md_checkbox_37">TUNAI</label>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr id="input_asuransi" <?php if($datas->jenis_customer == '1' ){ echo "style='display:none;'";}?> >
                                                        <td>Nama Asuransi</td>
                                                        <td>:</td>
                                                        <td>

                                                            <!-- <input type="text" class="form-control" id="namaAsuransi" placeholder="Type..." onkeyup="this.value = this.value.toUpperCase();" <?php if($datas->jenis_customer == '0' ){ echo "value = ".$datas->nama_asuransi."";}?>> -->
                                                            <?php
                                                                foreach($dataEstimasi as $datas){
                                                                    $asuransi = $datas->nama_asuransi;
                                                                }
                                                            ?>
                                                            

                                                            <select class="js-select-2" id="namaAsuransi" style="width: 100%;">
                                                                <option value="">--pilih asuransi--</option>
                                                                <?php
                                                                    foreach ($dataAsuransi as $datas) {
                                                                ?>
                                                                    <option value="<?= $datas->nama_asuransi ?>" <?php if($asuransi == $datas->nama_asuransi){echo "selected";} ?> ><?= $datas->nama_asuransi ?></option>
                                                                <?php } ?>
                                                                <!--
                                                                <option value="ADIRA DINAMIKA" <?=($datas->nama_asuransi == "ADIRA DINAMIKA") ? 'selected' : ''?>>ADIRA DINAMIKA</option>
                                                                <option value="ALLIANZ UTAMA INDONESIA"<?=($datas->nama_asuransi == "ALLIANZ UTAMA INDONESIA") ? 'selected' : ''?>>ALLIANZ UTAMA INDONESIA</option>
                                                                <option value="ASPAN" <?=($datas->nama_asuransi == "ASPAN") ? 'selected' : ''?>>ASPAN</option>
                                                                <option value="ASTRA BUANA" <?=($datas->nama_asuransi == "ASTRA BUANA") ? 'selected' : ''?>>ASTRA BUANA</option>
                                                                <option value="BCA INSURANCE" <?=($datas->nama_asuransi == "BCA INSURANCE") ? 'selected' : ''?>>BCA INSURANCE</option>
                                                                <option value="BINTANG" <?=($datas->nama_asuransi == "BINTANG") ? 'selected' : ''?>>BINTANG</option>
                                                                <option value="BOSOWA PERISCOPE" <?=($datas->nama_asuransi == "BOSOWA PERISCOPE") ? 'selected' : ''?>>BOSOWA PERISCOPE</option>
                                                                <option value="BSAM" <?=($datas->nama_asuransi == "BSAM") ? 'selected' : ''?>>BSAM</option>
                                                                <option value="BRINS" <?=($datas->nama_asuransi == "BRINS") ? 'selected' : ''?>>BRINS</option>
                                                                <option value="CAKRAWALA PROTEKSI INDONESIA" <?=($datas->nama_asuransi == "CAKRAWALA PROTEKSI INDONESIA") ? 'selected' : ''?>>CAKRAWALA PROTEKSI INDONESIA</option>
                                                                <option value="CENTRAL ASIA" <?=($datas->nama_asuransi == "CENTRAL ASIA") ? 'selected' : ''?>>CENTRAL ASIA</option>
                                                                <option value="CHUBB" <?=($datas->nama_asuransi == "CHUBB") ? 'selected' : ''?>>CHUBB</option>
                                                                <option value="HARTA AMAN PRATAMA" <?=($datas->nama_asuransi == "HARTA AMAN PRATAMA") ? 'selected' : ''?>>HARTA AMAN PRATAMA</option>
                                                                <option value="JASA INDONESIA" <?=($datas->nama_asuransi == "JASA INDONESIA") ? 'selected' : ''?>>JASA INDONESIA</option>
                                                                <option value="JASA TANIA" <?=($datas->nama_asuransi == "JASA TANIA") ? 'selected' : ''?>>JASA TANIA</option>
                                                                <option value="KRESNA MITRA TBK" <?=($datas->nama_asuransi == "KRESNA MITRA TBK") ? 'selected' : ''?>>KRESNA MITRA TBK</option>
                                                                <option value="LIPPO GENERAL INSURANCE TBK" <?=($datas->nama_asuransi == "LIPPO GENERAL INSURANCE TBK") ? 'selected' : ''?>>LIPPO GENERAL INSURANCE TBK</option>
                                                                <option value="MANDIRI AXA GENERAL INSURANCE" <?=($datas->nama_asuransi == "MANDIRI AXA GENERAL INSURANCE") ? 'selected' : ''?>>MANDIRI AXA GENERAL INSURANCE</option>
                                                                <option value="MNC ASURANSI INDONESIA" <?=($datas->nama_asuransi == "MNC ASURANSI INDONESIA") ? 'selected' : ''?>>MNC ASURANSI INDONESIA</option>
                                                                <option value="MSIG" <?=($datas->nama_asuransi == "MSIG") ? 'selected' : ''?>>MSIG</option>
                                                                <option value="RAMAYANA" <?=($datas->nama_asuransi == "RAMAYANA") ? 'selected' : ''?>>RAMAYANA</option>
                                                                <option value="SINAR MAS" <?=($datas->nama_asuransi == "SINAR MAS") ? 'selected' : ''?>>SINAR MAS</option>
                                                                <option value="SOMPO JAPAN NIPPONKOA INDONESIA" <?=($datas->nama_asuransi == "SOMPO JAPAN NIPPONKOA INDONESIA") ? 'selected' : ''?>>SOMPO JAPAN NIPPONKOA INDONESIA</option>
                                                            -->
                                                            </select>
                                                        </td>
                                                    </tr>
                                                <?php }
                                                ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <div class="table-mid">
                                        <div style="display: inline-block; margin-right:10px;">
                                            <h4>Data Kendaraan</h4>
                                        </div>
                                        <div style="display: inline-block">
                                            <button type="button" class="btn bg-orange btn-xs waves-effect js-edit-kendaraan-data">edit</button>
                                        </div>
                                        <div class="table-responsive table-customer">
                                            <table class="table">
                                                <tbody>
                                                <?php foreach($dataEstimasi as $datas){
                                                ?>
                                                    <tr>
                                                        <td>No. Polisi</td>
                                                        <td>:</td>
                                                        <td id="no_polisi"><?= $datas->no_polisi ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Jenis Kendaraan</td>
                                                        <td>:</td>
                                                        <td id="nama_jenis"><?= $datas->nama_jenis ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Warna:</td>
                                                        <td>:</td>
                                                        <td id="nama_color"><?= $datas->code_color."-".$datas->nama_color ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>No. Rangka:</td>
                                                        <td>:</td>
                                                        <td id="no_rangka"><?= $datas->no_rangka ?></td>
                                                    </tr>
                                                <?php }
                                                ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <style type="text/css">
                                    .section-info-estimasi .section-title h4{
                                        font-weight: 100 !important;

                                    }
                                    .section-info-estimasi .section-title h2 span{
                                        font-size: 29px !important;
                                        font-weight: 900 !important;

                                    }
                                    .section-info-estimasi .section-title h2{
                                        font-size: 15px !important;
                                        font-weight: 100 !important;

                                    }
                                    .table-info td {
                                        vertical-align: middle !important;
                                    }
                                    .table-info input {
                                        width: 60px !important;
                                        display: inline !important; 
                                    }

                                    .table td {
                                        vertical-align: middle !important; 
                                    }
                                    input[type=number]::-webkit-inner-spin-button, 
                                    input[type=number]::-webkit-outer-spin-button { 
                                      -webkit-appearance: none; 
                                      margin: 0; 
                                    }
                                </style>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <h4>List Jasa</h4>
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs tab-nav-right tab-col-orange" role="tablist">
                                        <li role="presentation" class="active"><a href="#outer" data-toggle="tab">OUTER</a></li>
                                        <li role="presentation" class=""><a href="#inner" data-toggle="tab">INNER</a></li>
                                        <li role="presentation" class=""><a href="#extention-cover" data-toggle="tab">EXTENTION/COVER</a></li>
                                        <li role="presentation" class=""><a href="#glasses-optional" data-toggle="tab">GLASSES & OPTIONAL</a></li>
                                        <li role="presentation" class=""><a href="#special" data-toggle="tab">SPECIAL</a></li>
                                        <li role="presentation" class=""><a href="#spot-repair" data-toggle="tab">SPOT REPAIR</a></li>
                                    </ul>
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane fade in active" id="outer">
                                            <div class="table-responsive">
                                                <table class="table table-hover dataTable js-exportable" id="example">
                                                    <thead style="display: none;">
                                                        <tr>
                                                            <th>Nama Item</th>
                                                            <th>Harga</th>
                                                            <th>Action</th>                                  
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php foreach($dataJasa as $items) {
                                                            if($items->posisi_jasa == '1'){
                                                         ?>   
                                                         <tr>
                                                            <td><?= $items->nama_item ?></td>
                                                            <td><?= rp($items->harga_item) ?></td>
                                                            <td><button type="button" class="btn bg-orange btn-xs waves-effect addJasa" data-row="<?= $items->id_item ?>" id="jasa_<?= $items->id_item ?>"><i class="material-icons">add_circle</i></button>
                                                            </td>
                                                        </tr>
                                                        <?php 
                                                        }
                                                        } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane fade" id="inner">
                                            <div class="table-responsive">
                                                <table class="table dataTable table-hover js-exportable" id="example">
                                                    <thead style="display: none;">
                                                        <tr>
                                                            <th>Nama Item</th>
                                                            <th>Harga</th>
                                                            <th>Action</th>                                  
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php foreach($dataJasa as $items) {
                                                            if($items->posisi_jasa == '2'){
                                                         ?>   
                                                          <tr>
                                                            <td><?= $items->nama_item ?></td>
                                                            <td><?= rp($items->harga_item) ?></td>
                                                            <td><button type="button" class="btn bg-orange btn-xs waves-effect addJasa" data-row="<?= $items->id_item ?>" id="jasa_<?= $items->id_item ?>"><i class="material-icons">add_circle</i></button>
                                                            </td>
                                                        </tr>
                                                        <?php 
                                                        }
                                                        } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane fade" id="extention-cover">
                                            <div class="table-responsive">
                                                <table class="table dataTable table-hover js-exportable" id="example">
                                                    <thead style="display: none;">
                                                        <tr>
                                                            <th>Nama Item</th>
                                                            <th>Harga</th>
                                                            <th>Action</th>                                  
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php foreach($dataJasa as $items) {
                                                            if($items->posisi_jasa == '3'){
                                                         ?>   
                                                          <tr>
                                                            <td><?= $items->nama_item ?></td>
                                                            <td><?= rp($items->harga_item) ?></td>
                                                            <td><button type="button" class="btn bg-orange btn-xs waves-effect addJasa" data-row="<?= $items->id_item ?>" id="jasa_<?= $items->id_item ?>"><i class="material-icons">add_circle</i></button>
                                                            </td>
                                                        </tr>
                                                        <?php 
                                                        }
                                                        } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane fade" id="glasses-optional">
                                            <div class="table-responsive">
                                                <table class="table dataTable table-hover js-exportable" id="example">
                                                    <thead style="display: none;">
                                                        <tr>
                                                            <th>Nama Item</th>
                                                            <th>Harga</th>
                                                            <th>Action</th>                                  
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php foreach($dataJasa as $items) {
                                                            if($items->posisi_jasa == '4'){
                                                         ?>   
                                                          <tr>
                                                            <td><?= $items->nama_item ?></td>
                                                            <td><?= rp($items->harga_item) ?></td>
                                                            <td><button type="button" class="btn bg-orange btn-xs waves-effect addJasa" data-row="<?= $items->id_item ?>" id="jasa_<?= $items->id_item ?>"><i class="material-icons">add_circle</i></button>
                                                            </td>
                                                        </tr>
                                                        <?php 
                                                        }
                                                        } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane fade" id="special">
                                            <div class="table-responsive">
                                                <table class="table dataTable table-hover js-exportable" id="example">
                                                    <thead style="display: none;">
                                                        <tr>
                                                            <th>Nama Item</th>
                                                            <th>Harga</th>
                                                            <th>Action</th>                                  
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php foreach($dataJasa as $items) {
                                                            if($items->posisi_jasa == '5'){
                                                         ?>   
                                                          <tr>
                                                            <td><?= $items->nama_item ?></td>
                                                            <td><?= rp($items->harga_item) ?></td>
                                                            <td><button type="button" class="btn bg-orange btn-xs waves-effect addJasa" data-row="<?= $items->id_item ?>" id="jasa_<?= $items->id_item ?>"><i class="material-icons">add_circle</i></button>
                                                            </td>
                                                        </tr>
                                                        <?php 
                                                        }
                                                        } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane fade" id="spot-repair">
                                            <div class="table-responsive">
                                                <table class="table dataTable table-hover js-exportable" id="example">
                                                    <thead style="display: none;">
                                                        <tr>
                                                            <th>Nama Item</th>
                                                            <th>Harga</th>
                                                            <th>Action</th>                                  
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php foreach($dataJasa as $items) {
                                                            if($items->posisi_jasa == '6'){
                                                         ?>   
                                                          <tr>
                                                            <td><?= $items->nama_item ?></td>
                                                            <td><?= rp($items->harga_item) ?></td>
                                                            <td><button type="button" class="btn bg-orange btn-xs waves-effect addJasa" data-row="<?= $items->id_item ?>" id="jasa_<?= $items->id_item ?>"><i class="material-icons">add_circle</i></button>
                                                            </td>
                                                        </tr>
                                                        <?php 
                                                        }
                                                        } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
        <!-- #END# Example Tab -->
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <h4>Added Jasa</h4>
                                    <div class="table-responsive">
                                        <table class="table" id="addedJasa">
                                             <thead>
                                                <tr>
                                                    <th>Type Jasa</th>
                                                    <th>Nama Jasa</th>
                                                    <th>LT.Proses</th>
                                                    <th>Harga</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody id="tableAddedJasa">
                                                <!-- added Jasa -->
                                            </tbody>
                                        </table>
                                    </div>
                                    <div style="margin-bottom: 10px;">
                                        <button type="button" class="btn bg-orange btn-xs waves-effect" id="addOptJasa"><i class="material-icons">add_circle</i><span>MANUAL</span></button>
                                    </div>
                                    <div class="table-responsive">
                                        <table class="table table-info">
                                            <tbody>
                                                <tr>
                                                    <td width="125">Sub Total Jasa</td>
                                                    <td width="1">:</td>
                                                    <td><b>Rp <span id="subTotalJasa"> 0</span></b></td>
                                                </tr>
                                                <tr>
                                                    <td>Diskon Jasa</td>
                                                    <td>:</td>
                                                    <td>
                                                        <input type="number" min="0" max="100" class="form-control" id="diskonJasa" placeholder="0" value="" style="height: 25px;" />
                                                        <span><b style="display: unset;">%<b> Rp <span id="hargaDiskonJasa"> 0</span></b></b></span>
                                                        <small style="color: red; display: none;" id="not_approval_jasa">Not Approved!</small>
                                                        <small style="color: green; display: none;" id="yes_approval_jasa">Approved!</small>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>PPN 10%</td>
                                                    <td>:</td>
                                                    <td><b>Rp <span id="totalPPNJasa"> 0</span></b></td>
                                                </tr>
                                                <tr>
                                                    <td>Total Jasa</td>
                                                    <td>:</td>
                                                    <td><b>Rp <span id="totalJasa"> 0</span></b></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="table-responsive">
                                        <h4 style="display: inline-block; margin-right: 10px;">LT. Prosess</h4>
                                        <button type="button" class="btn bg-green btn-xs waves-effect js-time-process-action" id="editTimeProcess" data-type="edit">EDIT</button>
                                        <button type="button" class="btn bg-orange btn-xs waves-effect js-time-process-action" id="setTimeProcess" data-type="set" disabled>SET</button>
                                        <table class="table" align="center">
                                            <thead>
                                                <tr>
                                                    <th>Body Repair</th>
                                                    <th>Preparation</th>
                                                    <th>Masking</th>
                                                    <th>Painting</th>
                                                </tr>
                                            </thead>
                                            <?php foreach($dataTimeProcess as $datas){ ?>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <input type="number" class="form-control js-time-process-input" id="lt1" value="<?= $datas->time_body_repair ?>" style="height: 25px; width: 50px;" disabled/>
                                                    </td>
                                                    <td>
                                                        <input type="number" class="form-control js-time-process-input" id="lt2" value="<?= $datas->time_preparation ?>" style="height: 25px; width: 50px;" disabled/>
                                                    </td>
                                                    <td>
                                                        <input type="number" class="form-control js-time-process-input" id="lt3" value="<?= $datas->time_masking ?>" style="height: 25px; width: 50px;" disabled/>
                                                    </td>
                                                    <td>
                                                        <input type="number" class="form-control js-time-process-input" id="lt4" value="<?= $datas->time_painting ?>" style="height: 25px; width: 50px;" disabled/>
                                                    </td>
                                                </tr>
                                            </tbody>
                                            <thead>
                                                <tr>
                                                    <th>Polishing</th>
                                                    <th>Re Assembling</th>
                                                    <th>Washing</th>
                                                    <th>Final Inspection</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>
                                                    <input type="number" class="form-control js-time-process-input" id="lt5" value="<?= $datas->time_polishing ?>" style="height: 25px; width: 50px;" disabled/>
                                                </td>
                                                <td>
                                                    <input type="number" class="form-control js-time-process-input" id="lt6" value="<?= $datas->time_re_assembling ?>" style="height: 25px; width: 50px;" disabled/>
                                                </td>
                                                <td>
                                                    <input type="number" class="form-control js-time-process-input" id="lt7" value="<?= $datas->time_washing ?>" style="height: 25px; width: 50px;" disabled/>
                                                </td>
                                                <td>
                                                    <input type="number" class="form-control js-time-process-input" id="lt8" value="<?= $datas->time_final_inspection ?>" style="height: 25px; width: 50px;" disabled/>
                                                </td>
                                            </tr>
                                            </tbody>
                                            <?php } ?>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- PARTS -->
                            <div class="row clearfix">
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <h4>List Parts</h4>
                                    <div class="row">
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <div class="input-group" style="max-width: 250px;">
                                                    <div class="form-line">
                                                        <input type="text" id="keywords" class="form-control" placeholder="Input No. Parts">
                                                    </div>
                                                    <span class="input-group-addon">
                                                        <button type="button" class="btn bg-deep-orange btn-circle waves-effect waves-circle waves-float searching">
                                                            <i class="material-icons">search</i>
                                                        </button>
                                                    </span>
                                                </div>  
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <form method="post" id="uploadParts" enctype="multipart/form-data">
                                                    <div class="input-group" style="max-width: 250px;">
                                                        <div class="form-line">
                                                            <input type="file" name="file" id="fileParts" class="form-control" required accept=".xls, .xlsx">
                                                        </div>
                                                         <?php foreach($dataEstimasi as $datas){?>
                                                            <input type="hidden" name="id_estimasi" value="<?= $datas->id_estimasi ?>" />
                                                        <?php } ?>
                                                        <span class="input-group-addon">
                                                            <button type="submit" class="btn bg-deep-orange btn-circle waves-effect waves-circle waves-float">
                                                                <i class="material-icons">file_upload</i>
                                                            </button>
                                                        </span>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    <div class="table-responsive">
                                        <table class="table dataTable table-hover" id="example">
                                            <thead style="">
                                                <tr>
                                                    <th>No.Parts</th>
                                                    <th>No.Parts Lama</th>
                                                    <th>Parts</th>
                                                    <th>Harga</th>
                                                    <th>Action</th>                                  
                                                </tr>
                                            </thead>
                                            <tbody id="searchParts">
                                                <!-- <?php foreach($dataParts as $items) {
                                                 ?>   
                                                 <tr>
                                                    <td><?= $items->nomor_parts?></td>
                                                    <td><?= $items->nama_item ?></td>
                                                    <td><?= rp($items->harga_item) ?></td>
                                                    <td><button type="button" class="btn bg-orange btn-xs waves-effect addParts" data-row="<?= $items->id_item ?>" id="parts_<?= $items->id_item ?>"><i class="material-icons">add_circle</i></button>
                                                    </td>
                                                </tr>
                                                <?php 
                                                
                                                } ?> -->
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            
        <!-- #END# Example Tab -->
                                <style type="text/css">
                                    .countQty, .countLT{
                                        height: 25px !important;
                                        text-align: center !important;
                                    }

                                </style>
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <h4>Added Parts</h4>
                                    <div class="table-responsive">
                                        <table class="table" id="addedParts">
                                             <thead>
                                                <tr>
                                                    <th>No. Parts</th>
                                                    <th>Nama Parts</th>
                                                    <th>Harga</th>
                                                    <th width="75">Qty</th>
                                                    <th>Tot</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody id="tableAddedParts">
                                                <!-- added Parts -->
                                            </tbody>
                                        </table>
                                    </div>
                                    <div style="margin-bottom: 10px;">
                                        <button type="button" class="btn bg-orange btn-xs waves-effect" id="addOptParts"><i class="material-icons">add_circle</i><span>MANUAL</span></button>
                                    </div>
                                    <div class="table-responsive">
                                        <table class="table table-info">
                                            <tbody>  
                                                <tr>
                                                    <td width="125">Sub Total Parts</td>
                                                    <td width="1">:</td>
                                                    <td><b>Rp.<span id="subTotalParts"> 0</span></b></td>
                                                </tr>
                                                <tr>
                                                    <td>Diskon Parts</td>
                                                    <td>:</td>
                                                    <td>
                                                        <input type="number" min="0" max="100" class="form-control" id="diskonParts" placeholder="0" value="" style="height: 25px;" />
                                                        <span><b style="display: unset;">% <b>Rp.<span id="hargaDiskonParts"> 0</span></b></b></span>
                                                        <small style="color: red; display: none;" id="not_approval_parts">Not Approved!</small>
                                                        <small style="color: green; display: none;" id="yes_approval_parts">Approved!</small>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>PPN 10%</td>
                                                    <td>:</td>
                                                    <td><b>Rp.<span id="totalPPNParts"> 0</span></b></td>
                                                </tr>
                                                <tr>
                                                    <td>Total Parts</td>
                                                    <td>:</td>
                                                    <td><b>Rp.<span id="totalParts"> 0</span></b></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <div class="table-responsive">
                                            <h4 style="display: inline-block; margin-right: 10px;">Estimasi Kedatangan Parts</h4>
                                            <button type="button" class="btn bg-green btn-xs waves-effect js-estimasi-kedatangan-action" id="editEstimasiKedatangan" data-type="edit">EDIT</button>
                                            <button type="button" class="btn bg-orange btn-xs waves-effect js-estimasi-kedatangan-action" id="setEstimasiKedatangan" data-type="set" disabled>SET</button>
                                            <table class="table" align="center">
                                                <thead>
                                                <tr>
                                                    <th>DEPO</th>
                                                    <th>TAM</th>
                                                    <th>PABRIK</th>
                                                </tr>
                                                </thead>
                                                <?php foreach($dataKedatanganParts as $datas){ ?>
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <input type="number" class="form-control js-estimasi-kedatangan-input" id="kedatangan1" value="<?= $datas->depo ?>" style="height: 25px; width: 50px;" disabled/>
                                                        </td>
                                                        <td>
                                                            <input type="number" class="form-control js-estimasi-kedatangan-input" id="kedatangan2" value="<?= $datas->tam ?>" style="height: 25px; width: 50px;" disabled/>
                                                        </td>
                                                        <td>
                                                            <input type="number" class="form-control js-estimasi-kedatangan-input" id="kedatangan3" value="<?= $datas->pabrik ?>" style="height: 25px; width: 50px;" disabled/>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                                <?php } ?>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <style type="text/css">
                                .centerin {
                                    text-align: center !important;
                                }

                                .centerin-everythinh {
                                    display: inline-block !important;
                                }
                            </style>
                            <div class="row clearfix">
                                <div class="col-md-12">
                                    <div class="centerin">
                                        <div class="centerin-everything">
                                            <button type="button" class="btn bg-orange btn-lg waves-effect submit" style="width: 160px;" ><i class="material-icons">save</i>
                                                <span>SAVE</span>
                                            </button>  
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
            <!-- #END# Advanced Form Example With Validation -->
            <div class="modal fade tgl_modal" id="smallModal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="smallModalLabel">Input Manual Jasa</h4>
                    </div>
                    <div class="modal-body">
                        <form>
                            <div class="form-group">
                                <div class="form-line">
                                    <select class="form-control" id="selectKCGC">
                                        <option value="K+C">K+C</option>
                                        <option value="G+C">G+C</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-line">
                                     <input type="text" class="form-control" id="optNameJasa" placeholder="Nama Jasa" value="" onkeyup="this.value = this.value.toUpperCase();" style="height: 25px;" />
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="number" class="form-control" id="optPriceJasa" placeholder="Harga Jasa" value="" style="height: 25px;" />
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="number" class="form-control" id="optLTJasa" placeholder="LT. Proses" value="" style="height: 25px;" />
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn bg-orange waves-effect insertJasa">INSERT</button>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade tgl_modal" id="smallModal2" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="smallModalLabel">Input Manual Parts</h4>
                    </div>
                    <div class="modal-body">
                        <form>
                            <div class="form-group">
                                <div class="form-line">
                                     <input type="text" class="form-control" id="optNoParts" placeholder="No Part" onkeyup="this.value = this.value.toUpperCase();" value="" style="height: 25px;" />
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-line">
                                     <input type="text" class="form-control" id="optNameParts" placeholder="Nama Part" onkeyup="this.value = this.value.toUpperCase();" value="" style="height: 25px;" />
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="number" class="form-control" id="optPriceParts" placeholder="Harga Part" value="" style="height: 25px;" />
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="number" class="form-control" id="optQtyParts" placeholder="Quantity" value="" style="height: 25px;" />
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn bg-orange waves-effect insertParts">INSERT</button>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                    </div>
                </div>
            </div>
        </div>
            <!-- default Size -->
        <div class="modal fade" id="editDataCustomer" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="defaultModalLabel">Edit Data Customer</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <div class="form-line">
                                <input type="text" class="form-control" id="editNamaCustomer" value="" onkeyup="this.value = this.value.toUpperCase();" style="height: 25px;" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-line">
                                <input type="number" class="form-control" id="editNoTelpCustomer" value="" style="height: 25px;" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-line">
                                <textarea rows="4" class="form-control no-resize" placeholder="" id="editAlamatCustomer" onkeyup="this.value = this.value.toUpperCase();"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn bg-orange waves-effect js-save-customer-data">SAVE</button>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="editDataKendaraan" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="defaultModalLabel">Edit Data Kendaraan</h4>
                    </div>
                    <div class="modal-body">
                        <?php foreach($dataEstimasi as $datas){
                        ?>
                        <div class="form-group">
                            <div class="form-line">
                                <input type="text" class="form-control" id="editNoPolisi" value="<?= $datas->no_polisi ?>" onkeyup="this.value = this.value.toUpperCase();" style="height: 25px;" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="">
                                <select name="id_jenis" id="editJenisKendaraan">
                                    <option value="" disabled selected>--pilih jenis kendaraan--</option>
                                    <?php foreach($dataKendaraan as $kendaraan){ ?>
                                        <option value="<?= $kendaraan->id_jenis ?>" <?= ($datas->id_jenis == $kendaraan->id_jenis ? 'selected' : '') ?>><?= $kendaraan->nama_jenis ?></option>
                                    <?php }?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="">
                                <select name="id_jenis" id="editWarnaKendaraan">
                                    <option value="" disabled>--pilih warna kendaraan--</option>
                                    <?php foreach ($dataColor as $color){ ?>
                                        <option value="<?= $color->id_color ?>" <?= ($datas->id_color == $color->id_color ? 'selected' : '') ?>><?=$color->code_color." - ".$color->nama_color ?></option>
                                    <?php }?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-line">
                                <input type="text" class="form-control" id="editNoRangka" value="<?= $datas->no_rangka ?>" style="height: 25px;" />
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn bg-orange waves-effect js-save-kendaraan-data">SAVE</button>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                    </div>
                </div>
            </div>
        </div>
</section>

<script type="text/javascript">
    $(document).ready(function(){
        var id_estimasi = $('#id_estimasi').text();

        $('.js-select-2').select2({
            placeholder: '--Pilih Asuransi--'
        });
        $('.katCus').on('change', function(){
            $('.katCus').not(this).prop('checked', false);  
            var checked =  $(this).val();
            if(checked == '0'){
                $('#input_asuransi').fadeIn();
            }else{
                $('#input_asuransi').fadeOut();
            }
            console.log(checked);
        });
        function rupiah(angka){
        var number_string = angka.toString(),
                sisa          = number_string.length % 3,
                rupiahs       = number_string.substr(0, sisa),
                ribuan        = number_string.substr(sisa).match(/\d{3}/g);
                    
            if (ribuan) {
                separator = sisa ? '.' : '';
                rupiahs += separator + ribuan.join('.');
            }
            return rupiahs;
        }

        function fetchJasa(){
            var level = $('#getLevel').val();
                            $.ajax({
                                url:"<?php echo base_url();?>service_advisor/fetchJasa",
                                method:"POST",
                                data : {id_estimasi : id_estimasi},
                                dataType:'json',
                                success: function(data)
                                {
                                    data.dataFetchJasa.forEach(function(listItem, index){
                                        $('#jasa_'+listItem.id_item).prop('disabled', true);
                                    });

                                    var hargaDiskonJasa = Math.round(data.subTotalJasa*data.diskonJasa/100);
                                    var totalJasa = Math.round((data.subTotalJasa-hargaDiskonJasa)+((data.subTotalJasa-hargaDiskonJasa)*0.1));

                                    $('#tableAddedJasa').html(data.dataJasa);
                                    $('#subTotalJasa').text(rupiah(data.subTotalJasa));
                                    $('#diskonJasa').val(data.diskonJasa);
                                    $('#hargaDiskonJasa').text(rupiah(hargaDiskonJasa));
                                    $('#totalPPNJasa').text(rupiah(Math.round((data.subTotalJasa-hargaDiskonJasa)*0.1)));
                                    $('#totalJasa').text(rupiah(totalJasa));

                                if(level == 'customer'){

                                   if(data.diskonJasa > 5){
                                        if (data.approvalDiskonJasa == '1') {
                                            $('#yes_approval_jasa').fadeIn();
                                            $('#not_approval_jasa').fadeOut();
                                            $('.submit').prop('disabled', false);
                                        }else{
                                            $('#not_approval_jasa').fadeIn();
                                            $('#yes_approval_jasa').fadeOut();
                                            $('.submit').prop('disabled', true);
                                        }
                                   }else{
                                        $('#yes_approval_jasa').fadeOut();
                                        $('#not_approval_jasa').fadeOut();
                                        $('.submit').prop('disabled', false);   
                                   }
                               }
                                }
                                 
                            });
                        }
        function fetchParts(){
            var level = $('#getLevel').val();
                            $.ajax({
                                url:"<?php echo base_url();?>service_advisor/fetchParts",
                                method:"POST",
                                data : {id_estimasi : id_estimasi},
                                dataType:'json',
                                success: function(data)
                                {
                                    data.dataFetchParts.forEach(function(listItem, index){
                                        $('#parts_'+listItem.id_item).prop('disabled', true);
                                    });

                                    var hargaDiskonParts = Math.round(data.subTotalParts*data.diskonParts/100);
                                    var totalParts = Math.round((data.subTotalParts-hargaDiskonParts)+((data.subTotalParts-hargaDiskonParts)*0.1));

                                    $('#tableAddedParts').html(data.dataParts);
                                    $('#subTotalParts').text(rupiah(data.subTotalParts));
                                    $('#diskonParts').val(data.diskonParts);
                                    $('#hargaDiskonParts').text(rupiah(hargaDiskonParts));
                                    $('#totalPPNParts').text(rupiah(Math.round((data.subTotalParts-hargaDiskonParts)*0.1)));
                                    $('#totalParts').text(rupiah(totalParts));

                            if(level == 'customer'){

                                    if(data.diskonParts > 5){
                                        if (data.approvalDiskonParts == '1') {
                                            $('#yes_approval_parts').fadeIn();
                                            $('#not_approval_parts').fadeOut();
                                            $('.submit').prop('disabled', false);
                                        }else{
                                            $('#not_approval_parts').fadeIn();
                                            $('#yes_approval_parts').fadeOut();
                                            $('.submit').prop('disabled', true);
                                        }
                                   }else{
                                        $('#yes_approval_parts').fadeOut();
                                        $('#not_approval_parts').fadeOut(); 
                                        $('.submit').prop('disabled', false);  
                                   }
                                }
                            }
                                 
                            });
                        }
        fetchJasa();
        fetchParts();

    $('.addJasa').on('click', function(e){
        e.preventDefault();
        var id_item = $(this).data('row');
       
        var data = {
                'id_estimasi'   : id_estimasi,
                'id_item'       : id_item  
                };

        $.ajax({
                    url:"<?php echo base_url();?>service_advisor/addJasa",
                    method:"POST",
                    data : data,
                    success: function(data)
                    {
                        fetchJasa();   
                    }
                     
                }); 
    });

    $(document).on('click','.addParts', function(e){
        e.preventDefault();
        var id_item = $(this).data('row');
        console.log('sdfsds');
        var data = {
                'id_estimasi'   : id_estimasi,
                'id_item'       : id_item  
                };

        $.ajax({
            url:"<?php echo base_url();?>service_advisor/addParts",
            method:"POST",
            data : data,
            success: function(data)
            {
                console.log(data);
                fetchParts();   
            }
        }); 
    });



    $(document).on('click', '.removeJasa', function(){
            var id_item = $(this).data("row");
            var data = {
                'id_estimasi'        : id_estimasi,
                'id_item'            : id_item,  
                };

            swal({
                title: "Are you sure?",
                text: "You will not be able to recover this this Process!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel it!",
                closeOnConfirm: false,
                closeOnCancel: false
            }, function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                            url:"<?php echo base_url();?>service_advisor/removeJasa",
                            method:"POST",
                            data : data,
                            success: function(data)
                            {   
                                swal({
                                    title:"Deleted!",
                                    text :"Your item has been deleted from list.",
                                    type :"success",
                                    showConfirmButton : false,
                                    timer: 1500,
                                });
                                $('#jasa_'+id_item).prop('disabled', false);
                                fetchJasa();
                            }
                        });
                } else {
                    swal({
                            title:"Canceled!",
                            text :"Your item is not remove from list.",
                            type :"error",
                            showConfirmButton : false,
                            timer: 1500,
                        });
                }
            });
        });

    $(document).on('click', '.removeParts', function(){
            var id_item = $(this).data("row");
            var data = {
                'id_estimasi'        : id_estimasi,
                'id_item'            : id_item,  
                };

            swal({
                title: "Are you sure?",
                text: "You will not be able to recover this this Process!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel it!",
                closeOnConfirm: false,
                closeOnCancel: false
            }, function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                            url:"<?php echo base_url();?>service_advisor/removeJasa",
                            method:"POST",
                            data : data,
                            success: function(data)
                            {   
                                swal({
                                    title:"Deleted!",
                                    text :"Your item has been deleted from list.",
                                    type :"success",
                                    showConfirmButton : false,
                                    timer: 1500,
                                });
                                $('#parts_'+id_item).prop('disabled', false);
                                fetchParts();
                            }
                        });
                } else {
                    swal({
                            title:"Canceled!",
                            text :"Your item is not remove from list.",
                            type :"error",
                            showConfirmButton : false,
                            timer: 1500,
                        });
                }
            });
        });


    $('#diskonJasa').change(function(e){
        e.preventDefault();

        // var hargaTot = $('#hargaTotal').text();
        var diskon_jasa   = $('#diskonJasa').val();
        if(diskon_jasa !== '') {
            diskon_jasa   = $('#diskonJasa').val();
        }else{
            diskon_jasa = 0;
        }

        if(diskon_jasa > 100){
            alert('Diskon maksimal 100');
             $('#diskon').val('0');
             diskon_jasa = 0;
        }else{

        }
        
        var data = {
                'id_estimasi'        : id_estimasi,
                'diskon_jasa'        : diskon_jasa,  
                }; 
        
            $.ajax({
                    url:"<?php echo base_url();?>service_advisor/diskonJasa",
                    method:"POST",
                    data : data,
                    success: function(data)
                    {   
                        fetchJasa();
                    }
                     
                });

        
    });

    $('#diskonParts').change(function(e){
        e.preventDefault();

        // var hargaTot = $('#hargaTotal').text();
        var diskon_parts   = $('#diskonParts').val();
        if(diskon_parts !== '') {
            diskon_parts   = $('#diskonParts').val();
        }else{
            diskon_parts = 0;
        }

        if(diskon_parts > 100){
            alert('Diskon maksimal 100');
             $('#diskon').val('0');
             diskon_parts = 0;
        }else{

        }
        
        var data = {
                'id_estimasi'        : id_estimasi,
                'diskon_parts'        : diskon_parts,  
                }; 
        
            $.ajax({
                    url:"<?php echo base_url();?>service_advisor/diskonParts",
                    method:"POST",
                    data : data,
                    success: function(data)
                    {   
                        fetchParts();
                    }
                     
                });

        
    });

    $(document).on('change','.countQty', function(e){
        e.preventDefault();

        // var hargaTot = $('#hargaTotal').text();
        var qty   = $(this).val();
        var id_item = $(this).data('row');
        
        var data = {
                'id_estimasi': id_estimasi,
                'qty'        : qty,
                'id_item'    : id_item,  
                }; 
        console.log(data);
            $.ajax({
                    url:"<?php echo base_url();?>service_advisor/qtyParts",
                    method:"POST",
                    data : data,
                    success: function(data)
                    {   
                        console.log(data);
                        fetchParts();
                    }
                     
                });

        
    });

    $(document).on('change','.countLT', function(e){
        e.preventDefault();

        // var hargaTot = $('#hargaTotal').text();
        var lt   = $(this).val();
        var id_item = $(this).data('row');
        
        var data = {
                'id_estimasi': id_estimasi,
                'lt'         : lt,
                'id_item'    : id_item,  
                }; 
        console.log(data);
            $.ajax({
                    url:"<?php echo base_url();?>service_advisor/ltJasa",
                    method:"POST",
                    data : data,
                    success: function(data)
                    {   
                        console.log(data);
                        fetchJasa();
                    }
                     
                });

        
    });


    $('.submit').on('click', function(e){
        e.preventDefault();
        var jenis_customer = '';
        // $('.katCus:checked').val();
        var katA = $('#md_checkbox_36').is(':checked');
        var katB = $('#md_checkbox_37').is(':checked');
        var nama_asuransi = $('#namaAsuransi').val();

        if(katA == true){
            jenis_customer = 0;
            if(nama_asuransi == ''){
                alert("Input nama Asuransi !");
                return false;
            }

        }else if(katB == true){
            jenis_customer = 1;
        }else{
            alert("Ceklis Salah Satu Kategori Customer !");
            return false;
        }
        var data = {
                'id_estimasi'       : id_estimasi,
                'jenis_customer'    : jenis_customer,
                'nama_asuransi'     : nama_asuransi,
                };
        console.log(data);
        swal({
                title: "Are you sure?",
                text: "You will not be able to cancel this Operation !",
                type: "warning",
                showCancelButton: true,
                closeOnConfirm: false,
                showLoaderOnConfirm: true,
            }, function () {
                
                $.ajax({
                    url:"<?php echo base_url();?>service_advisor/submitEstimasi",
                    method:"POST",
                    data : data,
                    // dataType:'json',
                    success: function(result)
                    {
                        console.log(result);
                        if(result == 'success'){
                            swal("Good job!", "You clicked the button!", "success");
                            setTimeout(function(){
                                   location.href = "<?php echo base_url();?>service_advisor/printEstimasi/"+id_estimasi;
                                }, 1000);
                        }else{
                            swal("Error Saving", " ", "warning");
                        }
                    }
                     
                });
            });
    });

    $('#addOptJasa').on('click', function(e){
        $('#smallModal').modal('show');
    });

    $('#addOptParts').on('click', function(e){
        $('#smallModal2').modal('show');
    });


    $('.insertJasa').on('click', function(e){
        var selectKCGC = $('#selectKCGC').val();
        var nama_item = $('#optNameJasa').val();
        var harga_item = $('#optPriceJasa').val();
        var LT_item = $('#optLTJasa').val();
        if(nama_item == ''){
            alert('Isi Nama Jasa !');
            return false;
        }

        if(harga_item == ''){
            alert('Isi Harga Jasa !');
            return false;
        }

        if(LT_item == ''){
            alert('Isi LT. Jasa !');
            return false;
        }
        var data = {
            'nama_item'     : nama_item,
            'harga_item'    : harga_item,
            'id_estimasi'   : id_estimasi,
            'LT_item'       : LT_item,
            'selectKCGC'    : selectKCGC
        };

        console.log(data);
        $.ajax({
            url:"<?php echo base_url();?>service_advisor/optJasa",
            method:"POST",
            data:data,
            success:function(result)
            {
                console.log(result);
                if(result !== 'success'){
                    swal("Error Insert", "Batas Maksimal 3 Item", "warning");
                }
                fetchJasa();
                $('#optNameJasa').val('');
                $('#optPriceJasa').val('');
                $('#smallModal').modal('hide');   
            }
        });

    });

    $('.insertParts').on('click', function(e){
        var nomor_parts = $('#optNoParts').val();
        var nama_item = $('#optNameParts').val();
        var harga_item = $('#optPriceParts').val();
        var qty = $('#optQtyParts').val();
        if(nomor_parts == '' || nama_item == '' || harga_item == '' || qty == ''){
            alert('Isi semua field !');
            return false;
        }
        var data = {
            'nomor_parts'  : nomor_parts,
            'nama_item'  : nama_item,
            'harga_item' : harga_item,
            'qty'        : qty,
            'id_estimasi': id_estimasi
        };
        
        console.log(data);
        $.ajax({
            url:"<?php echo base_url();?>service_advisor/optParts",
            method:"POST",
            data:data,
            success:function(result)
            {
                console.log(result);
                if(result !== 'success'){
                    swal("Error Insert", "Batas Maksimal 3 Item", "warning");
                }
                fetchParts();
                $('#optNoParts').val('');
                $('#optNameParts').val('');
                $('#optPriceParts').val('');
                $('#optQtyParts').val('');
                $('#smallModal2').modal('hide');   
            }
        });

    });

    $(document).on('click','.searching', function(e){
        e.preventDefault();
        var keywords = $('#keywords').val();
        console.log(keywords);
        $.ajax({
            url:"<?php echo base_url();?>service_advisor/searchParts",
            method:"POST",
            data:{keywords:keywords},
            dataType:'json',
            success:function(data)
            {
                console.log(data);
                $('#searchParts').html(data.listEstimasi);
            }
        });
    });

    $('#uploadParts').on('submit', function(event){
        event.preventDefault();
        $.ajax({
            url:"<?php echo base_url();?>service_advisor/uploadParts",
            method:"POST",
            data:new FormData(this),
            contentType:false,
            cache:false,
            processData:false,
            success: function(result)
            {
                $('#fileParts').val('');
                if(result == 'success'){
                    swal("Good job!", "Parts Berhasil Ditambahkan!", "success");
                }else{
                    swal("Error Saving", " ", "warning");
                }
                fetchParts();
            }
             
        });
    });

    $(document).on('click','.js-time-process-action', function(e){
        e.preventDefault();
        var type = $(this).data('type');
        if(type == 'edit'){
            $('.js-time-process-input').prop('disabled', false);
            $(this).prop('disabled', true);
            $('#setTimeProcess').prop('disabled', false);
        }else{
            var lt1 = $('#lt1').val();
            var lt2 = $('#lt2').val();
            var lt3 = $('#lt3').val();
            var lt4 = $('#lt4').val();
            var lt5 = $('#lt5').val();
            var lt6 = $('#lt6').val();
            var lt7 = $('#lt7').val();
            var lt8 = $('#lt8').val();

            var data = {
                'id_estimasi'   : id_estimasi,
                'lt1'           : lt1,
                'lt2'           : lt2,
                'lt3'           : lt3,
                'lt4'           : lt4,
                'lt5'           : lt5,
                'lt6'           : lt6,
                'lt7'           : lt7,
                'lt8'           : lt8
            }
            $.ajax({
                url:"<?php echo base_url();?>service_advisor/updateTimeProcess",
                method:"POST",
                data:data,
                success:function(data)
                {
                    console.log(data);
                    $('#setTimeProcess').prop('disabled', true);
                    $('#editTimeProcess').prop('disabled', false);
                    $('.js-time-process-input').prop('disabled', true);
                }
            });
        }
    });

    $(document).on('click','.js-estimasi-kedatangan-action', function(e){
        e.preventDefault();
        var type = $(this).data('type');
        if(type == 'edit'){
            $('.js-estimasi-kedatangan-input').prop('disabled', false);
            $(this).prop('disabled', true);
            $('#setEstimasiKedatangan').prop('disabled', false);
        }else{
            var kedatangan1 = $('#kedatangan1').val();
            var kedatangan2 = $('#kedatangan2').val();
            var kedatangan3 = $('#kedatangan3').val();

            var data = {
                'id_estimasi'   : id_estimasi,
                'depo'          : kedatangan1,
                'tam'           : kedatangan2,
                'pabrik'        : kedatangan3,
            }
            $.ajax({
                url:"<?php echo base_url();?>service_advisor/updateKedatanganParts",
                method:"POST",
                data:data,
                success:function(data)
                {
                    console.log(data);
                    $('#setEstimasiKedatangan').prop('disabled', true);
                    $('#editEstimasiKedatangan').prop('disabled', false);
                    $('.js-estimasi-kedatangan-input').prop('disabled', true);
                }
            });
        }
    });

    $('.js-edit-customer-data').click(function (e) {
        e.preventDefault();
        $.ajax({
            url:"<?php echo base_url();?>service_advisor/getDataCustomer",
            method:"POST",
            data:{id_estimasi:id_estimasi},
            dataType:"json",
            success:function(data)
            {
                $('#editDataCustomer').modal('show');
                $('#editNamaCustomer').val(data.dataEstimasi[0].nama_lengkap);
                $('#editNoTelpCustomer').val(data.dataEstimasi[0].no_hp);
                $('#editAlamatCustomer').val(data.dataEstimasi[0].alamat);
                $('.js-save-customer-data').attr('data-id',data.dataEstimasi[0].id_customer);
                console.log(data);
            }
        });
    });

    $('.js-save-customer-data').click(function (e) {
        e.preventDefault();
        var id_customer = $(this).data('id');
        var nama_customer = $('#editNamaCustomer').val();
        var no_telp       = $('#editNoTelpCustomer').val();
        var alamat         = $('#editAlamatCustomer').val();
        var data = {
            'id_customer': id_customer,
            'nama_customer': nama_customer,
            'no_telp': no_telp,
            'alamat': alamat
        };
        $.ajax({
            url:"<?php echo base_url();?>service_advisor/saveDataCustomer",
            method:"POST",
            data:data,
            success:function(result)
            {
                console.log(result);
                $('#namaCustomer').text(nama_customer);
                $('#no_telp').text(no_telp);
                $('#alamat').text(alamat);
                $('#editNamaCustomer').val('');
                $('#editNoTelpCustomer').val('');
                $('#editAlamatCustomer').val('');
                $('#editDataCustomer').modal('hide');
            }
        });
    });

    $('.js-edit-kendaraan-data').click(function (e) {
        e.preventDefault();
        $('#editDataKendaraan').modal('show');
    });

    $('.js-save-kendaraan-data').click(function (e) {
        e.preventDefault();
        var no_polisi = $('#editNoPolisi').val();
        var jenis_kendaraan = $('#editJenisKendaraan').val();
        var warna        = $('#editWarnaKendaraan').val();
        var no_rangka   = $('#editNoRangka').val();
        var data = {
            'id_estimasi' : id_estimasi,
            'no_polisi': no_polisi,
            'jenis_kendaraan': jenis_kendaraan,
            'warna': warna,
            'no_rangka': no_rangka,
        };
        $.ajax({
            url:"<?php echo base_url();?>service_advisor/saveDataKendaraan",
            method:"POST",
            data:data,
            success:function(result)
            {
                $('#editDataKendaraan').modal('hide');
                window.location.reload();
            }
        });
    });
});
</script>