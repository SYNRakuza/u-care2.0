
<section class="content">
        <div class="container-fluid">
            <!-- Input -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Form Tambah Notifikasi
                            </h2>
                        </div>
                        <style type="text/css">
                            input[type=number]::-webkit-inner-spin-button,
                                    input[type=number]::-webkit-outer-spin-button {
                                      -webkit-appearance: none;
                                      margin: 0;
                        </style>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-sm-12">
                                    <form method="POST" action="<?= base_url('partsman/notifCreateAction') ?>">
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <input type="text" class="form-control" name="title" placeholder="" value="" required />
                                                <label class="form-label">Title</label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <p>To:</p>
                                            <select id="optgroup" class="ms" multiple="multiple" name="receiver[]" required>
                                                <!-- <optgroup label="Pilih Penerima"> -->
                                                    <option selected value="1">admin</option>
                                                <?php foreach($dataUser as $datas){
                                                ?>
                                                    <?php if($datas->id_user > "1"){?>
                                                    <option value="<?= $datas->id_user ?>"><?= $datas->nama_lengkap_user ?></option>
                                                    <?php }?>
                                                <?php }?>
                                                <!-- </optgroup> -->
                                            </select>
                                        </div>

                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <textarea rows="4" class="form-control no-resize" name="message" placeholder="Message..." required></textarea>
                                                <label class="form-label"></label>
                                            </div>
                                        </div>
                                        <i style="color: green;"><?php echo $this->session->flashdata('success');?></i>
                                        <div class="form-group">
                                            <button type="submit" class="btn bg-orange waves-effect">
                                                <i class="material-icons">save</i>
                                                <span>SAVE</span>
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Input -->
        </div>
    </section>

