<section class="content">
        <div class="container-fluid">
            <!-- Input -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                EDIT DATA CUSTOMER
                            </h2>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-sm-12">
                                    <?php
                                    	foreach ($data as $datas) {
                                    ?>
                                        <form method="POST" action="<?= base_url('service_advisor/customerUpdateAction/'.$datas->id_customer) ?>">
                                        	<div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control" name="nama_lengkap" placeholder="Jenis Paket" value="<?= $datas->nama_lengkap ?>" />
                                            </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" class="form-control" name="no_hp" placeholder="No. Hp" value="<?= $datas->no_hp ?>" />
                                                </div>
                                            </div>
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <textarea rows="4" class="form-control no-resize" name="alamat" placeholder=""><?php echo($datas->alamat)?></textarea>
                                                </div>
                                            </div>
	                                        <div class="form-group">
	                                            <button type="submit" class="btn bg-orange waves-effect">
	                                                <i class="material-icons">save</i>
	                                                <span>Update</span>
	                                            </button>
	                                        </div>
	                                    </form>
                                    <?php
                                    	}
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Input -->
        </div>
    </section>
