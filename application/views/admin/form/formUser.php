<section class="content">
    <div class="container-fluid">
        <!-- Input -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            TAMBAH USER
                        </h2>
                    </div>
                    <style type="text/css">
                        input[type=number]::-webkit-inner-spin-button, 
                                input[type=number]::-webkit-outer-spin-button { 
                                  -webkit-appearance: none; 
                                  margin: 0; 
                    </style>
                    <div class="body">
                        <form action="<?=base_url('pimpinan/userCreateAction')?>" method="POST" id="formUser" name="createUser">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" class="form-control" name="nama_lengkap_user" placeholder="" value="" maxlength="100" required />
                                    <label class="form-label">Nama Lengkap User*</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" class="form-control" name="no_tlpUser" placeholder="" value="" maxlength="20" required />
                                    <label class="form-label">Nomor Hp*</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" class="form-control" name="username" placeholder="" value="" maxlength="100" required />
                                    <label class="form-label">Username*</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" class="form-control" name="password" placeholder="" value="" maxlength="100" required />
                                    <label class="form-label">Password*</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <label class="form-label">Level User</label>
                                    <select class="form-control show-tick" id="level" name="level" onchange="getLevel()" required>
                                        <option value="" selected disabled>-- Pilih Level--</option>
                                        <option value="admin">Admin</option>
                                        <option value="service advisor">Service Advisor</option>
                                        <option value="partsman">Partsman</option>
                                        <option value="ptm">Pembagi Tugas Mekanik</option>
                                        <option value="teknisi">Teknisi</option>
                                        <option value="qc">Foreman</option>
                                    </select>
                            </div>
                            <div class="form-group form-float" id="qcGroup">
                                <label class="form-label">Foreman Group</label>
                                    <select class="form-control show-tick" id="sublevel" name="sublevel">
                                        <option value="" selected disabled>-- Pilih Group--</option>
                                        <option value="body repair">Body Repair</option>
                                        <option value="preparation">Preparation</option>
                                        <option value="masking">Masking</option>
                                        <option value="painting">Painting</option>
                                        <option value="polishing">Polishing</option>
                                        <option value="re-assembling">Re-Assembling</option>
                                        <option value="washing">Washing</option>
                                        <option value="final inspection">Final Inspection</option>
                                    </select>
                            </div>
                            <div class="form-group">
                                <button type="button" class="btn bg-orange waves-effect js-create-user">
                                    <i class="material-icons">save</i>
                                        <span>SIMPAN</span>
                                </button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
</section>

<script type="text/javascript">

function initialize() {
var d = document.getElementById("qcGroup");
 d.style.display = "none";
}

initialize();

function getLevel(){
             
            if (document.createUser.level.value =='qc')
            {
                var d = document.getElementById("qcGroup");
                d.style.display = "block";
                document.getElementById("sublevel").required = true;
            }
            else
            {
                var d = document.getElementById("qcGroup");
                d.style.display = "none";    
            }
            
}

</script>

