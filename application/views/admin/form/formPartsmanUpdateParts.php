<section class="content">
    <div class="container-fluid">
        <!-- Exportable Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            Detail Update Parts
                        </h2>
                    </div>
                    <style type="text/css">
                        /*hilangkan exportable dan menyisakan input search di tabel*/
                        .dt-buttons {
                            display: none;
                        }
                    </style>
                    <div class="body">
                        <div class="table-responsive">
                            <form method="POST">
                            <table class="table">
                                <tbody>
                                    <?php
                                        foreach($data as $x){
                                    ?>
                                    <tr>
                                        <td><b>Kode Lokasi</b></td>
                                        <td><b>:</b></td>
                                        <td><b><?= $x->lokasi_order ?></b></td>
                                    </tr>
                                    <tr>
                                        <td class="col-lg-3 col-md-3 col-sm-3 col-xs-3"><b>Status Barang</b></td>
                                        <td class="col-lg-1 col-md-1 col-sm-1 col-xs-1"><b>:</b></td>
                                        <td>
                                            <?php
                                                if($x->status_barang == NULL){
                                            ?>
                                                <select class="form-control show-tick" name="status_barang">
                                                    <option value="" selected="selected">Not Yet</option>
                                                    <option value="0">Ready</option>
                                                </select>

                                            <?php
                                                }else{?>
                                                <select class="form-control show-tick" name="status_barang">
                                                    <option value="NULL">Not Yet</option>
                                                    <option value="0" selected="selected">Ready</option>
                                                </select>

                                            <?php } ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><b>Estimation Time Arrived</b></td>
                                        <td><b>:</b></td>
                                        <td><input type="date" name="eta" value="<?= $x->eta ?>" class="datepicker form-control"></td>
                                    </tr>

                                    <tr>
                                        <td><b>Actual Time of Arived</b></td>
                                        <td><b>:</b></td>
                                        <td>
                                             <input type="date" name="ata" value="<?= $x->ata ?>" class="datepicker form-control">
                                        </td>
                                    </tr>

                                    <tr>
                                        <td><b>Tanggal Pengambilan</b></td>
                                        <td><b>:</b></td>
                                        <td>
                                          <input type="date" name="tgl_pengambilan" value="<?= $x->tgl_pengambilan ?>" class="datepicker form-control">
                                        </td>
                                    </tr>

                                    <tr>
                                        <td><b>Nama Pengambil</b></td>
                                        <td><b>:</b></td>
                                        <td>
                                            <input type="text" name="pengambil" value="<?= $x->pengambil ?>" class="form-control" onkeyup="this.value = this.value.toUpperCase()" placeholder="Masukkan Nama Pengambil" />
                                        </td>
                                    </tr>

                                </tbody>

                            </table>
                            <input type="submit" formaction="<?= base_url('partsman/updatePartAction/'.$x->id_detail.'/'.$x->id_estimasi.'/') ?>"  name="" class="btn btn-warning waves-effect" value="Update">
                            <a href="<?= base_url('partsman/reOrder/'.$x->id_detail.'/'.$x->id_estimasi.'/') ?>" class="btn btn-success waves-effect">Order Kembali</a>
                             <?php } ?>
                         </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Exportable Table -->

        <!-- Exportable Table -->
    </div>
</section>
