<section class="content">
    <div class="container-fluid">
        <!-- Input -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            Tambah Jenis Kendaraan
                        </h2>
                    </div>
                    <div class="body">
                        <form method="POST" action="<?= base_url('pimpinan/createKendaraanAction')?>" id="formKendaraan">
                            <div class="form-group form-float">
                                <div class="form-line">
                                     <input type="text" class="form-control" name="nama_jenis" placeholder="" value="" maxlength="50" onkeyup="this.value = this.value.toUpperCase()" required/>
                                    <label class="form-label">Jenis Kendaraan*</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn bg-orange waves-effect js-create-kendaraan">
                                    <i class="material-icons">save_alt</i>
                                        <span>SAVE</span>
                                </button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
</section>
