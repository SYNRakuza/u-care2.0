
<section class="content">
    <div class="container-fluid">
        <!-- Input -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            UPDATE JENIS KENDARAAN
                        </h2>
                    </div>
                    <style type="text/css">
                        input[type=number]::-webkit-inner-spin-button, 
                                input[type=number]::-webkit-outer-spin-button { 
                                  -webkit-appearance: none; 
                                  margin: 0; 
                    </style>
                    <div class="body">
                            <?php
                                foreach($data as $x){
                            ?>
                        <form method="POST" action="<?= base_url('admin/actionUpdateKendaraan/'.$x->id_jenis); ?>">
                            <div class="form-group form-float">
                                <div class="form-line">
                                         <input type="text" class="form-control" name="nama_jenis" placeholder="" value="<?= $x->nama_jenis ?>" maxlength="50" required onkeyup="this.value = this.value.toUpperCase()" />
                                            <label class="form-label">Nama Kendaraan</label>
                                </div>
                            </div>

                            <?php
                                }
                            ?>

                            <div class="form-group">
                                <button type="submit" class="btn bg-blue waves-effect" id="tombol">
                                    <i class="material-icons">update</i>
                                        <span>UPDATE</span>
                                </button>
                            </div>

                            <div id="tes"></div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Exportable Table -->
</section>
