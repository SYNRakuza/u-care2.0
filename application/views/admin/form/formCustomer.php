
<section class="content">
        <div class="container-fluid">
            <!-- Input -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                FORM TAMBAH CUSTOMER
                            </h2>
                        </div>
                        <style type="text/css">
                            input[type=number]::-webkit-inner-spin-button, 
                                    input[type=number]::-webkit-outer-spin-button { 
                                      -webkit-appearance: none; 
                                      margin: 0; 
                        </style>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-sm-12">
                                    <form method="POST" action="<?= base_url('service_advisor/customerCreateAction') ?>">
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <input type="text" class="form-control" name="nama_lengkap" placeholder="" value="" onkeyup="this.value = this.value.toUpperCase();" pattern="[A-Za-z- ]+" required />
                                                <label class="form-label">Nama Lengkap*</label>
                                            </div>
                                        </div>
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <input type="number" class="form-control" name="no_hp" placeholder="" value="" required />
                                                <label class="form-label">No. Hp*</label>
                                            </div>
                                        </div>
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <textarea rows="4" class="form-control no-resize" name="alamat" placeholder="" required></textarea>
                                                <label class="form-label">Alamat</label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <button type="submit" class="btn bg-orange waves-effect">
                                                <i class="material-icons">save</i>
                                                <span>SAVE</span>
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Input -->
        </div>
    </section>

