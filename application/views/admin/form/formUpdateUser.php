
<section class="content">
    <div class="container-fluid">
        <!-- Input -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            EDIT DATA USER
                        </h2>
                    </div>
                    <style type="text/css">
                        input[type=number]::-webkit-inner-spin-button, 
                                input[type=number]::-webkit-outer-spin-button { 
                                  -webkit-appearance: none; 
                                  margin: 0; 
                    </style>
                    <div class="body">
                        <?php foreach ($data as $datas) {
                        ?>
                        <form>
                            <div class="form-group form-float">
                                <div class="form-line">
                                     <input type="text" class="form-control" id="nama_lengkap_user" placeholder="" value="<?= $datas->nama_lengkap_user?>" maxlength="100" required />
                                        <label class="form-label">Nama Lengkap User*</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                     <input type="text" class="form-control" id="no_tlpUser" placeholder="" value="<?= $datas->no_tlpUser?>" maxlength="20" required />
                                        <label class="form-label">Nomor Hp*</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                     <input type="text" class="form-control" id="username" placeholder="" value="<?= $datas->username?>" maxlength="100" required />
                                        <label class="form-label">Username*</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                     <input type="text" class="form-control" id="password" placeholder="" value="<?= $datas->password?>" maxlength="100" required />
                                        <label class="form-label">Password*</label>
                                </div>
                            </div>
                            <div class="input-group form-group form-float">
                                <label class="form-label">Level User</label>
                                <select class="form-control show-tick" id="level">
                                    <optgroup label="Current Level :" class="opsi">
                                    <option value="<?= $datas->level ?>"><?= $datas->level ?></option>
                                    </optgroup>
                                    <!-- <option>-- Pilih Level--</option> -->
                                    <optgroup label="New Level :" class="opsi">
                                        <option value="admin">Admin</option>
                                        <option value="service advisor">Service Advisor</option>
                                        <option value="partsman">Partsman</option>
                                        <option value="ptm">Pembagi Tugas Mekanik</option>
                                        <option value="teknisi">Teknisi</option>
                                    </optgroup>
                                </select>
                            </div>
                            <div class="form-group">
                                <button type="button" class="btn bg-orange waves-effect js-update-user" data-id="<?= $datas->id_user ?>"
                                        data-action="<?=base_url('admin/userUpdateAction')?>"
                                        data-redirect="<?=base_url('pimpinan/user')?>">
                                    <i class="material-icons">save</i>
                                        <span>UPDATE</span>
                                </button>
                            </div>

                            <div id="tes"></div>
                        </form>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Exportable Table -->
</section>
<script type="text/javascript">
    $('#tombol').on('click', function(){
        
        var id_user             = $(this).data('row');
        var nama_lengkap_user   = $('#nama_lengkap_user').val();
        var no_tlpUser          = $('#no_tlpUser').val();
        var username            = $('#username').val();
        var password            = $('#password').val();
        var level               = $('#level').val();

        swal({
                title: "Apakah Anda Yakin?",
                text: "Pastikan Data User anda Sudah Terisi Dengan Benar!",
                type: "warning",
                showCancelButton: true,
                closeOnConfirm: false,
                showLoaderOnConfirm: true,
            }, function () {
                var data = {
                'id_user'              : id_user,
                'nama_lengkap_user'    : nama_lengkap_user,
                'no_tlpUser'           : no_tlpUser,
                'username'             : username,
                'password'             : password,
                'level'                : level
                };

                $.ajax({
                    url:"<?php echo base_url();?>admin/userUpdateAction",
                    method:"POST",
                    data : data,
                    success: function(result)
                    {
                        console.log(result);
                        if(result == 'success'){
                            swal("Berhasil!", "User Baru Telah Ditambahkan", "success");
                            setTimeout(function(){
                                   window.location="<?php echo base_url();?>pimpinan/user";
                                }, 1000);
                        }else{
                            swal("Gagal!", "Terjadi kesalahan saat mengirim data!", "warning");
                            setTimeout(function(){
                                   window.location.reload(1);
                                }, 3000);
                        }
                        console.log('data');   
                    }
                });
            });
        });
</script>