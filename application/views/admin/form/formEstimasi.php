<section class="content">
        <div class="container-fluid">
            <!-- div class="logo" align="center">
                <img src="<?php echo base_url()?>assets/images/logo_orange.svg" alt="Logo Confie" width="150" style="margin-bottom: 5px; " >
                <br />
                <small style="color: #333;">Coworking Space</small>
            </div>
            <br /> -->
            <!-- Advanced Form Example With Validation -->
            <div class="row clearfix">
                <div class="col-lg-3 col-md-2 col-sm-2">
                </div>
                <div class="col-lg-6 col-md-8 col-sm-8 col-xs-12">
                    <div class="card">
                        <div class="header bg-orange" style="text-align: center;">
                            <h2>DATA ENTRY CUSTOMER</h2>
                        </div>
                        <style type="text/css">
                            .content {
                                margin: 25px 0 0 0 !important;
                            }
                            input[type=number]::-webkit-inner-spin-button, 
                                    input[type=number]::-webkit-outer-spin-button { 
                                      -webkit-appearance: none; 
                                      margin: 0; 
                        </style>
                        <div class="body">
                            <form method="POST" action="<?= base_url('service_advisor/estimasiCreateAction') ?>" id="formCustomer">
                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 form-control-label">
                                        <label for="email_address_2">Search:</label>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                                        <div class="input-group">
                                            <div class="form-line">
                                                <input type="text" id="keywords" class="form-control" placeholder="Input No. Polisi" onkeyup="this.value = this.value.toUpperCase();">
                                            </div>
                                            <span class="input-group-addon">
                                                <button type="button" class="btn bg-deep-orange btn-circle waves-effect waves-circle waves-float searching">
                                                    <i class="material-icons">search</i>
                                                </button>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                    <div class="form-group">
                                        <h5>CREATE NEW CUSTOMER</h5>
                                        <div class="switch">
                                            <label>NO
                                                <input type="hidden"  name="newCustomer" value="No" />
                                                <input type="checkbox"  name="newCustomer" id="checkbox" value="Yes" />
                                                <span class="lever switch-col-orange"></span>YES</label>
                                        </div>
                                    </div>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <b>Nama Lengkap*</b>
                                             <input type="text" class="form-control" name="nama_lengkap" id="nama" disabled onkeyup="this.value = this.value.toUpperCase();" pattern="[A-Za-z- ]+" />
                                        </div>
                                    </div>
                                    <div class="form-group form-float" style="display: none;">
                                        <div class="form-line">
                                            <b>Id</b>
                                             <input type="text" class="form-control" name="idCustomer" id="idCustomer" />
                                        </div>
                                    </div>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <b>No. Telp*</b>
                                            <input type="number" class="form-control" name="no_hp" id="tlp" disabled />
                                        </div>
                                    </div>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <b>Alamat</b>
                                            <textarea rows="4" class="form-control no-resize" name="alamat" placeholder="" id="alamat" onkeyup="this.value = this.value.toUpperCase();" disabled ></textarea>
                                        </div>
                                    </div>
                                    <style type="text/css">
                                        .dropup .dropdown-menu, .navbar-fixed-bottom .dropdown .dropdown-menu {
                                                bottom: 0%;
                                            }
                                    </style>
                                    <div class="row clearfix">
                                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 form-control-label">
                                            <label>No Polisi</label>
                                        </div>
                                        <div class="col-lg-10 col-md-10 col-sm-8 col-xs-8">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" class="form-control" name="no_polisi" id="no_polisi" onkeyup="this.value = this.value.toUpperCase();"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 form-control-label">
                                            <label>Jenis Kendaraan</label>
                                        </div>
                                        <div class="col-lg-10 col-md-10 col-sm-8 col-xs-8">
                                            <div class="form-group">
                                                <div class="form-control">
                                                    <select name="id_jenis" id="jenis_kendaraan">
                                                        <option value="">--pilih jenis kendaraan--</option>
                                                    <?php foreach($data as $datas){
                                                    ?>
                                                        <option value="<?= $datas->id_jenis ?>"><?= $datas->nama_jenis ?></option>
                                                    <?php }?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 form-control-label">
                                            <label>Warna</label>
                                        </div>
                                        <div class="col-lg-10 col-md-10 col-sm-8 col-xs-8">
                                            <select class="js-select-2" id="color" name="id_color" style="width: 100%;">
                                                <option value="">--pilih warna--</option>
                                                <?php foreach($dataColor as $datas){?>
                                                    <option value="<?=$datas->id_color?>"><?=$datas->code_color." - ".$datas->nama_color?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 form-control-label">
                                            <label>No. Rangka</label>
                                        </div>
                                        <div id="msg" style="color:red;"> </div>
                                        <input type="text" name="checkNoRangka" hidden disabled id="checkNoRangka">
                                        <div class="col-lg-10 col-md-10 col-sm-8 col-xs-8">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" disabled class="form-control" name="no_rangka" id="no_rangka" onkeyup="this.value = this.value.toUpperCase();"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn bg-orange waves-effect">
                                            <i class="material-icons">queue_play_next</i>
                                            <span>SUBMIT</span>
                                        </button>
                                    </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-2 col-sm-2">
                </div>
            </div>
            <!-- #END# Advanced Form Example With Validation -->
            <!-- default Size -->
        
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="defaultModalLabel">Searching Results:</h4>
                            </div>
                            <div class="modal-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped table-hover listCustomer">
                                        <thead>
                                            <tr>
                                                <th>No. Polisi</th>
                                                <th>Nama</th>
                                                <th>No. Tlp</th>
                                                <th style="display: none;">Alamat</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="modal-footer">    
                                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                            </div>
                        </div>
                    </div>
                </div>
               
    </section>
<script type="text/javascript">
    $(document).ready(function(){

         $('#no_rangka').keyup(function(){
            var no_rangka = $('#no_rangka').val();
            $.ajax({
                url: '<?php echo base_url() . 'service_advisor/checkRangka'?>',
                type: "POST",
                data: "no_rangka="+no_rangka,                     
                success: function(response){
                        $('#msg').html(response);
                        $('#checkNoRangka').val(response);
                }
            });
        });

        $('#color').select2({
            placeholder: '--Pilih Warna--'
        });
        $('#formCustomer').submit(function(){
            var nama = $('#nama').val();
            var no_polisi = $('#no_polisi').val();
            var jenis_kendaraan = $('#jenis_kendaraan').val();
            var check_no_rangka = $('#checkNoRangka').val();
            if(nama == '') {
                alert('Nama Lengkap tidak boleh Kosong !');
                return false;
            }

            if(jenis_kendaraan == ''){
                alert('Pilih Jenis Kendaraan !');
                return false;
            }
            if(no_polisi == ''){
                alert('Masukkan No. Polisi Kendaraan !');
                return false;
            }
            if(check_no_rangka !== ''){
                alert('No Rangka Telah Ada Sebelumnya !');
                return false;
            }
        });
        
        $('#checkbox').change(function () {
            $('#nama').val('');
            $('#tlp').val('');
            $('#alamat').val('');
            $('#no_polisi').val('');
            $('#no_rangka').val('');
            $('#jenis_kendaraan').val('').change();
            $('#color').val('').change();
            if (!this.checked){
                $('#nama').prop('disabled', true);
                $('#tlp').prop('disabled', true);
                $('#alamat').prop('disabled', true);
                $('#no_rangka').prop('disabled', true);
            }else{
                $('#nama').prop('disabled', false);
                $('#tlp').prop('disabled', false);
                $('#alamat').prop('disabled', false);
                $('#no_rangka').prop('disabled', false);
            }      
        });
        
        $('#checkbox').on('click',function() {
            if(!this.checked){
                $('#checkbox').attr('value','No');
            }else{
                $('#checkbox').attr('value','Yes');
            }
        });

        $('.searching').on('click', function(e){
            e.preventDefault();
            var keywords = $('#keywords').val();
            if(keywords !== ''){
                // console.log(keywords);
                $.ajax({  
                    url:"<?php echo base_url() . 'service_advisor/searchCustomer'?>",  
                    method:'POST',
                    dataType: 'json',
                    data:{keywords:keywords},
                    success:function(data)  
                    {  
                        $.each(data, function(index, element) {
                            // if(element.no_polisi == null ){
                            //     element.no_polisi = '-';
                            // }
                            $('tbody').append(
                                    '<tr>'+
                                        '<td id="no_polisi'+element.id_customer+'" style="text-align:center;">'+element.no_polisi+'</td>'+
                                        '<td id="nama'+element.id_customer+'">'+element.nama_lengkap+'</td>'+
                                        '<td id="no_hp'+element.id_customer+'">'+element.no_hp+'</td>'+
                                        '<td id="alamat'+element.id_customer+'" style="display:none">'+element.alamat+'</td>'+
                                        '<td style="text-align:center">'+
                                        '<button type="button" name="add" class="btn btn-info btn-xs waves-effect addCustomer" data-row="'+element.id_customer+'" data-no_polisi="'+element.no_polisi+'" data-no_rangka="'+element.no_rangka+'" data-color="'+element.id_color+'" data-kendaraan="'+element.id_jenis+'" data-nama_jenis="'+element.nama_jenis+'"><i class="material-icons">add_circle</i></button></td>'+
                                    '</tr>'
                                    );
                        });
                        // swal("Success!", "Data Submitted", "success");
                        // $('#listCustomer').html(data);
                        $('#myModal').modal('show');

                        $('.addCustomer').on('click', function(){
                            var add_row = $(this).data("row");
                            var nama = $('#nama'+add_row).text();
                            var tlp = $('#no_hp'+add_row).text();
                            var alamat = $('#alamat'+add_row).text();
                            var id_color = $(this).data("color");
                            var id_jenis = $(this).data("kendaraan");
                            var nama_jenis = $(this).data("nama_jenis");
                            var no_polisi = $(this).data("no_polisi");
                            var no_rangka = $(this).data("no_rangka");

                            console.log(id_jenis);
                            console.log(id_color);
                            console.log(nama_jenis);

                            document.getElementById("checkbox").checked = false;
                            $('#nama').val(nama);
                            $('#idCustomer').val(add_row);
                            $('#tlp').val(tlp);
                            $('#alamat').val(alamat);
                            $('#jenis_kendaraan').val(id_jenis).change();
                            $('#color').val(id_color).change();
                            $('#no_polisi').val(no_polisi);
                            $('#no_rangka').val(no_rangka);
                            $('#myModal').modal('hide');
                            $('#keywords').val('');
                            $('#nama').prop('disabled', true);
                            $('#tlp').prop('disabled', true);
                            $('#alamat').prop('disabled', true);
                            $('#no_rangka').prop('disabled', true);
                            
                        });
                    },
                        error:function(data){
                        swal("Oops...", "Something went wrong :(", "error");
                    }                 
            });
            }
        });


        $(".modal").on("hidden.bs.modal", function(){
            $("tbody").html("");
        });

    });
        
</script>

