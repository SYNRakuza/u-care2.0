
<section class="content">
    <div class="container-fluid">
        <!-- Input -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            EDIT DATA USER
                        </h2>
                    </div>
                    <style type="text/css">
                        input[type=number]::-webkit-inner-spin-button, 
                                input[type=number]::-webkit-outer-spin-button { 
                                  -webkit-appearance: none; 
                                  margin: 0; 
                    </style>
                    <div class="body">
                        <?php foreach ($data as $datas) {
                        ?>
                        <form name="updateUser">
                            <div class="form-group form-float">
                                <div class="form-line">
                                     <input type="text" class="form-control" id="nama_lengkap_user" placeholder="" value="<?= $datas->nama_lengkap_user?>" maxlength="100" required />
                                        <label class="form-label">Nama Lengkap User*</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                     <input type="number" class="form-control" id="no_tlpUser" placeholder="" value="<?= $datas->no_tlpUser?>" maxlength="20" required />
                                        <label class="form-label">Nomor Hp*</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                     <input type="text" class="form-control" id="username" placeholder="" value="<?= $datas->username?>" maxlength="100" required />
                                        <label class="form-label">Username*</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                     <input type="text" class="form-control" id="password" placeholder="" value="<?= $datas->password?>" maxlength="100" required />
                                        <label class="form-label">Password*</label>
                                </div>
                            </div> 

                            <div class="input-group form-group form-float">
                                <label class="form-label">Level User</label>
                                <?php if($datas->level == 'qc'):?>
                                <select class="form-control show-tick" id="level" name="level" onchange="getLevel1()">
                                <?php else: ?>
                                <select class="form-control show-tick" id="level" name="level" onchange="getLevel()">
                                <?php endif;?>
                                    <optgroup label="Current Level :" class="opsi">
                                    <option value="<?= $datas->level ?>"><?= $datas->level ?></option>
                                    </optgroup>
                                    <!-- <option>-- Pilih Level--</option> -->
                                    <optgroup label="New Level :" class="opsi">
                                        <option value="admin">Admin</option>
                                        <option value="service advisor">Service Advisor</option>
                                        <option value="partsman">Partsman</option>
                                        <option value="ptm">Pembagi Tugas Mekanik</option>
                                        <option value="teknisi">Teknisi</option>
                                        <option value="qc">Foreman (qc)</option>
                                    </optgroup>
                                </select>
                            </div>
                            <?php if($datas->level == 'qc'):?>
                            <div class="form-group form-float" id="qcGroup1">
                            <?php else: ?>
                            <div class="form-group form-float" id="qcGroup">
                            <?php endif; ?>
                                <label class="form-label">Foreman Group</label>
                                    <select class="form-control show-tick" id="sub_level" name="sub_level">
                                    <optgroup label="Current Group :" class="opsi">
                                        <option value="<?= $datas->sub_level ?>"><?= $datas->sub_level ?></option>
                                    </optgroup>
                                    <optgroup label="New Group :" class="opsi">
                                        <option value="body repair">Body Repair</option>
                                        <option value="preparation">Preparation</option>
                                        <option value="masking">Masking</option>
                                        <option value="painting">Painting</option>
                                        <option value="polishing">Polishing</option>
                                        <option value="re-assembling">Re-Assembling</option>
                                        <option value="washing">Washing</option>
                                        <option value="final inspection">Final Inspection</option>
                                    </optgroup>
                                    </select>
                            </div>
                            
                            <div class="form-group">
                                <button type="button" class="btn bg-orange waves-effect js-update-user" data-id="<?= $datas->id_user ?>" data-action="<?=base_url('pimpinan/userUpdateAction/'.$datas->password)?>" data-redirect="<?=base_url('pimpinan/user')?>">
                                    <i class="material-icons">save</i>
                                        <span>UPDATE</span>
                                </button>
                            </div>

                            <div id="tes"></div>
                        </form>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Exportable Table -->
</section>
<script type="text/javascript">

function initialize() {
    if(document.getElementById("qcGroup") !== null){
        var d = document.getElementById("qcGroup");
        d.style.display = "none";
    };
}

initialize();

function getLevel(){
             
            if (document.updateUser.level.value =='qc')
            {
                var d = document.getElementById("qcGroup");
                d.style.display = "block";
            }
            else
            {
                var d = document.getElementById("qcGroup");
                d.style.display = "none";
            }
            
}
function getLevel1(){
             
            if (document.updateUser.level.value =='qc')
            {
                document.getElementById("qcGroup1").style.display = "block";
            }
            else
            {
                document.getElementById("qcGroup1").style.display = "none";
            }
            
}

</script>
