
    <!-- Jquery DataTable Plugin Js -->
    <script src="<?=base_url('assets/')?>plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="<?=base_url('assets/')?>plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    <script src="<?=base_url('assets/')?>plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
    <script src="<?=base_url('assets/')?>plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
    <script src="<?=base_url('assets/')?>plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
    <script src="<?=base_url('assets/')?>plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
    <script src="<?=base_url('assets/')?>plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
    <script src="<?=base_url('assets/')?>plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
    <script src="<?=base_url('assets/')?>plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>

    <!-- Custom Js -->
    <script src="<?=base_url('assets/')?>js/admin.js"></script>
    <script src="<?=base_url('assets/')?>js/pages/tables/jquery-datatable.js"></script>

    <!-- Demo Js -->
    <script src="<?=base_url('assets/')?>js/demo.js"></script>

    <!-- SweetAlert Plugin Js -->
    <script src="<?=base_url('assets/')?>plugins/sweetalert/sweetalert.min.js"></script>
    <script src="<?=base_url('assets/')?>js/pages/ui/dialogs.js"></script>

    <!-- DataTables -->
    <script type="text/javascript" src="<?=base_url('assets/')?>plugins/DataTables/datatables.min.js"></script>

    <!-- select2 -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

    <!-- render moment.js -->
    <script src="https://cdn.datatables.net/plug-ins/1.10.21/dataRender/datetime.js"></script>