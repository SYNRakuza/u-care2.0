
    <!-- Jquery DataTable Plugin Js -->
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>

    <!-- Custom Js -->
    <script src="<?=base_url('assets/')?>js/admin.js"></script>
    <script src="<?=base_url('assets/')?>js/pages/tables/jquery-datatable.js"></script>

    <!-- Demo Js -->
    <script src="<?=base_url('assets/')?>js/demo.js"></script>

    <!-- SweetAlert Plugin Js -->
    <script src="<?=base_url('assets/')?>plugins/sweetalert/sweetalert.min.js"></script>
    <script src="<?=base_url('assets/')?>js/pages/ui/dialogs.js"></script>

    <!-- select2 -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

    <script type="text/javascript">

