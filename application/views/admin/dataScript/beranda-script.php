    <!-- Jquery CountTo Plugin Js -->
    <script src="<?=base_url('assets/')?>plugins/jquery-countto/jquery.countTo.js"></script>

    <!-- Morris Plugin Js -->
    <script src="<?=base_url('assets/')?>plugins/raphael/raphael.min.js"></script>


    <!-- ChartJs -->
    <!-- <script src="<?=base_url('assets/')?>plugins/chartjs/Chart.bundle.js"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>

    <!-- Flot Charts Plugin Js -->
    <script src="<?=base_url('assets/')?>plugins/flot-charts/jquery.flot.js"></script>
    <script src="<?=base_url('assets/')?>plugins/flot-charts/jquery.flot.resize.js"></script>
    <script src="<?=base_url('assets/')?>plugins/flot-charts/jquery.flot.pie.js"></script>
    <script src="<?=base_url('assets/')?>plugins/flot-charts/jquery.flot.categories.js"></script>
    <script src="<?=base_url('assets/')?>plugins/flot-charts/jquery.flot.time.js"></script>

    <!-- Sparkline Chart Plugin Js -->
    <script src="<?=base_url('assets/')?>plugins/jquery-sparkline/jquery.sparkline.js"></script>

    <!-- Custom Js -->
    <script src="<?=base_url('assets/')?>js/admin.js"></script>
    <!-- <script src="<?=base_url('assets/')?>js/pages/index.js"></script> -->
    <script src="<?=base_url('assets/')?>js/pages/charts/chartjs.js"></script>

    <!-- Demo Js -->
    <script src="<?=base_url('assets/')?>js/demo.js"></script>

    <!-- SA Dashboad Js -->
    <script src="<?=base_url('assets/')?>js/dashboard-sa.js"></script>