<!DOCTYPE HTML>
<html lang="en">

<head>
    <title>Halaman Login U-care</title>
    <!-- Meta tag Keywords -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8" />
    <meta name="keywords" content="Halaman Login U-cares" />
    <script>
        addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
    <!-- Meta tag Keywords -->

    <!-- css files -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/homepage/css/style_login.css" type="text/css" media="all" />
    <!-- Style-CSS -->
    <link href="<?php echo base_url(); ?>assets/homepage/css/font-awesome.min.css" rel="stylesheet">
    <!-- Font-Awesome-Icons-CSS -->
    <!-- //css files -->

    <!-- web-fonts -->
    <link href="//fonts.googleapis.com/css?family=Tangerine:400,700" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i" rel="stylesheet">
    <!-- //web-fonts -->
</head>

<body>
    <!-- title -->
    <h1>
        <span>H</span>alaman
        <span>L</span>ogin
        <span>A</span>dmin
    </h1>
    <!-- //title -->

    <!-- content -->
    <div class="sub-main-w3">
        <form class="login" action="<?php echo base_url('login/cekLogin'); ?> " method="post">
            <p class="legend">Login Here<span class="fa fa-hand-o-down"></span></p>
            <div class="input">
                <input type="text" placeholder="Username...." name="username" maxlength="20" required />
                <span class="fa fa-user"></span>
            </div>
            <div class="input">
                <input type="password" placeholder="Password...." name="password" maxlength="12" required />
                <span class="fa fa-lock"></span>
            </div>
            <label class="check" style="color: yellow;text-align: center;">

                <?php
                    $info = $this->session->flashdata('info');
                    if(!empty($info)){
                    echo $info;
                    }
                ?>

            </label>
            <button type="submit" class="submit">
                <span class="fa fa-sign-in"></span>
            </button>
        </form>
    </div>
    <!-- //content -->


</body>

</html>

<style type="text/css">
    body{
        background-image: url('<?php echo base_url(); ?>/assets/homepage/img/background_login.jpg');
        background-repeat: no-repeat;
        background-size: inherit;
    }
</style>