<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="author" content="colorlib.com">
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet" />
    <link href="<?php echo base_url() ?>/assets/landingpagev2/css/main.css" rel="stylesheet" />
  <style type="text/css">
    html { 
      background: url(<?php echo base_url()?>/assets/landingpage/img/bg-default.png) no-repeat center center fixed; 
      -webkit-background-size: cover;
      -moz-background-size: cover;
      -o-background-size: cover;
      background-size: cover;
    } 

    .tag-line{
    	text-align: center;
    	margin-bottom: 20px;
    }
    .tag-line h4 {
    	font-size: 36px;
    	margin-bottom: 10px;
    }

    .tag-line small{
    }

    .card {

	  /* Add shadows to create the "card" effect */
	  background-color: white;
	  box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
	  transition: 0.3s;
	  border-radius: 5px;
	  min-width: 340px;
	}

	/* On mouse-over, add a deeper shadow */
	.card:hover {
	  box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
	}

	/* Add some padding inside the card container */
	.container {
	  padding: 2px 16px 16px ;
	}

	.center-card {
		text-align: center;
	}
	.center-content {
		display: inline-block;
	}
	table {
		border: none;
		text-align: left;
	}
	p{
		text-align: left;
	}

	.btn-detail {
		background-color: orange; 
		border: none;
		color: white;
		padding: 10px 25px;
		text-align: center;
		text-decoration: none;
		display: inline-block;
		font-size: 16px;
		box-shadow: 0 4px 10px 0 rgba(0,0,0,0.2);
		border-radius: 45px;
	}
	 /*td {
	 	font-size: 14px;
	 }*/
</style>
  </head>
  <body>
    <div class="s130">
      <form>
      	<div class="tag-line">
      		<!-- <h4>U-Care</h4> -->
      		<div style="text-align: center;">
      		<img src="<?php echo base_url()?>assets/ucarelogo.png"> 
      		</div>
      		</br>
      		<font color="#009451"> <b>BODY & PAINT KALLA TOYOTA URIP SUMOHARDJO</b> </font>
      	</div>
        <div class="inner-form">
          <div class="input-field first-wrap">
            <div class="svg-wrapper">
              <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                <path d="M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z"></path>
              </svg>
            </div>
            <input id="search" type="text" placeholder="Masukkan No. WO !"/>
          </div>
          <div class="input-field second-wrap">
            <button class="btn-search search" type="button" data-url="<?php echo base_url()?>homepage/search">CARI</button>
          </div>
        </div>
        <div class="center-card">
        	<div class="center-content">
        		<div class="card" style="display: none;">		  
				  <img src="<?php echo base_url()?>assets/spinner.gif" id="loaderIcon">
				  <div class="container" style="display: none;">
				    <h3><b id="title">Hasil Pencarian</b></h3>
				    <hr> 
				    <table id="table_result" style="display: none;">
			        	<tbody>
			                <tr>
			                    <td>No. WO</td>
			                    <td>:</td>
			                    <td id="nomor_wo"></td>
			                </tr>
			                <tr>
			                    <td>No. Polisi</td>
			                    <td>:</td>
			                    <td id="nomor_polisi"></td>
			                </tr>
			                <tr>
			                	<td>Nama Customer</td>
			                	<td>:</td>
			                	<td id="nama_customer"></td>
			                </tr>
			                <tr>
			                	<td>Nama SA</td>
			                	<td>:</td>
			                	<td id="nama_sa"></td>
			                </tr>
                            <tr>
                                <td>No. Telp SA</td>
                                <td>:</td>
                                <td id="no_telp"></td>
                            </tr>
			                <tr>
			                	<td>Nama Teknisi</td>
			                	<td>:</td>
			                	<td id="nama_teknisi"></td>
			                </tr>
			                <tr>
			                	<td>Tgl. Masuk</td>
			                	<td>:</td>
			                	<td id="tgl_masuk"></td>
			                </tr>
			                <tr>
			                	<td>Tgl. Janji Penyerahan</td>
			                	<td>:</td>
			                	<td id="tgl_janji_penyerahan"></td>
			                </tr>
			                <tr>
			                	<td>Status Parts</td>
			                	<td>:</td>
			                	<td id="status_parts"></td>
			                </tr>
			                <tr>
			                	<td>Status Produksi</td>
			                	<td>:</td>
			                	<td id="status_produksi"></td>
			                </tr>
		               </tbody>
			        </table>			        
			        <!-- <p id="ket_">Ket: <br /><small id="keterangan"></small></p> -->
			        <br />
			        <div class="btn-detail-section" style="display: none;">
			        	<button class="btn-detail" id="btn_detail" type="button" data-url="<?=base_url()?>homepage/detail">LIHAT DETAIL</button>
			        </div>
				  </div>
				</div>
                <div>
                    <p style="text-align: center"><font color="009451">Terima kasih atas kepercayaan Anda<br/>telah memperbaiki kendaraan di bengkel kami</font></p>
                </div>
        	</div>
        </div>
      </form>
    </div>
    <script src="<?=base_url('assets/')?>plugins/jquery/jquery.js"></script>
    <script src="<?=base_url()?>/assets/landingpagev2/js/main.js"></script>
  </body><!-- This templates was made by Colorlib (https://colorlib.com) -->
</html>
