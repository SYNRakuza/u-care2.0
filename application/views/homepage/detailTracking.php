<section class="content">
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-3 col-md-2 col-sm-2">
            </div>
            <div class="col-lg-6 col-md-8 col-sm-8 col-xs-12">
                <div class="card">
                    <div class="header bg-orange" style="text-align: center;">
                        <h2>DETAIL INFORMASI</h2>
                    </div>
                    <style type="text/css">
                        .content {
                            margin: 25px 0 0 0 !important;
                        }
                        input[type=number]::-webkit-inner-spin-button, 
                        input[type=number]::-webkit-outer-spin-button { 
                                  -webkit-appearance: none; 
                                  margin: 0; }
                    </style>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="">
                                <h5>Data Customer</h5>
                                    <table class="table table-customer">
                                        <tbody>
                                        <?php 
                                        $carPosition = '';
                                        foreach($dataEstimasi as $datas){
                                            $carPosition = $datas->status_produksi;
                                        ?>
                                            <tr style="display: none;">
                                                <td width="125"></td>
                                                <td width="1"></td>
                                                <td id="id_estimasi"><?= $datas->id_estimasi ?></td>
                                            </tr>
                                            <tr style="display: none;">
                                                <td width="125"></td>
                                                <td width="1"></td>
                                                <td id="id_customer"><?= $datas->id_customer ?></td>
                                            </tr>
                                            <tr>
                                                <td width="125">Nama</td>
                                                <td width="1">:</td>
                                                <td><?= $datas->nama_lengkap ?></td>
                                            </tr>
                                            <tr>
                                                <td>No. Telp</td>
                                                <td>:</td>
                                                <td><?= $datas->no_hp ?></td>
                                            </tr>
                                            <tr>
                                                <td>Alamat</td>
                                                <td>:</td>
                                                <td><?= $datas->alamat ?></td>
                                            </tr>
                                            <tr>
                                                <td>Tipe Customer</td>
                                                <td>:</td>
                                                <!-- <td><?= $datas->jenis_customer ?></td> -->
                                                <?php if($datas->jenis_customer == '0'){ ?>
                                                    <td>Asuransi</td>
                                                <?php }else{ ?>
                                                    <td>Tunai</td>
                                                <?php } ?>
                                            </tr>
                                        <?php }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <h5>Data Kendaraan</h5>
                                <div class="">
                                    <table class="table table-customer">
                                        <tbody>
                                        <?php foreach($dataEstimasi as $datas){
                                            $diskonJasa = $datas->diskon_jasa;
                                            $diskonParts = $datas->diskon_parts;
                                        ?>
                                            <tr>
                                                <td>No. Polisi</td>
                                                <td>:</td>
                                                <td><?= $datas->no_polisi ?></td>
                                            </tr>
                                            <tr>
                                                <td width="155">Jenis Kendaraan</td>
                                                <td width="1">:</td>
                                                <td><?= $datas->nama_jenis ?></td>
                                            </tr>
                                            <tr>
                                                <td width="155">Warna</td>
                                                <td width="1">:</td>
                                                <td><?= $datas->code_color.' - '.$datas->nama_color ?></td>
                                            </tr>
                                            <tr>
                                                <td width="155">No. Rangka</td>
                                                <td width="1">:</td>
                                                <td><?= $datas->no_rangka ?></td>
                                            </tr>
                                        <?php }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <style type="text/css">
                            .icon-in {
                                color: green;
                            }
                            .icon-out {
                                color: red;
                            }
                            .icon-car {
                                vertical-align: middle;
                                color: orange;
                            }
                            .badge-process {
                                border-radius: 20px;
                                font-weight: 100;
                                background-color: orange;
                            }

                        </style>
                        <div class="row clearfix demo-icon-container">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 0px;">
                                <h5>Proses Produksi</h5>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <h5><span class="badge badge-process">1</span> Body Repair</h5>
                                <table class="table">
                                    <tbody>
                                    <?php foreach($dataJasa as $datas){ ?> 
                                        <tr>
                                            <td width="50">
                                                 <i class="material-icons icon-in">arrow_forward</i> <span class="icon-name"></span> 
                                            </td>
                                            <td align="left">
                                              <?php if($datas->body_repair_in !== NULL){
                                                echo "".date('d-m-Y H:m:s', strtotime($datas->body_repair_in));
                                               }else{
                                                echo "-";
                                               } ?>
                                            </td>
                                        </tr>   
                                        <tr>
                                            <td>
                                                 <i class="material-icons icon-out">arrow_back</i> <span class="icon-name"></span> 
                                            </td>
                                            <td>
                                               <?php if($datas->body_repair_out !== NULL){
                                                echo "".date('d-m-Y H:m:s', strtotime($datas->body_repair_out));
                                               }else{
                                                echo "-";
                                               } ?>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <h5><span class="badge badge-process">2</span> Preparation</h5>
                                <table class="table">
                                    <tbody>
                                    <?php foreach($dataJasa as $datas){ ?> 
                                        <tr>
                                            <td width="50">
                                                 <i class="material-icons icon-in">arrow_forward</i> <span class="icon-name"></span> 
                                            </td>
                                            <td align="left">
                                              <?php if($datas->preparation_in !== NULL){
                                                echo "".date('d-m-Y H:m:s', strtotime($datas->preparation_in));
                                               }else{
                                                echo "-";
                                               } ?>
                                            </td>
                                        </tr>   
                                        <tr>
                                            <td>
                                                 <i class="material-icons icon-out">arrow_back</i> <span class="icon-name"></span> 
                                            </td>
                                            <td>
                                               <?php if($datas->preparation_out !== NULL){
                                                echo "".date('d-m-Y H:m:s', strtotime($datas->preparation_out));
                                               }else{
                                                echo "-";
                                               } ?>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <h5><span class="badge badge-process">3</span> Masking</h5>
                                <table class="table">
                                    <tbody>
                                    <?php foreach($dataJasa as $datas){ ?> 
                                        <tr>
                                            <td width="50">
                                                 <i class="material-icons icon-in">arrow_forward</i> <span class="icon-name"></span> 
                                            </td>
                                            <td align="left">
                                              <?php if($datas->masking_in !== NULL){
                                                echo "".date('d-m-Y H:m:s', strtotime($datas->masking_in));
                                               }else{
                                                echo "-";
                                               } ?>
                                            </td>
                                        </tr>   
                                        <tr>
                                            <td>
                                                 <i class="material-icons icon-out">arrow_back</i> <span class="icon-name"></span> 
                                            </td>
                                            <td>
                                               <?php if($datas->masking_out !== NULL){
                                                echo "".date('d-m-Y H:m:s', strtotime($datas->masking_out));
                                               }else{
                                                echo "-";
                                               } ?>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <h5><span class="badge badge-process">4</span> Painting </h5>
                                <table class="table">
                                    <tbody>
                                    <?php foreach($dataJasa as $datas){ ?> 
                                        <tr>
                                            <td width="50">
                                                 <i class="material-icons icon-in">arrow_forward</i> <span class="icon-name"></span> 
                                            </td>
                                            <td align="left">
                                              <?php if($datas->painting_in !== NULL){
                                                echo "".date('d-m-Y H:m:s', strtotime($datas->painting_in));
                                               }else{
                                                echo "-";
                                               } ?>
                                            </td>
                                        </tr>   
                                        <tr>
                                            <td>
                                                 <i class="material-icons icon-out">arrow_back</i> <span class="icon-name"></span> 
                                            </td>
                                            <td>
                                               <?php if($datas->painting_out !== NULL){
                                                echo "".date('d-m-Y H:m:s', strtotime($datas->painting_out));
                                               }else{
                                                echo "-";
                                               } ?>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <h5><span class="badge badge-process">5</span> Polishing</h5>
                                <table class="table">
                                    <tbody>
                                    <?php foreach($dataJasa as $datas){ ?> 
                                        <tr>
                                            <td width="50">
                                                 <i class="material-icons icon-in">arrow_forward</i> <span class="icon-name"></span> 
                                            </td>
                                            <td align="left">
                                              <?php if($datas->polishing_in !== NULL){
                                                echo "".date('d-m-Y H:m:s', strtotime($datas->polishing_in));
                                               }else{
                                                echo "-";
                                               } ?>
                                            </td>
                                        </tr>   
                                        <tr>
                                            <td>
                                                 <i class="material-icons icon-out">arrow_back</i> <span class="icon-name"></span> 
                                            </td>
                                            <td>
                                               <?php if($datas->polishing_out !== NULL){
                                                echo "".date('d-m-Y H:m:s', strtotime($datas->polishing_out));
                                               }else{
                                                echo "-";
                                               } ?>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <h5><span class="badge badge-process">6</span> Re Assembling</h5>
                                <table class="table">
                                    <tbody>
                                    <?php foreach($dataJasa as $datas){ ?> 
                                        <tr>
                                            <td width="50">
                                                 <i class="material-icons icon-in">arrow_forward</i> <span class="icon-name"></span> 
                                            </td>
                                            <td align="left">
                                              <?php if($datas->re_assembling_in !== NULL){
                                                echo "".date('d-m-Y H:m:s', strtotime($datas->re_assembling_in));
                                               }else{
                                                echo "-";
                                               } ?>
                                            </td>
                                        </tr>   
                                        <tr>
                                            <td>
                                                 <i class="material-icons icon-out">arrow_back</i> <span class="icon-name"></span> 
                                            </td>
                                            <td>
                                               <?php if($datas->re_assembling_out !== NULL){
                                                echo "".date('d-m-Y H:m:s', strtotime($datas->re_assembling_out));
                                               }else{
                                                echo "-";
                                               } ?>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <h5><span class="badge badge-process">7</span> Washing</h5>
                                <table class="table">
                                    <tbody>
                                    <?php foreach($dataJasa as $datas){ ?> 
                                        <tr>
                                            <td width="50">
                                                 <i class="material-icons icon-in">arrow_forward</i> <span class="icon-name"></span> 
                                            </td>
                                            <td align="left">
                                              <?php if($datas->wasling_in !== NULL){
                                                echo "".date('d-m-Y H:m:s', strtotime($datas->wasling_in));
                                               }else{
                                                echo "-";
                                               } ?>
                                            </td>
                                        </tr>   
                                        <tr>
                                            <td>
                                                 <i class="material-icons icon-out">arrow_back</i> <span class="icon-name"></span> 
                                            </td>
                                            <td>
                                               <?php if($datas->wasling_out !== NULL){
                                                echo "".date('d-m-Y H:m:s', strtotime($datas->wasling_out));
                                               }else{
                                                echo "-";
                                               } ?>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <h5><span class="badge badge-process">8</span> Final Inspection</h5>
                                <table class="table">
                                    <tbody>
                                    <?php foreach($dataJasa as $datas){ ?> 
                                        <tr>
                                            <td width="50">
                                                 <i class="material-icons icon-in">arrow_forward</i> <span class="icon-name"></span> 
                                            </td>
                                            <td align="left">
                                              <?php if($datas->final_inspection_in !== NULL){
                                                echo "".date('d-m-Y H:m:s', strtotime($datas->final_inspection_in));
                                               }else{
                                                echo "-";
                                               } ?>
                                            </td>
                                        </tr>   
                                        <tr>
                                            <td>
                                                 <i class="material-icons icon-out">arrow_back</i> <span class="icon-name"></span> 
                                            </td>
                                            <td>
                                               <?php if($datas->final_inspection_out !== NULL){
                                                echo "".date('d-m-Y H:m:s', strtotime($datas->final_inspection_out));
                                               }else{
                                                echo "-";
                                               } ?>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <a href="<?=base_url()?>homepage" type="button" class="btn btn-sm bg-green waves-effect save">
                            <i class="material-icons">home</i>
                            <span>Halaman Utama</span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-2 col-sm-2">
            </div>
        </div>
        <!-- #END# Advanced Form Example With Validation -->
        <!-- default Size -->
    </div>
</section>

