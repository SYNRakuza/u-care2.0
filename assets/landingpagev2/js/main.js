$(document).on('click', '.search', function(){
    var no_wo = $('#search').val();
    var url = $(this).data('url');
    if(no_wo == ''){
    	return false;
    }
    $('.card').show();
	$('#table_result').hide();
	$('#loaderIcon').show();
    $.ajax({
        url:url,
        data:{no_wo:no_wo},
        method:"POST",
        dataType:"json",
        success:function(result){
            console.log(result);
            if(result.dataEstimasi != ""){
            	$('#table_result').show();
	            $('#loaderIcon').hide();
	            $('.container').show();
	            $('.btn-detail-section').show();
	            // $('#ket_').show();
	            $('#nomor_wo').html(result.dataEstimasi[0].nomor_wo);
	            $('#nomor_polisi').html(result.dataEstimasi[0].no_polisi);
	            $('#nama_customer').html(result.dataEstimasi[0].nama_lengkap);
	            $('#nama_sa').html(result.dataEstimasi[0].nama_sa);
	            $('#nama_teknisi').html(result.dataEstimasi[0].nama_teknisi);
	            $('#tgl_masuk').html(result.dataEstimasi[0].tgl_masuk);
	            $('#tgl_janji_penyerahan').html(result.dataEstimasi[0].tgl_janji_penyerahan);
	            $('#status_parts').html(result.dataEstimasi[0].status_parts);
	            $('#status_produksi').html(result.dataEstimasi[0].status_produksi);
	            $('#no_telp').html(result.dataEstimasi[0].no_telp);
	            // $('#keterangan').html(result.dataEstimasi[0].ket);
                var id_estimasi = result.dataEstimasi[0].id_estimasi;
            }else{
            	$('#table_result').hide();
	            $('#loaderIcon').hide();
	            $('.container').show();
	            $('#nomor_wo').html('');
	            $('#nomor_polisi').html('');
	            $('#nama_customer').html('');
	            $('#nama_sa').html('');
	            $('#nama_teknisi').html('');
	            $('#tgl_masuk').html('');
	            $('#tgl_janji_penyerahan').html('');
	            $('#status_produksi').html('');
	            $('#status_parts').html('');
	            $('#no_telp').html('');
	            $('#title').html('Data tidak ditemukan !');
	            $('.btn-detail-section').hide();
	            // $('#ket_').hide();
            }

            $('.btn-detail').click(function(){
	            // console.log(id_estimasi);
	            var urlDetail = $(this).data('url');
	            window.location.href = urlDetail+'/'+id_estimasi;
	
            });
            
        }
    });
})

