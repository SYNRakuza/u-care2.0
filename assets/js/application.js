(function ($) {
    $('.js-update-user').on('click', function(){
        var _this = $(this);
        var action = _this.data('action');
        var redirect = _this.data('redirect');
        var id_user = _this.data('id');
        var nama_lengkap_user = $('#nama_lengkap_user').val();
        var no_tlpUser = $('#no_tlpUser').val();
        var username = $('#username').val();
        var password = $('#password').val();
        var level = $('#level').val();
        var sub_level = $('#sub_level').val();

        swal({
            title: "Apakah Anda Yakin?",
            text: "Pastikan Data User anda Sudah Terisi Dengan Benar!",
            type: "warning",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
        }, function () {
            var data = {
                'id_user'              : id_user,
                'nama_lengkap_user'    : nama_lengkap_user,
                'no_tlpUser'           : no_tlpUser,
                'username'             : username,
                'password'             : password,
                'level'                : level,
                'sub_level'            : sub_level,
            };

            $.ajax({
                url:action,
                method:"POST",
                data : data,
                success: function(result)
                {
                    console.log(result);
                    if(result == 'success'){
                        swal("Berhasil!", "User Baru Telah Ditambahkan", "success");
                        setTimeout(function(){
                            window.location=redirect;
                        }, 1000);
                    }else{
                        swal("Gagal!", "Terjadi kesalahan saat mengirim data!", "warning");
                        setTimeout(function(){
                            window.location.reload(1);
                        }, 3000);
                    }
                    console.log('data');
                }
            });
        });
    });

    $('.js-delete-user').on('click', function(){
        var _this = $(this);
        var id_user = _this.data('id');
        var action = _this.data('action');
        swal({
            title: "Apakah Anda Yakin?",
            text: "User Yang Dihapus Tidak Akan Bisa Mengakses U-care!",
            type: "warning",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
        }, function () {
            var data = {
                'id_user'          : id_user
            };
            $.ajax({
                url:action,
                method:"POST",
                data : data,
                success: function(result)
                {
                    if(result == 'success'){
                        swal("Berhasil!", "User Telah Dihapus Dari Sistem!", "success");
                        setTimeout(function(){
                            window.location.reload(1);
                        }, 1000);
                    }else{
                        swal("Error Saving", " ", "warning");
                    }
                }
            });
        });
    });

    $('.js-create-user').on('click', function(e){
        e.preventDefault();
        var _form = $('#formUser');

        if(_form.validate()){
            _form.submit();
        }
    });
})(jQuery);