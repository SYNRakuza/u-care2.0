$(document).ready(function(){
    //Punya Data Estimasi Parts
    var dateFromParts = '';
    var dateToParts   = '';
    var inOutParts    = '';
    var urlParts  = $('#get_url_parts').val();

    //Punya Data Estimasi SA
    var dateFrom = '';
    var dateTo   = '';
    var inOut    = '';
    var url  = $('#get_url_2').val();

    //Mulai Ajax Estimasi In Out
    $(document).on('change','#dateFrom', function(){
        dateFrom = $(this).val();
        if(dateTo == '' || inOut == ''){
            return false;
        }
        // console.log(inOut);
        $.ajax({
            url:url,
            method:"POST",
            data:{dateFrom:dateFrom, dateTo:dateTo, inOut:inOut},
            dataType:'json',
            success:function(data)
            {
                $('#dataListEstimasi').html(data.listEstimasi);
            }
        });
    });

    $(document).on('change','#dateTo', function(){
        dateTo = $(this).val();
        if(dateFrom == '' || inOut == ''){
            return false;
        }
        $.ajax({
            url:url,
            method:"POST",
            data:{dateFrom:dateFrom, dateTo:dateTo, inOut:inOut},
            dataType:'json',
            success:function(data)
            {
                $('#dataListEstimasi').html(data.listEstimasi);
            }
        });
    });

    $(document).on('change','#dateInOut', function(){
        inOut = $(this).val();
        if(dateFrom == '' || dateTo == '' || inOut == ''){
            return false;
        }
        $.ajax({
            url:url,
            method:"POST",
            data:{dateFrom:dateFrom, dateTo:dateTo, inOut:inOut},
            dataType:'json',
            success:function(data)
            {
                $('#dataListEstimasi').html(data.listEstimasi);
            }
        }); 
    });

    //END Ajax Estimasi Inout

    
    //Mulai Ajax Estimasi Parts
    $(document).on('change','#dateFromParts', function(){
        dateFromParts = $(this).val();
        if(dateToParts == '' || inOutParts == ''){
            return false;
        }
        // console.log(inOut);
        $.ajax({
            url:urlParts,
            method:"POST",
            data:{dateFromParts:dateFromParts, dateToParts:dateToParts, inOutParts:inOutParts},
            dataType:'json',
            success:function(data)
            {
                $('#dataListEstimasiParts').html(data.listEstimasi);
            }
        });
    });

    $(document).on('change','#dateToParts', function(){
        dateToParts = $(this).val();
        if(dateFromParts == '' || inOutParts == ''){
            return false;
        }
        $.ajax({
            url:urlParts,
            method:"POST",
            data:{dateFromParts:dateFromParts, dateToParts:dateToParts, inOutParts:inOutParts},
            dataType:'json',
            success:function(data)
            {
                $('#dataListEstimasiParts').html(data.listEstimasi);
            }
        });
    });

    $(document).on('change','#dateInOutParts', function(){
        inOutParts = $(this).val();
        if(dateFromParts == '' || dateToParts == '' || inOutParts == ''){
            return false;
        }
        $.ajax({
            url:urlParts,
            method:"POST",
            data:{dateFromParts:dateFromParts, dateToParts:dateToParts, inOutParts:inOutParts},
            dataType:'json',
            success:function(data)
            {
                $('#dataListEstimasiParts').html(data.listEstimasi);
            }
        });
    });

    //Selesai Ajax Estimasi Parts


});