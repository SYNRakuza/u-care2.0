$(document).ready(function(){
    var dateFrom = '';
    var dateTo   = '';
    var inOut    = '';
    var url  = $('#get_url_2').val();
    $(document).on('change','#dateFrom', function(){
        dateFrom = $(this).val();
        if(dateTo == '' || inOut == ''){
            return false;
        }
        // console.log(inOut);
        $.ajax({
            url:url,
            method:"POST",
            data:{dateFrom:dateFrom, dateTo:dateTo, inOut:inOut},
            dataType:'json',
            success:function(data)
            {
                $('#dataListEstimasi').html(data.listEstimasi);
            }
        });
    });

    $(document).on('change','#dateTo', function(){
        dateTo = $(this).val();
        if(dateFrom == '' || inOut == ''){
            return false;
        }
        $.ajax({
            url:url,
            method:"POST",
            data:{dateFrom:dateFrom, dateTo:dateTo, inOut:inOut},
            dataType:'json',
            success:function(data)
            {
                $('#dataListEstimasi').html(data.listEstimasi);
            }
        });
    });

    $(document).on('change','#dateInOut', function(){
        inOut = $(this).val();
        if(dateFrom == '' || dateTo == '' || inOut == ''){
            return false;
        }
        $.ajax({
            url:url,
            method:"POST",
            data:{dateFrom:dateFrom, dateTo:dateTo, inOut:inOut},
            dataType:'json',
            success:function(data)
            {
                $('#dataListEstimasi').html(data.listEstimasi);
            }
        });
    });
});