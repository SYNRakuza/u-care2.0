$(document).ready(function(){


    //Punya Data Estimasi Parts
    var url = $('#get_url').val();

    $(document).on('change','#jenis_kendaraan', function(){
        jenis_kendaraan = $(this).val();
        // console.log(inOut);
        $.ajax({
            url:url,
            method:"POST",
            data:{jenis_kendaraan:jenis_kendaraan},
            dataType:'json',
            success:function(data)
            {
                $('#tabelJasa').html(data.headJasa+data.listJasa+data.footerJasa);

                $('#tabelJasa').DataTable({
                    processing: true,
                    bLengthChange: false,
                    destroy: true,
                });
            }
        });

    });

});