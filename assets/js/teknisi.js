$('.done').click(function(){
	var id_estimasi = $(this).data('row');
	var url = $(this).data('url');
	var url_redirect = $(this).data('redirect');
	console.log(url);

	swal({
            title: "Are you sure?",
            text: "You will not be able to cancel this Operation !",
            type: "warning",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
        }, function () {
        	$.ajax({
				url:url,
				data:{id_estimasi:id_estimasi},
				method:'POST',
				success:function(data){
					swal("Good job!", "Proses Estimasi berhasil !", "success");
					setTimeout(function(){
		               location.href = url_redirect;
		            }, 1000);
				}
			});
        });
	
});

$('.clockOn').click(function(){
	var status = $(this).data('row');
	var id_estimasi = $(this).data('id');
	var url = $(this).data('url');
	var id_lead		= $(this).data('idlead');
	console.log(url);
	swal({
        title: "Are you sure?",
        text: "You will not be able to cancel this Operation !",
        type: "warning",
        showCancelButton: true,
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
    }, function () {
    	$.ajax({
			url:url,
			data:{id_estimasi:id_estimasi, status:status, id_lead:id_lead},
			method:'POST',
			success:function(data){
				console.log(data);
				swal("Good job!", "Proses Estimasi berhasil !", "success");
				setTimeout(function(){
	               location.reload();
	            }, 1000);
			}
		});
    });
});
$('.clockOff').click(function(){
	var status = $(this).data('row');
	var id_estimasi = $(this).data('id');
	var url = $(this).data('url');
	var lead_start  = $(this).data('leadstart');
	var data_lead   = $(this).data('lead');
	var total_lead	= $(this).data('total');
	var id_lead		= $(this).data('idlead');
	console.log(url);
	swal({
        title: "Are you sure?",
        text: "You will not be able to cancel this Operation !",
        type: "warning",
        showCancelButton: true,
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
    }, function () {
    	$.ajax({
			url:url,
			data:{id_estimasi:id_estimasi, status:status, lead_start:lead_start, data_lead:data_lead, total_lead:total_lead, id_lead:id_lead},
			method:'POST',
			success:function(data){
				console.log(data);
				swal("Good job!", "Proses Estimasi berhasil !", "success");
				setTimeout(function(){
	               location.reload();
	            }, 1000);
			}
		});
    });
});

$('.qControl').click(function(){
	var id_estimasi = $(this).data('id');
	var status = $(this).data('row');
	var id_lead		= $(this).data('idlead');
	console.log(id_estimasi);
	$('#smallModal').modal('show');
	$('.save').click(function(){
		var url = $(this).data('url');
		var qc = $("input[name='group1']:checked").val();
		var ket = $('#ket').val();
		console.log(ket);
		$.ajax({
			url:url,
			data:{id_estimasi:id_estimasi, qc:qc, ket:ket, status:status, id_lead:id_lead},
			method:'POST',
			success:function(data){
				swal("Good job!", "Proses Estimasi berhasilsdf !", "success");
				setTimeout(function(){
	               location.reload();
	            }, 1000);
			}
		});
	})
});

$('.clockPause').click(function(){
	var id_estimasi = $(this).data('id');
	var status = $(this).data('row');
	var start_from  = $(this).data('start');
	var lead_start  = $(this).data('leadstart');
	var data_lead   = $(this).data('lead');
	var total_lead	= $(this).data('total');
	var id_lead		= $(this).data('idlead');
	console.log(id_lead);
	$('#pauseModal').modal('show');
	document.getElementById("start_from").innerHTML = "Start From : " + start_from;
	$('.pause').click(function(){
		var url = $(this).data('url');
		var note = $('#note').val();
		console.log(note);
		$.ajax({
			url:url,
			data:{id_estimasi:id_estimasi, note:note, status:status, lead_start:lead_start, data_lead:data_lead, total_lead:total_lead, id_lead:id_lead},
			method:'POST',
			success:function(data){
				swal("Good job!", "Estimasi telah diberhentikan sementara", "success");
				setTimeout(function(){
	               location.reload();
	            }, 1000);
			}
		});
	})
});

$('.clockStart').click(function(){
	var id_estimasi = $(this).data('id');
	var status = $(this).data('row');
	var last_pause  = $(this).data('pause');
	var note_pause  = $(this).data('note');
	var id_lead		= $(this).data('idlead');
	console.log(note);
	$('#startModal').modal('show');
	document.getElementById("note_pause").innerHTML = note_pause;
	document.getElementById("last_pause").innerHTML = "Last Pause : " + last_pause;
	$('.start').click(function(){
		var url = $(this).data('url');
		console.log(ket);
		$.ajax({
			url:url,
			data:{id_estimasi:id_estimasi, status:status, id_lead:id_lead},
			method:'POST',
			success:function(data){
				swal("Good job!", "Estimasi telah dilanjutkan", "success");
				setTimeout(function(){
	               location.reload();
	            }, 1000);
			}
		});
	})
});


