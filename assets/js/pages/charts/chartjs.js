$(document).ready(function(){
var user_level = $('#get_user_level').val();
var url = '';
var ctx = '';

if(user_level == 'service advisor'){
    url = $('#get_url').val();
    ctx = document.getElementById('myChart').getContext('2d');
    chartSA();
}else if(user_level == 'teknisi'){
    url = $('#get_url').val();
    ctx = document.getElementById('myChart2').getContext('2d');
    chartTim();
}else{
    url = $('#get_url').val();
    ctx = document.getElementById('myChart3').getContext('2d');
    chartParts();
}

var year = '2019';
var inOut = '';
var jenis_chart = 'bar';

    function chartSA(){
        $.ajax({
                url:url,
                method:"POST",
                dataType:"json",
                success: function(result)
                {
                    chart = '';
                    if(window.ctx != undefined)
                        window.ctx.destroy();
                    window.ctx = new Chart(ctx, {
                                    // The type of chart we want to create
                                    type: jenis_chart,

                                    // The data for our dataset
                                    data: {
                                        labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Augst", "Sept", "Okt", "Nov", "Des"],
                                        datasets: [{
                                            label: "Asuransi IN",
                                            backgroundColor: 'rgb(233, 30, 99)',
                                            data:result.dataCountAsuransiIn,
                                        },{
                                            label: "Asuransi OUT",
                                            backgroundColor: 'rgb(139, 195, 74)',
                                            data:result.dataCountAsuransiOut,
                                        },{
                                            label: "Tunai IN",

                                            backgroundColor: 'rgb(0, 188, 212)',
                                            data:result.dataCountTunaiIn,
                                        },{
                                            label: "Tunai OUT",
                                            backgroundColor: 'rgb(255, 193, 7)',
                                            data:result.dataCountTunaiOut,
                                        }]
                                    },

                                    // Configuration options go here
                                    options: {

                                    }
                                });



                }
        });
    }

    function chartTim(){
        $.ajax({
                url:url,
                method:"POST",
                dataType:"json",
                success: function(result)
                {
                    chart = '';
                    if(window.ctx != undefined)
                        window.ctx.destroy();
                    window.ctx = new Chart(ctx, {
                                    // The type of chart we want to create
                                    type: jenis_chart,

                                    // The data for our dataset
                                    data: {
                                        labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Augst", "Sept", "Okt", "Nov", "Des"],
                                        datasets: [{
                                            label: "Light",
                                            backgroundColor: 'rgb(233, 30, 99)',
                                            data:result.dataCountLight,
                                        },{
                                            label: "Medium",
                                            backgroundColor: 'rgb(139, 195, 74)',
                                            data:result.dataCountMedium,
                                        },{
                                            label: "Heavy",
                                            backgroundColor: 'rgb(0, 188, 212)',
                                            data:result.dataCountHeavy,
                                        }]
                                    },

                                    // Configuration options go here
                                    options: {

                                    }
                                });



                }
        });
    }

    function chartParts(){
        $.ajax({
                url:url,
                method:"POST",
                dataType:"json",
                success: function(result)
                {
                    console.log(result);
                    chart = '';
                    if(window.ctx != undefined)
                        window.ctx.destroy();
                    window.ctx = new Chart(ctx, {
                                    // The type of chart we want to create
                                    type: jenis_chart,

                                    // The data for our dataset
                                    data: {
                                        labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Augst", "Sept", "Okt", "Nov", "Des"],
                                        datasets: [{
                                            label: "Percentage of Service Rate",
                                            backgroundColor: 'rgb(233, 30, 99)',
                                            data:result.dataPersen,
                                        }]
                                    },

                                    // Configuration options go here
                                    options: {
                                        scales: {
                                            yAxes: [{
                                              ticks: {
                                                callback: function(tick) {
                                                  return tick.toString() + '%';
                                                }
                                              }
                                            }]
                                          }
                                    }
                                });



                }
        });
    }



    $(document).on('change', '#selectYear', function(){
        year = $(this).val();
        inOut = $('#selectInOut').val();
        $.ajax({
            url:url,
            method:"POST",
            dataType:"json",
            data:{year:year},
            success: function(result)
            {
                chart = '';
                if(window.ctx != undefined)
                    window.ctx.destroy();
                if(inOut == 'in'){
                    window.ctx = new Chart(ctx, {
                                // The type of chart we want to create
                                type: jenis_chart,

                                // The data for our dataset
                                data: {
                                    labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Augst", "Sept", "Okt", "Nov", "Des"],
                                    datasets: [{
                                        label: "Asuransi IN",
                                        backgroundColor: 'rgb(233, 30, 99)',
                                        data:result.dataCountAsuransiIn,
                                    },{
                                        label: "Tunai IN",

                                        backgroundColor: 'rgb(0, 188, 212)',
                                        data:result.dataCountTunaiIn,
                                    }]
                                },

                                // Configuration options go here
                                options: {

                                }
                            });
                }else if(inOut == 'out'){
                    window.ctx = new Chart(ctx, {
                                // The type of chart we want to create
                                type: jenis_chart,

                                // The data for our dataset
                                data: {
                                    labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Augst", "Sept", "Okt", "Nov", "Des"],
                                    datasets: [{
                                        label: "Asuransi OUT",
                                        backgroundColor: 'rgb(139, 195, 74)',
                                        data:result.dataCountAsuransiOut,
                                    },{
                                        label: "Tunai OUT",
                                        backgroundColor: 'rgb(255, 193, 7)',
                                        data:result.dataCountTunaiOut,
                                    }]
                                },

                                // Configuration options go here
                                options: {

                                }
                            });
                }else{
                    window.ctx = new Chart(ctx, {
                                // The type of chart we want to create
                                type: jenis_chart,

                                // The data for our dataset
                                data: {
                                    labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Augst", "Sept", "Okt", "Nov", "Des"],
                                    datasets: [{
                                        label: "Asuransi IN",
                                        backgroundColor: 'rgb(233, 30, 99)',
                                        data:result.dataCountAsuransiIn,
                                    },{
                                        label: "Asuransi OUT",
                                        backgroundColor: 'rgb(139, 195, 74)',
                                        data:result.dataCountAsuransiOut,
                                    },{
                                        label: "Tunai IN",

                                        backgroundColor: 'rgb(0, 188, 212)',
                                        data:result.dataCountTunaiIn,
                                    },{
                                        label: "Tunai OUT",
                                        backgroundColor: 'rgb(255, 193, 7)',
                                        data:result.dataCountTunaiOut,
                                    }]
                                },

                                // Configuration options go here
                                options: {

                                }
                            });
                }
            }
      });
    });

    $(document).on('change', '#selectInOut', function(){
        year = $('#selectYear').val();
        inOut = $(this).val();
        $.ajax({
            url:url,
            method:"POST",
            dataType:"json",
            data:{year:year},
            success: function(result)
            {
                chart = '';
                if(window.ctx != undefined)
                    window.ctx.destroy();
                if(inOut == 'in'){
                    window.ctx = new Chart(ctx, {
                                // The type of chart we want to create
                                type: jenis_chart,

                                // The data for our dataset
                                data: {
                                    labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Augst", "Sept", "Okt", "Nov", "Des"],
                                    datasets: [{
                                        label: "Asuransi IN",
                                        backgroundColor: 'rgb(233, 30, 99)',
                                        data:result.dataCountAsuransiIn,
                                    },{
                                        label: "Tunai IN",

                                        backgroundColor: 'rgb(0, 188, 212)',
                                        data:result.dataCountTunaiIn,
                                    }]
                                },

                                // Configuration options go here
                                options: {

                                }
                            });
                }else if(inOut == 'out'){
                    window.ctx = new Chart(ctx, {
                                // The type of chart we want to create
                                type: jenis_chart,

                                // The data for our dataset
                                data: {
                                    labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Augst", "Sept", "Okt", "Nov", "Des"],
                                    datasets: [{
                                        label: "Asuransi OUT",
                                        backgroundColor: 'rgb(139, 195, 74)',
                                        data:result.dataCountAsuransiOut,
                                    },{
                                        label: "Tunai OUT",
                                        backgroundColor: 'rgb(255, 193, 7)',
                                        data:result.dataCountTunaiOut,
                                    }]
                                },

                                // Configuration options go here
                                options: {

                                }
                            });
                }else{
                    window.ctx = new Chart(ctx, {
                                // The type of chart we want to create
                                type: jenis_chart,

                                // The data for our dataset
                                data: {
                                    labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Augst", "Sept", "Okt", "Nov", "Des"],
                                    datasets: [{
                                        label: "Asuransi IN",
                                        backgroundColor: 'rgb(233, 30, 99)',
                                        data:result.dataCountAsuransiIn,
                                    },{
                                        label: "Asuransi OUT",
                                        backgroundColor: 'rgb(139, 195, 74)',
                                        data:result.dataCountAsuransiOut,
                                    },{
                                        label: "Tunai IN",

                                        backgroundColor: 'rgb(0, 188, 212)',
                                        data:result.dataCountTunaiIn,
                                    },{
                                        label: "Tunai OUT",
                                        backgroundColor: 'rgb(255, 193, 7)',
                                        data:result.dataCountTunaiOut,
                                    }]
                                },

                                // Configuration options go here
                                options: {

                                }
                            });
                }
            }
      });
    });

    $(document).on('change', '#selectYear2', function(){
        year = $(this).val();
        $.ajax({
            url:url,
            method:"POST",
            dataType:"json",
            data:{year:year},
            success: function(result)
            {
                chart = '';
                if(window.ctx != undefined)
                    window.ctx.destroy();
                window.ctx = new Chart(ctx, {
                                // The type of chart we want to create
                                type: jenis_chart,

                                // The data for our dataset
                                data: {
                                    labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Augst", "Sept", "Okt", "Nov", "Des"],
                                    datasets: [{
                                        label: "Light",
                                        backgroundColor: 'rgb(233, 30, 99)',
                                        data:result.dataCountLight,
                                    },{
                                        label: "Medium",
                                        backgroundColor: 'rgb(139, 195, 74)',
                                        data:result.dataCountMedium,
                                    },{
                                        label: "Heavy",
                                        backgroundColor: 'rgb(0, 188, 212)',
                                        data:result.dataCountHeavy,
                                    }]
                                },

                                // Configuration options go here
                                options: {

                                }
                            });



            }
      });
    });

    $(document).on('change', '#selectChart', function(){
        jenis_chart = $(this).val();
        $.ajax({
            url:url,
            method:"POST",
            dataType:"json",
            data:{year:year},
            success: function(result)
            {
                chart = '';
                if(window.ctx != undefined)
                    window.ctx.destroy();
                window.ctx = new Chart(ctx, {
                                // The type of chart we want to create
                                type: jenis_chart,

                                // The data for our dataset
                                data: {
                                    labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Augst", "Sept", "Okt", "Nov", "Des"],
                                    datasets: [{
                                        label: "Out",
                                        backgroundColor: 'rgb(233, 30, 99)',
                                        data:result.dataCountOut,
                                    },{
                                        label: "In",
                                        backgroundColor: 'rgb(139, 195, 74)',
                                        data:result.dataCountIn,
                                    },{
                                        label: "Asuransi",
                                        backgroundColor: 'rgb(0, 188, 212)',
                                        data:result.dataCountAsuransi,
                                    },{
                                        label: "Tunai",
                                        backgroundColor: 'rgb(255, 193, 7)',
                                        data:result.dataCountTunai,
                                    }]
                                },

                                // Configuration options go here
                                options: {

                                }
                            });



            }
      });
    });
});