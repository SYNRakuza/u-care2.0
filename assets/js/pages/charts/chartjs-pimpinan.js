$(document).ready(function(){
var url = '';
var ctx = '';
var modul = $('#modul').val();


if(modul == 'service_advisor' || modul == ''){
    url = $('#get_url').val();
    ctx = document.getElementById('myChart').getContext('2d');
    chartSA();
}else if(modul == 'partsman'){
    url = $('#get_url_chart_parts').val();
    ctx = document.getElementById('myChart3').getContext('2d');
    chartParts();
}else if(modul == 'teknisi'){
    var id_teknisi = $('selectTeknisi').val();
    url = $('#selectTeknisi').val();
    ctx = document.getElementById('myChart2').getContext('2d');
    chartTim();
}else{
    url = $('#get_url_foreman').val();
    ctx = document.getElementById('myChart4').getContext('2d');
    chartForeman();
} 

var year = '2019';
var inOut = '';
var jenis_chart = 'bar';   
    
//}



    //Charts SA
    function chartSA(){
        $.ajax({
                url:url,
                method:"POST",
                dataType:"json",
                success: function(result)
                {
                    chart = '';
                    if(window.ctx != undefined)
                        window.ctx.destroy();
                    window.ctx = new Chart(ctx, {
                                    // The type of chart we want to create
                                    type: jenis_chart,

                                    // The data for our dataset
                                    data: {
                                        labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Augst", "Sept", "Okt", "Nov", "Des"],
                                        datasets: [{
                                            label: "Asuransi IN",
                                            backgroundColor: 'rgb(233, 30, 99)',
                                            data:result.dataCountAsuransiIn,
                                        },{
                                            label: "Asuransi OUT",
                                            backgroundColor: 'rgb(139, 195, 74)',
                                            data:result.dataCountAsuransiOut,
                                        },{
                                            label: "Tunai IN",

                                            backgroundColor: 'rgb(0, 188, 212)',
                                            data:result.dataCountTunaiIn,
                                        },{
                                            label: "Tunai OUT",
                                            backgroundColor: 'rgb(255, 193, 7)',
                                            data:result.dataCountTunaiOut,
                                        }]
                                    },

                                    // Configuration options go here
                                    options: {

                                    }
                                });



                }
        });
    }

   
//Punya Teknisi
    function chartTim(){
        $.ajax({
                url:url,
                method:"POST",
                dataType:"json",
                success: function(result)
                {
                    chart = '';
                    if(window.ctx != undefined)
                        window.ctx.destroy();
                    window.ctx = new Chart(ctx, {
                                    // The type of chart we want to create
                                    type: jenis_chart,

                                    // The data for our dataset
                                    data: {
                                        labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Augst", "Sept", "Okt", "Nov", "Des"],
                                        datasets: [{
                                            label: "Light",
                                            backgroundColor: 'rgb(233, 30, 99)',
                                            data:result.dataCountLight,
                                        },{
                                            label: "Medium",
                                            backgroundColor: 'rgb(139, 195, 74)',
                                            data:result.dataCountMedium,
                                        },{
                                            label: "Heavy",
                                            backgroundColor: 'rgb(0, 188, 212)',
                                            data:result.dataCountHeavy,
                                        }]
                                    },

                                    // Configuration options go here
                                 options: {
                                        scales: {
                                            yAxes: [{
                                              ticks: {
                                                callback: function(tick) {
                                                  return tick.toString() + '%';
                                                }
                                              }
                                            }]
                                          }
                                    }
                                });



                }
        });
    }

//Chart Foreman
function chartForeman(){
        $.ajax({
                url:url,
                method:"POST",
                dataType:"json",
                success: function(result)
                {
                    chart = '';
                    if(window.ctx != undefined)
                        window.ctx.destroy();
                    window.ctx = new Chart(ctx, {
                                    // The type of chart we want to create
                                    type: jenis_chart,

                                    // The data for our dataset
                                    data: {
                                        labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Augst", "Sept", "Okt", "Nov", "Des"],
                                        datasets: [{
                                            label: "Light",
                                            backgroundColor: 'rgb(233, 30, 99)',
                                            data:result.dataCountLight,
                                        },{
                                            label: "Medium",
                                            backgroundColor: 'rgb(139, 195, 74)',
                                            data:result.dataCountMedium,
                                        },{
                                            label: "Heavy",
                                            backgroundColor: 'rgb(0, 188, 212)',
                                            data:result.dataCountHeavy,
                                        }]
                                    },

                                    // Configuration options go here
                                    options: {
                                        scales: {
                                            yAxes: [{
                                              ticks: {
                                                callback: function(tick) {
                                                  return tick.toString() + '%';
                                                }
                                              }
                                            }]
                                          }
                                    }
                                });



                }
        });
    }    


//Charts Parts
    function chartParts(){
        $.ajax({
                url:url,
                method:"POST",
                dataType:"json",
                success: function(result)
                {
                    console.log(result);
                    chart = '';
                    if(window.ctx != undefined)
                        window.ctx.destroy();
                    window.ctx = new Chart(ctx, {
                                    // The type of chart we want to create
                                    type: jenis_chart,

                                    // The data for our dataset
                                    data: {
                                        labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Augst", "Sept", "Okt", "Nov", "Des"],
                                        datasets: [{
                                            label: "Persentase",
                                            backgroundColor: 'rgb(233, 30, 99)',
                                            data:result.dataPersen,
                                        }]
                                    },

                                    // Configuration options go here
                                    options: {
                                        scales: {
                                            yAxes: [{
                                              ticks: {
                                                callback: function(tick) {
                                                  return tick.toString() + '%';
                                                }
                                              }
                                            }]
                                          }
                                    }
                                });



                }
        });
    }

//Punya Teknisi Jika Dropdown Tahun Diganti
$(document).on('change', '#selectYearTeknisi', function(){
        yearTeknisi = $(this).val();
        url = $('#selectTeknisi').val();
        $.ajax({
            url:url,
            method:"POST",
            dataType:"json",
            data:{yearTeknisi:yearTeknisi},
            success: function(result)
            {
                chart = '';
                if(window.ctx != undefined)
                    window.ctx.destroy();
                window.ctx = new Chart(ctx, {
                                // The type of chart we want to create
                                type: jenis_chart,

                                // The data for our dataset
                                data: {
                                    labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Augst", "Sept", "Okt", "Nov", "Des"],
                                    datasets: [{
                                        label: "Light",
                                        backgroundColor: 'rgb(233, 30, 99)',
                                        data:result.dataCountLight,
                                    },{
                                        label: "Medium",
                                        backgroundColor: 'rgb(139, 195, 74)',
                                        data:result.dataCountMedium,
                                    },{
                                        label: "Heavy",
                                        backgroundColor: 'rgb(0, 188, 212)',
                                        data:result.dataCountHeavy,
                                    }]
                                },

                                // Configuration options go here
                                options: {
                                    cales: {
                                            yAxes: [{
                                              ticks: {
                                                callback: function(tick) {
                                                  return tick.toString() + '%';
                                                }
                                              }
                                            }]
                                          }
                                }
                            });



            }
      });
    });
$(document).on('change', '#selectTeknisi', function(){
        yearTeknisi = $('#selectYearTeknisi').val();
        url = $(this).val();
        $.ajax({
            url:url,
            method:"POST",
            dataType:"json",
            data:{yearTeknisi:yearTeknisi},
            success: function(result)
            {
                chart = '';
                if(window.ctx != undefined)
                    window.ctx.destroy();
                window.ctx = new Chart(ctx, {
                                // The type of chart we want to create
                                type: jenis_chart,

                                // The data for our dataset
                                data: {
                                    labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Augst", "Sept", "Okt", "Nov", "Des"],
                                    datasets: [{
                                        label: "Light",
                                        backgroundColor: 'rgb(233, 30, 99)',
                                        data:result.dataCountLight,
                                    },{
                                        label: "Medium",
                                        backgroundColor: 'rgb(139, 195, 74)',
                                        data:result.dataCountMedium,
                                    },{
                                        label: "Heavy",
                                        backgroundColor: 'rgb(0, 188, 212)',
                                        data:result.dataCountHeavy,
                                    }]
                                },

                                // Configuration options go here
                               options: {
                                        scales: {
                                            yAxes: [{
                                              ticks: {
                                                callback: function(tick) {
                                                  return tick.toString() + '%';
                                                }
                                              }
                                            }]
                                          }
                                    }
                                });



            }
      });
    });


//Punya Parts Jika Dropdown Tahun Diganti
    $(document).on('change', '#selectYear2', function(){
        yearParts = $(this).val();
        $.ajax({
            url:url,
            method:"POST",
            dataType:"json",
            data:{yearParts:yearParts},
            success: function(result)
            {
                chart = '';
                if(window.ctx != undefined)
                    window.ctx.destroy();
                window.ctx = new Chart(ctx, {
                                // The type of chart we want to create
                                type: jenis_chart,

                                // The data for our dataset
                                data: {
                                    labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Augst", "Sept", "Okt", "Nov", "Des"],
                                    datasets: [{
                                        label: "Persentase",
                                        backgroundColor: 'rgb(233, 30, 99)',
                                        data:result.dataPersen,
                                    }]
                                },

                                // Configuration options go here
                                options: {
                                    scales: {
                                            yAxes: [{
                                              ticks: {
                                                callback: function(tick) {
                                                  return tick.toString() + '%';
                                                }
                                              }
                                            }]
                                          }
                                }
                            });



            }
      });
    });

//Punya SA jika Dropdown diganti
 $(document).on('change', '#selectYear', function(){
        year = $(this).val();
        inOut = $('#selectInOut').val();
        $.ajax({
            url:url,
            method:"POST",
            dataType:"json",
            data:{year:year},
            success: function(result)
            {
                chart = '';
                if(window.ctx != undefined)
                    window.ctx.destroy();
                if(inOut == 'in'){
                    window.ctx = new Chart(ctx, {
                                // The type of chart we want to create
                                type: jenis_chart,

                                // The data for our dataset
                                data: {
                                    labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Augst", "Sept", "Okt", "Nov", "Des"],
                                    datasets: [{
                                        label: "Asuransi IN",
                                        backgroundColor: 'rgb(233, 30, 99)',
                                        data:result.dataCountAsuransiIn,
                                    },{
                                        label: "Tunai IN",

                                        backgroundColor: 'rgb(0, 188, 212)',
                                        data:result.dataCountTunaiIn,
                                    }]
                                },

                                // Configuration options go here
                                options: {

                                }
                            });
                }else if(inOut == 'out'){
                    window.ctx = new Chart(ctx, {
                                // The type of chart we want to create
                                type: jenis_chart,

                                // The data for our dataset
                                data: {
                                    labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Augst", "Sept", "Okt", "Nov", "Des"],
                                    datasets: [{
                                        label: "Asuransi OUT",
                                        backgroundColor: 'rgb(139, 195, 74)',
                                        data:result.dataCountAsuransiOut,
                                    },{
                                        label: "Tunai OUT",
                                        backgroundColor: 'rgb(255, 193, 7)',
                                        data:result.dataCountTunaiOut,
                                    }]
                                },

                                // Configuration options go here
                                options: {

                                }
                            });
                }else{
                    window.ctx = new Chart(ctx, {
                                // The type of chart we want to create
                                type: jenis_chart,

                                // The data for our dataset
                                data: {
                                    labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Augst", "Sept", "Okt", "Nov", "Des"],
                                    datasets: [{
                                        label: "Asuransi IN",
                                        backgroundColor: 'rgb(233, 30, 99)',
                                        data:result.dataCountAsuransiIn,
                                    },{
                                        label: "Asuransi OUT",
                                        backgroundColor: 'rgb(139, 195, 74)',
                                        data:result.dataCountAsuransiOut,
                                    },{
                                        label: "Tunai IN",

                                        backgroundColor: 'rgb(0, 188, 212)',
                                        data:result.dataCountTunaiIn,
                                    },{
                                        label: "Tunai OUT",
                                        backgroundColor: 'rgb(255, 193, 7)',
                                        data:result.dataCountTunaiOut,
                                    }]
                                },

                                // Configuration options go here
                                options: {

                                }
                            });
                }
            }
      });
    });

 $(document).on('change', '#selectInOut', function(){
        year = $('#selectYear').val();
        inOut = $(this).val();
        $.ajax({
            url:url,
            method:"POST",
            dataType:"json",
            data:{year:year},
            success: function(result)
            {
                chart = '';
                if(window.ctx != undefined)
                    window.ctx.destroy();
                if(inOut == 'in'){
                    window.ctx = new Chart(ctx, {
                                // The type of chart we want to create
                                type: jenis_chart,

                                // The data for our dataset
                                data: {
                                    labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Augst", "Sept", "Okt", "Nov", "Des"],
                                    datasets: [{
                                        label: "Asuransi IN",
                                        backgroundColor: 'rgb(233, 30, 99)',
                                        data:result.dataCountAsuransiIn,
                                    },{
                                        label: "Tunai IN",

                                        backgroundColor: 'rgb(0, 188, 212)',
                                        data:result.dataCountTunaiIn,
                                    }]
                                },

                                // Configuration options go here
                                options: {

                                }
                            });
                }else if(inOut == 'out'){
                    window.ctx = new Chart(ctx, {
                                // The type of chart we want to create
                                type: jenis_chart,

                                // The data for our dataset
                                data: {
                                    labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Augst", "Sept", "Okt", "Nov", "Des"],
                                    datasets: [{
                                        label: "Asuransi OUT",
                                        backgroundColor: 'rgb(139, 195, 74)',
                                        data:result.dataCountAsuransiOut,
                                    },{
                                        label: "Tunai OUT",
                                        backgroundColor: 'rgb(255, 193, 7)',
                                        data:result.dataCountTunaiOut,
                                    }]
                                },

                                // Configuration options go here
                                options: {

                                }
                            });
                }else{
                    window.ctx = new Chart(ctx, {
                                // The type of chart we want to create
                                type: jenis_chart,

                                // The data for our dataset
                                data: {
                                    labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Augst", "Sept", "Okt", "Nov", "Des"],
                                    datasets: [{
                                        label: "Asuransi IN",
                                        backgroundColor: 'rgb(233, 30, 99)',
                                        data:result.dataCountAsuransiIn,
                                    },{
                                        label: "Asuransi OUT",
                                        backgroundColor: 'rgb(139, 195, 74)',
                                        data:result.dataCountAsuransiOut,
                                    },{
                                        label: "Tunai IN",

                                        backgroundColor: 'rgb(0, 188, 212)',
                                        data:result.dataCountTunaiIn,
                                    },{
                                        label: "Tunai OUT",
                                        backgroundColor: 'rgb(255, 193, 7)',
                                        data:result.dataCountTunaiOut,
                                    }]
                                },

                                // Configuration options go here
                                options: {

                                }
                            });
                }
            }
      });
    });   


//Punya Foreman Jika Dropdown Diganti
$(document).on('change', '#selectYearForeman', function(){
        yearForeman = $(this).val();
        $.ajax({
            url:url,
            method:"POST",
            dataType:"json",
            data:{yearForeman:yearForeman},
            success: function(result)
            {
                chart = '';
                if(window.ctx != undefined)
                    window.ctx.destroy();
                window.ctx = new Chart(ctx, {
                                // The type of chart we want to create
                                type: jenis_chart,

                                // The data for our dataset
                                data: {
                                    labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Augst", "Sept", "Okt", "Nov", "Des"],
                                    datasets: [{
                                        label: "Light",
                                        backgroundColor: 'rgb(233, 30, 99)',
                                        data:result.dataCountLight,
                                    },{
                                        label: "Medium",
                                        backgroundColor: 'rgb(139, 195, 74)',
                                        data:result.dataCountMedium,
                                    },{
                                        label: "Heavy",
                                        backgroundColor: 'rgb(0, 188, 212)',
                                        data:result.dataCountHeavy,
                                    }]
                                },

                                // Configuration options go here
                                options: {
                                    cales: {
                                            yAxes: [{
                                              ticks: {
                                                callback: function(tick) {
                                                  return tick.toString() + '%';
                                                }
                                              }
                                            }]
                                          }
                                }
                            });



            }
      });
    });

});